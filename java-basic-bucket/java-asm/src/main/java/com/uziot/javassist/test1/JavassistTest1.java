package com.uziot.javassist.test1;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;

import java.io.IOException;

/**
 * @author shidt
 * @version V1.0
 * @className JavassistTest1
 * @date 2022-06-06 22:44:26
 * @description
 */

public class JavassistTest1 {
    public static void main(String[] args) throws CannotCompileException, IOException {
        ClassPool cp = ClassPool.getDefault();
        CtClass ctClass = cp.makeClass("com.uziot.javassist.Hello");
        ctClass.writeFile("./");
    }
}

package com.uziot.javassist.test1;

import javassist.*;

import java.io.IOException;
import java.net.URL;

/**
 * @author shidt
 * @version V1.0
 * @className JavassistTest2
 * @date 2022-06-06 22:47:00
 * @description
 */

public class JavassistTest2 {

    public static void main(String[] args) throws CannotCompileException, IOException {

        //找到本文件的路径，与之保存在一起
        URL resource = JavassistTest2.class.getClassLoader().getResource("");
        String file = resource.getFile();
        System.out.println("文件存储路径："+file);

        ClassPool cp = ClassPool.getDefault();
        CtClass ctClass = cp.makeClass("com.uziot.javassist.Hello");

        //创建一个类名为"hello"，传递参数的顺序为(int,double)，没有返回值的类
        /*
        CtMethod（...）源代码：
        public CtMethod(CtClass returnType,//这个方法的返回值类型，
                        String mname, //（method name）方法的名字是什么
                        CtClass[] parameters, //方法传入的参数类型是什么
                        CtClass declaring //添加到哪个类中
                        ) {....}
         */
        CtMethod ctMethod = new CtMethod(CtClass.voidType, "hello",
                new CtClass[]{CtClass.intType, CtClass.doubleType}, ctClass);
        //设置hello方法的权限为public
        ctMethod.setModifiers(Modifier.PUBLIC);

        //向ctClass中添加这个方法
        ctClass.addMethod(ctMethod);
        //写入本地
        ctClass.writeFile(file);
    }
}

package com.uziot.bucket.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shidt
 * @version V1.0
 * @className Column
 * @date 2021-06-04 12:23:09
 * @description
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Column {
    /**
     * 注解值
     *
     * @return String
     */
    String value();
}
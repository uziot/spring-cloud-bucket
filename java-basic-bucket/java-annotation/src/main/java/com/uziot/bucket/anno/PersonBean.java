package com.uziot.bucket.anno;

import lombok.Data;

/**
 * @author shidt
 * @version V1.0
 * @className Column
 * @date 2021-06-04 12:23:09
 * @description 人员实体
 */

@Data
@Table("user")
public class PersonBean {

    /**
     * 用户id
     */
    @Column("id")
    private Integer id;

    /**
     * 用户名称
     */
    @Column("username")
    private String username;

    /**
     * 用户昵称
     */
    @Column("nickname")
    private String nickname;

    /**
     * 用户年龄
     */
    @Column("age")
    private Integer age;
}

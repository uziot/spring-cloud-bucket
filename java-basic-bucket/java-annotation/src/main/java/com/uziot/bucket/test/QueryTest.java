package com.uziot.bucket.test;

import com.uziot.bucket.anno.Column;
import com.uziot.bucket.anno.PersonBean;
import com.uziot.bucket.anno.Table;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author shidt
 * @version V1.0
 * @className QueryTest
 * @date 2021-06-04 12:23:09
 * @description
 */

public class QueryTest {
    public static void main(String[] args) {
        PersonBean bean1 = new PersonBean();
        bean1.setId(10);

        PersonBean bean2 = new PersonBean();
        bean2.setId(10);
        bean2.setNickname("marven");

        PersonBean bean3 = new PersonBean();
        bean3.setAge(18);
        bean3.setId(10);
        bean3.setNickname("marven");

        String sql1 = sql(bean1);
        String sql2 = sql(bean2);
        String sql3 = sql(bean3);

        System.out.println(sql1);
        System.out.println(sql2);
        System.out.println(sql3);
    }

    private static String sql(PersonBean bean) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            // 获取类加载器
            Class<?> c = bean.getClass();
            stringBuilder.append("select * from ");
            // 拼接表名
            //  获取到表的注解
            boolean isTableAnnotationExist = c.isAnnotationPresent(Table.class);
            if (!isTableAnnotationExist) {
                return null;
            }
            // 获取到Table的注解
            Table tableAnnotation = c.getAnnotation(Table.class);
            // 获取到注解的值
            String tableName = tableAnnotation.value();
            // 拼接表名
            stringBuilder.append(tableName);
            // select * from user where 1= 1
            stringBuilder.append(" where 1 = 1");
            // 获取到所有的字段
            Field[] arrayFields = c.getDeclaredFields();
            // 对所有的字段进行遍历
            for (Field field : arrayFields) {
                boolean isFieldAnnotationExist = field.isAnnotationPresent(Column.class);
                if (!isFieldAnnotationExist) {
                    continue;
                }
                Column column = field.getAnnotation(Column.class);
                // 获取到表字段名称
                String columnName = column.value();
                // 获取到字段方法名 eg getId
                String methodName = "get" + columnName.substring(0, 1).toUpperCase() + columnName.substring(1);
                // 获取到这个类中的方法
                Method method = c.getMethod(methodName);
                // 调用这个方法  invoke 获取到属性值
                Object obj = method.invoke(bean);
                if (obj == null || (obj instanceof Integer && (Integer) obj == 0)) {
                    continue;
                }
                stringBuilder.append(" and ").append(columnName);
                // 如果值是字符串  在sql中使用‘’包装
                if (obj instanceof String) {
                    stringBuilder.append("=").append("'").append(obj).append("'");
                } else if (obj instanceof Integer) {
                    stringBuilder.append("=").append(obj);
                }
//
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}

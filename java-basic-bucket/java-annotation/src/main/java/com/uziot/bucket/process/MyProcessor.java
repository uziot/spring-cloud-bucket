package com.uziot.bucket.process;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * @author shidt
 * @version V1.0
 * @className MyProcessor
 * @date 2021-06-15 22:22:10
 * @description </p>
 * 1、init(ProcessingEnvironment env): 每一个注解处理器类都必须有一个空的构造函数。
 * 然而，这里有一个特殊的init()方法，它会被注解处理工具调用，并输入ProcessingEnviroment参数。
 * ProcessingEnviroment提供很多有用的工具类Elements, Types和Filer。后面我们将看到详细的内容。
 * 2、process(Set<? extends TypeElement> annotations, RoundEnvironment env) : 这相当于每个处理器的主函数main()。
 * 你在这里写你的扫描、评估和处理注解的代码，以及生成Java文件。输入参数RoundEnviroment，
 * 可以让你查询出包含特定注解的被注解元素。后面我们将看到详细的内容。
 * 3、getSupportedAnnotationTypes(): 这里你必须指定，这个注解处理器是注册给哪个注解的。
 * 注意，它的返回值是一个字符串的集合，包含本处理器想要处理的注解类型的合法全称。
 * 换句话说，你在这里定义你的注解处理器注册到哪些注解上。
 * 4、getSupportedSourceVersion(): 用来指定你使用的Java版本。通常这里返回SourceVersion.latestSupported()。
 * 然而，如果你有足够的理由只支持Java 6的话，你也可以返回SourceVersion.RELEASE_6。我推荐你使用前者。
 * <p>
 * <p>
 * . 注册处理器
 * 以上已经把完成了处理器的工作，但是处理器要被识别 ，还需要被注册。
 * 注册方式一：手动注册
 * 在使用注解处理器需要先声明，步骤：
 * 1、需要在 processors 库的 main 目录下新建 resources 资源文件夹；
 * 2、在 resources文件夹下建立 META-INF/services 目录文件夹
 * 方式二：自动注册
 * google提供了一个注册处理器的库AutoService。帮助将要编译的处理器进行编译。
 * compile ‘com.google.auto.service:auto-service:1.0-rc2’
 */

@SupportedAnnotationTypes("com.uziot.bucket.anno.Table")
public class MyProcessor extends AbstractProcessor {
    public MyProcessor() {
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        System.out.println("MyProcessor.process()");
        return false;
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        System.out.println("MyProcessor.init()");
        super.init(processingEnvironment);
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        System.out.println("MyProcessor.getSupportedAnnotationTypes()");
        return super.getSupportedAnnotationTypes();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        System.out.println("MyProcessor.getSupportedSourceVersion()");
        return super.getSupportedSourceVersion();
    }

}
 

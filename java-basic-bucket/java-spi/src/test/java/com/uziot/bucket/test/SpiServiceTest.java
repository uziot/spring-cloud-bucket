package com.uziot.bucket.test;

import com.uziot.bucket.java.spi.SpiService;
import org.junit.Test;

import java.util.ServiceLoader;

/**
 * @author shidt
 * @version V1.0
 * @className SpiServiceTest
 * @date 2021-06-04 10:00:42
 * @description
 */

public class SpiServiceTest {
    @Test
    public void testSpi() {
        ServiceLoader<SpiService> serviceLoader = ServiceLoader.load(SpiService.class);
        for (SpiService spiService : serviceLoader) {
            spiService.sayHello();
        }
    }
}

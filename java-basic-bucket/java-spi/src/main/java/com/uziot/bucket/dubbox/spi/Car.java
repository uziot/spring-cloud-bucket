package com.uziot.bucket.dubbox.spi;

import org.apache.dubbo.common.extension.SPI;

/**
 * @author shidt
 * @version V1.0
 * @className Car
 * @date 2021-06-07 10:57:15
 * @description
 */

@SPI
public interface Car {
    void run();
}
package com.uziot.bucket.java.spi.impl;

import com.uziot.bucket.java.spi.SpiService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shidt
 * @version V1.0
 * @className SpiServiceImpl01
 * @date 2021-06-04 09:59:25
 * @description
 */

@Slf4j
public class SpiService02Impl implements SpiService {
    @Override
    public void sayHello() {
        log.info("我是SpiService02.sayHello()");
    }
}

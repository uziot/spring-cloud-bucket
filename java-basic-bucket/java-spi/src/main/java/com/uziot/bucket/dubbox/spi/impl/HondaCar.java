package com.uziot.bucket.dubbox.spi.impl;

import com.uziot.bucket.dubbox.spi.Car;

/**
 * @author shidt
 * @version V1.0
 * @className HondaCar
 * @date 2021-06-07 10:57:15
 * @description
 */

public class HondaCar implements Car {
    @Override
    public void run() {
        System.out.println(this.getClass().getName());
    }
}

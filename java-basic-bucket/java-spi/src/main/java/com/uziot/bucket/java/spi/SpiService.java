package com.uziot.bucket.java.spi;

/**
 * @author shidt
 * @version V1.0
 * @className SpiService
 * @date 2021-06-04 09:58:58
 * @description 要使用SPI比较简单，只需要按照以下几个步骤操作即可：
 * <p>
 * 在jar包的META-INF/services目录下创建一个以"接口全限定名"为命名的文件，内容为实现类的全限定名
 * 接口实现类所在的jar包在classpath下
 * 主程序通过java.util.ServiceLoader动态状态实现模块，
 * 它通过扫描META-INF/services目录下的配置文件找到实现类的全限定名，把类加载到JVM
 * SPI的实现类必须带一个无参构造方法
 * <p>
 * Java中SPI的优缺点：
 * 不能按需加载。Java SPI在加载扩展点的时候，会一次性加载所有可用的扩展点，很多是不需要的，会浪费系统资源
 * 获取某个实现类的方式不够灵活，只能通过 Iterator 形式获取，不能根据某个参数来获取对应的实现类
 * 不支持AOP与IOC
 * 如果扩展点加载失败，会导致调用方报错，导致追踪问题很困难
 */

public interface SpiService {
    void sayHello();
}

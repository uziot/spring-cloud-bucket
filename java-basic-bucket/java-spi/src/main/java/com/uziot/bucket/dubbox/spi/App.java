package com.uziot.bucket.dubbox.spi;

import org.apache.dubbo.common.extension.ExtensionLoader;

import java.util.ServiceLoader;

/**
 * @author shidt
 * @version V1.0
 * @className App
 * @date 2021-06-07 10:57:15
 * @description Dubbo重新实现了一套功能更强的SPI机制,
 * 支持了AOP与依赖注入，并且利用缓存提高加载实现类的性能，同时支持实现类的灵活获取。
 */

public class App {
    public static void main(String[] args) {
        //  Java SPI
        ServiceLoader<Car> serviceLoader = ServiceLoader.load(Car.class);
        serviceLoader.forEach(Car::run);

        //  Dubbo SPI
        ExtensionLoader<Car> extensionLoader = ExtensionLoader.getExtensionLoader(Car.class);
        Car car = extensionLoader.getExtension("honda");
        car.run();
    }
}
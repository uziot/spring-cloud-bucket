package com.uziot.distributed.uaa.dao.mapper;

import com.uziot.distributed.uaa.dao.domain.SysMenu;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className SysMenuMapper
 * @date 2020-11-28 17:52:16
 * @description
 */
public interface SysMenuMapper {
    /**
     * delete by primary key
     *
     * @param menuId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long menuId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysMenu record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysMenu record);

    /**
     * select by primary key
     *
     * @param menuId primary key
     * @return object by primary key
     */
    SysMenu selectByPrimaryKey(Long menuId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysMenu record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysMenu record);

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<String> selectMenuPermsByUserId(Long userId);
}
package com.uziot.distributed.uaa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shidt
 * @version V1.0
 * @className UaaServer
 * @date 2020-11-28 17:52:16
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrix
@MapperScan("com.uziot.distributed.uaa.dao.mapper")
@EnableFeignClients(basePackages = {"com.uziot.distributed.uaa"})
public class UaaServer {
    public static void main(String[] args) {
        SpringApplication.run(UaaServer.class, args);
    }
}

package com.uziot.distributed.uaa.dao.domain;

import lombok.Data;

import java.util.Date;

/**
 * 功能描述: <br>
 * 授权码管理列表
 *
 * @author shidt
 * @date 2020-11-29 16:26
 */
@Data
public class OauthCode {
    private Date createTime;

    private String code;

    private byte[] authentication;
}
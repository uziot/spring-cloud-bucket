package com.uziot.distributed.uaa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * @author shidt
 * @version V1.0
 * @className UaaTokenManageServiceImpl
 * @date 2020-11-28 15:46:46
 * @description
 */
@Service
public class UaaTokenManageServiceImpl {
    @Autowired
    private RedisTokenStore redisTokenStore;


    @Resource
    private DefaultTokenServices tokenService;

    public void removeTokenByClientIdAndUsername() {
        Collection<OAuth2AccessToken> tokens = redisTokenStore.findTokensByClientIdAndUserName("", "");
        tokens.forEach(oAuth2AccessToken -> {
            String accessToken = oAuth2AccessToken.getValue();
            tokenService.revokeToken(accessToken);
        });
    }

    public void removeTokenByAccessToken(String accessToken) {
        tokenService.revokeToken(accessToken);
    }


}

package com.uziot.distributed.uaa.service.dto;

import com.uziot.distributed.uaa.dao.domain.SysUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author shidt
 * @version V1.0
 * @className OauthUser
 * @date 2020-11-28 01:55:39
 * @description
 */
@Data
public class OauthUser implements UserDetails, Serializable {

    private static final long serialVersionUID = 2966351172595405300L;
    private SysUser sysUser;
    private String[] permissions;

    public OauthUser(SysUser sysUser, String[] permissions) {
        this.sysUser = sysUser;
        this.permissions = permissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(permissions);
    }

    @Override
    public String getPassword() {
        return this.sysUser.getPassword();
    }

    @Override
    public String getUsername() {
        return this.sysUser.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}

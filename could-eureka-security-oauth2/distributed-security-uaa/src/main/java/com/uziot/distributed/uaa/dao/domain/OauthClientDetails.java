package com.uziot.distributed.uaa.dao.domain;

import lombok.Data;

import java.util.Date;

/**
 * 功能描述: <br>
 * <>接入客户端信息
 *
 * @author shidt
 * @date 2020-11-29 16:23
 */
@Data
public class OauthClientDetails {
    /**
     * 客户端标
     * 识
     */
    private String clientId;

    /**
     * 接入资源列表
     */
    private String resourceIds;

    /**
     * 客户端秘钥
     */
    private String clientSecret;

    /**
     * 客户端权限
     */
    private String scope;

    /**
     * 授权类型
     */
    private String authorizedGrantTypes;

    /**
     * 预配置转向地址
     */
    private String webServerRedirectUri;

    /**
     * 客户端权限列表
     */
    private String authorities;

    /**
     * accessToken有效时长
     */
    private Integer accessTokenValidity;
    /**
     * refreshToken有效时长
     */
    private Integer refreshTokenValidity;

    private String additionalInformation;

    private Date createTime;

    private Byte archived;

    private Byte trusted;

    private String autoapprove;
}
package com.uziot.distributed.uaa.dao.mapper;

import com.uziot.distributed.uaa.dao.domain.OauthCode;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className OauthCodeMapper
 * @date 2020-11-28 17:52:16
 * @description
 */
public interface OauthCodeMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(OauthCode record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(OauthCode record);

    /**
     * 授权码查询分页使用
     *
     * @return List
     */
    List<OauthCode> selectOauthCode();

    /**
     * 清空数据库中的授权码
     *
     * @return int
     */
    int deleteAllOauthCode();
}
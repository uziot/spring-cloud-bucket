package com.uziot.distributed.uaa.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className UaaJdbcClientDetailsServiceImpl
 * @date 2020-11-28 17:52:16
 * @description
 */
@Service
public class UaaJdbcClientDetailsServiceImpl extends JdbcClientDetailsService {

    public UaaJdbcClientDetailsServiceImpl(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * 分页查询客户端详情
     *
     * @param pageSize pageSize
     * @param pageNum  pageNum
     * @return list
     */
    public PageInfo<ClientDetails> selectClientsByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ClientDetails> clientDetails = super.listClientDetails();
        return new PageInfo<>(clientDetails);
    }

}

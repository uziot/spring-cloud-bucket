package com.uziot.distributed.uaa.dao.mapper;

import com.uziot.distributed.uaa.dao.domain.SysRole;
/**
 * @author shidt
 * @version V1.0
 * @className SysRoleMapper
 * @date 2020-11-28 17:52:16
 * @description
 */
public interface SysRoleMapper {
    /**
     * delete by primary key
     * @param roleId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long roleId);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(SysRole record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysRole record);

    /**
     * select by primary key
     * @param roleId primary key
     * @return object by primary key
     */
    SysRole selectByPrimaryKey(Long roleId);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysRole record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysRole record);
}
package com.uziot.distributed.uaa.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.distributed.uaa.dao.domain.OauthCode;
import com.uziot.distributed.uaa.dao.mapper.OauthCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className UaaJdbcAuthorizationCodeServiceImpl
 * @date 2020-11-29 15:57:35
 * @description 授权码模式授权码管理服务
 * 设置授权码模式的授权码如何存取
 */
@Service
public class UaaJdbcAuthorizationCodeServiceImpl extends JdbcAuthorizationCodeServices {

    @Autowired
    private OauthCodeMapper oauthCodeMapper;

    public UaaJdbcAuthorizationCodeServiceImpl(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * 授权码分页查询
     *
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @return PageInfo
     */
    public PageInfo<OauthCode> selectByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<OauthCode> oauthCodes = oauthCodeMapper.selectOauthCode();
        return new PageInfo<>(oauthCodes);
    }

    /**
     * 删除所有未消费的授权码
     *
     * @return int
     */
    public int deleteAllOauthCode() {
        return oauthCodeMapper.deleteAllOauthCode();
    }

}

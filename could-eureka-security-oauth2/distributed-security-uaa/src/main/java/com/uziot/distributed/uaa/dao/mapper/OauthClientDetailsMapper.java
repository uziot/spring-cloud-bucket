package com.uziot.distributed.uaa.dao.mapper;

import com.uziot.distributed.uaa.dao.domain.OauthClientDetails;

/**
 * @author shidt
 * @version V1.0
 * @className OauthClientDetailsMapper
 * @date 2020-11-28 17:52:16
 * @description
 */
public interface OauthClientDetailsMapper {
    /**
     * delete by primary key
     *
     * @param clientId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String clientId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(OauthClientDetails record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(OauthClientDetails record);

    /**
     * select by primary key
     *
     * @param clientId primary key
     * @return object by primary key
     */
    OauthClientDetails selectByPrimaryKey(String clientId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(OauthClientDetails record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(OauthClientDetails record);
}
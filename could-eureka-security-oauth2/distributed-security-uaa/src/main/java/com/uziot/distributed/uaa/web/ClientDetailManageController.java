package com.uziot.distributed.uaa.web;

import com.github.pagehelper.PageInfo;
import com.uziot.distributed.uaa.service.UaaJdbcClientDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author shidt
 * @version V1.0
 * @className ClientDetailManageController
 * @date 2020-11-28 18:36:09
 * @description
 */
@RequestMapping(value = "/clients")
@RestController
public class ClientDetailManageController {
    @Autowired
    private UaaJdbcClientDetailsServiceImpl uaaJdbcClientDetailsService;

    @GetMapping(value = "/{clientId}")
    public ClientDetails selectClientDetailById(@PathVariable("clientId") String clientId) {
        return uaaJdbcClientDetailsService.loadClientByClientId(clientId);
    }

    @GetMapping(value = "/page/{pageNum}/{pageSize}")
    public PageInfo<ClientDetails> selectClientsByPage(@PathVariable("pageNum") Integer pageNum,
                                                       @PathVariable("pageSize") Integer pageSize) {
        return uaaJdbcClientDetailsService.selectClientsByPage(pageNum, pageSize);
    }

    @PostMapping(value = "/edit")
    public int addClientDetails(@RequestBody ClientDetails clientDetail) {
        String clientId = clientDetail.getClientId();
        if (StringUtils.isEmpty(clientId)) {
            uaaJdbcClientDetailsService.addClientDetails(clientDetail);
        } else {
            uaaJdbcClientDetailsService.updateClientDetails(clientDetail);
        }
        return 1;
    }

    @DeleteMapping(value = "/{clientId}")
    public int deleteClientDetails(@PathVariable("clientId") String clientId) {
        uaaJdbcClientDetailsService.removeClientDetails(clientId);
        return 1;
    }
}

package com.uziot.test;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.SimpleDateFormat;

/**
 * @author shidt
 * @version V1.0
 * @className PasswordTest
 * @date 2020-11-28 02:02:27
 * @description
 */

public class PasswordTest {
    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");
        System.out.println(encode);


        System.out.println(System.currentTimeMillis());
        System.out.println("1606564890");

        boolean matches = bCryptPasswordEncoder.matches("123456", "$2a$10$Misyu6UQaxkf6Is/zFIsdejxdvzk9a.hfKcElerf/Kvg/RplavqRy");
        System.out.println(matches);


    }
}

package com.uziot.distributed.resource.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author shidt
 * @version V1.0
 * @className OrderServer
 * @date 2020-11-28 17:52:16
 * @description
 */

@RestController
public class OrderController {

    @GetMapping(value = "/r1")
    @PreAuthorize("hasAuthority('system:user:list')")//拥有p1权限方可访问此url
    public Object r1() {
        //获取用户身份信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        HashMap<String, Object> map = new HashMap<>(2);
        map.put("resource", "r1");
        map.put("user", authentication);

        return map;
    }

}
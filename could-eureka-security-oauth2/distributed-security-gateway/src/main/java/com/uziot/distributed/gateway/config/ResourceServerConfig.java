package com.uziot.distributed.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

/**
 * @author shidt
 * @version V1.0
 * @className ResourceServerConfig
 * @date 2020-11-28 17:52:16
 * @description
 */
@Configuration
public class ResourceServerConfig {

    public static final String RESOURCE_ID = "res1";

    /**
     * uaa资源服务配置
     */
    @Configuration
    @EnableResourceServer
    public static class UAAServerConfig extends ResourceServerConfigurerAdapter {

        @Autowired
        private RemoteTokenServices remoteTokenServices;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.tokenServices(remoteTokenServices).resourceId(RESOURCE_ID)
                    .stateless(true);

        }
        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/uaa/**").permitAll();
        }
    }


    /**
     * order资源
     */
    @Configuration
    @EnableResourceServer
    public static class OrderServerConfig extends ResourceServerConfigurerAdapter {


        @Autowired
        private RemoteTokenServices remoteTokenServices;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.tokenServices(remoteTokenServices).resourceId(RESOURCE_ID)
                    .stateless(true);
        }
        @Override
        public void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests()
                    .antMatchers("/resource/**").access("#oauth2.hasScope('ROLE_API')");
        }
    }


    //配置其它的资源服务..


}

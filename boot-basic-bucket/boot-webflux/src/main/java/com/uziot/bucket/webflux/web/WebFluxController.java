package com.uziot.bucket.webflux.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Spring MVC 因为是使用的同步阻塞式，更方便开发人员编写功能代码，
 * Debug 测试等，一般来说，如果 Spring MVC 能够满足的场景，就尽量不要用 WebFlux;
 * WebFlux 默认情况下使用 Netty 作为服务器;
 */
@RestController
public class WebFluxController {

    @GetMapping("/hello")
    public Mono<String> hello() {
        return Mono.just("我正在使用WebFlux");
    }
}

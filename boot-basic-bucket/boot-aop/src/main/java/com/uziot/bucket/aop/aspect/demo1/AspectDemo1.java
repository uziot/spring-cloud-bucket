package com.uziot.bucket.aop.aspect.demo1;

import com.uziot.bucket.common.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author shidt
 * @version V1.0
 * @className AspectDemo1
 * @date 2022-03-05 00:11:38
 * @description
 */

@Slf4j
@Aspect
@Component
public class AspectDemo1 {

    @Around("bean(beanAspectDemo)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) {
        long start = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        try {
            return proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            log.error("当前连接点位置发生异常，具体信息为：{}", ThrowableUtil.getStackTrace(e));
            return null;
        } finally {
            long end = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            String costTime = String.valueOf(end - start);
            log.info(">>>>>>>>>>>>>>当前业务处理消耗时间：【{}】MS", costTime);
        }
    }
}

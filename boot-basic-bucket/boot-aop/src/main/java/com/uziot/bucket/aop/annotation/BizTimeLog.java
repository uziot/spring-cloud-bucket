package com.uziot.bucket.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: <br>
 * 日志记录、自定义注解
 *
 * @author shidt
 * @date 2018-03-24 15:25
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BizTimeLog {
    /**
     * 业务的名称
     */
    String value() default "";
}

package com.uziot.bucket.aop.aspect;

import com.uziot.bucket.aop.annotation.BizTimeLog;
import com.uziot.bucket.common.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * 功能描述: <br>
 * 业务响应时间统计
 *
 * @author shidt
 * @date 2020-04-05 12:59
 */
@Slf4j
@Aspect
@Component
@Order(10)
public class BizTimeAspect {
    @Around("@annotation(bizTimeLog)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, BizTimeLog bizTimeLog) {
        long start = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        try {
            return proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            log.error("当前连接点位置发生异常，具体信息为：{}", ThrowableUtil.getStackTrace(e));
            return null;
        } finally {
            long end = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            String costTime = String.valueOf(end - start);
            String biz = bizTimeLog.value();
            log.info(">>>>>>>>>>>>>>当前业务【{}】处理消耗时间：【{}】MS", biz, costTime);
        }
    }
}

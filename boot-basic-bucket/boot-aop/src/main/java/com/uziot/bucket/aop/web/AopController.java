package com.uziot.bucket.aop.web;

import com.uziot.bucket.aop.annotation.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className AopController
 * @date 2020-11-30 11:59:05
 * @description
 */

@RestController
public class AopController {

    @Log
    @GetMapping(value = "/sub/{a}/{b}")
    public Integer subDemo(@PathVariable Integer a, @PathVariable Integer b) {
        return a / b;
    }
}

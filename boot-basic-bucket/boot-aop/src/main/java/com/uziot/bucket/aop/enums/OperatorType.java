package com.uziot.bucket.aop.enums;

/**
 * 功能描述: <br>
 *
 * @author shidt
 * @date 2020/11/30 11:58
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}

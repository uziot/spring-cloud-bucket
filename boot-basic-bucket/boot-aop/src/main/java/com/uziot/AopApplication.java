package com.uziot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author shidt
 * @version V1.0
 * @className AopApplication
 * @date 2020-11-30 11:34:57
 * @description
 */
@EnableAspectJAutoProxy
@SpringBootApplication
public class AopApplication {
    public static void main(String[] args) {
        SpringApplication.run(AopApplication.class);
    }
}

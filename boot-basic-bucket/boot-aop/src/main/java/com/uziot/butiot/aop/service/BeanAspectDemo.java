package com.uziot.butiot.aop.service;

import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className BeanAspectDemo
 * @date 2022-03-05 00:09:35
 * @description
 */

@Service
public class BeanAspectDemo {
    private Object test1() {
        return "test1";
    }

    protected Object test2() {
        return "test2";
    }

    public Object test3() {
        return "test3";
    }
}

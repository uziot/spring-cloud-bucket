package com.uziot.test;

import com.uziot.butiot.aop.service.BeanAspectDemo;

import java.lang.reflect.Method;

/**
 * @author shidt
 * @version V1.0
 * @className DemoTest
 * @date 2022-03-05 00:24:04
 * @description
 */

public class DemoTest {
    public static void main(String[] args) {
        // 查看方法被获取情况，public protected private能否被取到；
        Method[] methods = BeanAspectDemo.class.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName() + "()");
        }

        // 可以看到class.getMethods()方法在获取方法列表的时候
        // 只能获取到public修饰的方法，无法获取到protected private
    }
}

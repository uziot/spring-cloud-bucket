package com.uziot.bucket;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author shidt
 * @version V1.0
 * @className dblock
 * @date 2020-12-05 16:25:41
 * @description
 */
@Slf4j
@EnableScheduling
@MapperScan("com.uziot.bucket.tasklock.dao")
@SpringBootApplication
@EnableAspectJAutoProxy
public class AutoDbLockApplication {
    public static void main(String[] args) {
        SpringApplication.run(AutoDbLockApplication.class, args);
    }
}

package com.uziot.bucket.tasklock.task;

import com.uziot.bucket.tasklock.aspect.TaskDbLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 功能描述: <br>
 * 测试自动加锁数据库任务锁
 *
 * @author shidt
 * @date 2019-12-26 10:31
 */
@Slf4j
@Component
public class Scheduler3Task {

    @TaskDbLock
    @Scheduled(cron = "0/1 * * * * ? ")
    public void report() {
        log.info("同步了SchedulerTask3[{}]：{}", "netText", 10);
    }

}
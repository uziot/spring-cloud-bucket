package com.uziot.bucket.tasklock.dao;

/**
 * @author shidt
 * @version V1.0
 * @className TaskLogMapper
 * @date 2020-12-05 16:25:41
 * @description
 */
public interface TaskLogMapper {
    int saveTaskLog(TaskLog record);

    int updateTaskLog(TaskLog record);
}
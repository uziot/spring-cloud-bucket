package com.uziot.bucket.tasklock.aspect;

import java.lang.annotation.*;

/**
 * 功能描述: <br>
 * 定时任务锁，目前只能锁方法
 *
 * @author shidt
 * @date 2020-03-08 23:59
 */
@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TaskDbLock {
    /**
     * 要锁的参数角标
     */
    int[] lockIndexs() default {};

    /**
     * 要锁的参数的属性名
     */
    String[] fieldNames() default {};

    /**
     * 未取到锁时提示信息
     */
    String msg() default "交易失败，请稍后重试!";

    /**
     * 是否抛出异常
     *
     * @return boolean
     */
    boolean throwError() default false;
}
package com.uziot.bucket.tasklock.service;


import com.uziot.bucket.tasklock.dao.TaskLock;
import com.uziot.bucket.tasklock.dao.TaskLog;
import com.uziot.bucket.tasklock.service.impl.TaskLockServiceImpl;

/**
 * @author shidt
 * @version V1.0
 * @className TaskLockService
 * @date 2020-12-05 16:25:41
 * @description
 */

public interface TaskLockService {

    /**
     * 创建锁
     *
     * @param name 锁名称（唯一索引）
     * @return 锁对象
     */
    TaskLock createTaskLock(String name);

    /**
     * 创建日志对象
     *
     * @param name 锁名称
     * @return 日志对象
     */
    TaskLog createTaskLog(String name);

    /**
     * 尝试获得锁
     *
     * @param taskLock taskLock
     * @return boolean
     */
    boolean tryLock(TaskLock taskLock);

    /**
     * 保存日志
     *
     * @param taskLog taskLog
     */
    void saveTaskLog(TaskLog taskLog);

    /**
     * 更新日志
     *
     * @param taskLog taskLog
     */
    void updateTaskLog(TaskLog taskLog);

    /**
     * 移除锁
     *
     * @param taskLock id
     */
    void removeTaskLock(TaskLock taskLock);

    /**
     * 释放本机锁
     */
    void releaseThisHostLock();

    TaskLockServiceImpl.HostInfo getLocalHostInfo();
}

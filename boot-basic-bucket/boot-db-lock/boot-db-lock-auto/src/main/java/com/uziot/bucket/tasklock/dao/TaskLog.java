package com.uziot.bucket.tasklock.dao;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 * @author shidt
 * @version V1.0
 * @className TaskLog
 * @date 2020-12-05 16:25:41
 * @description
 */
@Data
public class TaskLog implements Serializable {
    private static final long serialVersionUID = 582667760031695800L;
    private String id;

    private String name;

    private String result;

    private String hostName;

    private String hostIp;

    private Date createTime;

    private Date updateTime;

    private Integer isDeleted;

}
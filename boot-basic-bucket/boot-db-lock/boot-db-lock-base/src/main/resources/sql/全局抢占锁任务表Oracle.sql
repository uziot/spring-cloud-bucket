-- 全局定时任务锁任务锁定表（ORACLE数据库请从新生成MAPPER）
create table TASK_LOCK
(
  id          VARCHAR2(40) not null,
  name        VARCHAR2(20) not null,
  host_name   VARCHAR2(32),
  create_time TIMESTAMP(6)
)
tablespace SYSTEM
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table TASK_LOCK
  add unique (NAME)
  using index 
  tablespace SYSTEM
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );



-- 全局定时任务锁跟踪日志表
create table TASK_LOG
(
  id          VARCHAR2(40) not null,
  name        VARCHAR2(20),
  result      VARCHAR2(8),
  host_name   VARCHAR2(32),
  create_time TIMESTAMP(6),
  update_time TIMESTAMP(6),
  is_deleted  NUMBER(1) default 0
)
tablespace SYSTEM
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table TASK_LOG
  add constraint PK_TASK_LOG primary key (ID)
  using index 
  tablespace SYSTEM
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

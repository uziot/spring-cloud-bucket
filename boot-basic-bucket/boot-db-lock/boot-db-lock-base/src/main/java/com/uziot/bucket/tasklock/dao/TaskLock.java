package com.uziot.bucket.tasklock.dao;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author shidt
 * @version V1.0
 * @className TaskLock
 * @date 2020-12-05 16:25:41
 * @description
 */
@Data
public class TaskLock implements Serializable {
    private static final long serialVersionUID = -2732977213276419987L;
    private String id;

    private String name;

    private String hostName;

    private String hostIp;

    private Date createTime;

}
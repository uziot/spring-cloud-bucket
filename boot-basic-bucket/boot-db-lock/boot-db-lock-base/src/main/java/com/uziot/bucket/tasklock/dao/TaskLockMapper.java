package com.uziot.bucket.tasklock.dao;
/**
 * @author shidt
 * @version V1.0
 * @className TaskLockMapper
 * @date 2020-12-05 16:25:41
 * @description
 */

public interface TaskLockMapper {
    int saveTaskLock(TaskLock record);

    int removeTaskLock(String taskLockId);

    int removeTaskLockByHostName(TaskLock taskLock);
}
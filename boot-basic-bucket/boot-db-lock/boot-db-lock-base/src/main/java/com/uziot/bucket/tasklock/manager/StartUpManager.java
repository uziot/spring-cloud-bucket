package com.uziot.bucket.tasklock.manager;

import com.uziot.bucket.tasklock.service.TaskLockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author shidt
 * @version V1.0
 * @className StartManager
 * @date 2020-12-05 19:04:30
 * @description SpringBoot启动事件
 */
@Slf4j
@Component
public class StartUpManager {

    @Autowired
    private TaskLockService taskLockService;

    @PostConstruct
    public void startUp() {
        this.created();
    }

    /**
     * 启动执行方法
     */
    private void created() {
        try {
            log.info("=======启动执行方法=======");
            taskLockService.releaseThisHostLock();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}

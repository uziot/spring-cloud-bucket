package com.uziot.bucket.tasklock.manager;

import com.uziot.bucket.tasklock.service.TaskLockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

/**
 * @author shidt
 * @version V1.0
 * @className ShutUpManager
 * @date 2020-12-05 19:04:30
 * @description SpringBoot退出事件
 */
@Slf4j
@Component
public class ShutUpManager {
    @Autowired
    private TaskLockService taskLockService;

    @PreDestroy
    public void destroy() {
        this.destroyed();
    }

    /**
     * 退出执行方法
     */
    private void destroyed() {
        try {
            log.info("=======退出执行方法=======");
            taskLockService.releaseThisHostLock();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}

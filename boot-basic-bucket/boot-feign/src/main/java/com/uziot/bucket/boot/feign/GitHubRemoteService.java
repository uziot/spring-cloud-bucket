package com.uziot.bucket.boot.feign;


import feign.Param;
import feign.RequestLine;
import lombok.Data;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className GitHubRemoteService
 * @date 2021-03-04 09:46:40
 * @description 构建feign框架的接口服务
 */

public interface GitHubRemoteService {

    @RequestLine("GET /repos/{owner}/{repo}/contributors")
    List<Contributor> contributors(@Param("owner") String owner, @Param("repo") String repo);

    @RequestLine("POST /repos/{owner}/{repo}/issues")
    void createIssue(Issue issue, @Param("owner") String owner, @Param("repo") String repo);


    @Data
    class Contributor {
        String login;
        int contributions;
    }

    @Data
    class Issue {
        String title;
        String body;
        List<String> assignees;
        int milestone;
        List<String> labels;
    }
}



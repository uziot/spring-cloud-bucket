package com.uziot.bucket.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className BootFeignApplication
 * @date 2021-03-04 09:44:27
 * @description
 */

@SpringBootApplication
public class BootFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(BootFeignApplication.class, args);
    }
}

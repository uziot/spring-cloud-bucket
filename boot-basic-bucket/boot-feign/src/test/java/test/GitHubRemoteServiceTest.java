package test;

import com.uziot.bucket.boot.feign.GitHubRemoteService;
import feign.Feign;
import feign.gson.GsonDecoder;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className GitHubRemoteServiceTest
 * @date 2021-03-04 09:46:40
 * @description 测试feign客户端对服务接口构建的使用
 */

public class GitHubRemoteServiceTest {
    public static void main(String[] args) {
        GitHubRemoteService github = Feign.builder().decoder(new GsonDecoder())
                .target(GitHubRemoteService.class, "https://api.github.com");

        // 获取并打印此库的贡献者列表
        List<GitHubRemoteService.Contributor> contributors =
                github.contributors("OpenFeign", "feign");
        for (GitHubRemoteService.Contributor contributor : contributors) {
            System.out.println(contributor.getLogin() + " (" + contributor.getContributions() + ")");
        }
    }
}
package com.uziot.bucket.flowable.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className ActRuTaskMapper
 * @date 2020-11-21 23:24:52
 * @description
 */
@Mapper
public interface ActRuTaskMapper {
    int deleteByPrimaryKey(String id);

    int insert(ActRuTask record);

    int insertSelective(ActRuTask record);

    ActRuTask selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ActRuTask record);

    int updateByPrimaryKey(ActRuTask record);

    List<ActRuTask> selectAll();
}
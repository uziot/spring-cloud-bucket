package com.uziot.bucket.flowable.listen;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

/**
 * @author shidt
 * @version V1.0
 * @className DatabaseConfiguration
 * @date 2020-11-21 23:24:52
 * @description
 */
@Slf4j
public class BossTaskHandler implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("老板监听到消息....");
        delegateTask.setAssignee("老板");
    }
}

package com.uziot.bucket.flowable.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.bucket.flowable.dao.ActRuTask;
import com.uziot.bucket.flowable.dao.ActRuTaskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className BucketAreaController
 * @date 2020-11-21 23:24:52
 * @description
 */
@RestController
@RequestMapping(value = "/task")
public class ActRuTaskController {

    private static final Logger log = LoggerFactory.getLogger(ActRuTaskController.class);

    @Autowired
    public ActRuTaskMapper actRuTaskMapper;

    @GetMapping(value = "/all/{pageNum}/{pageSize}")
    public PageInfo<ActRuTask > selectArea(@PathVariable Integer pageNum, @PathVariable Integer pageSize) throws SQLException {
        PageHelper.startPage(pageNum, pageSize);
        List<ActRuTask > bucketAreas = actRuTaskMapper.selectAll();
        return new PageInfo<>(bucketAreas);
    }
}

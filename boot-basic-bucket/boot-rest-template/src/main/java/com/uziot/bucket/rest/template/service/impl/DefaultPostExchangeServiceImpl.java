package com.uziot.bucket.rest.template.service.impl;

import com.uziot.bucket.rest.template.service.AbstractPostExchangeDataService;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className SimplePostExchangeServiceImpl
 * @date 2020-08-22 13:53:14
 * @description
 */
@Service
public class DefaultPostExchangeServiceImpl extends AbstractPostExchangeDataService {
    @Override
    public boolean support(Object firstReq, Object firstResp, Object secondOriginRequest) {
        return true;
    }

    @Override
    public Object exchange(Object firstReq, Object firstResp, Object secondOriginRequest) {
        return secondOriginRequest;
    }
}

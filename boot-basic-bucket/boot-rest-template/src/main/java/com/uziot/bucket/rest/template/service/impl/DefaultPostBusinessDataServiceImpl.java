package com.uziot.bucket.rest.template.service.impl;

import com.uziot.bucket.rest.template.service.AbstractPostBusinessDataService;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className DefaultBusinessDataServuceImpl
 * @date 2020-08-22 13:59:44
 * @description
 */
@Service
public class DefaultPostBusinessDataServiceImpl extends AbstractPostBusinessDataService {

    @Override
    public Object beforePost(Object reqData) {
        return reqData;
    }

    @Override
    public Object afterPost(Object respData) {
        return respData;
    }
}

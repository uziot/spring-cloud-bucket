package com.uziot.bucket.rest.template.service;

/**
 * @author shidt
 * @version V1.0
 * @className PostBusinessDataService
 * @date 2020-08-21 22:56:36
 * @description
 */

public interface PostBusinessDataService {

    /**
     * 请求发送前
     *
     * @param reqData 业务数据
     * @return Object
     */
    Object beforePost(Object reqData);

    /**
     * 业务数据发送
     *
     * @param url     url
     * @param reqData 请求数据
     * @return obj
     */
    Object postData(String url, Object reqData);

    /**
     * 请求发送前
     *
     * @param respData 响应数据
     * @return Object
     */
    Object afterPost(Object respData);


}

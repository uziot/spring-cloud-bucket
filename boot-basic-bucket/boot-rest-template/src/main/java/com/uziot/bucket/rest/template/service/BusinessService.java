package com.uziot.bucket.rest.template.service;

/**
 * @author shidt
 * @version V1.0
 * @className BusinessService
 * @date 2020-08-21 23:09:50
 * @description
 */

public interface BusinessService {

    /**
     * 方法执行器，入口方法，单一业务服务类
     *
     * @param requestData 请求入参数据，响应数据，都是JsonString
     * @return JsonString
     * @throws Exception Exception
     */
    Object execute(Object requestData) throws Exception;

    /**
     * 请求解包
     *
     * @param requestData 三方请求报文
     * @return 标准格式<head, body>
     * @throws Exception Exception
     * @description 如果三方是非标准报文，需要补充head和渠道号,格式需要统一
     */
    Object requestUnpack(Object requestData) throws Exception;

    /**
     * 数据校验
     *
     * @param requestData 拆包后标准格式报文
     * @throws Exception Exception
     */
    void requestValidate(Object requestData) throws Exception;

    /**
     * 拆包后标准格式报文 转化为 标准输入传输数据
     *
     * @param requestData 拆包后标准格式报文
     * @return 标准输入传输数据
     */
    Object requestMapping(Object requestData);

    /**
     * 业务处理
     *
     * @param bizInDtoData 标准输入传输数据
     * @return 标准输出DTO数据
     * @throws Exception Exception
     */
    Object bizHandler(Object bizInDtoData) throws Exception;

    /**
     * 返回报文映射DTO映射成外部VO
     *
     * @param bizOutDtoData 响应数据
     * @return 外部VO
     */
    Object responseMapping(Object bizOutDtoData);

    /**
     * 响应组包
     *
     * @param outRespData 标准输出DTO数据
     * @return 三方输出报文
     */
    Object responsePack(Object outRespData);

    /**
     * 异常流程
     *
     * @param requestData 标准输出DTO数据
     * @param e           异常信息
     * @return 响应结果
     */
    Object afterThrow(Object requestData, Exception e);


}

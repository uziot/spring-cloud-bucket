package com.uziot.bucket.rest.template.service.impl;

import com.uziot.bucket.rest.template.service.BizTemplate;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className DefaultBizTemplate
 * @date 2020-12-06 00:48:15
 * @description 奇怪的业务处理模板方法
 */
@Service
public class DefaultBizTemplate extends BizTemplate<String> {
    private String monitorKey;

    protected DefaultBizTemplate() {
        super.monitorKey = this.monitorKey;
    }

    @Override
    protected void checkParams() throws Exception {

    }

    @Override
    protected String process() throws Exception {
        return null;
    }

    public String getMonitorKey() {
        return monitorKey;
    }

    public void setMonitorKey(String monitorKey) {
        this.monitorKey = monitorKey;
    }
}

package com.uziot.bucket.rest.template.service.impl;

import com.uziot.bucket.rest.template.service.AbstractBusinessService;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className DefaultBusinessServiceImpl
 * @date 2020-08-22 13:57:39
 * @description
 */

@Service
public class DefaultBusinessServiceImpl extends AbstractBusinessService {
    @Override
    public Object bizHandler(Object bizInDtoData) {
        return null;
    }

    @Override
    public Object afterThrow(Object requestData, Exception e) {
        return null;
    }
}

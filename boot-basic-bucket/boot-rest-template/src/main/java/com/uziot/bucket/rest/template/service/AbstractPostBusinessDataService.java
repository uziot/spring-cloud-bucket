package com.uziot.bucket.rest.template.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

/**
 * @author shidt
 * @version V1.0
 * @className AbstractPostBusinessDataService
 * @date 2020-08-21 23:01:44
 * @description
 */

public abstract class AbstractPostBusinessDataService implements PostBusinessDataService {

    @Autowired
    public RestTemplate restTemplate;

    @Override
    public Object postData(String url, Object reqData) {
        // 执行前置方法
        Object postData = this.beforePost(reqData);
        // 执行请求
        Object result = restTemplate.postForObject(url, postData, Object.class);
        // 执行后置方法
        return this.afterPost(result);
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}

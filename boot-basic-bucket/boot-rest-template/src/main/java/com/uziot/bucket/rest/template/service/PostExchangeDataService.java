package com.uziot.bucket.rest.template.service;

import org.springframework.util.MultiValueMap;

/**
 * @author shidt
 * @version V1.0
 * @className PostExchangeDataService
 * @date 2020-08-19 22:51:28
 * @description
 */

public interface PostExchangeDataService {

    /**
     * 执行FormData请求
     *
     * @param url           请求地址
     * @param multiValueMap FormDataMap
     * @return 响应String
     */
    String postFormData(String url, MultiValueMap<String, Object> multiValueMap);

    /**
     * 两个请求交互
     *
     * @param firstUri  firstUri
     * @param secondUri secondUri
     * @param firstReq  firstRequest
     * @param secondReq secondRequest
     * @return JSONObject
     */
    Object multiPostForObject(String firstUri, String secondUri, Object firstReq, Object secondReq);

    /**
     * 是否开启多请求交换
     *
     * @param firstReq  第一个请求
     * @param firstResp 第一个响应
     * @param secondReq 第二个原始请求
     * @return 是否开启第二个请求交换
     */
    boolean support(Object firstReq, Object firstResp, Object secondReq);

    /**
     * 多请求信息交换
     *
     * @param firstReq  第一个请求
     * @param firstResp 第一个响应
     * @param secondReq 第二个原始请求
     * @return 第二个加工之后的请求
     */
    Object exchange(Object firstReq, Object firstResp, Object secondReq);

}

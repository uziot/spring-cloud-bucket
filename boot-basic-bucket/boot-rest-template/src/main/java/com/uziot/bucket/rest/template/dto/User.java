package com.uziot.bucket.rest.template.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2019-12-20 22:46:46
 * @description
 */

@JsonIgnoreProperties(ignoreUnknown=true)
public class User {

    private String name;
    private String blog;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", blog=" + blog + "]";
    }

}
package com.uziot.bucket.rest.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className RestTemplateApp
 * @date 2020-11-30 22:45:45
 * @description
 */
@SpringBootApplication
public class RestTemplateApp {
    public static void main(String[] args) {
        SpringApplication.run(RestTemplateApp.class, args);
    }
}

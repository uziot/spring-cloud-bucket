package com.uziot.bucket.rest.template.web;

import com.alibaba.fastjson.JSONObject;
import com.uziot.bucket.rest.template.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shidt
 * @version V1.0
 * @className RestTemplateController
 * @date 2020-11-30 22:46:41
 * @description
 */
@RestController
public class RestTemplateController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/restTemplate")
    public User requestBaidu() {
        String url = String.format("https://api.github.com/users/%s", "CloudFoundry");
        return restTemplate.getForObject(url, User.class);
    }

    @PostMapping(value = "/post")
    public String postSelf(@RequestBody JSONObject jsonObject, HttpServletRequest request, HttpServletResponse response) {
        String authorization = request.getHeader("Authorization");
        System.out.println("请求携带的认证信息：" + authorization);
        System.out.println("上送信息：" + jsonObject.toJSONString());
        return "{\n" +
                "  \"login\": \"cloudfoundry\",\n" +
                "  \"id\": 621746,\n" +
                "  \"node_id\": \"MDEyOk9yZ2FuaXphdGlvbjYyMTc0Ng==\",\n" +
                "  \"avatar_url\": \"https://avatars0.githubusercontent.com/u/621746?v=4\",\n" +
                "  \"gravatar_id\": \"\",\n" +
                "  \"url\": \"https://api.github.com/users/cloudfoundry\",\n" +
                "  \"html_url\": \"https://github.com/cloudfoundry\",\n" +
                "  \"followers_url\": \"https://api.github.com/users/cloudfoundry/followers\",\n" +
                "  \"following_url\": \"https://api.github.com/users/cloudfoundry/following{/other_user}\",\n" +
                "  \"gists_url\": \"https://api.github.com/users/cloudfoundry/gists{/gist_id}\",\n" +
                "  \"starred_url\": \"https://api.github.com/users/cloudfoundry/starred{/owner}{/repo}\",\n" +
                "  \"subscriptions_url\": \"https://api.github.com/users/cloudfoundry/subscriptions\",\n" +
                "  \"organizations_url\": \"https://api.github.com/users/cloudfoundry/orgs\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/cloudfoundry/repos\",\n" +
                "  \"events_url\": \"https://api.github.com/users/cloudfoundry/events{/privacy}\",\n" +
                "  \"received_events_url\": \"https://api.github.com/users/cloudfoundry/received_events\",\n" +
                "  \"type\": \"Organization\",\n" +
                "  \"site_admin\": false,\n" +
                "  \"name\": \"Cloud Foundry\",\n" +
                "  \"company\": null,\n" +
                "  \"blog\": \"https://www.cloudfoundry.org/\",\n" +
                "  \"location\": \"Worldwide\",\n" +
                "  \"email\": \"cf-dev@lists.cloudfoundry.org\",\n" +
                "  \"hireable\": null,\n" +
                "  \"bio\": \"Cloud Foundry Foundation active projects\",\n" +
                "  \"twitter_username\": null,\n" +
                "  \"public_repos\": 412,\n" +
                "  \"public_gists\": 0,\n" +
                "  \"followers\": 0,\n" +
                "  \"following\": 0,\n" +
                "  \"created_at\": \"2011-02-16T18:31:25Z\",\n" +
                "  \"updated_at\": \"2019-01-30T14:59:34Z\"\n" +
                "}";
    }


}

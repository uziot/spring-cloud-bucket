
package com.uziot.bucket.rest.template.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 功能描述: <br>
 * 获取堆栈错误信息
 *
 * @author shidt
 * @date 2020-05-11 23:04
 */
public class ThrowableUtil {

    /**
     * 获取堆栈信息
     */
    public static String getStackTrace(Throwable throwable) {
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }

}

package com.uziot.bucket.rest.template.service;

import com.uziot.bucket.rest.template.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shidt
 * @version V1.0
 * @className AbstractBusinessService
 * @date 2020-08-21 23:15:21
 * @description
 */
@Slf4j
public abstract class AbstractBusinessService implements BusinessService {

    @Override
    public Object execute(Object requestData) {
        try {
            // 输入拆包
            Object requestUnpack = this.requestUnpack(requestData);
            // 参数校验
            this.requestValidate(requestUnpack);
            // 外部参数映射内部参数
            Object bizInDtoData = this.requestMapping(requestData);
            // 业务处理
            Object result = this.bizHandler(bizInDtoData);
            // 处理结果映射外部响应
            Object outRespData = this.responseMapping(result);
            // 响应打包加密等
            return this.responsePack(outRespData);
        } catch (Exception e) {
            log.error("系统发生异常，具体信息为：{}", ThrowableUtil.getStackTrace(e));
            return this.afterThrow(requestData, e);
        }
    }

    @Override
    public Object requestUnpack(Object requestData) {
        return requestData;
    }

    @Override
    public void requestValidate(Object requestData) {
    }

    @Override
    public Object requestMapping(Object requestData) {
        return requestData;
    }

    @Override
    public Object responseMapping(Object bizOutDtoData) {
        return bizOutDtoData;
    }

    @Override
    public Object responsePack(Object outRespData) {
        return outRespData;
    }

}

package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className JustAuthApplication
 * @date 2021-09-01 21:37:30
 * @description
 */

@SpringBootApplication
public class JustAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(JustAuthApplication.class, args);
    }
}

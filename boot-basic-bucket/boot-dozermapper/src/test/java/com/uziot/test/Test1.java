package com.uziot.test;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.Application;
import com.uziot.bucket.mapper.config.DozerConverter;
import com.uziot.bucket.mapper.vo.DateDTO;
import com.uziot.bucket.mapper.vo.DateVO;
import com.uziot.bucket.mapper.vo.StudentDO;
import com.uziot.bucket.mapper.vo.StudentDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class Test1 {

    @Autowired
    private DozerConverter dozerConverter;

    @Test
    public void test() {
        DateDTO dto = new DateDTO();
        dto.setName("李四");
        dto.setAge(18);
        dto.setBirthday(new Date());
        dto.setCreateDate(new Date());
        dto.setUpdateDate(new Date());

        DateVO vo = dozerConverter.map(dto, DateVO.class);
        System.out.println(JSON.toJSONString(vo));
    }

    @Test
    public void test2() {
        StudentDTO dto = new StudentDTO();
        dto.setNo("11111");
        dto.setId(30L);
        dto.setCreateDate(new Date());
        dto.setUpdateTime(new Date());

        StudentDO vo = dozerConverter.map(dto, StudentDO.class);
        System.out.println(JSON.toJSONString(vo));
    }


}

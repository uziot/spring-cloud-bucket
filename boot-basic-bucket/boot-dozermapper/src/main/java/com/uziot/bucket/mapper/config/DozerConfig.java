package com.uziot.bucket.mapper.config;

import com.github.dozermapper.spring.DozerBeanMapperFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

/**
 * 功能描述: <br>
 * 映射配置
 *
 * @author shidt
 * @date 2021-12-10 22:18
 */
@Configuration
public class DozerConfig {

    @Bean
    @Scope(SCOPE_SINGLETON)
    public DozerBeanMapperFactoryBean dozerMapper(ResourcePatternResolver resourcePatternResolver) throws IOException {
        DozerBeanMapperFactoryBean factoryBean = new DozerBeanMapperFactoryBean();
        // 若application.yml中配置了, 则这里设不设置都可以, 若都设置了, 最好保持一致
        factoryBean.setMappingFiles(resourcePatternResolver.getResources("classpath*:/*mapping.xml"));
        return factoryBean;
    }
}
package com.uziot.bucket.mapper.vo;

import lombok.*;

/**
 * @author Shanks
 * @date 2020-10-31
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StudentDO {

    private String number;

    private Long studentId;

    private String createDate;

    private String updateTime;
}
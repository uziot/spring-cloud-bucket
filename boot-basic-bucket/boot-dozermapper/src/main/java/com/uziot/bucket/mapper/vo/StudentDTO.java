package com.uziot.bucket.mapper.vo;

import com.github.dozermapper.core.Mapping;
import lombok.*;

import java.util.Date;

/**
 * @author Shanks
 * @date 2020-10-31
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {

    private String no;

    @Mapping(value = "studentId")
    private Long id;

    private Date createDate;

    private Date updateTime;
}
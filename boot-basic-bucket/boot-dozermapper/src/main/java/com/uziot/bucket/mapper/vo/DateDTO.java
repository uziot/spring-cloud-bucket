package com.uziot.bucket.mapper.vo;

import com.github.dozermapper.core.Mapping;
import lombok.*;

import java.util.Date;

/**
 * @author Shanks
 * @date 2020-11-01
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DateDTO {

    private String name;
    private Integer age;

    private Date birthday;

    /** 名称不一致, 使用注解映射 */
    @Mapping(value = "createTime")
    private Date createDate;

    /** 名称不一致, 使用XML映射 */
    private Date updateDate;
}
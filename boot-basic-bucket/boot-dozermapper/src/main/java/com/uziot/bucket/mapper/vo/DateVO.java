package com.uziot.bucket.mapper.vo;

import lombok.*;

import java.util.Date;

/**
 * @author Shanks
 * @date 2020-11-01
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DateVO {

    private String name;
    private Integer age;

    private String birthday;

    private Date createTime;

    private Date updateTime;
}

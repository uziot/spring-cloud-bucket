package com.uziot.bucket.websoket.model;

/**
 * ResponseMessage
 *
 * @author shidt
 * @version 1.0
 * @since 2018/2/28
 */
public class ResponseMessage {
    private final String responseMessage;

    public ResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}

package com.uziot.bucket.websoket.model;

/**
 * RequestMessage
 *
 * @author shidt
 * @version 1.0
 * @since 2018/2/28
 */
public class RequestMessage {
    private String name;

    public String getName() {
        return name;
    }
}

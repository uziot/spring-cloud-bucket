package com.uziot.test;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.alibaba.fastjson.JSON;
import com.uziot.bucket.SecurityApplication;
import com.uziot.bucket.security.dao.domain.SysMenu;
import com.uziot.bucket.security.dao.mapper.SysMenuMapper;
import com.uziot.bucket.security.util.ToStringUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityApplication.class})
public class Test1 {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Test
    public void buildMenuTree() {
        List<SysMenu> menus = sysMenuMapper.selectAll();

        // 2.配置
        TreeNodeConfig config = new TreeNodeConfig();
        config.setIdKey("menuId");//默认为id可以不设置
        config.setParentIdKey("parentId");//默认为parentId可以不设置
        //config.setDeep(4);//最大递归深度
        config.setWeightKey("orderNum");//排序字段
        config.setChildrenKey("children");

        ArrayList<Long> ids = new ArrayList<>();

        // 3.转树，Tree<>里面泛型为id的类型,
        // 需要强烈注意的是下列传参的parentId必须和起点根的相同否则找不到根树返回空数组,
        // 也可从根节点获取到所有下行数据列表进行遍历和重新组装
        List<Tree<Long>> build = TreeUtil.build(menus, 0L, config, (menu, tree) -> {
            // 也可以使用 tree.setId(object.getId());等一些默认值
            tree.setParentId(menu.getParentId());
            tree.putExtra("menuId", menu.getMenuId());
            tree.putExtra("icon", menu.getIcon());
            tree.putExtra("name", menu.getMenuName());
            tree.putExtra("url", menu.getPath());
            tree.putExtra("priority", menu.getPerms());
            tree.putExtra("type", menu.getMenuType());
            tree.putExtra("order", menu.getOrderNum());
        });

        // 输出构建结果
        log.info(ToStringUtils.prettyJson(build));
        log.info(ToStringUtils.prettyJson(ids));


    }


}

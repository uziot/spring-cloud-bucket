package com.uziot.bucket.security.config.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.uziot.bucket.security.util.ServletUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shidt
 * @version V1.0
 * @className LogoutSuccessHandlerImpl
 * @date 2020-10-31 16:45:04
 * @description 自定义退出处理类 返回成功
 */

@Component
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationEntryPointImpl.class);
    /**
     * 退出处理
     * 查询退出用户信息，如果正常退出
     * 清空用户token信息
     * 并记录日志
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        log.info("用户在地址，退出已成功退出系统");

        JSONObject result = new JSONObject();
        result.put("code", 200);
        result.put("msg", "退出成功");
        String jsonString = JSON.toJSONString(result);
        ServletUtils.writeResponse(response, jsonString);
    }
}

package com.uziot.bucket.security.web;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.security.dao.domain.SysMenu;
import com.uziot.bucket.security.dao.mapper.SysMenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className SysMenuController
 * @date 2021-03-03 08:52:53
 * @description
 */

@Slf4j
@RestController
public class SysMenuController {
    @Autowired
    private SysMenuMapper sysMenuMapper;

    @GetMapping(value = "/tree")
    public List<SysMenu> buildMenuTree() {
        List<SysMenu> menus = sysMenuMapper.selectAll();
        log.info("查询到所有菜单列表：[{}]", JSON.toJSONString(menus));
        return menus;
    }
}

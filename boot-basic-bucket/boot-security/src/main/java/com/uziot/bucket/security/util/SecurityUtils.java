package com.uziot.bucket.security.util;

import com.uziot.bucket.security.config.LoginUser;
import com.uziot.bucket.security.dao.domain.SysRole;
import com.uziot.bucket.security.dao.domain.SysUser;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author shidt
 * @version V1.0
 * @className SecurityUtils
 * @date 2020-10-31 16:45:04
 * @description 安全服务工具类
 */

public class SecurityUtils extends SecurityContextHolder {
    /**
     * 获取用户账户
     **/
    public static String getUsername() {
        try {
            return getLoginUser().getUsername();
        } catch (Exception e) {
            throw new RuntimeException("获取用户账户异常");
        }
    }

    /**
     * 获取用户
     **/
    public static LoginUser getLoginUser() {
        try {
            return (LoginUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new RuntimeException("获取用户信息异常");
        }
    }

    /**
     * 获取当前登陆的系统用户
     **/
    public static SysUser getCurrentSysUser() {
        try {
            return getLoginUser().getUser();
        } catch (Exception e) {
            throw new RuntimeException("获取用户信息异常");
        }
    }

    /**
     * 获取用户请求认证中的IP地址
     *
     * @return ip
     */
    public static String getLoginUserIp() {
        WebAuthenticationDetails details = (WebAuthenticationDetails) getAuthentication().getDetails();
        return details.getRemoteAddress();
    }

    /**
     * 获取用户权限列表
     *
     * @return 列表
     */
    public static Set<String> getPermissions() {
        return getLoginUser().getPermissions();
    }


    /**
     * 获取用户角色列表
     *
     * @return 列表
     */
    public static Set<String> getRoles() {
        List<SysRole> sysRoles = getLoginUser().getUser().getRoles();
        return sysRoles.stream().map(SysRole::getRoleKey).collect(Collectors.toSet());
    }


    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword     真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 用户信息脱敏工具
     */
    public static void userInfoDesensitization(LoginUser loginUser) {
        SysUser sysUser = loginUser.getUser();
        userInfoDesensitization(sysUser);
    }

    /**
     * 用户信息脱敏
     */
    public static void userInfoDesensitization(SysUser sysUser) {
        sysUser.setPassword(null);
        sysUser.setSalt(null);
    }
}

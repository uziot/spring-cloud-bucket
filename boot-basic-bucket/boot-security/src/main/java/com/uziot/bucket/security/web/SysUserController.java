package com.uziot.bucket.security.web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.bucket.security.dao.domain.SysUser;
import com.uziot.bucket.security.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className SysUserController
 * @date 2020-12-04 11:25:02
 * @description 用户信息访问接口
 * <p>
 * 当前TOKEN校验的方法比较简单，不生成jwt，直接在请求header中加入username即可
 */


@RestController
@RequestMapping("/system/user")
public class SysUserController {
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 获取用户列表
     */
    @PreAuthorize("hasAuthority('system:user:page')")
    @GetMapping("/list/{pageSize}/{pageNum}")
    public PageInfo<SysUser> list(@PathVariable Integer pageSize, @PathVariable Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        List<SysUser> sysUsers = sysUserMapper.selectAll();
        return new PageInfo<>(sysUsers);
    }


    /**
     * 根据用户编号获取详细信息
     */
    @PreAuthorize("hasAuthority('system:user:query')")
    @GetMapping(value = {"/", "/{userId}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getInfo(@PathVariable(value = "userId", required = false) Long userId) {
        HashMap<String, Object> result = new HashMap<>();
        if (null != userId) {
            SysUser sysUser = sysUserMapper.selectByPrimaryKey(userId);
            result.put("data", sysUser);
            result.put("code", 0);
            result.put("msg", "查询成功！");
        }
        return result;
    }

}
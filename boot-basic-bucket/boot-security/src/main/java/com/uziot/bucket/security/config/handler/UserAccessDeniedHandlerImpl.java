package com.uziot.bucket.security.config.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.uziot.bucket.security.util.ServletUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shidt
 * @version V1.0
 * @className UserAccessDeniedHandlerImpl
 * @date 2020-12-04 11:25:02
 * @description 自定义权限校验失败返回结果包装
 */

@Component
public class UserAccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {

        JSONObject result = new JSONObject();
        result.put("code", 403);
        result.put("msg", "当前接口您没有权限访问！");
        result.put("data", request.getRequestURI());
        String jsonString = JSON.toJSONString(result);
        ServletUtils.writeResponse(response, jsonString);
    }
}

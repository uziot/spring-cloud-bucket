package com.uziot.bucket.security.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

/**
 * @author shidt
 * @version V1.0
 * @className ToStringUtils
 * @date 2020-05-12 21:08:05
 * @description
 */

public class ToStringUtils {

    /**
     * 美化JSON输出，一般用于打印日志
     *
     * @param obj obj
     * @return str
     */
    public static String prettyJson(Object obj) {
        SerializerFeature prettyFormat = SerializerFeature.PrettyFormat;
        SerializerFeature writeMapNullValue = SerializerFeature.WriteMapNullValue;
        SerializerFeature writeDateUseDateFormat = SerializerFeature.WriteDateUseDateFormat;
        return JSON.toJSONString(obj, prettyFormat, writeMapNullValue, writeDateUseDateFormat);
    }
}
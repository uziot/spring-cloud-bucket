package com.uziot.bucket.i18n.validate;

/**
 * @author shidt
 * @version V1.0
 * @className ValidationGroups
 * @date 2021-01-28 21:39:30
 * @description
 */
public class ValidationGroups {
    public interface Insert {
    }

    public interface Update {
    }

}

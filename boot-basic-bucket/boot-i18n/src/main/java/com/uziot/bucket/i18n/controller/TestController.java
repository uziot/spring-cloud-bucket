package com.uziot.bucket.i18n.controller;

import com.uziot.bucket.i18n.annotation.I18n;
import com.uziot.bucket.i18n.enums.RefTypeEnum;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangbowen
 * @email 2791951533@qq.com
 * @since 2020-06-16 15:51
 */
@RestController
public class TestController {

    @GetMapping("test")
    public ShopDemo
        test(@RequestHeader(value = "Accept-Language", defaultValue = "zh-CN", required = false) String language) {
        ShopDemo shopDemo = new ShopDemo();
        return shopDemo;
    }

    @Data
    class ShopDemo {
        private Integer id = 1;

        @I18n(refIdAlias = "id", refType = {RefTypeEnum.PRODUCT_NAME})
        private String name = "中文商品名称";
    }
}

package com.uziot.bucket.i18n.enums;

import lombok.Getter;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Getter
public enum RefTypeEnum {

    /**
     * 商品名称
     */
    PRODUCT_NAME("1", "商品名称");

    private final String code;
    private final String message;

    RefTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}

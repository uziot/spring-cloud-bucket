package com.uziot.bucket.i18n.schedule;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.uziot.bucket.i18n.config.CacheConfig;
import com.uziot.bucket.i18n.service.I18nService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className I18nSchedule
 * @date 2021-01-28 21:39:30
 * @description
 */
@Component
@Slf4j
public class I18nSchedule {

    @Autowired
    private I18nService i18nService;
    @Autowired
    @Qualifier("i18nMapCache")
    private Cache<String, Map<String, String>> i18nMapCache;

    /**
     * 刷新translate存在ehcache的缓存
     */
    @Scheduled(cron = "${schedule.cron.i18n}")
    public void refreshI18n() {
        try {
            log.info("【刷新翻译caffeine的缓存】定时任务刷新翻译caffeine缓存，每1分钟执行一次");

            CacheStats cacheStats = i18nMapCache.stats();
            log.info("【刷新翻译caffeine的缓存】统计明细,cacheStats:{}", cacheStats.toString());

            i18nMapCache.put(CacheConfig.I18N_KEY, i18nService.getRefreshI18nMap());
            log.info("刷新翻译caffeine的缓存】成功");
        } catch (Exception e) {
            log.error("【刷新翻译caffeine的缓存】失败，key:{}，message={}", CacheConfig.I18N_KEY, e.toString(), e);
        }
    }

}

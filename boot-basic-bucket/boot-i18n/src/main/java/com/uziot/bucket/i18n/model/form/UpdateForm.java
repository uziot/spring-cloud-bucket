package com.uziot.bucket.i18n.model.form;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Data
public class UpdateForm {
    /**
     * 翻译后的语言建议同时翻译多种语言(原始语言，默认语言类型是中文)
     */
    @NotEmpty
    @Valid
    private List<I18nForm> i18nList;
}

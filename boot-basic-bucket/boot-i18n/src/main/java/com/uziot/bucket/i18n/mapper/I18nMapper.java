package com.uziot.bucket.i18n.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uziot.bucket.i18n.model.entity.I18n;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Mapper
public interface I18nMapper extends BaseMapper<I18n> {

}

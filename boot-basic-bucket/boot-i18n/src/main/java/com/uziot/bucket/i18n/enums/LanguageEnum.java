package com.uziot.bucket.i18n.enums;

import lombok.Getter;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Getter
public enum LanguageEnum {

    /**
     * 中文简体
     */
    ZH_CN("zh-CN", "中文简体"),
    /**
     * 中国香港繁体
     */
    ZH_HK("zh-HK", "香港繁体"),
    /**
     * 中国台湾繁体
     */
    ZH_TW("zh-TW", "台湾繁体"),
    /**
     * 英文
     */
    EN_US("en-US", "英文"),
    /**
     * 日语
     */
    JA_JP("ja-JP", "日语");

    /**
     * 国际化语言code
     */
    private final String code;
    /**
     * 国际语言中文名称
     */
    private final String message;

    LanguageEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}

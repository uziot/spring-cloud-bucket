package com.uziot.bucket.i18n.model.vo;

import com.uziot.bucket.i18n.model.entity.I18n;
import lombok.Data;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Data
public class I18nVo {

    private Integer id;
    /**
     * 关联ID
     */
    private Integer refId;
    /**
     * 编号，不能类型必须唯一
     */
    private String refType;
    /**
     * 语言类型，国际通用列表:zh-CN(中文)，zh-HK(香港繁体)，zh-TW(台湾繁体)，en-US(英文)，ja-JP(日语)
     */
    private String languageType;
    /**
     * 翻译内容
     */
    private String translateText;

    public I18nVo(I18n i18n) {
        this.id = i18n.getId();
        this.refId = i18n.getRefId();
        this.refType = i18n.getRefType();
        this.languageType = i18n.getLanguageType();
        this.translateText = i18n.getTranslateText();
    }
}

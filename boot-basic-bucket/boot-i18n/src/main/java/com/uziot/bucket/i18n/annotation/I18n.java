package com.uziot.bucket.i18n.annotation;

import com.uziot.bucket.i18n.enums.RefTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface I18n {
    /**
     * 指定要翻译字段的主键id名称，eg：refId=id
     *
     * @return
     */
    String refIdAlias();

    /**
     * 按顺序翻译，如果第一个值为空，就用第二个，以此类推
     *
     * @return
     */
    RefTypeEnum[] refType();
}

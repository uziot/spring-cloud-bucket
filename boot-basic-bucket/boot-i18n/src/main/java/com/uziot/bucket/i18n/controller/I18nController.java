package com.uziot.bucket.i18n.controller;


import com.uziot.bucket.i18n.model.form.CreateForm;
import com.uziot.bucket.i18n.model.form.UpdateForm;
import com.uziot.bucket.i18n.model.vo.I18nVo;
import com.uziot.bucket.i18n.service.I18nService;
import com.uziot.bucket.i18n.validate.ValidationGroups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@RestController
@RequestMapping("/i18n")
public class I18nController {

    @Autowired
    private I18nService i18nService;

    @PostMapping("/create")
    public void create(@RequestBody @Validated(ValidationGroups.Insert.class) CreateForm form) {
        i18nService.create(form);
    }

    @PutMapping("/update")
    public void update(@RequestBody @Validated(ValidationGroups.Update.class) UpdateForm form) {
        i18nService.update(form);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam("refId") Integer refId, @RequestParam("refType") String refType) {
        i18nService.delete(refId, refType);
    }

    @GetMapping("/list")
    public List<I18nVo> list() {
        return i18nService.listVo();
    }

    @GetMapping("/info")
    public List<I18nVo> info(@RequestParam("refId") Integer refId, @RequestParam("refType") String refType) {
        return i18nService.info(refId, refType);
    }
}

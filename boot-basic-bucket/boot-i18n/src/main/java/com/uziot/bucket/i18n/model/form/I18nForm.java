package com.uziot.bucket.i18n.model.form;

import com.uziot.bucket.i18n.validate.ValidationGroups;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Data
public class I18nForm {

    /**
     * 主键
     */
    @NotNull(groups = ValidationGroups.Update.class)
    @Null(groups = ValidationGroups.Insert.class)
    private Integer id;

    /**
     * 关联ID
     */
    @NotNull
    private Integer refId;
    /**
     * RefTypeEnum.class
     */
    @NotBlank
    private String refType;
    /**
     * LanguageEnum.class
     */
    @NotBlank
    private String languageType;
    /**
     * 翻译后的内容
     */
    @NotBlank
    private String translateText;
}

package com.uziot.bucket.i18n.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className I18nVo
 * @date 2021-01-28 21:39:30
 * @description
 */
@Configuration
public class CacheConfig {

    public static final String I18N_KEY = "i18nMap";

    @Bean("i18nMapCache")
    public Cache<String, Map<String, String>> i18nMapCache() {
        return Caffeine.newBuilder().recordStats()
                .expireAfterWrite(3, TimeUnit.MINUTES)
                .maximumSize(10000).build();
    }
}

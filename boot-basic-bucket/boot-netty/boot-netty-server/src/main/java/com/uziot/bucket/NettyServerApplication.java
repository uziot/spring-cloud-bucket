package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className NettyServerApplication
 * @date 2021-05-20 22:21:34
 * @description
 */

@SpringBootApplication
public class NettyServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(NettyServerApplication.class, args);
    }
}

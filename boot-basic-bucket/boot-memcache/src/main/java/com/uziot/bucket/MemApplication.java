package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MemApplication
 * @date 2020-12-05 23:46:18
 * @description
 */

@SpringBootApplication
public class MemApplication {
    public static void main(String[] args) {
        SpringApplication.run(MemApplication.class, args);
    }
}

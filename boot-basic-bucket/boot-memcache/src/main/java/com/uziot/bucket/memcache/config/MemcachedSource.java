package com.uziot.bucket.memcache.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className MemcacheSource
 * @date 2020-12-05 22:37:29
 * @description
 */
@Component
@ConfigurationProperties(prefix = "memcache")
public class MemcachedSource {

    private String ip;

    private int port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

package com.uziot.bucket.memcache.config;

import net.spy.memcached.MemcachedClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author shidt
 * @version V1.0
 * @className MemcachedRunner
 * @date 2020-12-05 22:37:29
 * @description
 */
@Component
public class MemcachedRunner implements CommandLineRunner {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemcachedSource memcachedSource;

    private MemcachedClient client = null;

    @Override
    public void run(String... args) {
        try {
            client = new MemcachedClient(new InetSocketAddress(memcachedSource.getIp(), memcachedSource.getPort()));
        } catch (IOException e) {
            logger.error("inint MemcachedClient failed ", e);
        }
    }

    public MemcachedClient getClient() {
        return client;
    }

}
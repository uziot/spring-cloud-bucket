import com.uziot.bucket.MemApplication;
import com.uziot.bucket.memcache.config.MemcachedRunner;
import lombok.extern.slf4j.Slf4j;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className MongoTest
 * @date 2020-12-05 22:42:12
 * @description
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        MemApplication.class
})
@SpringBootConfiguration
public class MemTest {

    @Autowired
    public MemcachedRunner memcachedRunner;

    @Test
    public void testConn() {
        MemcachedClient client = memcachedRunner.getClient();
        OperationFuture<Boolean> test = client.set("123", 1000, "test");
        log.info("设定结果：" + test);
        Object o = client.get("123");
        log.info("取出结果：" + o);


    }
}

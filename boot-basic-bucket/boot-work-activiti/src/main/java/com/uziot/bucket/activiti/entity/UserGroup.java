package com.uziot.bucket.activiti.entity;

/**
 * @author shidt
 * @version V1.0
 * @className UserGroup
 * @date 2021-02-26 10:38:41
 * @description 用户组
 */

public class UserGroup {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

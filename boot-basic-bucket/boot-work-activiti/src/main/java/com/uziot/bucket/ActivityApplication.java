package com.uziot.bucket;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className ActivitiApplication
 * @date 2021-02-26 10:38:41
 * @description
 */

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class ActivityApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActivityApplication.class, args);
    }
}

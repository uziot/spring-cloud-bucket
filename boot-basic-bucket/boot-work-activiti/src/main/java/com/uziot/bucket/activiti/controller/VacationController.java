package com.uziot.bucket.activiti.controller;

import com.uziot.bucket.activiti.entity.VacTask;
import com.uziot.bucket.activiti.entity.Vacation;
import com.uziot.bucket.activiti.generatorImg.ActGenImageUtils;
import com.uziot.bucket.activiti.service.VacationService;
import com.uziot.bucket.activiti.util.ToStringUtils;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author shidt
 * @version V1.0
 * @className VacationController
 * @date 2021-02-26 10:38:41
 * @description 请假服务接口
 */

@Slf4j
@RestController
public class VacationController {

    @Resource
    private VacationService vacationService;

    /**
     * 我要请假
     *
     * @param vac     vac
     * @param session session
     * @return startVac
     */
    @PostMapping("/startVac")
    public Object startVac(@RequestBody Vacation vac, HttpSession session) {
        String userName = (String) session.getAttribute("userName");

        log.info("收到请假流程申请，报文为：{}", ToStringUtils.prettyJson(vac));
        Object startVac = vacationService.startVac(userName, vac);
        log.info("请假申请成功，返回结果为：{}", ToStringUtils.prettyJson(startVac));
        return startVac;
    }

    /**
     * 我正在申请的假
     *
     * @param session session
     * @return myVac
     */
    @GetMapping("/myVac")
    public Object myVac(HttpSession session) {
        String userName = (String) session.getAttribute("userName");
        Object myVac = vacationService.myVac(userName);
        log.info("我正在申请的假，返回结果为：{}", ToStringUtils.prettyJson(myVac));
        return myVac;
    }

    /**
     * 待我审核的请假
     *
     * @param session session
     * @return Object
     */
    @GetMapping("/myAudit")
    public Object myAudit(HttpSession session) {
        String userName = (String) session.getAttribute("userName");
        Object myAudit = vacationService.myAudit(userName);
        log.info("待我审核的请假，返回结果为：{}", ToStringUtils.prettyJson(myAudit));
        return myAudit;
    }

    /**
     * 审核
     *
     * @param session session
     * @param vacTask vacTask
     * @return PostMapping
     */
    @PostMapping("/passAudit")
    public Object passAudit(HttpSession session, @RequestBody VacTask vacTask) {
        String userName = (String) session.getAttribute("userName");
        log.info("审核申请，请求报文为：{}", ToStringUtils.prettyJson(vacTask));
        Object passAudit = vacationService.passAudit(userName, vacTask);
        log.info("审核成功，返回结果为：{}", ToStringUtils.prettyJson(passAudit));
        return passAudit;

    }

    /**
     * 我申请过的假
     *
     * @param session session
     * @return GetMapping
     */
    @GetMapping("/myVacRecord")
    public Object myVacRecord(HttpSession session) {
        String userName = (String) session.getAttribute("userName");
        Object myVacRecord = vacationService.myVacRecord(userName);
        log.info("我申请过的假，返回结果为：{}", ToStringUtils.prettyJson(myVacRecord));
        return myVacRecord;
    }

    /**
     * 我的审核记录
     *
     * @param session session
     * @return GetMapping
     */
    @GetMapping("/myAuditRecord")
    public Object myAuditRecord(HttpSession session) {
        String userName = (String) session.getAttribute("userName");
        Object myAuditRecord = vacationService.myAuditRecord(userName);
        log.info("我的审核记录，返回结果为：{}", ToStringUtils.prettyJson(myAuditRecord));
        return myAuditRecord;
    }

    /**
     * 生成流程图
     *
     * @param response  httpServletResponse
     * @param processId processId
     * @throws Exception v
     */
    @RequestMapping(value = "/processDiagram")
    public void genProcessDiagram(HttpServletResponse response, String processId) throws Exception {
        ServletOutputStream outputStream = response.getOutputStream();
        ActGenImageUtils.getFlowImgByInstanceId(processId, outputStream);
    }


}

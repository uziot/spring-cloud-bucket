package com.uziot.bucket.activiti.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className TestController
 * @date 2021-02-26 10:38:41
 * @description 测试服接口
 */

@RestController
public class TestController {

    @GetMapping("/test")
    public Object test() {
        return "okay!";
    }
}

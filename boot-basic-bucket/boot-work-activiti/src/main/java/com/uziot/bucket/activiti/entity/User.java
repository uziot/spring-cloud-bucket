package com.uziot.bucket.activiti.entity;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2021-02-26 10:38:41
 * @description 用户信息
 */

public class User {

    private String userName;

    private String password;

    private String groupId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}

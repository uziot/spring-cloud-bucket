package com.uziot.bucket.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.bucket.domain.BucketArea;
import com.uziot.bucket.mapper.BucketAreaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className BucketAreaController
 * @date 2020-11-21 23:24:52
 * @description
 */
@RestController
@RequestMapping(value = "/area")
public class BucketAreaController {

    private static final Logger log = LoggerFactory.getLogger(BucketAreaController.class);

    @Autowired
    public BucketAreaMapper bucketAreaMapper;

    @GetMapping(value = "/selectAll/{pageNum}/{pageSize}")
    public PageInfo<BucketArea> selectArea(@PathVariable Integer pageNum, @PathVariable Integer pageSize) throws SQLException {
        PageHelper.startPage(pageNum, pageSize);
        List<BucketArea> bucketAreas = bucketAreaMapper.selectAllArea();
        return new PageInfo<>(bucketAreas);
    }

    @GetMapping(value = "/selectArea")
    public List<BucketArea> selectArea(String code) {
        log.info("读取国家省市地区编码表信息：{}", code);
        List<BucketArea> areaListAll = bucketAreaMapper.selectAllArea();
        ArrayList<BucketArea> returnList = new ArrayList<>();
        // 查询所有省
        if (StringUtils.isEmpty(code)) {
            for (BucketArea area : areaListAll) {
                String caAreaParentCode = area.getCaAreaParentCode();
                if (StringUtils.isEmpty(caAreaParentCode)) {
                    returnList.add(area);
                }
            }
        }
        //查询所有地市
        else {
            for (BucketArea area : areaListAll) {
                String caAreaParentCode = area.getCaAreaParentCode();
                if (!StringUtils.isEmpty(caAreaParentCode) && caAreaParentCode.equals(code.trim())) {
                    returnList.add(area);
                }
            }
        }
        log.info("合计找到当前地区数量：{}", returnList.size());
        return returnList;
    }

}

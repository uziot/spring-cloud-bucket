package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className JasyptApplication
 * @date 2020-12-18 23:30:27
 * @description
 */
@SpringBootApplication
public class JasyptApplication {
    public static void main(String[] args) {
        SpringApplication.run(JasyptApplication.class, args);
    }
}

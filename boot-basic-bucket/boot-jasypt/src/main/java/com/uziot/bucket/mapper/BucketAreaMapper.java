package com.uziot.bucket.mapper;
import com.uziot.bucket.domain.BucketArea;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/**
 * @author shidt
 * @version V1.0
 * @className BucketAreaMapper
 * @date 2020-11-21 23:24:52
 * @description
 */
@Mapper
public interface BucketAreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BucketArea record);

    int insertSelective(BucketArea record);

    BucketArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BucketArea record);

    int updateByPrimaryKey(BucketArea record);

    List<BucketArea> selectAllArea();


}
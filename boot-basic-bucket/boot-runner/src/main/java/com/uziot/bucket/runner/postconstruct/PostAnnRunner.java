package com.uziot.bucket.runner.postconstruct;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author shidt
 * @version V1.0
 * @className PostAnnRunner
 * @date 2020-12-01 22:13:15
 * @description
 */
@Component
public class PostAnnRunner {
    @PostConstruct
    public void run() {
        System.out.println("我在启动的时候执行..........");
    }
}

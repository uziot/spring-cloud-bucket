package com.uziot.bucket.runner.commandlinerunner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 功能描述: <br>
 * 缓存初始化检查
 *
 * @author shidt
 * @date 2020-11-07 16:19
 */

@Component
public class CommandRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("The Runner start to initialize ...");
    }
}
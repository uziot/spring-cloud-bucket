package com.uziot.bucket.runner.applicationrunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 功能描述: <br>
 * 缓存初始化检查
 *
 * @author shidt
 * @date 2020-11-07 16:19
 */
@Component
public class RedisInitRunner implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(RedisInitRunner.class);


    @Autowired
    private ConfigurableApplicationContext context;

    @Override
    public void run(ApplicationArguments args) {
        try {
            log.info("Redis连接中 ······");
            log.info("------------检查连接状况逻辑------------");
            log.info("Redis连接成功 ·····");
        } catch (Exception e) {
            log.error("缓存初始化失败，{}", e.getMessage());
            log.error(" ____   __    _   _ ");
            log.error("| |_   / /\\  | | | |");
            log.error("|_|   /_/--\\ |_| |_|__");
            log.error("                        ");
            log.error("SpringbootApplication启动失败              ");
            log.error("Redis连接异常，请检查Redis连接配置并确保Redis服务已启动");
            // 关闭 UDAMIN
            context.close();
        }
    }
}

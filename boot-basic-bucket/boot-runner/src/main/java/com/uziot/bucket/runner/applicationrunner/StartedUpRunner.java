package com.uziot.bucket.runner.applicationrunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * 功能描述: <br>
 * 启动时环境检查
 *
 * @author shidt
 * @date 2020-11-07 19:48
 */
@Order
@Component
public class StartedUpRunner implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(StartedUpRunner.class);

    @Autowired
    private ConfigurableApplicationContext context;

    @Override
    public void run(ApplicationArguments args) {
        if (context.isActive()) {
            String port = context.getEnvironment().getProperty("server.port");
            String time = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toString();
            log.info(" __    ___   _      ___   _     ____ _____  ____ ");
            log.info("/ /`  / / \\ | |\\/| | |_) | |   | |_   | |  | |_  ");
            log.info("\\_\\_, \\_\\_/ |_|  | |_|   |_|__ |_|__  |_|  |_|__ ");
            log.info("                                                      ");
            log.info("SpringbootApplication Successful startup port [{}],Time[{}]", port, time);
        }
    }

}

package com.uziot.boot.jetty;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className ListenerRun
 * @date 2022-03-11 00:12:40
 * @description
 */

@Component
public class ListenerRun implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        new Thread(new JettyHttpServer()).start();
    }
}

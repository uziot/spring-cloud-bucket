package com.uziot.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className ApplicationJetty
 * @date 2022-03-11 00:03:30
 * @description
 */

@SpringBootApplication
public class ApplicationJetty {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationJetty.class, args);
    }
}

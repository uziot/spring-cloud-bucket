package com.uziot.boot.jetty;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shidt
 * @version V1.0
 * @className JettyServlet
 * @date 2022-03-11 00:05:22
 * @description
 */

@Slf4j
public class JettyServlet extends HttpServlet {
    private static final long serialVersionUID = -4763899540378158392L;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String hello = request.getParameter("hello");
        log.info("收到上送参数：{}", hello);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        response.getWriter().println("测试输出");
    }
}

package com.uziot.boot.jetty;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className SpringController
 * @date 2022-03-11 00:20:37
 * @description
 */

@RestController
public class SpringController {
    @GetMapping(value = "/test/{aaa}")
    public @ResponseBody
    Object hello(@PathVariable String aaa) {
        return "Hello :" + aaa;
    }
}

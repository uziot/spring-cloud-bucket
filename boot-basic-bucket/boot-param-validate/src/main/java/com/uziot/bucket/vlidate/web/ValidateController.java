package com.uziot.bucket.vlidate.web;

import com.uziot.bucket.vlidate.dto.Role;
import com.uziot.bucket.vlidate.dto.UserDTO;
import com.uziot.bucket.vlidate.group.DefaultGroup;
import com.uziot.bucket.vlidate.util.ValidatorUtil;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author shidt
 * @version V1.0
 * @className ValidateController
 * @date 2020-11-30 14:23:04
 * @description
 */
@RestController
public class ValidateController {
    public static void main(String[] args) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("用户名");
        // userDTO.setPassword("123456");
        Role role = new Role();
        // role.setRoleName("角色名称");
        role.setRoleId(1);
        ArrayList<Role> roles = new ArrayList<>();


        roles.add(role);
        userDTO.setRole(role);
        userDTO.setRoles(roles);

        // 执行单个校验，列表、对象校验
        HashSet<String> validateResult = ValidatorUtil.getValidateResult(userDTO);

        System.out.println(validateResult);
        ValidatorUtil.validate(userDTO);

        // 执行分组校验、关联校验
        // userDTO.setEmail("abc@user.com");
        HashSet<String> validateResult1 = ValidatorUtil.getValidateResult(userDTO, DefaultGroup.class);
        System.out.println(validateResult1);
        ValidatorUtil.validate(userDTO, DefaultGroup.class);


        System.out.println(role);
    }
}

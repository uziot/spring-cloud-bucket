package com.uziot.bucket.vlidate.cust;

import com.uziot.bucket.vlidate.group.DefaultGroup;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author shidt
 * @version V1.0
 * @className Cust
 * @date 2021-08-10 10:45:28
 * @description
 */

@Data
public class Cust {

    @NotEmpty(message = "姓名不能为空")
    private String name;

    @Numeric(message = "年龄必须为数字")
    private Integer age;

    @Numeric(message = "电话不能为空",groups = {DefaultGroup.class})
    private Integer tel;

    @NotEmpty(message = "地址不能为空",groups = DefaultGroup.class)
    private String address;


}

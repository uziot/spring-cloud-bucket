package com.uziot.bucket.vlidate.dto;

import com.uziot.bucket.vlidate.group.DefaultGroup;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className UserDTO
 * @date 2020-11-30 14:12:09
 * @description
 */
@Data
public class UserDTO {
    @NotBlank(message = "用户名不能为空username")
    private String username;

    @NotBlank(message = "用户密码不能为空：password")
    private String password;

    private String mobile;

    @Email(message = "邮箱格式不正确email", groups = DefaultGroup.class)
    @NotBlank(message = "邮箱不能为空email", groups = DefaultGroup.class)
    private String email;

    @Valid
    @NotNull(message = "用户角色必输role")
    private Role role;

    @NotEmpty(message = "用户角色列表不能为空！")
    @Valid
    private List<Role> roles;
}

package com.uziot.bucket.vlidate.cust;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.uziot.bucket.vlidate.util.StringUtils;

/**
 * @author shidt
 * @version V1.0
 * @className NumericValidator
 * @date 2021-08-07 16:45:04
 * @description   是否为数字自定义校验注解
 */

public class NumericValidator implements ConstraintValidator<Numeric, String> {
    @Override
    public void initialize(Numeric constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || StringUtils.isBlank(value)) {
            return false;
        }
        if (!StringUtils.isNumeric(value)) {
            return false;
        }
        return true;
    }
}

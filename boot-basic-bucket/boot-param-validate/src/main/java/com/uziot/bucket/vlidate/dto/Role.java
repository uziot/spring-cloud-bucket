package com.uziot.bucket.vlidate.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author shidt
 * @version V1.0
 * @className Role
 * @date 2020-11-30 14:15:35
 * @description
 */
@Data
public class Role {
    @NotNull(message = "角色ID不能为空roleId")
    private Integer roleId;
    @NotBlank(message = "角色名称不能为空roleName")
    private String roleName;
    private String roleKey;
}

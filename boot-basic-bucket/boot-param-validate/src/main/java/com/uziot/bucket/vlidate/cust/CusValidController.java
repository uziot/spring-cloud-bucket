package com.uziot.bucket.vlidate.cust;

import com.uziot.bucket.vlidate.group.DefaultGroup;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className CusValidController
 * @date 2021-08-10 10:51:41
 * @description
 */

@RestController
public class CusValidController {

    @PostMapping(value = "/check1")
    public boolean singleCheck(@Validated Cust cust ){
        return true;
    }

    @PostMapping(value = "/check2")
    public boolean groupCheck(@Validated({DefaultGroup.class}) Cust cust){
        return true;
    }
}

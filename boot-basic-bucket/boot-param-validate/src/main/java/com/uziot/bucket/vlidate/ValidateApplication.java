package com.uziot.bucket.vlidate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className ValidateApplication
 * @date 2020-11-30 14:04:11
 * @description
 */
@SpringBootApplication
public class ValidateApplication {
    public static void main(String[] args) {
        SpringApplication.run(ValidateApplication.class, args);
    }
}

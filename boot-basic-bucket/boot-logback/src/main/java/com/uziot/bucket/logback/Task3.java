package com.uziot.bucket.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className Task1
 * @date 2020-12-18 23:58:47
 * @description
 */
@Component
public class Task3 {
    private final Logger log = LoggerFactory.getLogger(Task3.class);

    int start = 0;

    @Scheduled(fixedDelay = 1000)
    public void run() {
        log.error("未知异常--------[{}]", start++);
    }


}

package com.uziot.bucket.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className Task1
 * @date 2020-12-18 23:58:47
 * @description
 */
@Component
public class Task1 {
    private final Logger log = LoggerFactory.getLogger("com.uziot.traceApiRequest");

    int start = 0;
    @Scheduled(fixedDelay = 1000)
    public void run() {
        MDC.put("traceId",String.valueOf(start));
        log.info("定时任务1com.uziot.traceApiRequest--------[{}]", start++);
    }


}

package com.uziot.bucket.shiro.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述: <br>
 * 退出登录
 *
 * @author shidt
 * @date 2020/12/4 15:09
 */
@Slf4j
public class SystemLogoutFilter extends LogoutFilter {


    private static final Logger logger = LoggerFactory.getLogger(SystemLogoutFilter.class);

    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) {
        Subject subject = super.getSubject(request, response);
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            log.info("这是退出登录系统处理逻辑......");
            subject.logout();
        } catch (Exception ex) {
            logger.error("退出登录错误", ex);
        }
        return true;
    }

}    
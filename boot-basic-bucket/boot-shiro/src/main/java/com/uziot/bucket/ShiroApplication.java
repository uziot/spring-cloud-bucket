package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className ShiroApplication
 * @date 2020-12-01 22:45:00
 * @description
 */
@MapperScan("com.uziot.bucket.shiro.dao.mapper")
@SpringBootApplication
public class ShiroApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShiroApplication.class, args);
    }
}

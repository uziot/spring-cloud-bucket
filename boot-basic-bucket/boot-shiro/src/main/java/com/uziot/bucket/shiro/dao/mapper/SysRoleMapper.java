package com.uziot.bucket.shiro.dao.mapper;


import com.uziot.bucket.shiro.dao.domain.SysRole;

import java.util.List;
import java.util.Set;

public interface SysRoleMapper {
    /**
     * delete by primary key
     *
     * @param roleId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long roleId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysRole record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysRole record);

    /**
     * select by primary key
     *
     * @param roleId primary key
     * @return object by primary key
     */
    SysRole selectByPrimaryKey(Long roleId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysRole record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysRole record);

    /**
     * 查询用户角色信息
     *
     * @param username 用户名
     * @return 角色列表
     */
    Set<String> selectUserRoles(String username);
}
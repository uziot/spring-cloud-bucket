package com.uziot.bucket.shiro.config.handler;

import com.alibaba.fastjson.JSONObject;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shidt
 * @version V1.0
 * @className ShiroExceptionHandler
 * @date 2020-11-16 23:48:56
 * @description 由于Shiro框架对认证失败不会对异常消息封装
 * 因此我们需要全局捕获当前失败消息，封装统一的返回结果
 */

@RestControllerAdvice
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class ShiroExceptionHandler {
    @ExceptionHandler(value = {AuthorizationException.class, UnauthorizedException.class})
    public Object handleAuthorizationException(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        result.put("code", 403);
        result.put("msg", "当前接口您没有权限访问！");
        result.put("data", request.getRequestURI());
        return result;
    }
}
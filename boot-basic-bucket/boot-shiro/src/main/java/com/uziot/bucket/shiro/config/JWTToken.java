package com.uziot.bucket.shiro.config;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author shidt
 * @version V1.0
 * @className JWTToken
 * @date 2020-11-16 23:48:56
 * @description
 */

@Data
public class JWTToken implements AuthenticationToken {

    private static final long serialVersionUID = 1282057025599826155L;

    private String token;

    private String exipreAt;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}

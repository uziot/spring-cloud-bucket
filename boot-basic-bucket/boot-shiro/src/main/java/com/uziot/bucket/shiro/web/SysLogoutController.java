package com.uziot.bucket.shiro.web;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shidt
 * @version V1.0
 * @className SysLogoutController
 * @date 2020-12-05 12:05:56
 * @description
 */
@Slf4j
@RestController
public class SysLogoutController {

    @GetMapping(value = "/logout",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object logout(HttpServletRequest request) {
        log.info("执行退出登陆！");

        JSONObject result = new JSONObject();
        result.put("code", 200);
        result.put("msg", "退出登陆成功！");
        result.put("data", request.getRequestURI());
        return result;
    }
}

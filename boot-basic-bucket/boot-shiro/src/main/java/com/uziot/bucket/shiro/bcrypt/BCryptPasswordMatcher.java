package com.uziot.bucket.shiro.bcrypt;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordMatcher;

/**
 * @author shidt
 * @version V1.0
 * @className BCryptPasswordMacher
 * @date 2020-12-02 11:19:46
 * <p>
 * 在这里重写密码匹配策略
 * 使用spring容器初始化对象会造成注入失败，待研究
 * @description
 */
public class BCryptPasswordMatcher extends PasswordMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken credentials = (UsernamePasswordToken) token;
        String password = String.valueOf(credentials.getPassword());
        String dbPassword = (String) info.getCredentials();
        return new BCryptPasswordEncoder().matches(password, dbPassword);
    }
}

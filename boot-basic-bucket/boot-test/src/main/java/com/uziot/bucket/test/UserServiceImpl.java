package com.uziot.bucket.test;

import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author shidt
 * @version V1.0
 * @className UserServiceImpl
 * @date 2021-02-26 09:36:08
 * @description
 */

@Service
public class UserServiceImpl {
    public static final HashMap<String, String> USERS = new HashMap<>();

    static {
        USERS.put("001", "张1");
        USERS.put("002", "张2");
        USERS.put("003", "张3");
        USERS.put("004", "张4");
        USERS.put("005", "张5");
    }

    public String getUsernameById(String userId) {
        return USERS.get(userId) == null ? "" : USERS.get(userId);
    }


}

package com.uziot.bucket.test;

import com.uziot.bucket.TestApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestApplication.class})
public class Test1 {

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void getUserNameById() {
        String username = userService.getUsernameById("001");
        log.info("通过用户ID：[{}]查询到用户名为：[{}]", "001", username);

    }


}

package com.uziot.bucket.data.jpa.repository;

import com.uziot.bucket.data.jpa.domain.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Lenovo
 */
public interface SysRoleRepository extends JpaRepository<SysRole, Long> {

    SysRole findSysRoleByAndRoleNameEquals(String roleName);


}
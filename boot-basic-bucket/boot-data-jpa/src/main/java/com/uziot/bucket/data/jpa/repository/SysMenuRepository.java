package com.uziot.bucket.data.jpa.repository;

import com.uziot.bucket.data.jpa.domain.SysMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * @author Lenovo
 */
public interface SysMenuRepository extends JpaRepository<SysMenu, Long> {

    SysMenu findSysMenusByMenuIdIn(List<Integer> ids);

}
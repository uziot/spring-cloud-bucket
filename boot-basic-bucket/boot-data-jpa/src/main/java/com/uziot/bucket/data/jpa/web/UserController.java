package com.uziot.bucket.data.jpa.web;

import com.uziot.bucket.data.jpa.domain.SysUser;
import com.uziot.bucket.data.jpa.repository.SysUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className UserController
 * @date 2020-11-29 01:14:59
 * @description
 */
@RestController
public class UserController {
    @Autowired
    private SysUserRepository sysUserRepository;

    @GetMapping(value = "/userName/{userName}")
    public SysUser selectUserByName(@PathVariable String userName) {
        return sysUserRepository.findSysUserByUserNameEquals(userName);
    }
}

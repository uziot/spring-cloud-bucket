package com.uziot.bucket.data.jpa.repository;

import com.uziot.bucket.data.jpa.domain.SysUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Lenovo
 */
public interface SysUserRepository extends JpaRepository<SysUser, Long>, JpaSpecificationExecutor<SysUser> {

    SysUser findSysUserByUserNameEquals(String userName);

    SysUser findByUserNameOrEmail(String username, String email);

    @Transactional(timeout = 10)
    @Modifying
    @Query("update SysUser set userName = ?1 where id = ?2")
    int modifyById(String userName, Long id);

    @Transactional
    @Modifying
    @Query("delete from SysUser where userId = ?1")
    void deleteByUserId(Long userId);

    @Query("select u from SysUser u where u.email = ?1")
    SysUser findByEmail(String email);

    @Query("select u from SysUser u")
    Page<SysUser> findALL(Pageable pageable);

    Page<SysUser> findByNickName(String nickName, Pageable pageable);

    Slice<SysUser> findByNickNameAndEmail(String nickName, String email, Pageable pageable);


}
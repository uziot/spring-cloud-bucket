package com.uziot.bucket.poi.office.word.impl;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import com.uziot.bucket.poi.office.word.WordConvert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author shidt
 * @version V1.0
 * @className WordConvertImpl
 * @date 2020-07-10 23:53:04
 * @description
 */

@Slf4j
@Service
public class WordConvertImpl implements WordConvert {
    /**
     * 提供WORD转PDF方法
     *
     * @param wordInPath     word路径
     * @param pdfOutPathName pdf输出路径
     */
    @Override
    public void wordConvertPdf(String wordInPath, String pdfOutPathName) {

        log.info("Word 转 PDF 开始执行任务...");
        long start = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        FileOutputStream fileOutputStream = null;
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(wordInPath);
            Document docTemp = new Document(fileInputStream);
            File file = new File(pdfOutPathName);
            fileOutputStream = new FileOutputStream(file);
            docTemp.save(fileOutputStream, SaveFormat.PDF);
        } catch (Exception e) {
            log.error("word转pdf异常", e);
            throw new RuntimeException("word转pdf异常");
        } finally {
            long end = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            log.info("Word 转 PDF完成,合计耗时：【{}】MS", (end - start));
            closeQuietly(fileOutputStream);
            closeQuietly(fileInputStream);
        }
        log.info("Word 转 PDF 完成：{}", pdfOutPathName);
    }

    /**
     * 加载word转换证书文件
     *
     * @return 证书文件
     */
    @PostConstruct
    public static License getLicense() {
        log.info("开始读取word注册文件");
        InputStream inputStream = WordConvertImpl.class.getResourceAsStream("/static/license.xml");
        License result = null;
        try {
            License aposeLic = new License();
            aposeLic.setLicense(inputStream);
            result = aposeLic;
            inputStream.close();
        } catch (Exception e) {
            log.error("读取word注册文件发生异常");
        } finally {
            closeQuietly(inputStream);
        }
        log.info("读取word注册文件完成！");
        return result;
    }

    /**
     * 关闭流
     *
     * @param closeable closeable
     */
    public static void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException ignored) {
        }

    }


    public static void main(String[] args) {
        WordConvertImpl wordConvert = new WordConvertImpl();
        String wordInPath = "C:\\Users\\Lenovo\\Desktop\\aa\\aaa.doc";
        String pdfOutPathName = "C:\\Users\\Lenovo\\Desktop\\aa\\doc.pdf";
        getLicense();
        wordConvert.wordConvertPdf(wordInPath, pdfOutPathName);


    }
}

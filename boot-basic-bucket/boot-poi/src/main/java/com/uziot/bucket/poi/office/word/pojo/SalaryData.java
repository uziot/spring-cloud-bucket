package com.uziot.bucket.poi.office.word.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.List;
/**
 * @author shidt
 * @version V1.0
 * @className SalaryData
 * @date 2020-07-10 23:53:04
 * @description 文档模板数据
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SalaryData {
    /** 公司名称 */
    private String companyName;
    /** 员工薪资列表 */
    private List<Worker> workerList;
    /** 工资总和 */
    private BigDecimal totalSalary;
    /** 公司log */
    private BufferedImage logImg;
}


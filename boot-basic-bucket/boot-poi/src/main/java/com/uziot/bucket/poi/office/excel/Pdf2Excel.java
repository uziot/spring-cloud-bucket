package com.uziot.bucket.poi.office.excel;

import com.aspose.cells.License;
import com.aspose.pdf.Document;
import com.aspose.pdf.SaveFormat;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author shidt
 * @version V1.0
 * @className Excel2PdfUtil
 * @date 2020-07-10 23:53:04
 * @description Pdf 转 XLSX
 */

public class Pdf2Excel {

    public static void main(String[] args) throws IOException {
        getLicense();
        pdf2excel("C:\\Users\\detao\\Desktop\\cc.pdf");
    }


    /**
     * pdf转doc
     * @param pdfPath pdf转doc
     */
    public static void pdf2excel(String pdfPath) {
        long old = System.currentTimeMillis();
        try {
            //新建一个word文档
            String wordPath = pdfPath.substring(0, pdfPath.lastIndexOf(".")) + ".xlsx";
            FileOutputStream os = new FileOutputStream(wordPath);
            //doc是将要被转化的word文档
            Document doc = new Document(pdfPath);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(os, SaveFormat.Excel);
            os.close();
            //转化用时
            long now = System.currentTimeMillis();
            System.out.println("Pdf 转 XLSX 共耗时：" + ((now - old) / 1000.0) + "秒");
        } catch (Exception e) {
            System.out.println("Pdf 转 XLSX 失败...");
            e.printStackTrace();
        }
    }

    /**
     * 获取认证，去除水印
     *
     * @return 是否成功
     */
    public static boolean getLicense() {
        boolean result = false;
        try {
            //这个文件应该是类似于密码验证(证书？)，用于获得去除水印的权限
            InputStream is = Pdf2Excel.class.getClassLoader().getResourceAsStream("/static/license.xml");
            License license = new License();
            license.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
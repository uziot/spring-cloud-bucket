package com.uziot.bucket.poi.image;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: <br>
 * 二维码生成工具类
 *
 * @author shidt
 * @date 2020-04-03 0:28
 */
public class QRCodeUtil {

    public static BitMatrix generateQrCode(String content) throws WriterException {
        return generateQrCode(content, 300, 300, 1, ErrorCorrectionLevel.L);
    }

    public static BitMatrix generateQrCode(
            String content,
            Integer width,
            Integer height,
            Integer border,
            ErrorCorrectionLevel level) throws WriterException {
        //设置图片的文字编码以及内边框
        Map<EncodeHintType, Object> hints = new HashMap<>(5);
        //编码
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        //边框距
        hints.put(EncodeHintType.MARGIN, border);
        hints.put(EncodeHintType.ERROR_CORRECTION, level);
        BitMatrix bitMatrix;
        bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        return bitMatrix;

    }

}
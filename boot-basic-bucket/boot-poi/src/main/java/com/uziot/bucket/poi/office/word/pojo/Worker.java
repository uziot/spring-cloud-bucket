package com.uziot.bucket.poi.office.word.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author shidt
 * @version V1.0
 * @className Worker
 * @date 2020-07-10 23:53:04
 * @description 员工薪资信息
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Worker {
    /** 员工姓名 */
    private String name;
    /** 年龄 */
    private Integer age;
    /** 性别 */
    private String sex;
    /** 工资 */
    private BigDecimal salary;
}


package com.uziot.bucket.poi.pdf;

import java.util.Set;

/**
 * @author shidt
 * @version V1.0
 * @className PdfConvertService
 * @date 2020-07-11 00:06:53
 * @description
 */

public interface PdfConvertService {
    /**
     * pdfIntoPicture
     *
     * @param pdfPathName       pdfPathName
     * @param finalImagesFolder dstImgFolder 保存的文件夹
     * @return s
     */
    Set<String> pdfConvertImage(String pdfPathName, String finalImagesFolder);

}

package com.uziot.bucket.poi.office.word;

/**
 * @author shidt
 * @version V1.0
 * @className WorldConvert
 * @date 2020-07-10 23:52:14
 * @description
 */

public interface WordConvert {
    /**
     * 将word转换成PDF输出
     *
     * @param wordPath    word路径
     * @param pdfPathName PDF路径 全路径包含文件名
     */
    void wordConvertPdf(String wordPath, String pdfPathName);
}

package com.uziot.bucket.poi.office.word.run;

import com.aspose.words.SaveFormat;
import com.uziot.bucket.poi.office.word.pojo.SalaryData;
import com.uziot.bucket.poi.office.word.pojo.Worker;
import com.uziot.bucket.poi.office.word.util.PoiUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className App
 * @date 2020-07-10 23:53:04
 * @description 根据模板生成文档
 */

public class App {

    public static void main(String[] args) throws Exception {
        // 封装员工薪资信息
        Worker worker1 = Worker.builder()
                .name("张三")
                .age(18)
                .sex("男")
                .salary(BigDecimal.valueOf(1000))
                .build();
        Worker worker2 = Worker.builder()
                .name("李四")
                .age(16)
                .sex("女")
                .salary(BigDecimal.valueOf(2000))
                .build();
        List<Worker> workerList = Arrays.asList(worker1, worker2);

        // 计算工资总和
        BigDecimal totalSalary = workerList.stream()
                .map(Worker::getSalary)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BufferedImage image = ImageIO.read(App.class.getResourceAsStream("/static/鱼.png"));
        BufferedImage bufferedImage = PoiUtil.resizeBufferedImage(image, 50, 50, true);

        // 封装文档模板数据
        SalaryData data = SalaryData.builder()
                .companyName("王多鱼股份有限公司")
                .workerList(workerList)
                .totalSalary(totalSalary)
                .logImg(bufferedImage)
                .build();


        // 读取模板文件
        Map<String, Object> dataMap = PoiUtil.getStringObjectMap(data);
        String templatePath = "/static/模板.docx";
        String docxPath = "C:\\Users\\detao\\Desktop\\员工薪资统计表.docx";
        String docPath = "C:\\Users\\detao\\Desktop\\员工薪资统计表.doc";
        String pdfPath = "C:\\Users\\detao\\Desktop\\员工薪资统计表.pdf";

        PoiUtil.fillWordDataByMap(App.class.getResourceAsStream(templatePath), new FileOutputStream(docxPath), dataMap, SaveFormat.DOCX);
        PoiUtil.fillWordDataByMap(App.class.getResourceAsStream(templatePath), new FileOutputStream(docPath), dataMap, SaveFormat.DOC);
        PoiUtil.fillWordDataByMap(App.class.getResourceAsStream(templatePath), new FileOutputStream(pdfPath), dataMap, SaveFormat.PDF);
    }
}

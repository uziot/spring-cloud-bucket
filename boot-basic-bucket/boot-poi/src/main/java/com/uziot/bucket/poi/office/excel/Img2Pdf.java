package com.uziot.bucket.poi.office.excel;

import com.aspose.cells.License;
import com.aspose.pdf.Document;
import com.aspose.pdf.Image;
import com.aspose.pdf.Page;
import com.aspose.pdf.SaveFormat;
import com.uziot.bucket.poi.office.word.util.PoiUtil;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Img2Pdf {

    public static void main(String[] args) throws Exception {
        String source1 = "C:\\Users\\detao\\Desktop\\a\\1.jpg";
        String source2 = "C:\\Users\\detao\\Desktop\\a\\2.jpg";
        String target = "C:\\Users\\detao\\Desktop\\1.pdf";
        ArrayList<String> paths = new ArrayList<>();
        paths.add(source1);
        paths.add(source2);
        //image2pdf(source, target, "png");
        image2pdf(paths, target, "jpg");

    }

    /**
     * 获取license 去除水印
     */
    public static boolean getLicense() {
        try {
            String license = "PExpY2Vuc2U+DQogIDxEYXRhPg0KICAgIDxQcm9kdWN0cz4NCiAgICAgIDxQcm9kdWN0PkFzcG9zZS5Ub3RhbCBmb3IgSmF2YTwvUHJvZHVjdD4NCiAgICAgIDxQcm9kdWN0PkFzcG9zZS5Xb3JkcyBmb3IgSmF2YTwvUHJvZHVjdD4NCiAgICA8L1Byb2R1Y3RzPg0KICAgIDxFZGl0aW9uVHlwZT5FbnRlcnByaXNlPC9FZGl0aW9uVHlwZT4NCiAgICA8U3Vic2NyaXB0aW9uRXhwaXJ5PjIwOTkxMjMxPC9TdWJzY3JpcHRpb25FeHBpcnk+DQogICAgPExpY2Vuc2VFeHBpcnk+MjA5OTEyMzE8L0xpY2Vuc2VFeHBpcnk+DQogICAgPFNlcmlhbE51bWJlcj44YmZlMTk4Yy03ZjBjLTRlZjgtOGZmMC1hY2MzMjM3YmYwZDc8L1NlcmlhbE51bWJlcj4NCiAgPC9EYXRhPg0KICA8U2lnbmF0dXJlPnNOTExLR01VZEYwcjhPMWtLaWxXQUdkZ2ZzMkJ2SmIvMlhwOHA1aXVEVmZaWG1ocHBvK2QwUmFuMVA5VEtkalY0QUJ3QWdLWHhKM2pjUVRxRS8ySVJmcXduUGY4aXROOGFGWmxWM1RKUFllRDN5V0U3SVQ1NUd6NkVpalVwQzdhS2Vvb2hUYjR3MmZwb3g1OHdXb0YzU05wNnNLNmpEZmlBVUdFSFlKOXBqVT08L1NpZ25hdHVyZT4NCjwvTGljZW5zZT4=";
            InputStream is = baseToInputStream(license);
            License aposeLic = new License();
            aposeLic.setLicense(is);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * base64转inputStream
     */
    private static InputStream baseToInputStream(String base64string) {
        ByteArrayInputStream stream = null;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] bytes1 = decoder.decodeBuffer(base64string);
            stream = new ByteArrayInputStream(bytes1);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return stream;
    }

    /**
     * 图片转PDF
     */
    public static void image2pdf(List<String> sourcePath, String targetPath, String imgType) throws IOException {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (!getLicense()) {
            return;
        }
        //创建文档
        Document doc = new Document();
        for (String path : sourcePath) {
            //新增一页
            Page page = doc.getPages().add();
            //设置页边距
            page.getPageInfo().getMargin().setTop(0);
            page.getPageInfo().getMargin().setLeft(0);
            page.getPageInfo().getMargin().setBottom(0);
            page.getPageInfo().getMargin().setRight(0);
            //创建图片对象
            Image image = new Image();
            BufferedImage bufferedImage = ImageIO.read(new File(path));
            // 统一图片尺寸
            BufferedImage targetImg = PoiUtil.resizeBufferedImage(bufferedImage, 1920, 1080, true);
            //获取图片尺寸
            int height = targetImg.getHeight();
            int width = targetImg.getWidth();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(targetImg, imgType, baos);
            baos.flush();
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            image.setImageStream(bais);
            //设置pdf页的尺寸与图片一样
            page.getPageInfo().setHeight(height);
            page.getPageInfo().setWidth(width);
            //添加图片
            page.getParagraphs().add(image);
        }
        //保存
        doc.save(targetPath, SaveFormat.Pdf);
        // pdf转word
        Pdf2Word.pdf2doc(targetPath);
    }

    /**
     * 图片转PDF
     */
    public static void image2pdf(String sourcePath, String targetPath, String imgType) throws IOException {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (!getLicense()) {
            return;
        }
        //创建文档
        Document doc = new Document();
        //新增一页
        Page page = doc.getPages().add();
        //设置页边距
        page.getPageInfo().getMargin().setBottom(0);
        page.getPageInfo().getMargin().setTop(0);
        page.getPageInfo().getMargin().setLeft(0);
        page.getPageInfo().getMargin().setRight(0);
        //创建图片对象
        Image image = new Image();
        BufferedImage bufferedImage = ImageIO.read(new File(sourcePath));
        //获取图片尺寸
        int height = bufferedImage.getHeight();
        int width = bufferedImage.getWidth();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, imgType, baos);
        baos.flush();
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        image.setImageStream(bais);
        //设置pdf页的尺寸与图片一样
        page.getPageInfo().setHeight(height);
        page.getPageInfo().setWidth(width);
        //添加图片
        page.getParagraphs().add(image);
        //保存
        doc.save(targetPath, SaveFormat.Pdf);

        // pdf转word
        Pdf2Word.pdf2doc(targetPath);
    }

}




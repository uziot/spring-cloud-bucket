package com.uziot.bucket.poi.office.excel;

import com.aspose.cells.*;
import com.aspose.pdf.Document;
import org.apache.poi.util.IOUtils;

import java.io.*;

/**
 * @author shidt
 * @version V1.0
 * @className Excel2PdfUtil
 * @date 2020-07-10 23:53:04
 * @description Pdf 转 XLSX
 */

public class Pdf2Html {

    public static void main(String[] args) {
        //new com.aspose.pdf.License().setLicense(is);
        getLicense();
        pdf2html("C:\\Users\\detao\\Desktop\\h\\1.一年级英语寒假学习册.pdf");
    }


    /**
     * pdf2doc
     * @param pdfPath pdf2doc
     */
    public static void pdf2html(String pdfPath) {
        long old = System.currentTimeMillis();
        try {
            //新建一个word文档
            String wordPath = pdfPath.substring(0, pdfPath.lastIndexOf(".")) + ".html";
            OutputStream os = new FileOutputStream(wordPath);
            //doc是将要被转化的word文档
            Document doc = new Document(pdfPath);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            //SaveOptions saveOptions = new HtmlSaveOptions();
            com.aspose.pdf.HtmlSaveOptions htmlSaveOptions = new com.aspose.pdf.HtmlSaveOptions();
            htmlSaveOptions.setSplitIntoPages(false);
            htmlSaveOptions.setFixedLayout(true);
            //我偏向于所有资源存放在一个文件夹内，因此，需要重写下面三个函数
            htmlSaveOptions.setCustomResourceSavingStrategy(new com.aspose.pdf.HtmlSaveOptions.ResourceSavingStrategy() {
                @Override
                public String invoke(com.aspose.pdf.SaveOptions.ResourceSavingInfo arg0) {
                    try {
                        File file = new File("C:\\Users\\detao\\Desktop\\h\\" + arg0.getSupposedFileName());// 输出路径
                        FileOutputStream fileOs = new FileOutputStream(file);
                        fileOs.write(arg0.getContentStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return arg0.getSupposedFileName();
                }
            });
            // css保存步骤
            htmlSaveOptions.setCustomCssSavingStrategy(new com.aspose.pdf.HtmlSaveOptions.CssSavingStrategy() {
                @Override
                public void invoke(com.aspose.pdf.HtmlSaveOptions.CssSavingInfo arg0) {
                    try {
                        File file = new File(arg0.getSupposedURL());// 输出路径
                        byte[] b = IOUtils.toByteArray(arg0.getContentStream());
                        FileOutputStream fileOs = new FileOutputStream(file);
                        fileOs.write(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            htmlSaveOptions.setCustomStrategyOfCssUrlCreation(new com.aspose.pdf.HtmlSaveOptions.CssUrlMakingStrategy() {
                @Override
                public String invoke(com.aspose.pdf.HtmlSaveOptions.CssUrlRequestInfo arg0) {
                    return "C:\\Users\\detao\\Desktop\\h\\test.css";
                }
            });
            doc.save(os, htmlSaveOptions);
            os.close();
            //转化用时
            long now = System.currentTimeMillis();
            System.out.println("Pdf 转 html 共耗时：" + ((now - old) / 1000.0) + "秒");
        } catch (Exception e) {
            System.out.println("Pdf 转 html 失败...");
            e.printStackTrace();
        }
    }

    /**
     * 获取认证，去除水印
     *
     * @return 是否成功
     */
    public static boolean getLicense() {
        boolean result = false;
        try {
            //这个文件应该是类似于密码验证(证书？)，用于获得去除水印的权限
            InputStream is = Pdf2Html.class.getClassLoader().getResourceAsStream("/static/license.xml");
            License license = new License();
            license.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
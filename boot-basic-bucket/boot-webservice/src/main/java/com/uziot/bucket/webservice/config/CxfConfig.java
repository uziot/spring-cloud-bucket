package com.uziot.bucket.webservice.config;

import javax.xml.ws.Endpoint;

import com.uziot.bucket.webservice.service.ICommonService;
import org.apache.cxf.Bus;
import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.ext.logging.LoggingOutInterceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 默认服务在 Host:port/services/*** 路径下
 * 这里相当于把Commonservice接口发布在了路径/services/CommonService下
 * wsdl文档路径为http://localhost:8080/services/CommonService?wsdl
 * wsdl2java -encoding  utf8  http://localhost:8092/services/CommonService?wsdl
 * @author shidt
 * @version 1.0
 * @since 2018/6/15
 */
@Configuration
public class CxfConfig {
    @Autowired
    private Bus bus;
    @Autowired
    ICommonService commonService;

    @Bean
    public LoggingInInterceptor loggingInInterceptor() {
        return new LoggingInInterceptor();
    }

    @Bean
    public LoggingOutInterceptor loggingOutInterceptor() {
        return new LoggingOutInterceptor();
    }

    /**
     * JAX-WS
     **/
    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, commonService);
        endpoint.publish("/CommonService");
        // 日志拦截器
        endpoint.getInInterceptors()
                .add(loggingInInterceptor());
        endpoint.getOutInterceptors()
                .add(loggingOutInterceptor());
        return endpoint;
    }
}

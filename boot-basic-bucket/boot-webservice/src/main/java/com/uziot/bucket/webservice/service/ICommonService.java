package com.uziot.bucket.webservice.service;


import com.uziot.bucket.webservice.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * ICommonService
 *
 * @author shidt
 * @version 1.0
 * @since 2018/6/15
 */
@WebService(name = "CommonService", // 暴露服务名称
        targetNamespace = "http://model.webservice.bucket.uziot.com/"// 命名空间,一般是接口的包名倒序
)
public interface ICommonService {
    @WebMethod
//    @WebResult(name = "String", targetNamespace = "")
    String sayHello(@WebParam(name = "userName") String name);

    @WebMethod
//    @WebResult(name = "String", targetNamespace = "")
    User getUser(@WebParam(name = "userName") String name);
}

package com.uziot.bucket.scheduler.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 功能描述: <br>
 * 缓存初始化检查
 *
 * @author shidt
 * @date 2019-11-07 16:19
 */

@Component
public class SchedulerTask {

    private int count = 0;

    @Scheduled(cron = "*/6 * * * * ?")
    private void process() {
        System.out.println("定时任务正在执行" + (count++));
    }

}

package com.uziot.bucket.scheduler.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能描述: <br>
 * 缓存初始化检查
 *
 * @author shidt
 * @date 2019-11-07 16:19
 */

@Component
public class Scheduler2Task {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 6000, initialDelay = 6000)
    public void reportCurrentTime() {
        System.out.println("定时任务正在执行，现在时间：" + DATE_FORMAT.format(new Date()));
    }

}

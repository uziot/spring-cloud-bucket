package com.uziot.bucket.scheduler.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className TimerTask
 * @description 这个类是定时任务类
 * @date 2019/11/29 23:41
 */
@Slf4j
@Component
public class TimerTask implements SchedulingConfigurer {


    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        String cron = "*/6 * * * * ?";
        Runnable runnable = () -> {
            log.info("---------------------------TimerTask-Start-----------------------------");
            log.info("定时任务启动，将执行指定的任务。。。。。。。。。。");
            log.info("---------------------------TimerTask-End-----------------------------");
        };
        scheduledTaskRegistrar.addCronTask(runnable, cron);
    }
}

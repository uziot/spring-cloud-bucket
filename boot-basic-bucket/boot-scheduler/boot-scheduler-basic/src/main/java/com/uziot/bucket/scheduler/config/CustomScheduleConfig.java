package com.uziot.bucket.scheduler.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 功能描述: <br>
 * 通过配置文件设定是否开启定时任务
 *
 * @author shidt
 * @date 2019-11-07 16:19
 */


@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "schedule.notify", name = "enabled", havingValue = "true")
public class CustomScheduleConfig {

    /*
     * 定制控制是否启动定时调度任务
     * 如果不配置该项信息，则默认该服务不启动（定时推送服务只作为单一的界面服务）
     *#{schedule.notify.enabled}
     * @return
     */
}
package com.uziot.bucket.scheduler.controller;

import com.uziot.bucket.scheduler.task.ScheduleTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className TimerTask
 * @description 这个类是定时任务类, 可以动态调整对象的执行表达式
 * @date 2019/11/29 23:41
 * @see ScheduleTask
 * 可以从URL动态调整执行频率更改时间
 * http://localhost:8080/test/updateCron?cron=0/1%20*%20*%20*%20*%20?
 */

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    private final ScheduleTask scheduleTask;

    @Autowired
    public TestController(ScheduleTask scheduleTask) {
        this.scheduleTask = scheduleTask;
    }

    @GetMapping("/updateCron")
    public String updateCron(String cron) {
        log.info("new cron :{}", cron);
        scheduleTask.setCron(cron);
        return "ok";
    }
}
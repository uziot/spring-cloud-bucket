package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicSchedulerApplication
 * @date 2020-12-20 19:51:39
 * @description
 */
@MapperScan("com.uziot.bucket.dynamic.dao")
@SpringBootApplication
public class DynamicSchedulerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicSchedulerApplication.class, args);
    }
}

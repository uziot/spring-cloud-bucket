package com.uziot.bucket.dynamic.web;

import com.uziot.bucket.dynamic.config.SchedulerRunnable;
import com.uziot.bucket.dynamic.config.SchedulerTaskRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicController
 * @date 2020-12-20 20:38:19
 * @description
 */
@RestController
public class DynamicController {
    @Autowired
    private SchedulerTaskRegistrar schedulerTaskRegistrar;

    @GetMapping(value = "/add")
    public void addTask() {
        SchedulerRunnable schedulerRunnable =
                new SchedulerRunnable("demoTask", "taskNoParams");
        String cron = "0/2 * * * * ?";
        schedulerTaskRegistrar.addCronTask(schedulerRunnable, cron);
    }

    @GetMapping(value = "/cancelBeanName")
    public void cancelBeanName() {
        String beanName = "demoTask";
        schedulerTaskRegistrar.removeCronTask(beanName);
    }

    @GetMapping(value = "/cancelMethodName")
    public void cancelMethodName() {
        String beanName = "demoTask";
        String taskNoParams = "taskNoParams";
        SchedulerRunnable schedulerRunnable = new SchedulerRunnable(beanName, taskNoParams);
        schedulerTaskRegistrar.removeCronTask(schedulerRunnable);
    }

    @GetMapping(value = "/add2")
    public void addTask2() {
        SchedulerRunnable schedulerRunnable =
                new SchedulerRunnable("demoTask", "taskWithParams", "haha", 99);
        String cron = "0/2 * * * * ?";
        schedulerTaskRegistrar.addCronTask(schedulerRunnable, cron);
    }
}

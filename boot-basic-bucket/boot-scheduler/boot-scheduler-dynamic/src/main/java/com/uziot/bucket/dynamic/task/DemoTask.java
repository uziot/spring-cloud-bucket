package com.uziot.bucket.dynamic.task;

import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className DemoTask
 * @date 2020-11-21 23:24:52
 * @description simple-demo
 * 为了更好的控制定时任务的调度，定时任务的取消仅支持如下：
 * - 组件名称
 * - 组件名称 + 方法名称
 * - 组件名称 + 方法名称 + 参数列表
 */
@Component("demoTask")
public class DemoTask {

    public void taskWithParams(String param1, Integer param2) {
        System.out.println("这是有参示例任务：" + param1 + param2);
    }
    public void taskWithParams(String param1, String param2) {
        System.out.println("这是有参示例任务：" + param1 + param2);
    }

    public void taskNoParams() {
        System.out.println("执行无参示例任务");
    }
}

package com.uziot.bucket.dynamic.config;

import java.util.concurrent.ScheduledFuture;

/**
 * @author shidt
 * @version V1.0
 * @className SchedulerTaskFuture
 * @date 2020-11-21 23:24:52
 * @description 定时任务计划控制类
 */
public final class SchedulerTaskFuture {

    public volatile ScheduledFuture<?> scheduledFuture;

    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.scheduledFuture;
        if (future != null) {
            future.cancel(true);
        }
    }
}

package com.uziot.bucket.dynamic.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.uziot.bucket.dynamic.dao.domain.DynamicTask;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicTaskMapper
 * @date 2020-11-21 23:24:52
 * @description
 */
public interface DynamicTaskMapper extends BaseMapper<DynamicTask> {

}

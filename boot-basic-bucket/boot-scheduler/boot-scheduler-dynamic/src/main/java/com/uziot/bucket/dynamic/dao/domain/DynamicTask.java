package com.uziot.bucket.dynamic.dao.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicTask
 * @date 2020-11-21 23:24:52
 * @description 动态定时任务model
 */
@TableName("t_task")
@Data
public class DynamicTask {

    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("cron")
    private String cron;
}

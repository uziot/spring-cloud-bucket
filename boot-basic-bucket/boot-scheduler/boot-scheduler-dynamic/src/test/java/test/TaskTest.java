package test;

import com.uziot.bucket.DynamicSchedulerApplication;
import com.uziot.bucket.dynamic.config.SchedulerRunnable;
import com.uziot.bucket.dynamic.config.SchedulerTaskRegistrar;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className TaskTest
 * @date 2020-11-21 23:24:52
 * @description 测试定时任务
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        DynamicSchedulerApplication.class
})
@SpringBootConfiguration
public class TaskTest {

    @Autowired
    SchedulerTaskRegistrar schedulerTaskRegistrar;

    @Test
    public void testTask() throws InterruptedException {
        SchedulerRunnable task = new SchedulerRunnable("demoTask", "taskNoParams", null);
        schedulerTaskRegistrar.addCronTask(task, "0/10 * * * * ?");

        // 便于观察
        Thread.sleep(3000000);
    }

    @Test
    public void testHaveParamsTask() throws InterruptedException {
        SchedulerRunnable task = new SchedulerRunnable("demoTask", "taskWithParams", "haha", 23);
        schedulerTaskRegistrar.addCronTask(task, "0/10 * * * * ?");

        // 便于观察
        Thread.sleep(3000000);
    }
}

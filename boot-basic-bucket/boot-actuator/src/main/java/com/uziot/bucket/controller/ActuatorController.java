package com.uziot.bucket.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className ActuatorController
 * @date 2020-12-05 22:37:29
 * @description
 */
@Slf4j
@RefreshScope
@RestController
public class ActuatorController {

    @Value("${baby.name}")
    private String userName;

    @Autowired
    private Environment environment;

    @RequestMapping("/hello")
    public String index() {
        log.info("加载到文件：" + userName);
        return "Hello:" + userName;
    }
}
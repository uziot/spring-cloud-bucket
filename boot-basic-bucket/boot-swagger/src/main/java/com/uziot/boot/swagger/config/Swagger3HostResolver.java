package com.uziot.boot.swagger.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.stereotype.Component;
import springfox.documentation.oas.web.OpenApiTransformationContext;
import springfox.documentation.oas.web.WebMvcOpenApiTransformationFilter;
import springfox.documentation.spi.DocumentationType;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @author shidt
 * @version V1.0
 * @className Swagger3HostResolver
 * @date 2021-07-22 11:34:37
 * @description swagger3的不同环境服务器配置
 */

@Component
@SuppressWarnings("All")
public class Swagger3HostResolver implements WebMvcOpenApiTransformationFilter {

    @Override
    public OpenAPI transform(OpenApiTransformationContext<HttpServletRequest> context) {
        OpenAPI swagger = context.getSpecification();
        Server server = new Server();
        server.setUrl("http://www.uziot.com");
        Server server2 = new Server();
        server2.setUrl("http://localhost:8080");

        ArrayList<Server> servers = new ArrayList<>();
        servers.add(server);
        servers.add(server2);
        swagger.setServers(servers);
        return swagger;
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return DocumentationType.OAS_30.equals(delimiter);
    }
}
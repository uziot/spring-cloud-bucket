package com.uziot.boot.swagger.web;

import com.uziot.boot.swagger.web.vo.RequestVo;
import com.uziot.boot.swagger.web.vo.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shidt
 * @version V1.0
 * @className SwaggerTestController
 * @date 2020-11-22 16:05:21
 * @description
 */
@RestController
@Api(tags = "Swagger文档测试")
@RequestMapping(value = "/swagger")
public class SwaggerTestController {

    @ApiOperation(value = "测试接口1")
    @PostMapping(value = "/demo")
    public ResponseVo swaggerTest(HttpServletRequest request, @RequestBody RequestVo requestVo) {
        String authorization = request.getHeader("Authorization");
        System.out.println("认证信息：" + authorization);
        System.out.println("上送信息：" + requestVo);

        ResponseVo responseVo = new ResponseVo();
        responseVo.setUserName("张三");
        responseVo.setPassword("123");

        return responseVo;
    }

}

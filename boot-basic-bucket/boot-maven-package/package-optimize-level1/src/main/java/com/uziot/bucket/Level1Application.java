package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className Level0Application
 * @date 2021-06-07 10:12:26
 * @description
 *
 * 启动命令java -jar -Djava.ext.dirs=lib package-optimize-level1.jar
 *
 */

@SpringBootApplication
public class Level1Application {
    public static void main(String[] args) {
        SpringApplication.run(Level1Application.class, args);
    }
}

package com.uziot.bucket.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className Level0Controller
 * @date 2021-06-07 10:14:10
 * @description
 */

@Slf4j
@RestController
public class Level1Controller {

    @GetMapping(value = "/hello")
    public String hello() {
        log.info("控制器：{}收到请求！", this.getClass().getName());
        return this.getClass().getName();
    }
}

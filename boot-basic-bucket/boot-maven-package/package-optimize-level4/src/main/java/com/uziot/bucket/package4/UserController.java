package com.uziot.bucket.package4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className UserController
 * @date 2021-06-07 10:14:10
 * @description
 */

@RestController
@RequestMapping("/user")
public class UserController {

//    @Value("${user.name}")
//    private String name;
//    @Value("${user.age}")
//    private Integer age;
//    @Value("${user.tel}")
//    private String tel;
//    @Value("${user.sex}")
//    private String sex;

//    @Autowired
//    private Environment env;

    @Autowired
    private User user;

    @GetMapping("/{id}")
    public String getById(@PathVariable Integer id){
        System.out.println("userController getById, running ......");
        System.out.println("id ====> " + id);
        //System.out.println("name ====> " + name);
        //System.out.println("name ====> " + env.getProperty("user.name"));
        System.out.println("name ====> " + user.getName());
        //System.out.println("age ====> " + age);
        //System.out.println("age ====> " + env.getProperty("user.age"));
        System.out.println("age ====> " + user.getAge());
        //System.out.println("tel ====> " + tel);
        //System.out.println("tel ====> " + env.getProperty("user.tel"));
        System.out.println("tel ====> " + user.getTel());
        //System.out.println("sex ====> " + sex);
        //System.out.println("sex ====> " + env.getProperty("user.sex"));
        System.out.println("sex ====> " + user.getSex());
        return "userController getById running , id ===> " + id;
    }

    @GetMapping
    public String index(){
        return "index.html";
    }


}

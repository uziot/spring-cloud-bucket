package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2021-06-07 10:14:10
 * @description springboot打包分离lib和config
 */

@SpringBootApplication
public class Level4Application {

    public static void main(String[] args) {
        SpringApplication.run(Level4Application.class, args);
    }

}

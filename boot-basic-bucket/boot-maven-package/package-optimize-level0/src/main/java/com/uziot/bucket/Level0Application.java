package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className Level0Application
 * @date 2021-06-07 10:12:26
 * @description
 */

@SpringBootApplication
public class Level0Application {
    public static void main(String[] args) {
        SpringApplication.run(Level0Application.class, args);
    }
}

package com.uziot.bucket.mdc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MdcApplication
 * @date 2020-12-01 22:24:15
 * @description
 */
@SpringBootApplication
public class MdcApplication {
    public static void main(String[] args) {
        SpringApplication.run(MdcApplication.class, args);
    }
}

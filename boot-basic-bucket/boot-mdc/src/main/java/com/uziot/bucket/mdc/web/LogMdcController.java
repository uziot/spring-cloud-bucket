package com.uziot.bucket.mdc.web;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className LogMdcController
 * @date 2020-12-01 22:31:08
 * @description
 */
@Slf4j
@RestController
public class LogMdcController {
    int i = 0;

    @GetMapping(value = "/mdc", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object mdc() {
        log.info("控制器收到查询请求.........[{}]", i++);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("traceId", MDC.get("traceId"));
        return jsonObject;
    }
}

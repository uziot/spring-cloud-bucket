package com.uziot.bucket.mdc.web;

import com.uziot.bucket.mdc.util.MdcLogUtil;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author shidt
 * @version V1.0
 * @className WebFilter
 * @date 2020-12-01 22:33:48
 * @description
 */
@Component
public class WebFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        MDC.put("traceId", MdcLogUtil.generateTraceId());
        filterChain.doFilter(servletRequest, servletResponse);
    }
}

package com.uziot.bucket.xstream.convert;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XStreamAlias(value = "cat")
public class Cat {
    @XStreamAlias(value = "name" )
    private String name;
    @XStreamAlias(value = "age" )
    private String age;
}
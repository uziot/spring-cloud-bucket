package com.uziot.bucket.xstream;

import com.uziot.bucket.xstream.convert.Cat;
import com.uziot.bucket.xstream.convert.User;
import com.uziot.bucket.xstream.util.XmlUtil;

import java.util.ArrayList;

/**
 * @author shidt
 * @version V1.0
 * @className Test
 * @date 2020-12-07 12:30:57
 * @description
 */

public class Test {
    public static void main(String[] args) {
        User user = new User("zhangsan", "123456");
        ArrayList<Cat> cats = new ArrayList<>();
        Cat huahua = new Cat("huahua", "1");
        Cat taotao = new Cat("taotao", "1");
        cats.add(huahua);
        cats.add(taotao);

        user.setCats(cats);

        String toXml = XmlUtil.toXml(user);
        System.out.println(toXml);
        User toBean = XmlUtil.toBean(toXml, User.class);
        System.out.println(toBean);

        System.out.println(XmlUtil.toCompressXml(toBean));

        System.out.println(XmlUtil.compressXml(toXml));
    }
}

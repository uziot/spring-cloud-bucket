package com.uziot.bucket.xstream.convert;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-12-07 12:21:51
 * @description
 */
@Data
@XStreamAlias(value = "user")
public class User {
    @XStreamAlias(value = "name")
    private String username;
    private String password;

    @XStreamImplicit(itemFieldName = "cats")
    private List<Cat> cats;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

}

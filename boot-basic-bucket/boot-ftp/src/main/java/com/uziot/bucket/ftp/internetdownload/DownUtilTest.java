package com.uziot.bucket.ftp.internetdownload;


/**
 * @author Lenovo
 */
public class DownUtilTest {

    public static void main(String[] args) throws Exception {
        final DownUtil downUtil =
                new DownUtil(
                        "https://mirrors.tuna.tsinghua.edu.cn/apache//ant/binaries/apache-ant-1.9.15-bin.zip",
                        "D:\\apache-ant-1.9.15-bin.zip", 3);

        downUtil.download();


        new Thread(() -> {
            try {
                int completeRate = downUtil.getCompleteRate();
                while (completeRate < 100) {
                    if (completeRate == downUtil.getCompleteRate()) {
                        continue;
                    }
                    System.out.println("已完成:" + downUtil.getCompleteRate() + "%");
                    completeRate = downUtil.getCompleteRate();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

}

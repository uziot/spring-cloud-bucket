package com.uziot.bucket.ftp.arithmetic;

/**
 * @author Lenovo
 */
public class Demo {
    public static void main(String[] args) {
        int[] arr = {1, 2, 21, 545, 84, 65, 469, 585, 56, 46, 56, 489, 25, 468, 456, 487, 986, 25};
        int temp = 0; // 开辟一个临时空间, 存放交换的中间值
        // 要遍历的次数
        for (int i = 0; i < arr.length - 1; i++) {
            System.out.format("第 %d 遍：\n", i + 1);
            //依次的比较相邻两个数的大小，遍历一次后，把数组中第i小的数放在第i个位置上
            for (int j = 0; j < arr.length - 1 - i; j++) {
                // 比较相邻的元素，如果前面的数小于后面的数，就交换
                if (arr[j] < arr[j + 1]) {
                    temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
                System.out.format("第 %d 遍的第%d 次交换：", i + 1, j + 1);
                for (int count : arr) {
                    System.out.print(count);
                }
                System.out.println("");
            }
            System.out.format("第 %d 遍最终结果：", i + 1);
            for (int count : arr) {
                System.out.print(count);
            }
            System.out.println("\n#########################");
        }
    }
}


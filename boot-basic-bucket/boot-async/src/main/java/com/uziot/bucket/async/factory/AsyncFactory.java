package com.uziot.bucket.async.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author shidt
 */
public class AsyncFactory {
    private static final Logger log = LoggerFactory.getLogger("sys-user");

    /**
     * 记录登陆信息
     *
     * @param username 用户名
     * @param status   状态
     * @param message  消息
     * @param args     列表
     * @return 任务task
     */
    public static TimerTask recordLoginInfo(final String username, final String status, final String message,
                                            final Object... args) {
        return new TimerTask() {
            @Override
            public void run() {
                // 打印信息到日志
                log.info(status + status + message, args);
                // 获取客户端操作系统
                //SpringUtils.getBean();
                log.info("这里调用异步线程插入");
            }
        };
    }

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordLog(final String operLog) {
        return new TimerTask() {
            @Override
            public void run() {
                log.info("执行定时操作插入数据：" + operLog);
            }
        };
    }
}

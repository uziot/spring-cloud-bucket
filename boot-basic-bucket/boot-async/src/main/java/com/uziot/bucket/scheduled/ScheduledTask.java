package com.uziot.bucket.scheduled;

import com.uziot.bucket.task.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * @author shidt
 * @version V1.0
 * @className ScheduledTask
 * @date 2020-12-19 16:25:50
 * @description
 */
@Slf4j
@EnableScheduling
@Component
public class ScheduledTask {
    @Autowired
    private Task task;

    @Scheduled(fixedRate = 10000, initialDelay = 10000)
    public void test() throws Exception {
        log.info("开始执行定时任务.......");
        Future<String> str1 = task.taskOne();
        Future<String> str2 = task.taskTwo();
        while (true) {
            // 如果任务都做完就执行如下逻辑
            if (str1.isDone() && str2.isDone()) {
                log.info("任务一和任务二已经全部执行完成，执行回调方法....");
                log.info(str1.get() + ":" + str2.get());
                break;
            }
        }
    }
}

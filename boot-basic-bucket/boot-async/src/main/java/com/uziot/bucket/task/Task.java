package com.uziot.bucket.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * @author shidt
 * @version V1.0
 * @className Task
 * @date 2020-12-19 16:22:03
 * @description
 */
@Slf4j
@Component
public class Task {

    @Async(value = "threadPoolTaskExecutor")
    public Future<String> taskOne() {
        log.info("任务一正在执行.......");
        return new AsyncResult<>("任务一执行完成");

    }

    @Async(value = "threadPoolTaskExecutor2")
    public Future<String> taskTwo() {
        log.info("任务二正在执行.......");
        return new AsyncResult<>("任务二执行完成");
    }
}
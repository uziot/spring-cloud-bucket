package com.uziot.bucket.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("threadLocalScope")
@Service
public class CService {
    public void add() {
    }
}
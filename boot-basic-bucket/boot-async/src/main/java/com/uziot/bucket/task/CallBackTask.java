package com.uziot.bucket.task;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shidt
 * @version V1.0
 * @className CallBackTask
 * @date 2020-12-19 16:59:58
 * @description
 */
@Slf4j
public class CallBackTask implements Runnable {
    @Override
    public void run() {
        log.info("执行异步回调线程任务-开始...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行异步回调线程任务-完成...");
    }
}

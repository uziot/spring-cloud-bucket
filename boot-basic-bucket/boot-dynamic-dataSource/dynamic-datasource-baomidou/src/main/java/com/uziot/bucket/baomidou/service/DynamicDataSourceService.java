package com.uziot.bucket.baomidou.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.uziot.bucket.baomidou.dao.domain.BucketArea;
import com.uziot.bucket.baomidou.dao.domain.SysConfig;
import com.uziot.bucket.baomidou.dao.mapper.BucketAreaMapper;
import com.uziot.bucket.baomidou.dao.mapper.SysConfigMapper;
import com.uziot.bucket.baomidou.web.DynamicDataSourceController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className BmDynamicApplication
 * @date 2020-12-22 08:38:27
 * @description </p>
 * </p>
 * -@DS 可以注解在方法上或类上，同时存在就近原则 方法上注解 优先于 类上注解。
 * https://github.com/baomidou/dynamic-datasource-spring-boot-starter
 * <p>
 * 本框架只做 切换数据源 这件核心的事情，并不限制你的具体操作，切换了数据源可以做任何CRUD。
 * 配置文件所有以下划线 _ 分割的数据源 首部 即为组的名称，相同组名称的数据源会放在一个组下。
 * 切换数据源可以是组名，也可以是具体数据源名称。组名则切换时采用负载均衡算法切换。
 * 默认的数据源名称为 master ，你可以通过 spring.datasource.dynamic.primary 修改。
 * 方法上的注解优先于类上注解。
 * 强烈建议只在service的类和方法上添加注解，不建议在mapper上添加注解。
 */
@Service
public class DynamicDataSourceService {
    private static final Logger log = LoggerFactory.getLogger(DynamicDataSourceController.class);
    @Autowired
    public BucketAreaMapper bucketAreaMapper;
    @Autowired
    public SysConfigMapper sysConfigMapper;

    @DS("master")
    public Map<String, Object> masterDataSource() {
        BucketArea bucketArea = bucketAreaMapper.selectByPrimaryKey(1);
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("dataSource", "MASTER");
        map.put("bucketArea", bucketArea);
        log.info("使用主数据源查询：{}", bucketArea);
        return map;
    }

    @DS("slave_1")
    public Map<String, Object> slaveDataSource() {
        SysConfig sysConfig = sysConfigMapper.selectByPrimaryKey("statement_truncate_len");
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("dataSource", "SLAVE");
        map.put("sysConfig", sysConfig);
        log.info("使用从数据源查询：{}", sysConfig);
        return map;
    }
}

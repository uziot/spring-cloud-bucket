package com.uziot.bucket;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author shidt
 * @version V1.0
 * @className BmDynamicApplication
 * @date 2020-12-22 08:38:27
 * @description </p>
 * </p>
 * https://github.com/baomidou/dynamic-datasource-spring-boot-starter
 * <p>
 * 集成druid数据源需要排除druid配置
 */
@EnableAspectJAutoProxy(proxyTargetClass = true)
@MapperScan("com.uziot.bucket.baomidou.dao.mapper")
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
public class BmDynamicApplication {
    public static void main(String[] args) {
        SpringApplication.run(BmDynamicApplication.class, args);
    }
}

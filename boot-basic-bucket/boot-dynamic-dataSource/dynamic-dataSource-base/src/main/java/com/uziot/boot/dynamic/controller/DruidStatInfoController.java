package com.uziot.boot.dynamic.controller;

import com.alibaba.druid.stat.DruidStatManagerFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className DruidStatInfoController
 * @date 2022-09-02 23:13:18
 * @description 测试了使用Druid数据源获取监控数据并返回前端界面的接口使用方法案例
 */

@RestController
public class DruidStatInfoController {
    @GetMapping(value = "/getDruidInfo")
    public HashMap<String, Object> getDruidInfo() {
        DruidStatManagerFacade druidStatManagerFacade = DruidStatManagerFacade.getInstance();
        List<Map<String, Object>> sourceStatDataList = druidStatManagerFacade.getDataSourceStatDataList();
        List<List<String>> activeConnStackTraceList = druidStatManagerFacade.getActiveConnStackTraceList();

        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("sourceStatDataList", sourceStatDataList);
        objectHashMap.put("activeConnStackTraceList", activeConnStackTraceList);
        return objectHashMap;
    }
}

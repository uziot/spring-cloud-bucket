package com.uziot.boot.dynamic.datasource;

/**
 * 数据源
 *
 * @author shidt
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}

package com.uziot.boot.dynamic.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicDataSourceController
 * @date 2020-11-22 00:45:49
 * @description
 */
@RestController
@RequestMapping(value = "/dynamic")
public class DynamicDataSourceController {
    private static final Logger log = LoggerFactory.getLogger(DynamicDataSourceController.class);

    @Autowired
    private DynamicDataSourceService dynamicDataSourceService;

    @RequestMapping(value = "/master")

    public Map<String, Object> masterDataSource() {
        return dynamicDataSourceService.masterDataSource();
    }

    @RequestMapping(value = "/slave")

    public Map<String, Object> slaveDataSource() {
        return dynamicDataSourceService.slaveDataSource();
    }
}

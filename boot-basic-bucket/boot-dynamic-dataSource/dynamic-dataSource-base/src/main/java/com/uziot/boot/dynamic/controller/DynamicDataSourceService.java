package com.uziot.boot.dynamic.controller;

import com.uziot.boot.dynamic.aspect.DataSource;
import com.uziot.boot.dynamic.dao.domain.BucketArea;
import com.uziot.boot.dynamic.dao.domain.SysConfig;
import com.uziot.boot.dynamic.dao.mapper.BucketAreaMapper;
import com.uziot.boot.dynamic.dao.mapper.SysConfigMapper;
import com.uziot.boot.dynamic.datasource.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className DynamicDataSourceService
 * @date 2020-11-22 01:15:47
 * @description
 */
@Service
public class DynamicDataSourceService {

    @Autowired
    public BucketAreaMapper bucketAreaMapper;

    @Autowired
    public SysConfigMapper sysConfigMapper;


    @DataSource(value = DataSourceType.MASTER)
    public Map<String, Object> masterDataSource() {

        BucketArea bucketArea = bucketAreaMapper.selectByPrimaryKey(1);
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("dataSource", "MASTER");
        map.put("bucketArea", bucketArea);

        return map;
    }

    @DataSource(value = DataSourceType.SLAVE)
    public Map<String, Object> slaveDataSource() {
        SysConfig sysConfig = sysConfigMapper.selectByPrimaryKey("statement_truncate_len");
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("dataSource", "MASTER");
        map.put("sysConfig", sysConfig);
        return map;
    }
}

package com.uziot.boot.dynamic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动程序
 *
 * @author shidt
 * 功能介绍
 * MapperScan("com.uziot.uadmin.dao.*.mapper")
 * 指定要扫描的Mapper类的包的路径
 * 开启切面自动代理EnableAspectJAutoProxy(proxyTargetClass = true)
 */
@EnableAspectJAutoProxy(proxyTargetClass = true)
@MapperScan("com.uziot.boot.dynamic.dao.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class DynamicApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicApplication.class, args);
    }
}

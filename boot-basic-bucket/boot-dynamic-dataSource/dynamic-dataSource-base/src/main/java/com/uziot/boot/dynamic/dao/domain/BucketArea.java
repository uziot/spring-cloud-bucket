package com.uziot.boot.dynamic.dao.domain;

public class BucketArea {
    /**
    * id
    */
    private Integer id;

    /**
    * 地区编码
    */
    private String caAreaCode;

    /**
    * 地区名称
    */
    private String caAreaName;

    /**
    * 父级编码
    */
    private String caAreaParentCode;

    /**
    * 地区类型
    */
    private String caAreaType;

    /**
    * 拼音首字母
    */
    private String caAreaSpell;

    /**
    * 邮政编码
    */
    private String caZipCode;

    /**
    * 是否展示
    */
    private Integer isShow;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaAreaCode() {
        return caAreaCode;
    }

    public void setCaAreaCode(String caAreaCode) {
        this.caAreaCode = caAreaCode;
    }

    public String getCaAreaName() {
        return caAreaName;
    }

    public void setCaAreaName(String caAreaName) {
        this.caAreaName = caAreaName;
    }

    public String getCaAreaParentCode() {
        return caAreaParentCode;
    }

    public void setCaAreaParentCode(String caAreaParentCode) {
        this.caAreaParentCode = caAreaParentCode;
    }

    public String getCaAreaType() {
        return caAreaType;
    }

    public void setCaAreaType(String caAreaType) {
        this.caAreaType = caAreaType;
    }

    public String getCaAreaSpell() {
        return caAreaSpell;
    }

    public void setCaAreaSpell(String caAreaSpell) {
        this.caAreaSpell = caAreaSpell;
    }

    public String getCaZipCode() {
        return caZipCode;
    }

    public void setCaZipCode(String caZipCode) {
        this.caZipCode = caZipCode;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }
}
package com.uziot.boot.config;

import com.uziot.boot.properties.MonitorProperty;
import com.uziot.boot.properties.FlowProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * FlowConfig的装配类
 * 这个装配类主要是把监控器的配置参数类和流程配置参数类作一个合并，转换成统一的配置参数类。
 * 同时这里设置了默认的参数路径，如果在springboot的application.properties/yml里没取到的话，就取默认值
 *
 * @author shidt
 */
@Configuration
@EnableConfigurationProperties({FlowProperty.class, MonitorProperty.class})
@PropertySource(
        name = "Default Properties",
        value = "classpath:/META-INF/flow-default.properties")
public class FlowPropertyAutoConfiguration {

    @Bean
    public FlowConfig flowConfig(FlowProperty property, MonitorProperty monitorProperty) {
        FlowConfig flowConfig = new FlowConfig();
        flowConfig.setRuleSource(property.getRuleSource());
        flowConfig.setSlotSize(property.getSlotSize());
        flowConfig.setThreadExecutorClass(property.getThreadExecutorClass());
        flowConfig.setWhenMaxWaitSeconds(property.getWhenMaxWaitSeconds());
        flowConfig.setEnableLog(monitorProperty.isEnableLog());
        flowConfig.setQueueLimit(monitorProperty.getQueueLimit());
        flowConfig.setDelay(monitorProperty.getDelay());
        flowConfig.setPeriod(monitorProperty.getPeriod());
        flowConfig.setWhenMaxWorkers(property.getWhenMaxWorkers());
        flowConfig.setWhenQueueLimit(property.getWhenQueueLimit());
        flowConfig.setParseOnStart(property.isParseOnStart());
        flowConfig.setEnable(property.isEnable());
        flowConfig.setSupportMultipleType(property.isSupportMultipleType());
        flowConfig.setRetryCount(property.getRetryCount());
        flowConfig.setZkNode(property.getZkNode());
        flowConfig.setPrintBanner(property.isPrintBanner());
        flowConfig.setNodeExecutorClass(property.getNodeExecutorClass());
        return flowConfig;
    }
}

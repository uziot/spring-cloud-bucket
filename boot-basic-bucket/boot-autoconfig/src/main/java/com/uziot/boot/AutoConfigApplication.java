package com.uziot.boot;

import com.uziot.boot.properties.MonitorProperty;
import com.uziot.boot.properties.FlowProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className AutoConfigApplication
 * @date 2022-03-13 22:59:41
 * @description
 */

@RestController
@SpringBootApplication
public class AutoConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(AutoConfigApplication.class, args);
    }

    @Autowired
    private FlowProperty flowProperty;
    @Autowired
    private MonitorProperty monitorProperty;

    @GetMapping(value = "get1")
    public Object getResources1() {
        return flowProperty;
    }

    @GetMapping(value = "get2")
    public Object getResources2() {
        return monitorProperty;
    }
}

package com.uziot.bucket.batch.listener;

import com.uziot.bucket.batch.model.RecordsWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Lenovo
 */
@Component
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("!!! JOB FINISHED! Time to verify the results");

            List<RecordsWriter> results = jdbcTemplate.query("SELECT id, full_name, random_num FROM writer", (rs, row) -> {
                RecordsWriter recordsWriter = new RecordsWriter();
                recordsWriter.setId(rs.getLong("id"));
                recordsWriter.setFullName(rs.getString("full_name"));
                recordsWriter.setRandomNum(rs.getString("random_num"));
                return recordsWriter;
            });

            for (RecordsWriter recordsWriter : results) {
                log.info("Found <" + recordsWriter + "> in the database.");
            }
        }
    }
}
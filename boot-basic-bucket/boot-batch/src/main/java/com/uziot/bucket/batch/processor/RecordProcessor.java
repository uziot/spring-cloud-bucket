package com.uziot.bucket.batch.processor;

import com.uziot.bucket.batch.model.RecordsReader;
import com.uziot.bucket.batch.model.RecordsWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * @author Lenovo
 */
public class RecordProcessor implements ItemProcessor<RecordsReader, RecordsWriter> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordProcessor.class);

    @Override
    public RecordsWriter process(RecordsReader item) throws Exception {
        LOGGER.info("Processing Record: {}", item);
        RecordsWriter recordsWriter = new RecordsWriter();
        recordsWriter.setId(item.getId());
        recordsWriter.setFullName(item.getFirstName() + " " + item.getLastName());
        String substring = String.valueOf(Math.random()).substring(3, 8);
        if (substring.contains("0")) {
            // 返回空，即词条不插入
            return null;
        }
        recordsWriter.setRandomNum(substring);
        LOGGER.info("Processed Writer: {}", recordsWriter);
        return recordsWriter;
    }
}
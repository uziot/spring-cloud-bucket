package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className JnaApplication
 * @date 2021-03-28 20:34:13
 * @description
 */

@SpringBootApplication
public class JnaApplication {
    public static void main(String[] args) {
        SpringApplication.run(JnaApplication.class, args);
    }
}

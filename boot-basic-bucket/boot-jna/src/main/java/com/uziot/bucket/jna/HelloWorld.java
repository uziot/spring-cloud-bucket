package com.uziot.bucket.jna;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

public class HelloWorld {

    public interface Dll extends StdCallLibrary {
        Dll INSTANCE = Native.loadLibrary("SiInterface", Dll.class);//加载动态库文件
        int INIT();//动态库中调用的方法
    }

    public static void main(String[] args) {
        System.out.println(Dll.INSTANCE.INIT());

    }

}
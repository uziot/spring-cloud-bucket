package com.uziot.bucket.email.service;

/**
 * @author shidt
 * @version V1.0
 * @className GuavaApplication
 * @date 2020-11-30 23:10:58
 * @description
 */
public interface MailService {

    void sendSimpleMail(String to, String subject, String content);

    void sendHtmlMail(String to, String subject, String content);

    void sendAttachmentsMail(String to, String subject, String content, String filePath);

    void sendInlineResourceMail(String to, String subject, String content, String rscPath, String rscId);

}

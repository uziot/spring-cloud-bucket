package com.uziot.bucket.notify;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className NotifyController
 * @date 2022-02-12 21:05:07
 * @description
 */

@Slf4j
@RestController
public class NotifyController {
    private static final Object OBJ = new Object();

    @GetMapping(value = "/wait1")
    public void wait1() {
        try {
            log.info("线程进入等待wait1：{}", Thread.currentThread().getName());
            synchronized (OBJ) {
                OBJ.wait();
            }
            log.info("线程进入执行wait1：{}", Thread.currentThread().getName());
        } catch (Exception e) {
            log.error("线程等待失败", e);
        }
    }

    @GetMapping(value = "/wait2")
    public void wait2() {
        try {
            log.info("线程进入等待wait2：{}", Thread.currentThread().getName());
            synchronized (OBJ) {
                OBJ.wait();
            }
            log.info("线程进入执行wait2：{}", Thread.currentThread().getName());
        } catch (Exception e) {
            log.error("线程等待失败", e);
        }
    }

    @GetMapping(value = "/wait3")
    public void wait3() {
        try {
            log.info("线程进入等待wait3：{}", Thread.currentThread().getName());
            synchronized (OBJ) {
                OBJ.wait();
            }
            log.info("线程进入执行wait3：{}", Thread.currentThread().getName());
        } catch (Exception e) {
            log.error("线程等待失败", e);
        }
    }

    @GetMapping(value = "/notify")
    public void notify1() {
        try {
            log.info("notify...");
            synchronized (OBJ) {
                OBJ.notify();
            }
        } catch (Exception e) {
            log.error("线程等待失败", e);
        }
    }

    @GetMapping(value = "/notifyAll")
    public void notifyAll1() {
        try {
            log.info("notifyAll...");
            synchronized (OBJ) {
                OBJ.notifyAll();
            }
        } catch (Exception e) {
            log.error("线程等待失败", e);
        }
    }


}

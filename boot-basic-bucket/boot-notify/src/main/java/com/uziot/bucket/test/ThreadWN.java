package com.uziot.bucket.test;

public class ThreadWN implements Runnable {
    public String name;

    public String getName() {
        return name;
    }

    static int a = 9;

    public ThreadWN(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (a > 0) {
            synchronized (ThreadWN.class) {
                a--;
                System.out.println("当前执行" + this.name + ",并进入等待中.......");
                try {
                    ThreadWN.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TestTH tth = new TestTH("唤醒");
        Thread t = new Thread(tth);
        ThreadWN wna = new ThreadWN("A");
        ThreadWN wnb = new ThreadWN("B");
        ThreadWN wnc = new ThreadWN("C");
        ThreadWN wnd = new ThreadWN("D");
        ThreadWN wne = new ThreadWN("E");
        ThreadWN wnf = new ThreadWN("F");
        Thread tha = new Thread(wna);
        Thread thb = new Thread(wnb);
        Thread thc = new Thread(wnc);
        Thread thd = new Thread(wnd);
        Thread the = new Thread(wne);
        Thread thf = new Thread(wnf);

        tha.start();
        Thread.sleep(10);
        thb.start();
        Thread.sleep(10);
        thc.start();
        Thread.sleep(10);
        thd.start();
        Thread.sleep(10);
        the.start();
        Thread.sleep(10);
        thf.start();
        Thread.sleep(10);
        t.start();

    }

}

class TestTH implements Runnable {

    private String name;

    public TestTH(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 6; i++) {
            synchronized (ThreadWN.class) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO 自动生成的 catch 块
                    e.printStackTrace();
                }
                System.out.println(this.getName() + "进程：" + "唤醒了一个线程！！！");
                ThreadWN.class.notify();
            }
        }
    }

}
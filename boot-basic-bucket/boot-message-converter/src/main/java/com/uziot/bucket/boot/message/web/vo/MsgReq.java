package com.uziot.bucket.boot.message.web.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className MsgReq
 * @date 2021-07-30 10:20:13
 * @description
 */

@Data
public class MsgReq {
    private String name;
    private Integer channel;
    private HashMap<String, String> maps;
    private List<String> lists;

}

package com.uziot.bucket.boot.message.convert;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author shidt
 * @version V1.0
 * @className FastJsonConverterConfig
 * @date 2021-07-29 19:42:44
 * @description
 */

@Configuration
@ConfigurationProperties(prefix = "message.converter")
public class ConverterConfig {

    private ConverterType type;

    public ConverterType getType() {
        return type;
    }

    public void setType(ConverterType type) {
        this.type = type;
    }
}

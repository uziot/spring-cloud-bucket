package com.uziot.bucket.boot.message.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 功能描述: <br>
 * <>
 * controller出现异常时响应JSON串
 *
 * @author shidt
 * @date 2020-03-09 0:45
 */
@ControllerAdvice
public class CommonExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(CommonExceptionHandler.class);

    /**
     * catch所有controller异常，响应json
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public void handleException(Exception e) {
        log.error("controller advice,", e);
    }


}

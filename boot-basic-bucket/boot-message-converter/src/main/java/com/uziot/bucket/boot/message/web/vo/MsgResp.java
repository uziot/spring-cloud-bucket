package com.uziot.bucket.boot.message.web.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className MsgResp
 * @date 2021-07-30 10:20:06
 * @description
 */

@Data
public class MsgResp {
    private String name;
    private Integer channel;
    private HashMap<String, String> maps;
    private List<String> lists;

    private String nameNull;
    private Integer channelNull;
    private HashMap<String, String> mapsNull;
    private List<String> listsNull;

}

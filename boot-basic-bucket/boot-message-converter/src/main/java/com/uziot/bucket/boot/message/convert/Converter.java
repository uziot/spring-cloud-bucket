package com.uziot.bucket.boot.message.convert;

/**
 * @author shidt
 * @version V1.0
 * @className Convert
 * @date 2021-07-29 19:53:20
 * @description
 */

public class Converter {
    public static final String FAST_JSON = "fastJson";
    public static final String JACKSON = "jackson";
}

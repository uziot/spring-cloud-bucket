package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MsgConvertApplication
 * @date 2021-07-30 10:09:35
 * @description
 */

@SpringBootApplication
public class MsgConvertApplication {
    public static void main(String[] args) {
        SpringApplication.run(MsgConvertApplication.class, args);
    }
}

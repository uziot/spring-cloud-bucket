package com.uziot.bucket.boot.message.web;

import com.uziot.bucket.boot.message.web.vo.MsgReq;
import com.uziot.bucket.boot.message.web.vo.MsgResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author shidt
 * @version V1.0
 * @className MsgController
 * @date 2021-07-30 10:16:03
 * @description
 */

@Slf4j
@Controller
public class MsgController {

    @PostMapping(value = "/msg")
    public @ResponseBody
    MsgResp testMsg(@RequestBody MsgReq msgReq) {
        log.info("上送信息为：{}", msgReq);

        MsgResp msgResp = new MsgResp();
        msgReq.setChannel(6124);
        msgResp.setName("张三");

        HashMap<String, String> map = new HashMap<>();
        map.put("email", "123456@126.com");
        map.put("text", "信息化");
        msgResp.setMaps(map);

        ArrayList<String> list = new ArrayList<>();
        list.add("王麻子");
        msgResp.setLists(list);

        return msgResp;
    }

    @PostMapping(value = "/msgStr")
    public @ResponseBody
    MsgResp testMsg(@RequestBody String msgReq) {

        log.info("上送信息为：{}", msgReq);

        MsgResp msgResp = new MsgResp();
        msgResp.setChannel(6124);
        msgResp.setName("张三");

        HashMap<String, String> map = new HashMap<>();
        map.put("email", "123456@126.com");
        map.put("text", "信息化");
        msgResp.setMaps(map);

        ArrayList<String> list = new ArrayList<>();
        list.add("王麻子");
        msgResp.setLists(list);

        return msgResp;
    }
}

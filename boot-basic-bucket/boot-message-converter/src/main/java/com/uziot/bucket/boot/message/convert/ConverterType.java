package com.uziot.bucket.boot.message.convert;

/**
 * @author shidt
 * @version V1.0
 * @className ConverterType
 * @date 2021-07-29 19:45:06
 * @description
 */

public enum  ConverterType {
    jackson,fastJson
}

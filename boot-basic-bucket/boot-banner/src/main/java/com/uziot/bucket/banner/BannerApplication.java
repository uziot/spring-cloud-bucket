package com.uziot.bucket.banner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className BannerApplication
 * @date 2020-12-01 22:07:26
 * @description
 */
@SpringBootApplication
public class BannerApplication {
    public static void main(String[] args) {
        SpringApplication.run(BannerApplication.class,args);
    }
}

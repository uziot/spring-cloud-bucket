package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author shidt
 * @version V1.0
 * @className ScanLoginApplication
 * @date 2020-12-27 00:06:14
 * @description
 */
@EnableAsync
@SpringBootApplication
public class ScanLoginApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScanLoginApplication.class, args);
    }
}

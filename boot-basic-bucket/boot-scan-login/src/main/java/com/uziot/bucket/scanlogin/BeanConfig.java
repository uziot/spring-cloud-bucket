package com.uziot.bucket.scanlogin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * @author shidt
 * @version V1.0
 * @className BeanConfig
 * @date 2020-11-21 23:24:52
 * @description
 */
@Configuration
public class BeanConfig {

    @Bean
    public StringRedisTemplate template(RedisConnectionFactory connectionFactory) {
        return new StringRedisTemplate(connectionFactory);
    }

    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter listenerAdapter) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);

        // 订阅登录消息
        container.addMessageListener(listenerAdapter, new PatternTopic(Receiver.TOPIC_NAME));
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(Receiver receiver) {
        // 方法名
        String methodName = "receiveLogin";
        return new MessageListenerAdapter(receiver, methodName);
    }

    @Bean
    public Receiver receiver() {
        return new Receiver();
    }


    @Configuration
    public static class TaskConfiguration {
        @Bean("taskExecutor")
        public ThreadPoolTaskExecutor taskExecutor() {
            ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
            taskExecutor.setCorePoolSize(5);
            taskExecutor.setMaxPoolSize(10);
            taskExecutor.setQueueCapacity(10);
            taskExecutor.setThreadNamePrefix("asyncTask");
            return taskExecutor;
        }
    }
}

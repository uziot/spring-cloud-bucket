package com.uziot.bucket.cros.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 功能描述: <br>
 * 通用跨域请求释放配置方法
 *
 * @author shidt
 * @date 2020-01-25 0:50
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    /**
     * 此类方式设置的跨域在springboot 2.4版本之前成功，2.4以及之后不行
     * @param registry 注册
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("*")
                .maxAge(3600);
    }

    /**
     * 跨域配置,新版本使用此种方式设置，亲测成功
     */
   // @Bean
    public CorsFilter corsFilter() {
        return new CorsFilter();
    }

}
package com.uziot.bucket.cros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className CrosApplication
 * @date 2020-12-01 23:36:42
 * @description
 */
@SpringBootApplication
public class CrosApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrosApplication.class, args);
    }
}

package com.uziot.bucket.rabbitmq.rabbit.object;

import com.uziot.bucket.rabbitmq.model.User;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * @author shidt
 * @version V1.0
 * @className ObjectSender
 * @date 2020-12-15 23:01:47
 * @description
 */
@Component
public class ObjectSender {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	public void send(User user) {
		System.out.println("Sender object: " + user.toString());
		this.rabbitTemplate.convertAndSend("object", user);
	}

}
package com.uziot.bucket.rabbitmq.rabbit.topic;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-12-15 23:01:47
 * @description 交换器 Topic(主题，规则匹配) 接受者
 */

@Component
public class TopicReceiver2 {

    @RabbitListener(queues = "topic.messages")
    public void process(String message) {
        System.out.println("Topic Receiver2  : " + message);
    }

}

package com.uziot.bucket.rabbitmq.rabbit.fanout;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className FanoutReceiverC
 * @date 2020-12-15 23:01:47
 * @description RabbitMQ—fanout（广播模式）接收者C
 */

@Component
public class FanoutReceiverC {

    @RabbitListener(queues = "fanout.C")
    public void process(String message) {
        System.out.println("fanout Receiver C: " + message);
    }

}

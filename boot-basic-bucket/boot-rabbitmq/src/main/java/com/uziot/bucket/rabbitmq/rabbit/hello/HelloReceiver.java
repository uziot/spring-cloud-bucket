package com.uziot.bucket.rabbitmq.rabbit.hello;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className HelloReceiver
 * @date 2020-12-15 23:01:47
 * @description
 */

@Component
public class HelloReceiver {

    @RabbitListener(queues = "hello")
    public void process(String hello) {
        System.out.println("Receiver  : " + hello);
    }

}

package com.uziot.bucket.rabbitmq.model;

import java.io.Serializable;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-12-15 23:01:47
 * @description
 */
public class User implements Serializable {

    private String name;

    private String pass;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}

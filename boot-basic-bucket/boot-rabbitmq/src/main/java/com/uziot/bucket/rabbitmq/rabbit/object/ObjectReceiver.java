package com.uziot.bucket.rabbitmq.rabbit.object;

import com.uziot.bucket.rabbitmq.model.User;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-12-15 23:01:47
 * @description
 */
@Component
public class ObjectReceiver {

    @RabbitListener(queues = "object")
    public void process(User user) {
        System.out.println("Receiver object : " + user);
    }

}

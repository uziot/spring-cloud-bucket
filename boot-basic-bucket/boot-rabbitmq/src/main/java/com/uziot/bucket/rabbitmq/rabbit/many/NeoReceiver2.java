package com.uziot.bucket.rabbitmq.rabbit.many;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-12-15 23:01:47
 * @description
 */

@Component
public class NeoReceiver2 {

    @RabbitListener(queues = "neo")
    public void process(String neo) {
        System.out.println("Receiver 2: " + neo);
    }

}

搜索rabbitmq镜像
docker search rabbitmq:management


下载镜像
docker pull rabbitmq:management

启动容器
docker run -d --hostname localhost --name rabbitmq -p 15672:15672 -p 5672:5672 rabbitmq:management


打印容器
docker logs rabbitmq


访问RabbitMQ Management
http://localhost:15672
账户密码默认:guest


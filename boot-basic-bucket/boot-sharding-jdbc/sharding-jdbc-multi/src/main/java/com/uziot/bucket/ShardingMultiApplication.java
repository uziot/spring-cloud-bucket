package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MybatisOracleAppcication
 * @date 2020-12-17 23:22:45
 * @description
 */
@SpringBootApplication
public class ShardingMultiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShardingMultiApplication.class, args);
    }
}

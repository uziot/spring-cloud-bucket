package com.uziot.bucket.sharding.dao.mapper;

import com.uziot.bucket.sharding.dao.domain.Address;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddressMapper {
    /**
     * 保存
     */
    void save(Address address);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    Address get(Long id);
}
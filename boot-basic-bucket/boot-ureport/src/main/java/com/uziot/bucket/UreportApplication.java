package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className UreportApplication
 * @date 2021-02-22 16:49:41
 * @description 访问地址：http://127.0.0.1:8081/ureport/designer
 */
@SpringBootApplication
public class UreportApplication {
    public static void main(String[] args) {
        SpringApplication.run(UreportApplication.class, args);
    }
}

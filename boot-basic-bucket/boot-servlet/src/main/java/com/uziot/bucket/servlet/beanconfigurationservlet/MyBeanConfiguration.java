package com.uziot.bucket.servlet.beanconfigurationservlet;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import javax.servlet.Filter;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;
import java.util.Collections;

/**
 * @author shidt
 * @version V1.0
 * @className MyBeanConfiguration
 * @date 2021-10-09 10:56:32
 * @description
 */

@SpringBootConfiguration
public class MyBeanConfiguration {

    // 注册servlet
    @Bean
    public ServletRegistrationBean<HttpServlet> createMyBeanConfigurationServlet() {
        return new ServletRegistrationBean<>(
            new MyBeanConfigurationServlet(), "/book.do");
    }

    // 注册filter
    @Bean
    public FilterRegistrationBean<Filter> createMyBeanConfigurationFilter() {
        FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new MyBeanConfigurationFilter());
        filterRegistrationBean.setUrlPatterns(Collections.singletonList("/book.do"));
        return filterRegistrationBean;
    }

    // 注册监听器
    @Bean
    public ServletListenerRegistrationBean<ServletContextListener> createMyBeanConfigurationServletContextListener() {
        return new ServletListenerRegistrationBean<>(new MyBeanConfigurationServletContextListener());
    }
}

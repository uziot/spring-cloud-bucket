package com.uziot.bucket.servlet.beanconfigurationservlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shidt
 * @version V1.0
 * @className MyBeanConfigurationServlet
 * @date 2021-10-09 10:56:32
 * @description 直接在SpringBoot的方式在项目中创建Servlet接口服务的过程
 */

public class MyBeanConfigurationServlet extends HttpServlet {

    private static final long serialVersionUID = 2071038285677399214L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("this is springboot servlet by bean configuration");
    }
}

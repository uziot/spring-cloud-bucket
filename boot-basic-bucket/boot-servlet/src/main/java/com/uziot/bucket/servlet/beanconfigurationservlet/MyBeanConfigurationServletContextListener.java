package com.uziot.bucket.servlet.beanconfigurationservlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author shidt
 * @version V1.0
 * @className MyBeanConfigurationServletContextListener
 * @date 2021-10-09 10:56:32
 * @description
 */

public class MyBeanConfigurationServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("springboot servlet context listener by bean configuration started!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}

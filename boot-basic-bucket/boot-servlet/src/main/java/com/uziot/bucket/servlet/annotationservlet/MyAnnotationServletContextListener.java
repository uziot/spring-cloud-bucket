package com.uziot.bucket.servlet.annotationservlet;

import java.time.LocalDateTime;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author shidt
 * @version V1.0
 * @className MyAnnotationServletContextListener
 * @date 2021-10-09 10:56:32
 * @description
 */

@WebListener
public class MyAnnotationServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("app startup at " + LocalDateTime.now().toString());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}

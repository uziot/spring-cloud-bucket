package com.uziot.bucket.servlet.annotationservlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shidt
 * @version V1.0
 * @className MyAnnotationServlet
 * @date 2021-10-09 10:56:32
 * @description
 */

@WebServlet("/user/servlet")
public class MyAnnotationServlet extends HttpServlet {

    private static final long serialVersionUID = 5411662299184978237L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        resp.getWriter().write("user servlet");
    }
}

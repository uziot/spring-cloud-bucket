package com.uziot.bucket.minio;

import lombok.Data;

import java.io.Serializable;

/**
 * @author shidt
 * @version V1.0
 * @className MinioUploadDto
 * @date 2020-01-18 23:16:45
 * @description 主要实现了对象存储服务的操作
 * MinIO对象存储管理 ，需要在Linux中安装对象存储服务，并在代码中调用
 */

@Data
public class MinioUploadDto implements Serializable {
    private String name;
    private String url;
    private String shareUrl;
}

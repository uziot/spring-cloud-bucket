package com.uziot.bucket;

import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author shidt
 * @version V1.0
 * @className SmartStop04App
 * @date 2021-04-19 23:19:54
 * @description 第四种方法，通过调用一个SpringApplication.exit(）方法也可以退出程序，
 * 同时将生成一个退出码，这个退出码可以传递给所有的context。
 * 这个就是一个JVM的钩子，通过调用这个方法的话会把所有PreDestroy的方法执行并停止，
 * 并且传递给具体的退出码给所有Context。
 * 通过调用System.exit(exitCode)可以将这个错误码也传给JVM。
 * 程序执行完后最后会输出：Process finished with exit code 0，给JVM一个SIGNAL。
 */

@SpringBootApplication
public class SmartStop04App {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(SmartStop04App.class, args);
        SmartStop04App.exitApplication(ctx);
    }

    public static void exitApplication(ConfigurableApplicationContext context) {
        int exitCode = SpringApplication.exit(context, (ExitCodeGenerator) () -> 0);

        System.exit(exitCode);
    }
}

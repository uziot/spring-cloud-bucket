package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className SmartStop02
 * @date 2021-04-19 23:12:56
 * @description 第二种方法也比较简单，获取程序启动时候的context，
 * 然后关闭主程序启动时的context。这
 * 样程序在关闭的时候也会调用PreDestroy注解。
 * 如下方法在程序启动十秒后进行关闭。
 */

@SpringBootApplication
public class SmartStop02App {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(SmartStop02App.class, args);

        try {
            TimeUnit.SECONDS.sleep(10);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ctx.close();
    }
}

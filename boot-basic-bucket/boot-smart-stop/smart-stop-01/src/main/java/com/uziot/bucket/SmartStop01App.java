package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className SmartStop01App
 * @date 2021-04-19 23:07:29
 * @description 雅停止服务的几种方法
 */

@SpringBootApplication
public class SmartStop01App {
    public static void main(String[] args) {
        SpringApplication.run(SmartStop01App.class, args);
    }
}

package com.uziot.bucket.config;

import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;


/**
 * @author shidt
 * @version V1.0
 * @className TerminateBean
 * @date 2021-04-19 23:07:29
 * @description act地址调用的停机会调用PreDestroy注解标注的方法
 */

@Configuration
public class TerminateBean {

    @PreDestroy
    public void preDestroy() {
        System.out.println("TerminalBean is destroyed");
    }

}
package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;

/**
 * @author shidt
 * @version V1.0
 * @className SmartStop03App
 * @date 2021-04-19 23:16:41
 * @description 第三种方法，在springboot启动的时候将进程号写入一个app.pid文件，
 * 生成的路径是可以指定的，
 * 可以通过命令 cat /Users/uziot/app.id | xargs kill
 * 命令直接停止服务，这个时候bean对象的PreDestroy方法也会调用的。
 * 这种方法大家使用的比较普遍。
 * 写一个start.sh用于启动springboot程序，
 * 然后写一个停止程序将服务停止。
 */

@SpringBootApplication
public class SmartStop03App {
    public static void main(String[] args) {
        /*
         * 在指定路径中生成pid，同时使用命令关闭pid :
         *  'cat /Users/uziot/app.pid | xargs kill'
         */
        SpringApplication application = new SpringApplication(SmartStop03App.class);
        application.addListeners(new ApplicationPidFileWriter("/Users/uziot/app.pid"));
        application.run();
    }
}

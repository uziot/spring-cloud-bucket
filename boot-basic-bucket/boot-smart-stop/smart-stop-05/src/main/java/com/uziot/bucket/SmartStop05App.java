package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className SmartStop05App
 * @date 2021-04-19 23:23:10
 * @description 第五种方法，自己写一个Controller，然后将自己写好的Controller获取到程序的context，
 * 然后调用自己配置的Controller方法退出程序。
 * 通过调用自己写的/shutDownContext
 * 方法关闭程序：curl -X POST http://localhost:8080/shutDownContext。
 */

@SpringBootApplication
public class SmartStop05App {
    public static void main(String[] args) {
        SpringApplication.run(SmartStop05App.class, args);
    }
}

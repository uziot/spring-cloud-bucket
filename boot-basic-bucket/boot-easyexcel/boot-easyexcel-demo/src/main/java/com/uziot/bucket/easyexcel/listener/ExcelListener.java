package com.uziot.bucket.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述: <br>
 * 这个类是 ExcelListener 监听器初始化类，要操作使用EasyExcel必备的类
 * 操作Excel需要引入依赖，然后就可以操作了非常简单
 *
 * @author shidt
 * @date 2020-01-05 1:42
 */
public class ExcelListener extends AnalysisEventListener<Object> {

    private final List<Object> data = new ArrayList<>();

    @Override
    public void invoke(Object o, AnalysisContext analysisContext) {
        data.add(o);
    }

    public List<Object> getData() {
        return data;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}

package com.uziot.bucket.easyexcel.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-01-05 01:28:26
 * @description
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class UserVo extends BaseRowModel implements Serializable {
    private static final long serialVersionUID = 2478102622678693580L;
    @ExcelProperty(value = "用户名称", index = 0)
    String userName;
    @ExcelProperty(value = "手机号码", index = 1)
    String mobile;
}

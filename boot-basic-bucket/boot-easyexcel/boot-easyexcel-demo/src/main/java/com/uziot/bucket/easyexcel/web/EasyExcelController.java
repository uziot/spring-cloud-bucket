package com.uziot.bucket.easyexcel.web;

import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.uziot.bucket.easyexcel.dto.SimpleCompanyVo;
import com.uziot.bucket.easyexcel.dto.UserVo;
import com.uziot.bucket.easyexcel.listener.ExcelListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述: <br>
 * 使用EasyExcel
 *
 * @author shidt
 * @date 2020-01-05 1:04
 */
@Slf4j
@RestController
public class EasyExcelController {
    private static List<SimpleCompanyVo> company;

    /**
     * 导出数据到excel中
     *
     * @param response 响应
     * @throws IOException IOException
     */
    @GetMapping("/export")
    public void exportExcel(HttpServletResponse response) throws IOException {
        ExcelWriter writer;
        OutputStream outputStream = response.getOutputStream();
        try {
            response.setHeader("Content-disposition", "attachment; filename=" + "company.xlsx");
            response.setContentType("application/msexcel;charset=UTF-8");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);

            writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX, true);
            Sheet sheet = new Sheet(1, 0, SimpleCompanyVo.class);
            Sheet sheet2 = new Sheet(2, 0, SimpleCompanyVo.class);
            sheet.setSheetName("公司列表");
            sheet2.setSheetName("公司列表2");

            writer.write(company, sheet);
            writer.write(company, sheet2);
            writer.finish();

            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 接收excel文件，并解析
     *
     * @param file 文件
     * @return 列表
     * @throws IOException IOException
     */
    @PostMapping(value = "/import", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> importExcel(@RequestParam("file") MultipartFile file) throws IOException {
        System.out.println("导入成功");
        InputStream inputStream = file.getInputStream();
        //实例化实现了AnalysisEventListener接口的类
        ExcelListener listener = new ExcelListener();
        //传入参数
        ExcelReader excelReader = new ExcelReader(inputStream, null, listener);
        //读取信息
        excelReader.read(new Sheet(1, 1, SimpleCompanyVo.class));
        excelReader.read(new Sheet(2, 1, UserVo.class));

        inputStream.close();
        //获取数据
        List<Object> list = listener.getData();

        List<SimpleCompanyVo> companyList = new ArrayList<>();
        List<UserVo> userVoList = new ArrayList<>();
        //转换数据类型,并插入到数据库
        for (Object o : list) {
            if (o instanceof UserVo) {
                UserVo userVo = (UserVo) o;
                userVoList.add(userVo);
                log.info("解析到用户对象：" + userVo);
            }
            if (o instanceof SimpleCompanyVo) {
                SimpleCompanyVo simpleCompanyVo = (SimpleCompanyVo) o;
                companyList.add(simpleCompanyVo);
                log.info("解析到公司对象：" + simpleCompanyVo);
            }
        }
        company = companyList;
        HashMap<String, Object> map = new HashMap<>(20);
        map.put("userList", userVoList);
        map.put("companyList", companyList);
        return map;
    }

}



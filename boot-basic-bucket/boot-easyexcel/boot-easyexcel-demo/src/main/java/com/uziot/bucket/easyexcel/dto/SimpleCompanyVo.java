package com.uziot.bucket.easyexcel.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author shidt
 * @version V1.0
 * @className SimpleCompanyVo
 * @date 2020-01-05 00:47:52
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SimpleCompanyVo extends BaseRowModel implements Serializable {
    private static final long serialVersionUID = 5462608862337425606L;
    @ExcelProperty(value = "公司名称", index = 0)
    String companyName;
    @ExcelProperty(value = "税号", index = 1)
    String taxCode;
    @ExcelProperty(value = "地址", index = 2)
    String address;
    @ExcelProperty(value = "电话", index = 3)
    String telephone;
    @ExcelProperty(value = "开户银行", index = 4)
    String accountBank;
    @ExcelProperty(value = "银行账号", index = 5)
    String bankAccount;
}

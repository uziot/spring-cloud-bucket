package com.uziot.bucket.transactional.mutil;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className SubTrans001
 * @date 2021-09-29 09:59:35
 * @description
 */

@Service
public class SubTrans001 {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Transactional(rollbackFor = Exception.class)
    public int addUser(SysUser sysUser) {
        return sysUserMapper.insert(sysUser);
    }
}

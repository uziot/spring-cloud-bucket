package com.uziot.bucket.transactional.cases;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className Case03
 * @date 2021-05-15 22:37:58
 * @description
 */

@Component
public class Case03 {

    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     * 直接调用，事务生效,异常回滚
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean testCompileException2(long id) throws Exception {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
        sysUser.setNickName("测试昵称1");

        sysUserMapper.updateByPrimaryKeySelective(sysUser);

        sysUser.setDeptId(999L);

        sysUserMapper.updateByPrimaryKeySelective(sysUser);
        throw new Exception("参数异常");
    }

    /**
     * 非直接调用，不生效
     */
    public boolean testCall(long id) throws Exception {
        return testCompileException2(id);
    }
}

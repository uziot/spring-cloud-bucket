package com.uziot.bucket.transactional.cases;

import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className Case02
 * @date 2021-05-15 22:37:16
 * @description 1. 数据库是否支持事务管理
 * 事务生效的前提是你的数据源得支持事务，
 * 比如 mysql 的 MyISAM 引擎就不支持事务，而 Innodb 支持事务
 */

@Component
public class Case02 {
}

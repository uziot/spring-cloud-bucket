package com.uziot.bucket.transactional.specialcase;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className PushTransService02
 * @date 2021-05-15 23:09:48
 * @description </p>
 * 针对消息推送服务的事务
 * <p>
 * 主要针对近日的事务控制+消息推送服务做一个总结，基本代码结构如下
 * <p>
 * 主要问题：
 * 因为涉及快速操作业务，本系统调用推送消息后对方系统立即调用咱们服务做签收
 * 并发操作下，由于本地事务由于先推送完成再提交事务
 * 由于已经推送完成，本地方法还未提交完成事务，导致其他服务再次调用咱们服务
 * 可能发生事务未提交完成的情况，导致业务发生异常;
 * 但这种业务场景还是很常见的，因此我们要为这种模式进行代码优化
 * 如此优化推送服务为异步推送
 */

@Slf4j
@Service
public class PushTransService02 {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Transactional(rollbackFor = Exception.class)
    public boolean updateAndPushTransService() {
        // 业务逻辑
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(104L);
        sysUser.setNickName("测试昵称Push");
        sysUserMapper.updateByPrimaryKeySelective(sysUser);
        sysUser.setDeptId(1010L);
        sysUserMapper.updateByPrimaryKeySelective(sysUser);

        // 调用其他服务推送
        ThreadPoolFactory.threadPoolTaskExecutor().submit(new PushTask(this));

        // 这样会采用异步推送，此时会将推送任务交由其他线程处理，
        // 主线程执行完成，事务提交，解决推送后立即接收请求下可能导致的事务未提交完成的问题
        return true;

    }


    public void push() {
        log.info("开始像其他系统推送消息，推送成功后对方系统立即签收并发操作！");
    }
}


class PushTask implements Runnable {
    private final PushTransService02 pushTransService02;

    public PushTask(PushTransService02 pushTransService02) {
        this.pushTransService02 = pushTransService02;
    }

    @Override
    public void run() {
        pushTransService02.push();
    }
}

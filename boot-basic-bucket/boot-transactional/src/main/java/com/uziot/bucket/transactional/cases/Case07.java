package com.uziot.bucket.transactional.cases;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className Case03
 * @date 2021-05-15 22:37:58
 * @description
 */

@Slf4j
@Component
public class Case07 {

    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     * 这个看着好像没有毛病，抛出线程，事务回滚，
     * 可惜两个子线程的修改并不会被回滚
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean testCompileException(long id) throws Exception {
        new Thread(() -> {
            log.info("开始子线程更新");
            SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
            sysUser.setNickName("测试昵称07");
            sysUserMapper.updateByPrimaryKeySelective(sysUser);

            sysUser.setDeptId(1007L);

            // 抛出异常和捕获的异常不一致导致事务无法回退
            sysUserMapper.updateByPrimaryKeySelective(sysUser);

        }).start();

        throw new Exception("异常抛出未拦截回滚");
    }

}

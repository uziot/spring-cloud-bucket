package com.uziot.bucket.transactional.service;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className SysUserService
 * @date 2021-05-14 22:27:14
 * @description
 */

@Service
public class SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    public int add(SysUser sysUser) {
        return addUser(sysUser);
    }


    @Transactional(rollbackFor = Exception.class)
    int addUser(SysUser sysUser) {
        SysUser user = sysUserMapper.selectUserByUserName(sysUser.getUserName());
        if (null != user) {
            throw new RuntimeException("用户已存在！");
        }
        return sysUserMapper.insert(sysUser);
    }


}

package com.uziot.bucket.transactional.mutil;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author shidt
 * @version V1.0
 * @className TansTestService
 * @date 2021-09-29 09:59:21
 * @description
 */

@Service
public class TansTestService {

    @Autowired
    private SubTrans001 subTrans001;
    @Autowired
    private SubTrans002 subTrans002;
    @Autowired
    private ExceptionTrans exceptionTrans;

    @Transactional(rollbackFor = Exception.class)
    public void testTrans() {
        SysUser sysUser = new SysUser();
        sysUser.setDeptId(0L);
        sysUser.setUserName("18888");
        sysUser.setLoginDate(new Date());
        sysUser.setNickName("ttttt");
        sysUser.setCreateBy("");
        sysUser.setUpdateBy("");
        sysUser.setUpdateTime(new Date());

        sysUser.setUserId(18L);
        subTrans001.addUser(sysUser);

        sysUser.setUserId(19L);
        subTrans002.addUser(sysUser);

        sysUser.setUserId(20L);
        exceptionTrans.addUser(sysUser);
    }
}

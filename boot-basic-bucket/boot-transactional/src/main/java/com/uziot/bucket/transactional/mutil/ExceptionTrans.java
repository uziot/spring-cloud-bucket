package com.uziot.bucket.transactional.mutil;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className SubTrans002
 * @date 2021-09-29 09:59:46
 * @description
 */

@Service
public class ExceptionTrans {

    public int addUser(SysUser sysUser) {
        try {
            if (null != sysUser) {
                throw new RuntimeException("测试信息异常！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }
}

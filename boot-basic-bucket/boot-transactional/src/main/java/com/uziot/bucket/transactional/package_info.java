package com.uziot.bucket.transactional;

/**
 * @author shidt
 * @version V1.0
 * @className package_info
 * @date 2021-05-14 22:10:39
 * @description </p>
 * Transactional有哪些失效场景
 * <p>
 * 将会在此包中对其中的失效场景进行整理
 * <p>
 * <p>
 * 数据库不支持事务
 * 注解放在了私有方法上
 * 类内部调用
 * 未捕获异常
 * 多线程场景
 * 传播属性设置问题
 */


package com.uziot.bucket.transactional.cases;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className Case04
 * @date 2021-05-15 22:46:28
 * @description 私有方法上的注解，不生效
 */

@Component
public class Case04 {

    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     * 私有方法上的注解，不生效
     * 直接使用时，下面这种场景不太容易出现，
     * 因为 IDEA 会有提醒，文案为: Methods annotated with '@Transactional' must be overridable
     */
    @Transactional(rollbackFor = Exception.class)
    private boolean testCompileException2(long id) throws Exception {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
        sysUser.setNickName("测试昵称4");

        sysUserMapper.updateByPrimaryKeySelective(sysUser);

        sysUser.setDeptId(1004L);

        sysUserMapper.updateByPrimaryKeySelective(sysUser);
        throw new Exception("参数异常");
    }

}

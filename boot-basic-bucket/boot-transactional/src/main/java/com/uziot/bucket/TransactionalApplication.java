package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author shidt
 * @version V1.0
 * @className TransactionalApplication
 * @date 2021-05-14 22:09:18
 * @description
 */

@EnableTransactionManagement
@MapperScan(value = {"com.uziot.bucket.transactional"})
@SpringBootApplication
public class TransactionalApplication {
    public static void main(String[] args) {
        SpringApplication.run(TransactionalApplication.class, args);
    }
}

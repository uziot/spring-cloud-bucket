package com.uziot.bucket.transactional.cases;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author shidt
 * @version V1.0
 * @className Case01
 * @date 2021-05-15 22:35:02
 * @description * 1.启动类开启事务注解
 * * @EnableTransactionManagement
 * <p>
 * 在springboot中未开启事务管理器的，可能事务不会生效
 */

@Slf4j
@EnableTransactionManagement
@Component
public class Case01 {
    public void print() {
        log.info("需要在启动类上开启事务管理器，不开启的事务可能不生效！");
    }
}

package com.uziot.bucket.transactional.cases;

import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shidt
 * @version V1.0
 * @className Case03
 * @date 2021-05-15 22:37:58
 * @description -@Transactional注解默认处理运行时异常，
 * 即只有抛出运行时异常时，才会触发事务回滚，否则并不会如
 */

@Component
public class Case05 {

    @Autowired
    private SysUserMapper sysUserMapper;


    /**
     * 非运行异常，且没有通过 rollbackFor 指定抛出的异常，不生效
     * spring事务默认异常回滚为运行时异常
     */
    @Transactional
    public boolean testCompileException(long id) throws Exception {
        SysUser sysUser = sysUserMapper.selectByPrimaryKey(id);
        sysUser.setNickName("测试昵称05");

        sysUserMapper.updateByPrimaryKeySelective(sysUser);

        sysUser.setDeptId(1005L);

        sysUserMapper.updateByPrimaryKeySelective(sysUser);
        throw new Exception("异常抛出未拦截回滚");
    }

}

package uziot.bucket.test;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.cases.Case03;
import com.uziot.bucket.transactional.cases.Case05;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class TestCase05 {

    @Autowired
    private Case05 case05;

    /**
     * 非运行异常，且没有通过 rollbackFor 指定抛出的异常，不生效
     * spring事务默认异常回滚为运行时异常
     */
    @Test
    public void updateUser2() throws Exception {
        boolean testCall = case05.testCompileException(104L);
    }
}

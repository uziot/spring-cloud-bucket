package uziot.bucket.test;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.cases.Case05;
import com.uziot.bucket.transactional.cases.Case06;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class TestCase06 {

    @Autowired
    private Case06 case06;

    @Test
    public void updateUser2() throws Exception {
        boolean testCall = case06.testCompileException(104L);
    }
}

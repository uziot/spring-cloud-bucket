package uziot.bucket.test;

import java.util.Date;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import com.uziot.bucket.transactional.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class Test1 {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserService sysUserService;

    @Test
    public void selectAll() {
        List<SysUser> sysUsers = sysUserMapper.selectAll();
        log.info("通过用户：[{}]", sysUsers);
    }

    @Test
    public void addUser() {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(0L);
        sysUser.setDeptId(0L);
        sysUser.setUserName("tony2");
        sysUser.setLoginDate(new Date());
        sysUser.setNickName("ttttt");
        sysUser.setCreateBy("");
        sysUser.setUpdateBy("");
        sysUser.setUpdateTime(new Date());

        int insert = sysUserService.add(sysUser);
        log.info("添加用户：[{}]", insert);
    }


}

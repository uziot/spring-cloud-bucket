package uziot.bucket.test;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.cases.Case07;
import com.uziot.bucket.transactional.cases.Case08;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class TestCase08 {

    @Autowired
    private Case08 case08;

    @Test
    public void updateUser2() throws Exception {
        boolean testCall = case08.testCompileException(104L);
    }
}

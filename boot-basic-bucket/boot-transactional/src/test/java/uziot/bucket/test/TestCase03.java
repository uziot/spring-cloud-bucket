package uziot.bucket.test;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.cases.Case03;
import com.uziot.bucket.transactional.dao.domain.SysUser;
import com.uziot.bucket.transactional.dao.mapper.SysUserMapper;
import com.uziot.bucket.transactional.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-02-26 09:47:15
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class TestCase03 {

    @Autowired
    private Case03 case03;

    /**
     * 直接调用，事务生效,异常回滚
     */
    @Test
    public void updateUser() throws Exception {
        boolean testCall = case03.testCompileException2(104L);
    }

    /**
     * 非直接调用，不生效,会直接提交
     */
    @Test
    public void updateUser2() throws Exception {
        boolean testCall = case03.testCall(104L);
    }
}

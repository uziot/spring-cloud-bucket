package uziot.bucket.test;

import com.uziot.bucket.TransactionalApplication;
import com.uziot.bucket.transactional.cases.Case08;
import com.uziot.bucket.transactional.specialcase.PushTransService02;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className PushTransService02Test
 * @date 2021-05-15 23:28:06
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TransactionalApplication.class})
public class PushTransService02Test {
    @Autowired
    private PushTransService02  pushTransService02;

    @Test
    public void updateUser2() throws Exception {
        boolean res = pushTransService02.updateAndPushTransService();
    }
}

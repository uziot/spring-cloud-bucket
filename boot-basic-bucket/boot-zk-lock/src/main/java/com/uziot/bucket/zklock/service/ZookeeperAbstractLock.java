package com.uziot.bucket.zklock.service;

import org.I0Itec.zkclient.ZkClient;

import java.util.concurrent.CountDownLatch;

/**
 * @author shidt
 * @version V1.0
 * @className ZookeeperAbstractLock
 * @date 2021-10-09 10:56:32
 * @description 抽象类
 */

public abstract class ZookeeperAbstractLock implements Lock {

    /**
     * 为了方便测试，下边的常量就不写到配置文件中
     * zk连接地址
     */
    private static final String CONNECT_STRING = "127.0.0.1:2181";
    /**
     * 创建zk连接
     */
    protected ZkClient zkClient = new ZkClient(CONNECT_STRING);
    protected static final String PATH = "/lock";
    /**
     * 信号量变量
     */
    protected CountDownLatch countDownLatch = null;


    /**
     * 是否获取锁成功，若成功：true，失败：false
     */
    abstract boolean tryLock();

    /**
     * 等待锁释放
     */
    abstract void waitLock();


    /**
     * 获取锁实现
     */
    @Override
    public void getLock() {
        if (tryLock()) {
            System.out.println("#####获取锁成功#####");
        } else {
            //没有获取到锁。表明其他服务器正在使用，此时等待
            waitLock();
            //当zk断开连接，则信号量down,重新获取锁
            getLock();
        }
    }

    /**
     * 释放锁实现
     */
    @Override
    public void unlock() {
        if (zkClient != null) {
            //关闭连接
            System.out.println("#####关闭连接#####");
            zkClient.close();
        }
    }


}

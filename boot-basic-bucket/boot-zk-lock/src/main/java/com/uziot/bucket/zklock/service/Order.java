package com.uziot.bucket.zklock.service;


import com.uziot.bucket.zklock.utils.OrderNum;

/**
 * @author shidt
 * @version V1.0
 * @className Order
 * @date 2021-10-09 10:56:32
 * @description 产生订单号
 * zk分布锁，测试高并发情况
 */

public class Order implements Runnable {
    OrderNum num = new OrderNum();
    /**
     * 使用自定义的zk锁
     */
    private final Lock lock = new ZookeeperDistributeLock();

    @Override
    public void run() {
        try {
            lock.getLock();
            getOrderNum();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //创建完成后关闭zk连接
            lock.unlock();
        }
    }

    private void getOrderNum() {
        System.out.println("当前线程：" + Thread.currentThread().getName() + "---" + "订单号：" + num.createOderNum());
    }

    public static void main(String[] args) {
        System.out.println("生产订单号开始");
        //模拟多个节点获取连接
        for (int i = 0; i < 101; i++) {
            new Thread(new Order()).start();
        }
    }

}

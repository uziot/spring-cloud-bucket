package com.uziot.bucket.zklock.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className OrderNum
 * @date 2021-10-09 10:56:32
 * @description
 */

public class OrderNum {
    private static Integer count = 0;

    public String createOderNum() {
        //为了测试效果，创建订单号设置慢一点
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (Exception e) {
            // TODO: handle exception
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String oderNum = format.format(new Date()) + ++count;
        return oderNum.replace("-", "");

    }

    public static void main(String[] args) {
        System.out.println(new OrderNum().createOderNum());
    }
}

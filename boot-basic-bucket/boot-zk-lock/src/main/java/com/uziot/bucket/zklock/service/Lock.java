package com.uziot.bucket.zklock.service;

/**
 * @author shidt
 * @version V1.0
 * @className Lock
 * @date 2021-10-09 10:56:32
 * @description ZK实现分布式锁接口
 * 实现原理：根据ZK临时节点创建，断开连接后不会被保存
 * 判断是否存在节点
 * 存   在 ：表示有资源占用，进入等待状态
 * 不存在：表示无资源占用，获取到锁
 */

public interface Lock {
    /**
     * 获取锁
     */
    void getLock();

    /**
     * 释放锁
     */
    void unlock();
}

package com.uziot.bucket.guava.pojo;

/**
 * 功能描述: <br>
 * 统一返回样式
 *
 * @author shidt
 * @date 2020-03-09 0:34
 */
public class ResultUtil {
    public static Result<?> success() {
        return success(null);
    }

    public static Result<?> success(Object object) {
        Result<Object> result = new Result<>();
        result.setCode(ResultCode.SUCCESS);
        result.setMsg("成功");
        result.setData(object);
        return result;
    }

    public static Result<Object> success(Integer code, Object object) {
        Result<Object> result = new Result<>();
        result.setCode(code);
        result.setMsg("成功");
        result.setData(object);
        return result;
    }

    public static Result<?> error(String msg) {
        Result<Object> result = new Result<>();
        result.setCode(ResultCode.ERROR);
        result.setMsg(msg);
        return result;
    }

    public static Result<?> error(Integer code, String msg) {
        Result<Object> result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}


package com.uziot.bucket.guava.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * @author shidt
 * @version V1.0
 * @className GuavaCacheContriller
 * @date 2020-11-30 23:16:56
 * @description
 */
@RestController
public class GuavaCacheController {

    @Resource
    private GuavaCacheService guavaCacheService;

    @GetMapping(value = "/selectId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object selectOneByCtfid(@PathVariable String id) throws ExecutionException {
        return guavaCacheService.selectById(id);
    }
}

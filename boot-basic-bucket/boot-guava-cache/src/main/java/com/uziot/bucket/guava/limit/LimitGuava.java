package com.uziot.bucket.guava.limit;

import java.lang.annotation.*;

/**
 * 功能描述: <br>
 * guava接口限流
 *
 * @author shidt
 * @date 2020-03-08 23:59
 */
@Inherited
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LimitGuava {
    double limitSecondNum() default 20;  //默认每秒放入桶中的token
}
package com.uziot.bucket.guava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className GuavaApplication
 * @date 2020-11-30 23:10:58
 * @description
 */
@SpringBootApplication
public class GuavaApplication {
    public static void main(String[] args) {
        SpringApplication.run(GuavaApplication.class, args);
    }
}

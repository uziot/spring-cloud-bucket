package com.uziot.bucket.guava.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className GuavaLock
 * @date 2020-04-07 16:54:38
 * @description
 */
@Configuration
public class GuavaConfig {
    /**
     * 需要指定失效时间
     *
     * @return 缓存
     */
    @Bean
    public Cache<String, String> createCache() {
        // 缓存有效期为5秒
        return CacheBuilder.newBuilder().expireAfterWrite(30L, TimeUnit.SECONDS).build();
    }
}

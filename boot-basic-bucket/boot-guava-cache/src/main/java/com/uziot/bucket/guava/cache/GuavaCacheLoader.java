package com.uziot.bucket.guava.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className GuavaCacheBuilder
 * @date 2020-04-25 15:41:33
 * @description
 */
@Slf4j
public class GuavaCacheLoader {
    /**
     * 全局缓存设置
     * <p>
     * 缓存项最大数量：100000
     * 缓存有效时间（天）：10
     *
     * @param cacheLoader 全局缓存配置
     * @return 装载对象
     */
    private static LoadingCache<String, Object> loadCache(CacheLoader<String, Object> cacheLoader) {
        return CacheBuilder.newBuilder()
                //缓存池大小，在缓存项接近该大小时， Guava开始回收旧的缓存项
                .maximumSize(100000)
                //设置时间对象没有被读/写访问则对象从内存中删除(在另外的线程里面不定期维护)
                .expireAfterAccess(60, TimeUnit.MINUTES)
                // 设置缓存在写入之后 设定时间 后失效
                .expireAfterWrite(60, TimeUnit.MINUTES)
                //移除监听器,缓存项被移除时会触发
                .removalListener((RemovalListener<String, Object>) rn -> {
                    //逻辑操作
                })
                //开启Guava Cache的统计功能
                .recordStats()
                .build(cacheLoader);
    }

}

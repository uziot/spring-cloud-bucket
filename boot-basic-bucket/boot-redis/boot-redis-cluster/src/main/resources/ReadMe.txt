异常分析
从报错误的信息ERR This instance has cluster support disabled很明显看得出来，是没有启动redis集群功能，可是我项目配置的集群的配置方式，要么修改代码为单机配置，要么修改redis为集群方式。

解决办法
1、可以修改配置为单机redis配置：

spring:
  redis:
    host: qianxunclub.com
    port: 6666
2、在安装redis的目录找到redis配置文件redis.conf，里面会找到配置：

# cluster-enabled yes
把注释去掉就可以了
cluster-enabled yes
package com.uziot.bucket.redis.cache.util;

import com.alibaba.fastjson.JSON;
import org.springframework.validation.support.BindingAwareModelMap;

/**
 * @author shidt
 * @version V1.0
 * @className RedisConfig
 * @date 2020-02-28 21:38:01
 * @description 缓存key相关的工具类
 */
public class CacheKeyUtil {

    /**
     * 获取方法参数组成的key
     *
     * @param params 参数数组
     */
    public static String getMethodParamsKey(Object... params) {
        if (null == params || params.length == 0) {
            return "";
        }
        StringBuilder key = new StringBuilder("(");
        for (Object obj : params) {
            if (obj.getClass().equals(BindingAwareModelMap.class)) {
                continue;
            }
            key.append(JSON.toJSONString(obj).replaceAll("\"", "'"));
        }
        key.append(")");
        return key.toString();
    }

}

package com.uziot.bucket.redis.web;

import com.uziot.bucket.redis.cache.aspect.RedisCache;
import com.uziot.bucket.redis.dao.domain.SysUser;
import com.uziot.bucket.redis.dao.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className UserController
 * @date 2020-12-09 21:23:52
 * @description
 */
@RestController
public class UserController {
    @Autowired
    private SysUserMapper sysUserMapper;

    @RedisCache
    @GetMapping(value = "/user/{userId}")
    public SysUser selectById(@PathVariable Long userId) {
        return sysUserMapper.selectByPrimaryKey(userId);
    }


}

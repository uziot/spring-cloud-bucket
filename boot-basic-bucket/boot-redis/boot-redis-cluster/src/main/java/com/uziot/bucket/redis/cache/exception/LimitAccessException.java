package com.uziot.bucket.redis.cache.exception;

/**
 * 功能描述: <br>
 *
 * 限流异常
 *
 * @author shidt
 * @date 2020-03-06 12:42
 */
public class LimitAccessException extends RuntimeException {

    private static final long serialVersionUID = -3608667856397125671L;

    public LimitAccessException(String message) {
        super(message);
    }
}
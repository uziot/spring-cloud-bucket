package com.uziot.bucket.redis.cache.service;

import com.uziot.bucket.redis.cache.config.RedisInfo;
import com.uziot.bucket.redis.cache.exception.RedisConnectException;

import java.util.List;
import java.util.Map;

/**
 * 功能描述: <br>
 * <>
 * Redis 工具类，只封装了几个常用的 redis 命令，
 * 可根据实际需要按类似的方式扩展即可。
 *
 * @author shidt
 * @date 2020-03-15 1:16
 */
public interface RedisInfoService {

    /**
     * 获取 redis 的详细信息
     *
     * @return List
     */
    List<RedisInfo> getRedisInfo() throws RedisConnectException;

    /**
     * 获取 redis key 数量
     *
     * @return Map
     */
    Map<String, Object> getKeysSize() throws RedisConnectException;

    /**
     * 获取 redis 内存信息
     *
     * @return Map
     */
    Map<String, Object> getMemoryInfo() throws RedisConnectException;

}

package com.uziot.bucket.redis.cache.service;

import com.uziot.bucket.redis.cache.exception.RedisConnectException;

/**
 * 功能描述: <br>
 * Redis缓存操作执行器
 *
 * @author shidt
 * @date 2020-04-05 23:00
 */
@FunctionalInterface
public interface JedisExecutor<T, R> {
    /**
     * 执行方法
     *
     * @param t t
     * @return R
     * @throws RedisConnectException RedisConnectException
     */
    R excute(T t) throws RedisConnectException;
}

package com.uziot.bucket.redis.cache.aspect;

/**
 * 功能描述: <br>
 * 限流枚举类型
 *
 * @author shidt
 * @date 2020-03-06 12:42
 */
public enum LimitType {
    // 传统类型
    CUSTOMER,
    // 根据 IP 限制
    IP;
}

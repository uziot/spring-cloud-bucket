package com.uziot.bucket.redis.cache.aspect;

import com.uziot.bucket.redis.cache.aspect.LimitType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: <br>
 * 限流注解
 *
 * @author shidt
 * @date 2020-03-06 12:42
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Limit {

    /**
     * 资源名称，用于描述接口功能
     *
     * @return String
     */
    String name() default "";

    /**
     * 资源 key
     * 介绍Key选项如下
     * 1、LimitType.CUSTOMER 参数时候，使用key作为限流标识
     * 2、LimitType.IP的时候，使用IP作为限流标识
     * 3、如果不指定当前参数，默认使用方法名作为限流标识
     *
     * @return key
     */
    String key() default "";

    /**
     * Redis Key前缀
     * 缓存到redis中的key值会使用当前key值+资源key的连接之后保存
     *
     * @return key prefix
     */
    String prefix() default "";

    /**
     * 时间的，单位秒
     *
     * @return 时间秒数
     */
    int period();

    /**
     * 限制访问次数
     *
     * @return 限制访问次数
     */
    int count();

    /**
     * 限制类型
     *
     * @return 限制类型
     */
    LimitType limitType() default LimitType.CUSTOMER;
}

package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author shidt
 * @version V1.0
 * @className RedisCacheApplication
 * @date 2020-12-09 21:18:56
 * @description
 */
@EnableAspectJAutoProxy(exposeProxy = true)
@SpringBootApplication
public class RedisCacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisCacheApplication.class, args);
    }
}

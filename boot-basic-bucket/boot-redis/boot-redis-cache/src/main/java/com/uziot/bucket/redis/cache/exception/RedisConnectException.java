package com.uziot.bucket.redis.cache.exception;

/**
 * @author shidt
 * @version V1.0
 * @className RedisConnectException
 * @date 2020-11-28 17:52:16
 * @description Redis 连接异常
 */
public class RedisConnectException extends Exception {


    private static final long serialVersionUID = -3017197540078078522L;

    public RedisConnectException(String message) {
        super(message);
    }
}

package com.uziot.bucket.redis.cache.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author shidt
 * @version V1.0
 * @className RedisLock
 * @date 2020-04-06 00:15:24
 * @description Redis锁功能
 */

@Slf4j
@Service
public class RedisLockServiceImpl {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 加锁
     * //加锁
     * long time = System.currentTimeMillis() + 1000*10;  //超时时间：10秒，最好设为常量
     * <p>
     * boolean isLock = redisLock.lock(String.valueOf(id), String.valueOf(time));
     *
     * @param key   商品id
     * @param value 当前时间+超时时间
     * @return 是否加锁成功
     */
    public boolean lock(String key, String value) {
        Boolean result = redisTemplate.opsForValue().setIfAbsent(key, value);
        //这个其实就是setnx命令，只不过在java这边稍有变化，返回的是boolean
        if (null == result) {
            throw new RuntimeException("Redis分布式锁出现异常！加锁结果：" + result);
        }
        if (result) {
            log.info("Redis分布式锁,加锁成功KEY:{}", key);
            return true;
        }
        //避免死锁，且只让一个线程拿到锁
        String currentValue = redisTemplate.opsForValue().get(key);
        //如果锁过期了
        if (!StringUtils.isEmpty(currentValue) && Long.parseLong(currentValue) < System.currentTimeMillis()) {
            //获取上一个锁的时间
            String oldValues = redisTemplate.opsForValue().getAndSet(key, value);
            /*
               只会让一个线程拿到锁
               如果旧的value和currentValue相等，只会有一个线程达成条件，因为第二个线程拿到的oldValue已经和currentValue不一样了
             */
            boolean b = !StringUtils.isEmpty(oldValues) && oldValues.equals(currentValue);
            if (b) {
                log.info("Redis分布式锁,加锁成功KEY:{}", key);
            } else {
                log.error("Redis分布式锁,加锁失败KEY:{}", key);
            }
            return b;
        }
        log.error("Redis分布式锁,加锁失败KEY:{}", key);
        return false;
    }


    /**
     * 解锁
     * redisLock.unlock(String.valueOf(id),String.valueOf(time));
     *
     * @param key   k
     * @param value v
     */
    public void unlock(String key, String value) {
        try {
            String currentValue = redisTemplate.opsForValue().get(key);
            if (!StringUtils.isEmpty(currentValue) && currentValue.equals(value)) {
                Boolean delete = redisTemplate.opsForValue().getOperations().delete(key);
                if (null == delete) {
                    log.error("Redis分布式锁，解锁异常结果：{}", delete);
                    throw new RuntimeException("Redis锁出现异常！解锁锁结果:" + delete);
                }
                if (delete) {
                    log.info("Redis分布式锁,解锁成功KEY:{}", key);
                } else {
                    log.error("Redis分布式锁,解锁失败KEY:{}", key);
                }
            }
        } catch (Exception e) {
            log.error("Redis分布式锁，解锁异常:{}", e.toString());
        }
    }

}
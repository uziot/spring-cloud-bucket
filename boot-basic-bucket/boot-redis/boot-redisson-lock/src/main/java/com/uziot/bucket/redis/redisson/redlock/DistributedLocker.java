package com.uziot.bucket.redis.redisson.redlock;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className FlowableConfig
 * @date 2020-11-21 23:24:52
 * @description
 */
public interface DistributedLocker {

    /**
     * 获得锁
     *
     * @param lockKey lockKey
     * @return RLock
     */
    RLock getLock(String lockKey);

    /**
     * 锁lockKey
     *
     * @param lockKey lockKey
     * @return RLock
     */
    RLock lock(String lockKey);

    /**
     * 锁lockKey timeout
     *
     * @param lockKey lockKey
     * @param timeout timeout
     * @return RLock
     */
    RLock lock(String lockKey, long timeout);

    /**
     * 锁lockKey
     *
     * @param lockKey lockKey
     * @param timeout timeout
     * @param unit unit
     * @return RLock
     */
    RLock lock(String lockKey, long timeout, TimeUnit unit);

    /**
     * 尝试锁
     *
     * @param lockKey lockKey
     * @param waitTime waitTime
     * @param leaseTime leaseTime
     * @param unit unit
     * @return 是否成功
     */
    boolean tryLock(String lockKey, long waitTime, long leaseTime, TimeUnit unit);

    /**
     * 尝试锁
     *
     * @param lock lock
     * @param waitTime waitTime
     * @param leaseTime leaseTime
     * @param unit unit
     * @return 是否成功
     */
    boolean tryLock(RLock lock, long waitTime, long leaseTime, TimeUnit unit);

    /**
     * 解锁
     *
     * @param lockKey lockKey
     */
    void unlock(String lockKey);

    /**
     * 解锁
     *
     * @param lock lock
     */
    void unlock(RLock lock);
}

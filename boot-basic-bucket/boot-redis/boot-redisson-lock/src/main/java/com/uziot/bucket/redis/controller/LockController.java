package com.uziot.bucket.redis.controller;

import com.github.pagehelper.PageInfo;
import com.uziot.bucket.redis.service.DemoRedlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className LockController
 * @date 2020-12-26 15:39:20
 * @description
 */
@RestController
@RequestMapping(value = "/lock")
public class LockController {
    @Autowired
    public DemoRedlockService demoRedlockService;

    @GetMapping(value = "/redlock2")
    public Object redlock2() throws Exception {
        demoRedlockService.redlock2();
        return new PageInfo<>();
    }

    @GetMapping(value = "/redlock3/{p1}")
    public Object redlock3(@PathVariable String p1) throws Exception {
        demoRedlockService.redlock3(p1);
        return new PageInfo<>();
    }

    @GetMapping(value = "/redlock2/{p1}/{p2}")
    public Object redlock4(@PathVariable String p1, @PathVariable Integer p2) throws Exception {
        demoRedlockService.redlock4(p1, p2);
        return new PageInfo<>();
    }
}

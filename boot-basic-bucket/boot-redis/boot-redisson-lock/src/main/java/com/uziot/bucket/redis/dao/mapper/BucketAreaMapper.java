package com.uziot.bucket.redis.dao.mapper;

import com.uziot.bucket.redis.dao.domain.BucketArea;

import java.util.List;

public interface BucketAreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BucketArea record);

    int insertSelective(BucketArea record);

    BucketArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BucketArea record);

    int updateByPrimaryKey(BucketArea record);

    List<BucketArea> selectAllArea();


}
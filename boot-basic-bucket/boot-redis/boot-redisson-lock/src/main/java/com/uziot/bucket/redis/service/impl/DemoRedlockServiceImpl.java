package com.uziot.bucket.redis.service.impl;
import com.uziot.bucket.redis.redisson.aspect.annotation.RedissonLock;
import com.uziot.bucket.redis.service.DemoRedlockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 庄金明
 * @date 2020年3月24日
 */
@Service
@Slf4j
public class DemoRedlockServiceImpl implements DemoRedlockService {

    /**
     * 通过注解@RedissonLock锁整个方法
     */
    @Override
    @RedissonLock
    public void redlock2() throws Exception {
        Thread.sleep(2000);
        log.info("锁整个方法");
    }

    /**
     * 通过RedissonLock注解锁第一个参数
     *
     * @param someId
     */
    @Override
    @RedissonLock(lockIndexs = 0)
    public void redlock3(String someId) throws Exception {
        Thread.sleep(2000);
        log.info("锁第一个参数：" + someId);
    }

    /**
     * 通过RedissonLock注解锁第一个参数、第二个参数组合
     *
     * @param someId
     * @param someInt
     */
    @Override
    @RedissonLock(lockIndexs = {0, 1})
    public void redlock4(String someId, int someInt) throws Exception {
        Thread.sleep(2000);
        log.info("锁第一个参数、第二个参数组合：" + someId + "," + someInt);
    }

}

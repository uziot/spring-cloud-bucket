package com.uziot.bucket.redis.service;

/**
 * @author 庄金明
 * @date 2020年3月24日
 */
public interface DemoRedlockService {

    /**
     * 通过注解@RedissonLock锁整个方法
     *
     * @throws Exception
     */
    void redlock2() throws Exception;

    /**
     * 通过RedissonLock注解锁第一个参数
     *
     * @param someId
     * @throws Exception
     */
    void redlock3(String someId) throws Exception;

    /**
     * 通过RedissonLock注解锁第一个参数、第二个参数组合
     *
     * @param someId
     * @param someInt
     * @throws Exception
     */
    void redlock4(String someId, int someInt) throws Exception;

}

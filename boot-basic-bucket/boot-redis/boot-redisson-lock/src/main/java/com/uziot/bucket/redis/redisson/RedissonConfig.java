package com.uziot.bucket.redis.redisson;

import com.uziot.bucket.redis.redisson.aspect.components.RepeatRequestComponent;
import com.uziot.bucket.redis.redisson.redlock.RedissonDistributedLocker;
import lombok.SneakyThrows;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shidt
 * @version V1.0
 * @className RedissonConfig
 * @date 2020-11-21 23:24:52
 * @description
 */
@Configuration
@ConditionalOnProperty(name = "uziot.redisson.enabled", havingValue = "true")
public class RedissonConfig {

    @Value("${uziot.redisson.config}")
    private String redissonConfig;

    @Bean
    @SneakyThrows
    public RedissonClient redissonClient() {
        Config config = Config.fromJSON(redissonConfig);
        return Redisson.create(config);
    }

    @Bean
    public RedissonDistributedLocker redissonDistributedLocker() {
        return new RedissonDistributedLocker(redissonClient());
    }

    @Bean
    public RepeatRequestComponent repeatRequestComponent() {
        return new RepeatRequestComponent(redissonDistributedLocker());
    }
}

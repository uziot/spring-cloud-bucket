package com.uziot.bucket.redis.redisson.aspect;

import com.uziot.bucket.redis.redisson.aspect.annotation.RedissonLock;
import com.uziot.bucket.redis.redisson.redlock.RedissonDistributedLocker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.annotation.Order;

import java.util.concurrent.TimeUnit;

/**
 * @author shidt
 * @version V1.0
 * @className FlowableConfig
 * @date 2020-11-21 23:24:52
 * @description order设置的小一点，若注解用于service类，让该切面优先于Transactional注解
 */

@Aspect
@ConditionalOnBean(RedissonDistributedLocker.class)
@Order(1)
@Slf4j
@SuppressWarnings("ALL")
public class RedissonLockAspect {
    @Autowired
    private RedissonDistributedLocker redissonDistributedLocker;

    /**
     * 处理锁
     *
     * @param joinPoint    joinPoint
     * @param redissonLock redissonLock
     * @return Object
     * @throws Throwable Throwable
     */
    @Around("@annotation(redissonLock)")
    public Object around(ProceedingJoinPoint joinPoint, RedissonLock redissonLock) throws Throwable {
        Object obj;
        // 方法内的所有参数
        Object[] params = joinPoint.getArgs();

        // 等待多久，默认10秒
        int waitTime = redissonLock.leaseTime();
        // 多久会自动释放，默认10秒
        int leaseTime = redissonLock.leaseTime();

        String lockParams = "";
        String[] fieldNames = redissonLock.fieldNames();
        int[] lockIndexs = redissonLock.lockIndexs();
        // 锁2个及2个以上参数时，fieldNames数量应与lockIndexs一致
        if (fieldNames.length > 1 && lockIndexs.length != fieldNames.length) {
            throw new Exception("lockIndexs与fieldNames数量不一致");
        }
        // 数组为空代表锁整个方法
        if (lockIndexs.length > 0) {
            StringBuilder lockParamsBuffer = new StringBuilder();
            for (int i = 0; i < lockIndexs.length; i++) {
                if (fieldNames.length == 0 || fieldNames[i] == null || fieldNames[i].length() == 0) {
                    lockParamsBuffer.append(".").append(params[lockIndexs[i]]);
                } else {
                    Object lockParamValue = PropertyUtils.getSimpleProperty(params[lockIndexs[i]], fieldNames[i]);
                    lockParamsBuffer.append(".").append(lockParamValue);
                }
            }
            lockParams = lockParamsBuffer.toString();
        }
        // 取得方法名
        String key =
                joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName() + lockParams;

        RLock rlock = redissonDistributedLocker.getLock(key);
        boolean isSuccess = redissonDistributedLocker.tryLock(rlock, waitTime, leaseTime, TimeUnit.SECONDS);
        if (isSuccess) {
            log.info("取到锁[" + key + "]");
            try {
                obj = joinPoint.proceed();
            } finally {
                log.info("释放锁[" + key + "]");
                redissonDistributedLocker.unlock(rlock);
            }
        } else {
            throw new Exception(redissonLock.msg());
        }
        return obj;
    }
}

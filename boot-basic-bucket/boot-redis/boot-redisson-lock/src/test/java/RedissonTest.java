import com.uziot.bucket.RedissonApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RBucket;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className RedissonTest
 * @date 2020-12-09 09:57:38
 * @description
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RedissonApplication.class})
public class RedissonTest {

    @Autowired
    private RedissonClient redissonClient;

    @Test
    public void testBucket() {
        RBucket<Object> bucket = redissonClient.getBucket("name", StringCodec.INSTANCE);
        Object o = bucket.get();
        System.out.println(o);
    }

    @Test
    public void testList() {
        RList<String> interests = redissonClient.getList("interests");
        interests.add("篮球");
        interests.add("爬山");
        interests.add("编程");
    }
}
package com.uziot.bucket.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className RedisSentinelApplication
 * @date 2021-08-19 11:20:44
 * @description
 */

@Slf4j
@RestController
public class DemoController {

    @Autowired
    private RedisService redisService;

    @GetMapping("/set")
    public void setObject() {
        redisService.set("key0001", "value0001");
    }


    @GetMapping("/get")
    public Object getObject() {
        log.info("eeeeeeeeeeeeeeeeeeeeeeeee");
        return redisService.get("key0001");
    }
}
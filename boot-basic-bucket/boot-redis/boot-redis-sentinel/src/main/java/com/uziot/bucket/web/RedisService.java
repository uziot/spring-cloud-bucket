package com.uziot.bucket.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className RedisSentinelApplication
 * @date 2021-08-19 11:20:44
 * @description
 */

@Service
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;


    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }
}
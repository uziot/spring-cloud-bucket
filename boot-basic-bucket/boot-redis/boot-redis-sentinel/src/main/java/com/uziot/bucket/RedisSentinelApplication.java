package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className RedisSentinelApplication
 * @date 2021-08-19 11:20:44
 * @description
 */

@SpringBootApplication
public class RedisSentinelApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisSentinelApplication.class,args);
    }
}

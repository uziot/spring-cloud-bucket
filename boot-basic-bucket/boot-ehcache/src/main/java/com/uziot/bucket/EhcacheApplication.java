package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author shidt
 * @version V1.0
 * @className EhcacheApplication
 * @date 2020-12-03 08:28:24
 * @description
 */
@MapperScan("com.uziot.bucket.ehcache.dao.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class EhcacheApplication {
    public static void main(String[] args) {
        SpringApplication.run(EhcacheApplication.class, args);
    }
}

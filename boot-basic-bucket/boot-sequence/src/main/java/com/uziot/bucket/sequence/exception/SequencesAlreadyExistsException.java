package com.uziot.bucket.sequence.exception;

/**
 * @author shidt
 * @version V1.0
 * @className SequencesAlreadyExistsException
 * @date 2020-12-30 22:43:30
 * @description 序列已存在异常
 */
public class SequencesAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SequencesAlreadyExistsException(String sequencesName, Throwable cause) {
        super("sequences: \"" + sequencesName + "\" already exists", cause);
    }

    public SequencesAlreadyExistsException(String sequencesName) {
        super("sequences: \"" + sequencesName + "\" already exists");
    }
}

package com.uziot.bucket.sequence.service;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * @author shidt
 * @version V1.0
 * @className SequencesNameManager
 * @date 2021-01-08 23:33:22
 * @description 管理序列名称，为了后面快速检索是否有已经存在名称
 */
public class SequencesNameManager {

    private final Set<String> sequencesNames = new ConcurrentSkipListSet<>();

    private static volatile SequencesNameManager sequencesNameManager;

    /**
     * 获取当前实例对象
     *
     * @return 序列管理器
     */
    public static SequencesNameManager getInstance() {
        if (null != sequencesNameManager) {
            return sequencesNameManager;
        } else {
            synchronized (SequencesNameManager.class) {
                sequencesNameManager = new SequencesNameManager();
            }
        }
        return sequencesNameManager;
    }

    /**
     * 添加名称
     *
     * @param name name
     */
    public void addName(String name) {
        sequencesNames.add(name);
    }

    public boolean contains(String name) {
        return sequencesNames.contains(name);
    }
}

package com.uziot.bucket.sequence.dao.mapper;


import com.uziot.bucket.sequence.dao.domain.Sequences;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/**
 * @author shidt
 * @version V1.0
 * @className SequencesMapper
 * @date 2020-11-16 23:48:56
 * @description mapper
 */
@Mapper
public interface SequencesMapper {
    int deleteByPrimaryKey(String seqName);

    int insert(Sequences record);

    int insertSelective(Sequences record);

    Sequences selectByPrimaryKey(String seqName);

    int updateByPrimaryKeySelective(Sequences record);

    int updateByPrimaryKey(Sequences record);

    int setCurrentValue(Sequences sequences);

    Sequences getSequence(Sequences sequences);

    List<Sequences> getCacheSequences();

    List<Sequences> getAllSequences();
}
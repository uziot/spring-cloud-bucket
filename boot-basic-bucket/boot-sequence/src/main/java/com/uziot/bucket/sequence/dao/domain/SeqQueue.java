package com.uziot.bucket.sequence.dao.domain;

import java.util.Queue;

/**
 * @author shidt
 * @version V1.0
 * @className SeqQueue
 * @date 2021-01-08 23:33:22
 * @description 序列队列
 */
public class SeqQueue {

    private String sequenceName;

    private long cacheCount;

    private Queue<Long> queue;

    public SeqQueue() {
    }

    public SeqQueue(String sequenceName, long cacheCount, Queue<Long> queue) {
        this.sequenceName = sequenceName;
        this.cacheCount = cacheCount;
        this.queue = queue;
    }


    public String getSequenceName() {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public long getCacheCount() {
        return cacheCount;
    }

    public void setCacheCount(long cacheCount) {
        this.cacheCount = cacheCount;
    }

    public Queue<Long> getQueue() {
        return queue;
    }

    public void setQueue(Queue<Long> queue) {
        this.queue = queue;
    }

    public boolean hasQueue() {
        boolean flag = false;
        if (null != queue && queue.size() > 0) {
            flag = true;
        }
        return flag;
    }


}

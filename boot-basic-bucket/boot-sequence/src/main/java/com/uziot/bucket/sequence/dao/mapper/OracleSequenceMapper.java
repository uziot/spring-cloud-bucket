package com.uziot.bucket.sequence.dao.mapper;

import com.uziot.bucket.sequence.dao.domain.Sequences;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className LoginUser
 * @date 2020-11-16 23:48:56
 * @description mapper
 */
@Mapper
public interface OracleSequenceMapper {

    /**
     * 创建序列
     *
     * @param sequenceName 序列名
     */
    void createSequenceByName(@Param(value = "sequenceName") String sequenceName);

    /**
     * 根据序列号查询序列
     *
     * @param sequenceName sequenceName
     * @return 序列值
     */
    int selectSequenceNextByName(@Param(value = "sequenceName") String sequenceName);

    /**
     * 根据序列号查询序列
     *
     * @param sequenceName sequenceName
     * @return 序列值
     */
    int selectSequenceCurrentByName(@Param(value = "sequenceName") String sequenceName);

    /**
     * 重置序列通过序列名
     * 执行存储过程
     *
     * @param sequenceName sequenceName
     */
    void resetSequenceByName(@Param(value = "sequenceName") String sequenceName);

    /**
     * 查询所有存在序列
     * @return List
     */
    List<Map<String, String>> selectAllSequences();


    /**
     * 创建
     *
     * @param sequence sequence
     */
    void create(Sequences sequence);

    /**
     * 获取
     *
     * @param sequence sequence
     * @return seq
     */
    long getSeq(Sequences sequence);

    /**
     * 列表获取
     *
     * @param sequence sequence
     * @return 列表
     */
    List<Long> getSeqList(Sequences sequence);

    /**
     * 是否存在当前序列
     *
     * @param sequence sequence
     * @param user     用户 可为null
     * @return int
     */
    int containsSequence(@Param(value = "sequence") Sequences sequence,
                         @Param(value = "user") String user);

    /**
     * 查询系统已有序列
     *
     * @param user sequence
     * @return 列表
     */
    List<Sequences> getSequences(@Param(value = "user") String user);

    /**
     * 是否存在当前序列
     *
     * @param seqName seqName
     * @param user    用户 可为null
     * @return int
     */
    List<Sequences> getLikeSequences(@Param(value = "seqName") String seqName,
                                     @Param(value = "user") String user);

    /**
     * 重置所有序列表中的序列
     * 执行存储过程
     */
    void resetAllSequences();

}
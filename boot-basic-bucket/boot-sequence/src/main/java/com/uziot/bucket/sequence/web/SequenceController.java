package com.uziot.bucket.sequence.web;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.sequence.dao.domain.Sequences;
import com.uziot.bucket.sequence.dao.mapper.OracleSequenceMapper;
import com.uziot.bucket.sequence.dao.mapper.SequencesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className SequenceController
 * @date 2019-12-08 02:32:53
 * @description
 */
@RestController
public class SequenceController {

    @Autowired
    private OracleSequenceMapper oracleSequenceMapper;

    @Autowired
    private SequencesMapper sequencesMapper;

    @GetMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean createSequenceByName() {
        try {
            oracleSequenceMapper.createSequenceByName("BUSINESS_SEQ_002");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object selectAllSequences() {
        List<Map<String, String>> maps = oracleSequenceMapper.selectAllSequences();
        return JSON.toJSONString(maps);
    }

    @GetMapping(value = "/seq1", produces = MediaType.APPLICATION_JSON_VALUE)
    public int seq1() {
        return oracleSequenceMapper.selectSequenceNextByName("BUSINESS_SEQ_001");
    }

    @GetMapping(value = "/seq2", produces = MediaType.APPLICATION_JSON_VALUE)
    public int seq2() {
        return oracleSequenceMapper.selectSequenceNextByName("BUSINESS_SEQ_002");
    }

    /**
     * 一次性获取多个序列
     *
     * @param cont cont
     * @return 列表
     */
    @GetMapping(value = "/seqList/{cont}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Long> seqList(@PathVariable Long cont) {
        if (cont == null || cont < 0L) {
            cont = 10L;
        }
        Sequences sequences = new Sequences();
        sequences.setSeqName("BUSINESS_SEQ_001");
        sequences.setSeqCount(cont);
        return oracleSequenceMapper.getSeqList(sequences);
    }

    @GetMapping(value = "/reset", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean resetSequenceByName() {
        try {
            oracleSequenceMapper.resetSequenceByName("BUSINESS_SEQ_001");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @GetMapping(value = "/resetAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean resetAll() {
        try {
            oracleSequenceMapper.resetAllSequences();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


}

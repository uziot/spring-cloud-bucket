package com.uziot.bucket.sequence.exception;

/**
 * @author shidt
 * @version V1.0
 * @className SequencesAlreadyExistsException
 * @date 2020-12-30 22:43:30
 * @description 序列不存在异常
 */
public class SequencesNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SequencesNotFoundException(String sequencesName, Throwable cause) {
        super("sequences: \"" + sequencesName + "\" not found", cause);
    }

    public SequencesNotFoundException(String sequencesName) {
        super("sequences: \"" + sequencesName + "\" not found");
    }
}

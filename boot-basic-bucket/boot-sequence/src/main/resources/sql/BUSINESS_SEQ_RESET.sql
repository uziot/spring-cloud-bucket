-- 根据序列名称传入重置序列
CREATE OR REPLACE PROCEDURE BUSINESS_SEQ_RESET(v_seq_name varchar2) AS
    t_num number(10);
    t_sql varchar2(100);
BEGIN
    -- 执行重置序列
    EXECUTE IMMEDIATE 'select ' || v_seq_name || '.nextval from dual' INTO t_num;
    t_num := -t_num;
    t_sql := 'alter sequence ' || v_seq_name || ' increment by ' || t_num;
    EXECUTE IMMEDIATE t_sql;
    EXECUTE IMMEDIATE 'select ' || v_seq_name || '.nextval from dual' INTO t_num;
    t_sql := 'alter sequence ' || v_seq_name || ' increment by 1';
    EXECUTE IMMEDIATE t_sql;

END BUSINESS_SEQ_RESET;
/
create sequence BUSINESS_SEQ_001
    minvalue 1 --增长最小值
    maxvalue 9999999999 --增长最大值,也可以设置NOMAXvalue -- 不设置最大值
    start with 1 --从101开始计数
    increment by 1 --自增步长为1
    cache 20 --设置缓存cache个序列，如果系统down掉了或者其它情况将会导致序列不连续，也可以设置为---NOCACHE防止跳号
    cycle; --循环,当达到最大值时,不是从start with设置的值开始循环。而是从1开始循环
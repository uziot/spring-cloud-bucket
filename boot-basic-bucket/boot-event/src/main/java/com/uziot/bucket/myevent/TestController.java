package com.uziot.bucket.myevent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shidt
 * @version V1.0
 * @className TestController
 * @date 2022-07-20 20:33:27
 * @description
 */

@RestController
public class TestController {
    @Autowired
    private UserService userService;
    @GetMapping("/user")
    public String userService(HttpServletRequest request) {
        userService.getUser2();
        return "success";
    }

    @GetMapping("/request")
    public String getRequestInfo(HttpServletRequest request) {
        System.out.println("requestListener中的初始化的name数据：" + request.getAttribute("name"));
        return "success";
    }
}

package com.uziot.bucket.myevent;

import org.springframework.context.ApplicationEvent;

/**
 * 功能描述: <br>
 * <> 自定义事件监听
 *
 * @param
 * @author shidt
 * @return
 * @date 2022-07-20 20:30
 */
public class MyEvent extends ApplicationEvent {

    private final User user;

    public MyEvent(Object source, User user) {
        super(source);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
// 省去get、set方法
}
package com.uziot.bucket.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationListener;

/**
 * 功能描述: <br>
 * 这个事件紧随ApplicationReadyEvent
 * 这个事件在任何 application/ command-line runners 调用之后发送。事件之后发送，
 * 状态：ReadinessState.ACCEPTING_TRAFFIC，表示应用可以开始准备接收请求了
 *
 * @author shidt
 * @date 2020/12/16 8:36
 */
@Slf4j
public class OnApplicationEventListener implements ApplicationListener<AvailabilityChangeEvent<?>> {

    @Override
    public void onApplicationEvent(AvailabilityChangeEvent event) {
        log.info("监听到事件：" + event);
        if (ReadinessState.ACCEPTING_TRAFFIC == event.getState()) {
            log.info("应用启动完成，可以请求了……");
        }
    }

}
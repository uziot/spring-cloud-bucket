package com.uziot.bucket.myevent;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2022-07-20 20:29:04
 * @description
 */
import javax.annotation.Resource;

@Service
public class UserService {

    @Resource
    private ApplicationContext applicationContext;

    /**
     * 发布事件
     *
     */
    public User getUser2() {
        User user = new User(1L, "倪升武", "123456");
        // 发布事件
        MyEvent event = new MyEvent(this, user);
        applicationContext.publishEvent(event);
        return user;
    }
}
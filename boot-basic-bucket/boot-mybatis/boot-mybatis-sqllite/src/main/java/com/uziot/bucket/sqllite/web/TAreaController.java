package com.uziot.bucket.sqllite.web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.bucket.sqllite.dao.domain.TArea;
import com.uziot.bucket.sqllite.dao.mapper.TAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className TAreaController
 * @date 2021-02-07 15:02:09
 * @description 简单的测试SqlLite的使用
 */
@RestController
public class TAreaController {

    @Autowired
    private TAreaMapper tAreaMapper;

    @GetMapping(value = "/selectAll/{pageNum}/{pageSize}")
    public PageInfo<TArea> selectArea(@PathVariable Integer pageNum, @PathVariable Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<TArea> areas = tAreaMapper.selectByRecord(null);
        return new PageInfo<>(areas);
    }
}

package com.uziot.bucket.sqllite.dao.mapper;

import com.uziot.bucket.sqllite.dao.domain.TArea;

import java.util.List;

public interface TAreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TArea record);

    int insertSelective(TArea record);

    TArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TArea record);

    int updateByPrimaryKey(TArea record);

    List<TArea> selectByRecord(TArea record);
}
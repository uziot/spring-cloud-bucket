package com.uziot.bucket.sqllite.dao.domain;

public class TArea {
    private Integer id;

    private String caAreaCode;

    private String caAreaName;

    private String caAreaParentCode;

    private String caAreaType;

    private String caAreaSpell;

    private String caZipCode;

    private Integer isShow;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaAreaCode() {
        return caAreaCode;
    }

    public void setCaAreaCode(String caAreaCode) {
        this.caAreaCode = caAreaCode;
    }

    public String getCaAreaName() {
        return caAreaName;
    }

    public void setCaAreaName(String caAreaName) {
        this.caAreaName = caAreaName;
    }

    public String getCaAreaParentCode() {
        return caAreaParentCode;
    }

    public void setCaAreaParentCode(String caAreaParentCode) {
        this.caAreaParentCode = caAreaParentCode;
    }

    public String getCaAreaType() {
        return caAreaType;
    }

    public void setCaAreaType(String caAreaType) {
        this.caAreaType = caAreaType;
    }

    public String getCaAreaSpell() {
        return caAreaSpell;
    }

    public void setCaAreaSpell(String caAreaSpell) {
        this.caAreaSpell = caAreaSpell;
    }

    public String getCaZipCode() {
        return caZipCode;
    }

    public void setCaZipCode(String caZipCode) {
        this.caZipCode = caZipCode;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }
}
package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author shidt
 * 功能介绍
 * MapperScan("com.uziot.uadmin.dao.*.mapper")
 * 指定要扫描的Mapper类的包的路径
 */

@MapperScan("com.uziot.bucket.sqllite.dao.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SqlLiteApplication {
    public static void main(String[] args) {
        SpringApplication.run(SqlLiteApplication.class, args);
    }
}

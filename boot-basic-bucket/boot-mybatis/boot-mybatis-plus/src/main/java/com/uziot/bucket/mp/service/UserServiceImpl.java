package com.uziot.bucket.mp.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uziot.bucket.mp.dao.User;
import com.uziot.bucket.mp.dao.UserMapper;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className BucketAreaService
 * @date 2020-12-21 22:55:36
 * @description
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> {
}

package com.uziot.bucket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MpApplication
 * @date 2020-12-21 22:53:09
 * @description
 */
@MapperScan("com.uziot.bucket.mp.dao")
@SpringBootApplication
public class MpApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(MpApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.uziot.bucket.mp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * @author shidt
 * @version V1.0
 * @className UserMapper
 * @date 2020-11-21 23:24:52
 * @description
 */
public interface UserMapper extends BaseMapper<User> {
    int deleteByPrimaryKey(Long id);

    int insertUser(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}
package com.uziot.bucket.mp.dao;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author shidt
 * @version V1.0
 * @className User
 * @date 2020-11-21 23:24:52
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private String email;
    /**
     * 字段添加填充内容
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    /**
     * 乐观锁Version注解
     */
    @Version
    private Integer version;
    /**
     * 逻辑删除（0 未删除、1 删除）
     */
    @TableLogic
    private Integer deleted;
} 
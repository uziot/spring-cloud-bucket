package test;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.uziot.bucket.MpApplication;
import com.uziot.bucket.mp.dao.User;
import com.uziot.bucket.mp.dao.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className MongoTest
 * @date 2020-12-05 22:42:12
 * @description
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        MpApplication.class
})
@SpringBootConfiguration
public class MpTest {

    @Autowired
    private UserMapper userMapper;


    @Test
    public void testSelect() {
        User user = userMapper.selectByPrimaryKey(8L);
        user.setName("李云龙5");
        userMapper.updateById(user);
        System.out.println(user);
    }

    // 测试插入
    @Test
    public void testInsert() {
        User user = new User();
        user.setName("插入");
        user.setAge(15);
        user.setEmail("310697723@qq.com");

        // 帮我们自动生成id
        int result = userMapper.insertUser(user);
        // 受影响的行数
        System.out.println(result);
        // 看到id会自动填充
        System.out.println(user);
    }

    // 测试乐观锁失败！多线程下
    @Test
    public void testOptimisticLocker2() {

        // 线程 1
        User user = userMapper.selectById(1L);
        user.setName("kwhua111");
        user.setEmail("123456@qq.com");

        // 模拟另外一个线程执行了插队操作
        User user2 = userMapper.selectById(1L);
        user2.setName("kwhua222");
        user2.setEmail("123456@qq.com");
        userMapper.updateById(user2);

        // 自旋锁来多次尝试提交！
        // 如果没有乐观锁就会覆盖插队线程的值！
        userMapper.updateById(user);
    }

    @Test
    public void testPage() {
        // 参数一：当前页
        // 参数二：页面大小
        Page<User> page = new Page<>(2, 2);
        userMapper.selectPage(page, null);
        page.getRecords().forEach(System.out::println);

        System.out.println("分页结果：" + JSON.toJSONString(page));
    }

    /**
     * 测试逻辑删除
     */
    @Test
    public void testDelete() {
        userMapper.deleteById(8L);
    }
}

package com.uziot.bucket.web;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uziot.bucket.dao.domain.PersionHouse;
import com.uziot.bucket.dao.mapper.PersionHouseMapper;
import com.uziot.bucket.service.PersonHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className PersonHoustController
 * @date 2019-12-08 02:32:53
 * @description
 */
@RestController
public class PersonHouseController {
    @Autowired
    private PersionHouseMapper persionHouseMapper;
    @Autowired
    private PersonHouseService personHouseService;

    @GetMapping(value = "/idcard/{idno}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PageInfo<PersionHouse> selectOneByCtfid(@RequestParam(defaultValue = "1") Integer pageNum,
                                                   @RequestParam(defaultValue = "10") Integer pageSize,
                                                   @PathVariable String idno) {
        PageHelper.startPage(pageNum, pageSize);
        List<PersionHouse> houses = persionHouseMapper.selectOneByCtfid(idno);
        return new PageInfo<>(houses);
    }


    @GetMapping(value = "/create")
    public String create() {
        personHouseService.createFile();
        return "OK";
    }
}

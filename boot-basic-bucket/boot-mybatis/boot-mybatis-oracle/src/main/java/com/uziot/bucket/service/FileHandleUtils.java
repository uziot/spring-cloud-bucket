package com.uziot.bucket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * 功能描述: <br>
 *
 * @author shidt
 * @date 2022-06-01 21:14
 */

public class FileHandleUtils {
    private static final Logger log = LoggerFactory.getLogger(FileHandleUtils.class);

    /**
     * 该方法会覆盖文件原内容
     *
     * @param content  内容
     * @param fileName 文件名
     * @param path     路径
     */
    public static boolean writeFile(String content, String fileName, String path) {
        return writeFile(content, fileName, path, false);
    }

    /**
     * @param content  内容
     * @param fileName 文件名
     * @param path     路径
     * @param append   是否覆盖原内容，false覆盖原内容，true不覆盖
     * @return boolean
     */

    public static boolean writeFile(String content, String fileName, String path, boolean append) {
        boolean result = false;
        FileOutputStream fos = null;
        FileChannel channel = null;
        File filePath;

        try {
            //判断路径是否存在
            filePath = new File(path);
            if (!filePath.exists()) {
                boolean mkdirs = filePath.mkdirs();
            }
            //写内容文件
            fos = new FileOutputStream(new File(path + fileName), append);
            channel = fos.getChannel();
            ByteBuffer src = Charset.forName("gbk").encode(content);
            channel.write(src);
            result = true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("生成文件异常：", e);
            }
        } finally {
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    log.error("关闭文件输出关闭异常:", e);
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    log.error("关闭文件输出关闭异常:", e);
                }
            }
        }
        return result;
    }

    /**
     * 删除文件
     *
     * @param pathFileName 路径+文件名
     * @return true: 删除成功
     */
    public static boolean deleteFile(String pathFileName) {
        boolean result = false;
        File file = new File(pathFileName);
        if (file.exists()) {
            result = file.delete();
        } else {
            result = true;
        }
        return result;
    }

    /**
     * @param pathFileName 路径+文件名
     * @return 返回文件大小，以 Byte 为单位
     * @throws FileNotFoundException
     */
    public static long getFileLength(String pathFileName) throws FileNotFoundException {
        long len = 0;
        File file = new File(pathFileName);
        if (!file.exists()) {
            throw new FileNotFoundException(pathFileName + " 目标文件不存在");
        } else {
            len = file.length();
        }
        return len;
    }
}

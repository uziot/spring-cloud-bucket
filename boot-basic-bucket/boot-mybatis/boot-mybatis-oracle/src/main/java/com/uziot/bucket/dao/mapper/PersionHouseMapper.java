package com.uziot.bucket.dao.mapper;


import com.uziot.bucket.dao.domain.PersionHouse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className LoginUser
 * @date 2020-11-16 23:48:56
 * @description mapper
 */
@Mapper
public interface PersionHouseMapper {
    /**
     * 测试方法
     *
     * @return 列表1个
     */
    int insert(PersionHouse record);

    /**
     * 测试方法
     *
     * @return 列表1个
     */
    int insertSelective(PersionHouse record);

    /**
     * 测试方法
     *
     * @return 列表1个
     */
    List<PersionHouse> selectOne();

    /**
     * 测试方法
     *
     * @return 列表1个
     */
    List<PersionHouse> selectOneByCtfid(@Param("ctfid") String ctfid);

    /**
     * 分块查询需要抽取的数据
     *
     * @return list
     */
    List<Map<String, Object>> selectList(@Param("startIndex") Integer startIndex,
                                         @Param("rowNum") Integer rowNum);

    /**
     * 查询总条数
     *
     * @return 符合抽取数据的总数
     */
    Integer selectCount();
}
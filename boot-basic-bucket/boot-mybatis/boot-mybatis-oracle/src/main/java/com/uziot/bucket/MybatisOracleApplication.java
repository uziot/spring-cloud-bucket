package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author shidt
 * @version V1.0
 * @className MybatisOracleAppcication
 * @date 2020-12-17 23:22:45
 * @description
 */

@EnableAspectJAutoProxy
@SpringBootApplication
public class MybatisOracleApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybatisOracleApplication.class, args);
    }
}

package com.uziot.bucket.dao.domain;
import	java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * 功能描述: <br>
 * 酒店信息表实体类
 * @author shidt
 * @date 2019/12/14 21:01
 */
@Data
public class PersionHouse implements Serializable{
    private String name;

    private String cardno;

    private String descriot;

    private String ctftp;

    private String ctfid;

    private String gender;

    private String birthday;

    private String address;

    private String zip;

    private String dirty;

    private String district1;

    private String district2;

    private String district3;

    private String district4;

    private String district5;

    private String district6;

    private String firstnm;

    private String lastnm;

    private String duty;

    private String mobile;

    private String tel;

    private String fax;

    private String email;

    private String nation;

    private String taste;

    private String education;

    private String company;

    private String ctel;

    private String caddress;

    private String czip;

    private String family;

    private Date version;

    private String idData;

    private BigDecimal cid;
}
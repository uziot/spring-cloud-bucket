package com.uziot.boot.mybatis.mapper;

import com.uziot.boot.mybatis.core.CrudMapper;
import com.uziot.boot.mybatis.domain.BucketArea;

import java.util.List;
/**
 * @author shidt
 * @version V1.0
 * @className BucketAreaMapper
 * @date 2020-12-17 23:22:45
 * @description DAO支持类实现
 */
public interface BucketAreaMapper extends CrudMapper<BucketArea> {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(BucketArea record);

    BucketArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BucketArea record);

    int updateByPrimaryKey(BucketArea record);

    List<BucketArea> selectAllArea();

    List<BucketArea> selectByRecord(BucketArea record);

}
package com.uziot.boot.mybatis.core;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className MybatisOracleAppcication
 * @date 2020-12-17 23:22:45
 * @description Service基类
 */

@Transactional(readOnly = true)
public abstract class CrudService<M extends CrudMapper<T>, T extends DataEntity> {

    /**
     * 持久层对象
     */
    @Autowired
    private M crudMapper;

    /**
     * 获取mapper
     *
     * @return D
     */
    public M getMapper() {
        return crudMapper;
    }

    /**
     * 获取单条数据
     *
     * @param id 主键
     * @return 数据实体
     */
    public T get(String id) {
        return crudMapper.selectByPkId(Integer.valueOf(id));
    }

    /**
     * 获取单条数据
     *
     * @param entity 实体对象
     * @return 实体对象
     */
    public T get(T entity) {
        return crudMapper.selectOne(entity);
    }

    /**
     * 查询列表数据
     *
     * @param entity 实体对象
     * @return 实体对象列表
     */
    public List<T> findList(T entity) {
        return crudMapper.findList(entity);
    }

    /**
     * 查询列表数据
     *
     * @param queryMap 查询条件
     * @return 实体对象列表
     */
    public List<T> queryList(Map<String, Object> queryMap) {
        return crudMapper.queryList(queryMap);
    }

    /**
     * 查询分页数据
     *
     * @param page   分页对象
     * @param entity 实体对象
     * @return 分页数据
     */
    public PageInfo<T> findPage(Page page, T entity) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getOrderBy());
        List<T> list = crudMapper.findList(entity);
        return new PageInfo<>(list);
    }

    /**
     * 查询分页数据
     *
     * @param page     分页对象
     * @param queryMap 查询条件
     * @return 分页数据
     */
    public PageInfo<T> queryPage(Page page, Map<String, Object> queryMap) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getOrderBy());
        List<T> list = crudMapper.queryList(queryMap);
        return new PageInfo<>(list);
    }

    /**
     * 保存数据（插入或更新）
     *
     * @param entity 实体对象
     * @return 实体对象
     */
    @Transactional(readOnly = false)
    public T save(T entity) {
        if (entity.getIsNewRecord()) {
            entity.preInsert();
            crudMapper.insert(entity);
        } else {
            entity.preUpdate();
            crudMapper.update(entity);
        }
        return entity;
    }

    /**
     * 删除数据
     *
     * @param entity 实体对象
     */
    @Transactional(readOnly = false)
    public void delete(T entity) {
        crudMapper.delete(entity);
    }

    /**
     * 删除数据
     *
     * @param id 主键
     */
    @Transactional(readOnly = false)
    public void deleteById(String id) {
        crudMapper.deleteById(id);
    }

}

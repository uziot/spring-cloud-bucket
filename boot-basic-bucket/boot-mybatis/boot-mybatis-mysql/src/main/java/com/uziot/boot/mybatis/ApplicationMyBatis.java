package com.uziot.boot.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author shidt
 * 功能介绍
 * MapperScan("com.uziot.uadmin.dao.*.mapper")
 * 指定要扫描的Mapper类的包的路径
 */

@MapperScan("com.uziot.boot.mybatis.mapper")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ApplicationMyBatis {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationMyBatis.class, args);
    }
}

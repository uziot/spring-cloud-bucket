package com.uziot.boot.mybatis.controller;

import com.uziot.boot.mybatis.core.CrudService;
import com.uziot.boot.mybatis.domain.BucketArea;
import com.uziot.boot.mybatis.mapper.BucketAreaMapper;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className AreaServiceImpl
 * @date 2021-01-10 16:11:20
 * @description
 */
@Service
public class AreaServiceImpl extends CrudService<BucketAreaMapper, BucketArea> {
}

import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2021-10-30 20:43:51
 * @description
 */

public class Test1 {
    public static void main(String[] args) throws IOException {
        Process ipconfig = Runtime.getRuntime().exec("ipconfig");
        String string = StreamUtils.copyToString(ipconfig.getInputStream(), StandardCharsets.UTF_8);
        System.out.println(string);
    }
}

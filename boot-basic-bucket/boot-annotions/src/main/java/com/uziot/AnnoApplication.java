package com.uziot;

import com.uziot.boot.annotions.AnoAutoOriginBean;
import com.uziot.boot.enableannotion.EnableCustomFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className AnnoApplication
 * @date 2021-10-30 12:28:13
 * @description
 */

@EnableCustomFeature
@RestController
@SpringBootApplication
public class AnnoApplication {

    @Autowired
    private AnoAutoOriginBean anoAutoOriginBean;
    @GetMapping(value = "/hello")
    public String hello(){
        return  anoAutoOriginBean.print();
    }

    public static void main(String[] args) {
        SpringApplication.run(AnnoApplication.class,args);
    }
}

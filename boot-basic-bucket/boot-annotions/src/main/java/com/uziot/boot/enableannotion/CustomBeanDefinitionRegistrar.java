package com.uziot.boot.enableannotion;

import com.uziot.boot.enableannotion.comps.TestUserCmp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author shidt
 * @version V1.0
 * @className CustomBeanDefinitionRegistrar
 * @date 2022-03-25 20:24:52
 * @description 通过单个对象手动注入BeanDefinition的方式注册对象
 */

@Slf4j
public class CustomBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    /**
     * 根据注解值动态注入资源服务器的相关属性
     *
     * @param metadata 注解信息
     * @param registry 注册器
     */
    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        BeanDefinitionBuilder beanDefinitionBuilder =
                BeanDefinitionBuilder.genericBeanDefinition(TestUserCmp.class);
        registry.registerBeanDefinition("testUserCmp",
                beanDefinitionBuilder.getBeanDefinition());
    }

}

package com.uziot.boot.annotions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnoAutoOriginBean {
    @Autowired
    private AutoBean autoBean;

    public AnoAutoOriginBean() {
        System.out.println("AnoAutoOriginBean init: " + System.currentTimeMillis());
    }

    public String print() {
        System.out.println("[AnoAutoOriginBean] print！！！ autoBean == null ? " + (autoBean == null));
        return "[AnoAutoOriginBean] print！！！ autoBean == null ? " + (autoBean == null);
    }
}
package com.uziot.boot.enableannotion.comps;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author shidt
 * @version V1.0
 * @className TestUserCmp
 * @date 2022-03-25 20:52:31
 * @description
 */

public class TestUserCmp {
    @Autowired
    private TestRoleCmp testRoleCmp;

    public void test1(){
        testRoleCmp.test1();
    }

}

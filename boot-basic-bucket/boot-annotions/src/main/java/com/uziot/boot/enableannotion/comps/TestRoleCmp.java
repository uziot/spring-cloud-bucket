package com.uziot.boot.enableannotion.comps;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shidt
 * @version V1.0
 * @className TestRoleService
 * @date 2022-03-25 20:49:29
 * @description
 */

@Slf4j
public class TestRoleCmp {
    public TestRoleCmp() {
        log.info("===========>TestRoleCmp构造函数初始化");
    }

    public void test1() {
        log.info("===========>test1被调用");
    }
}

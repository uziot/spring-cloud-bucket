package com.uziot.boot.annotions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class OriginBean {

    private LocalDateTime time;

    public OriginBean() {
        time = LocalDateTime.now();
    }

    public String print(String msg) {
        System.out.println("[OriginBean] print msg: " + msg + ", time: " + time);
        return "[OriginBean] print msg: " + msg + ", time: " + time;
    }
}

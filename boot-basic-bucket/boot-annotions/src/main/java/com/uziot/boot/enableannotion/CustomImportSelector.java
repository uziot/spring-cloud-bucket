package com.uziot.boot.enableannotion;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author shidt
 * @version V1.0
 * @className SelfImportSelector
 * @date 2022-03-25 20:24:52
 * @description 通过扫描包的方式注册实例化范围
 */

public class CustomImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.uziot.boot.enableannotion.comps.TestSelectorImport"};
    }
}
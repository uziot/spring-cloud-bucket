package com.uziot.boot.enableannotion.comps;

import lombok.Data;

/**
 * @author shidt
 * @version V1.0
 * @className TestSelectorImport
 * @date 2022-03-25 23:05:53
 * @description
 */

@Data
public class TestSelectorImport {
    public String aaa;
    public String bbb;

}

package com.uziot.boot.annotions;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 *
 * @author shidt
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 模块
     */
    String title() default "";

    /**
     * 是否保存请求的参数
     */
    boolean isSaveRequestData() default true;
}

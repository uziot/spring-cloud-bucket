package com.uziot.boot.enableannotion;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author shidt
 * @version V1.0
 * @className EnableCustomFeature
 * @date 2022-03-25 20:24:52
 * @description 自定义一个开关注解
 */

@Documented
@Inherited
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({CustomAutoConfiguration.class, CustomBeanDefinitionRegistrar.class,CustomImportSelector.class})
public @interface EnableCustomFeature {
}

package com.uziot.boot.enableannotion;

import com.uziot.boot.enableannotion.comps.TestRoleCmp;
import org.springframework.context.annotation.Bean;

/**
 * @author shidt
 * @version V1.0
 * @className CustomAutoConfiguration
 * @date 2022-03-25 20:24:52
 * @description 通过导入配置清单的方式注册对象
 */

public class CustomAutoConfiguration {

    @Bean
    public TestRoleCmp testRoleCmp() {
        return new TestRoleCmp();
    }
}

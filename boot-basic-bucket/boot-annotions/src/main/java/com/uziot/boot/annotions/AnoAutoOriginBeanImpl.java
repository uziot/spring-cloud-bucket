package com.uziot.boot.annotions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author shidt
 * @version V1.0
 * @className AnoAutoOriginBeanImpl
 * @date 2021-10-31 00:28:47
 * @description
 */

@Log
@Component
public class AnoAutoOriginBeanImpl extends AnoAutoOriginBean {
    @Autowired
    private OriginBean originBean;


    public AnoAutoOriginBeanImpl() {
        System.out.println("AnoAutoOriginBeanImpl init: " + System.currentTimeMillis());
    }

    @Override
    public String print() {
        originBean.print("originBean==>自动注入");
        System.out.println("[AnoAutoOriginBeanImpl] print！！！ autoBean == null ? " + (originBean == null));
        return "[AnoAutoOriginBeanImpl] print！！！ autoBean == null ? " + (originBean == null);
    }
}

package com.uziot.bucket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shidt
 * @version V1.0
 * @className UploadFileInfoDto
 * @date 2022-10-05 23:02:31
 * @description 文件上传信息
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UploadFileInfoDto {


    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 当前分片
     */
    private Integer currentChunk;

    /**
     * 总分片
     */
    private Integer chunks;


}

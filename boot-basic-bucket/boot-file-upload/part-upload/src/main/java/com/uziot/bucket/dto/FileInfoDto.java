package com.uziot.bucket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shidt
 * @version V1.0
 * @className FileInfoDto
 * @date 2022-10-05 23:02:31
 * @description  文件信息
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FileInfoDto {
    /**
     * 文件大小
     */
    private long fileSize;
    /**
     * 文件名称
     */
    private String fileName;

}

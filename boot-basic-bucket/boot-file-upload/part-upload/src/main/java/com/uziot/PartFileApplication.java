package com.uziot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className PartFileApplication
 * @date 2020-12-02 14:47:24
 * @description
 */
@SpringBootApplication
public class PartFileApplication {
    public static void main(String[] args) {
        SpringApplication.run(PartFileApplication.class, args);
    }
}

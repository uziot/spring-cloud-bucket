package com.uziot.bucket.controller;


import com.uziot.bucket.service.PartFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shidt
 * @version V1.0
 * @className PartFileController
 * @date 2022-10-05 23:02:31
 * @description 文件分段传输上传测试控制
 */

@RestController
public class PartFileController {


    @Autowired
    private PartFileService partFileService;


    /**
     * 单个文件上传，支持断点续传
     */
    @PostMapping("/upload")
    public void upload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        partFileService.upload(request, response);
    }

    /**
     * 文件下载
     */
    @GetMapping("/download")
    public void download(HttpServletRequest request, HttpServletResponse response) throws IOException {
        partFileService.download(request, response);
    }

    /**
     * 分片下载
     */
    @GetMapping("/slice-download")
    public String sliceDownload() throws IOException {
        partFileService.sliceDownload();
        return "success";
    }

}

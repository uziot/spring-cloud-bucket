package com.uziot.bucket.file.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author shidt
 * @version V1.0
 * @className FilesUploadController
 * @date 2021-03-19 14:18:07
 * @description
 */

@Controller
public class FilesUploadController {

    @PostMapping("/uploads")
    @ResponseBody
    public String create(@RequestPart MultipartFile[] files) throws IOException {
        StringBuilder message = new StringBuilder();

        String storagePath = "D:\\app\\";

        for (MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            String filePath = storagePath + fileName;

            File dest = new File(filePath);
            Files.copy(file.getInputStream(), dest.toPath());
            message.append("Upload file success : ")
                    .append(dest.getAbsolutePath()).append("<br>");
        }
        return message.toString();
    }
}

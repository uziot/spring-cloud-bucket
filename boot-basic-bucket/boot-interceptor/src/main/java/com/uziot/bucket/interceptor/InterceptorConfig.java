package com.uziot.bucket.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className InterceptorConfig
 * @date 2020-03-14 13:48:02
 * @description
 */

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private ResourceAuthInterceptor authenticationInterceptor;

    @Bean
    public ResourceAuthInterceptor authenticationInterceptor() {
        return new ResourceAuthInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 放行URL策略,放行的接口不走拦截器
        List<String> execUrl = Arrays.asList("/aaa", "bbb");
        ArrayList<String> urls = new ArrayList<>(execUrl);
        urls.add("/user/**");
        urls.add("/cache/cleanCache");
        registry.addInterceptor(authenticationInterceptor)
                .excludePathPatterns(urls);
    }
}
package com.uziot.bucket.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className InterceptorController
 * @date 2020-12-12 00:06:24
 * @description
 */
@RestController
public class InterceptorController {
    @GetMapping(value = "/user/a")
    public Object test1() {
        return "用户1";
    }

    @GetMapping(value = "/admin/a")
    public Object test2() {
        return "管理1";
    }
}

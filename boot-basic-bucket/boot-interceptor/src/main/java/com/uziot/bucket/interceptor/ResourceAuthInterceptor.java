package com.uziot.bucket.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author shidt
 * @version V1.0
 * @className AuthenticationInterceptor
 * @date 2020-03-14 13:42:15
 * @description 资源验证拦截器
 */
@Slf4j
@SuppressWarnings("ALL")
public class ResourceAuthInterceptor implements HandlerInterceptor {

    /**
     * 拦截器返回true表示放行，返回false表示拦截禁止访问
     *
     * @param httpServletRequest  httpServletRequest
     * @param httpServletResponse httpServletResponse
     * @param object              object
     * @return boolean
     * @throws Exception Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,
                             HttpServletResponse httpServletResponse, Object object) throws Exception {
        String requestUrI = httpServletRequest.getRequestURI();
        log.info("当前请求接口地址为：{}", requestUrI);
        return true;
    }

}
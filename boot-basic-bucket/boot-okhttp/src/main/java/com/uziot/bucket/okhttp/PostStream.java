package com.uziot.bucket.okhttp;

import com.squareup.okhttp.*;
import okio.BufferedSink;

import java.io.IOException;

/**
 * @author Lenovo
 */
public class PostStream {
    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient();
        final MediaType mediaType = MediaType.parse("text/plain");
        final String postBody = "Hello World";

        RequestBody requestBody = new RequestBody() {
            @Override
            public MediaType contentType() {
                return mediaType;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                sink.writeUtf8(postBody);
            }

            @Override
            public long contentLength() throws IOException {
                return postBody.length();
            }
        };

        Request request = new Request.Builder()
                .url("http://www.baidu.com")
                .post(requestBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("服务器端错误: " + response);
        }

        System.out.println(response.body().string());
    }
}

package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className AdviceApplication
 * @date 2020-12-01 23:40:35
 * @description
 */
@SpringBootApplication
public class AdviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdviceApplication.class, args);
    }
}

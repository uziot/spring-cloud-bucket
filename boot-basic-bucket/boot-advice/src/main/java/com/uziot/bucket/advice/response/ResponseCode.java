package com.uziot.bucket.advice.response;

/**
 * 功能描述: <br>
 * 返回提示信息和返回码
 *
 * @author shidt
 * @date 2020-01-01 0:21
 */
public enum ResponseCode {
    /**
     * a
     */
    SUCCESS("0", "成功"),
    UNKNOWN("-1", "未知的错误");

    private final String code;
    private final String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    private ResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
package com.uziot.bucket.advice.response;

import java.lang.annotation.*;

/**
 * 功能描述: <br>
 * 定义一个注解，给控制器使用这个注解，将不使用统一返回样式，
 * 而是会使用当前返回对象的转换样式
 *
 * @author shidt
 * @date 2020-03-14 13:01
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
public @interface NativeResponse {
    String value() default "0";
}

package com.uziot.bucket.advice.web;

import com.alibaba.fastjson.JSONObject;
import com.uziot.bucket.advice.response.DefaultResponse;
import com.uziot.bucket.advice.response.NativeResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author shidt
 * @version V1.0
 * @className AdviceController
 * @date 2020-12-01 23:51:06
 * @description
 */
@RestController
public class AdviceController {
    @GetMapping(value = "/advice")
    public JSONObject apiAdvice() {
        JSONObject jsonObject = new JSONObject();
        String aaa = null;
        jsonObject.put("user", "zhang");
        jsonObject.put("mobi", "zhang");
        jsonObject.put("zhang", aaa);
        jsonObject.put("age", new Date());
        jsonObject.put("response", DefaultResponse.builder().code("300").msg(null).build());

        return jsonObject;
    }

    @NativeResponse
    @GetMapping(value = "/advice1")
    public JSONObject apiAdvice1() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", "zhang");
        jsonObject.put("mobi", "zhang");

        return jsonObject;
    }
}

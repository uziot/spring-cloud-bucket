package com.uziot.bucket.advice.response.result;

/**
 * 功能描述: <br>
 * 统一返回码
 *
 * @author shidt
 * @date 2020-03-09 0:35
 */
public class ResultCode {
    public static final Integer SUCCESS = 200;
    public static final Integer ERROR = 201;
    public static final Integer UNKNOWERROR = 202;
}

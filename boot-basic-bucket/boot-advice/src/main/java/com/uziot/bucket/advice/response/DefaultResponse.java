package com.uziot.bucket.advice.response;


import lombok.Getter;
import lombok.Setter;


/**
 * 功能描述: <br>
 * 默认返回提示信息
 *
 * @author shidt
 * @date 2020-01-01 0:21
 */
@Setter
@Getter
@SuppressWarnings("rawtypes")
public class DefaultResponse<T> {
    private String code = "0";
    private String msg = "ok";
    private T data;

    public DefaultResponse() {
    }

    public DefaultResponse(ResponseBuilder<T> builder) {
        this.code = builder.code;
        this.msg = builder.msg;
        this.data = builder.data;
    }

    public static ResponseBuilder builder() {
        return new ResponseBuilder();
    }

    public static class ResponseBuilder<T> {
        private String code = ResponseCode.SUCCESS.getCode();
        private String msg = ResponseCode.SUCCESS.getMessage();
        private T data;

        public ResponseBuilder code(String code) {
            this.code = code;
            return this;
        }

        public ResponseBuilder msg(String msg) {
            this.msg = msg;
            return this;
        }

        public ResponseBuilder data(T data) {
            this.data = data;
            return this;
        }

        public ResponseBuilder glue(ResponseCode responseCode) {
            this.code = responseCode.getCode();
            this.msg = responseCode.getMessage();
            return this;
        }

        public DefaultResponse build() {
            return new DefaultResponse<>(this);
        }
    }
}

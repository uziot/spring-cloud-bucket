package com.uziot.bucket;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;

/**
 * @author shidt
 * @version V1.0
 * @className DockerApplication
 * @date 2021-06-02 09:51:26
 * @description
 */

@Slf4j
@RestController
@SpringBootApplication
public class DockerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DockerApplication.class, args);
    }

    @GetMapping("/query")
    public JSONObject query() {
        log.info("接收查询请求:>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        JSONObject result = new JSONObject();
        result.put("code", "000000");
        result.put("msg", "请求查询成功！");
        result.put("data", new SecureRandom().nextInt());
        return result;
    }

    @GetMapping("/hello")
    public JSONObject hello() {
        log.info("接收到请求:>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        JSONObject result = new JSONObject();
        result.put("code", "000000");
        result.put("msg", "请求成功！");
        result.put("data", "");
        return result;
    }

    @PostMapping("/docker")
    public JSONObject docker(@RequestBody JSONObject request) {
        log.info("接收到请求:{}", request);
        JSONObject result = new JSONObject();
        result.put("code", "000000");
        result.put("msg", "请求成功！");
        result.put("data", request);
        return result;
    }
}

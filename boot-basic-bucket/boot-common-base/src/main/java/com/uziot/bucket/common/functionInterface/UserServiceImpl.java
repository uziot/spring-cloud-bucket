package com.uziot.bucket.common.functionInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className UserServiceImpl
 * @date 2021-01-29 09:05:58
 * @description
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserService userService;

    @Override
    public <T> T executeByExecutor(UserExecutor<UserService, T> u) {
        return u.execute(userService);
    }

    @Override
    public void print() {
        System.out.println("测试");
    }

    public void test1() {
        String o = this.executeByExecutor(u -> {
            u.print();
            return "简单Lambda返回测试！";
        });
        this.executeByExecutor(userService1 -> "");
        UserService userService = this.build(1, u -> {
            u.print();
            return u;
        });
    }

    public <T> T build(int aaa, UserExecutor<UserService, T> u) {
        if (aaa == 1) {
            System.out.println("你好");
        }
        return u.execute(userService);
    }

}

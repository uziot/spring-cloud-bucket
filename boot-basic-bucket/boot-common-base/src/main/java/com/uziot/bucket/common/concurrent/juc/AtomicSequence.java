package com.uziot.bucket.common.concurrent.juc;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shidt
 * @version V1.0
 * @className AtomicSeq
 * @date 2021-05-18 23:27:07
 * @description
 */

public class AtomicSequence {
    /**
     * Atomic
     */
    private static final AtomicInteger AI = new AtomicInteger(0);

    /**
     * 序列最大值 暂时按照5000万统计
     */
    private static final int MAX_VALUE = 10000 * 5000;

    /**
     * 序列 自增（保证原子性 ）(高效)
     */

    public static int getSequence() {
        int next = AI.incrementAndGet();
        if (next > MAX_VALUE) {
            synchronized (AI) {
                if (AI.get() > MAX_VALUE) {
                    AI.set(0);
                }
                next = AI.incrementAndGet();
            }
        }
        return next;
    }

}

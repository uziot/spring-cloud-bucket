package com.uziot.bucket.common.util.safe;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * @author shidt
 * @version V1.0
 * @className DigitUtil
 * @date 2019-12-06 16:20:07
 * @description
 */

public class DigitUtil {
    /**
     * 判断一个字符串是否都为数字
     * @param strNum 判断一个字符串是否都为数字
     * @return 判断一个字符串是否都为数字
     */
    public static boolean isDigit0(String strNum) {
        return strNum.matches("[0-9]{1,}");
    }

    /**
     * 判断一个字符串是否都为数字
     * @param strNum 判断一个字符串是否都为数字
     * @return 判断一个字符串是否都为数字
     */
    public static boolean isDigit(String strNum) {
        Pattern pattern = compile("[0-9]{1,}");
        Matcher matcher = pattern.matcher((CharSequence) strNum);
        return matcher.matches();
    }

    /**
     * 截取数字
     * @param content 内容
     * @return 内容
     */
    public static String getNumbers(String content) {
        Pattern pattern = compile("\\d+");
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    /**
     * 截取非数字
     * @param content 内容
     * @return 处理后
     */
    public static String splitNotNumber(String content) {
        Pattern pattern = compile("\\D+");
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    /**
     * 判断一个字符串是否含有数字
     * @param content 内容
     * @return 是否
     */
    public static boolean hasDigit(String content) {
        boolean flag = false;
        Pattern p = compile(".*\\d+.*");
        Matcher m = p.matcher(content);
        if (m.matches()) {
            flag = true;
        }
        return flag;
    }

/*    public static void main(String[] args) {
        String name = "张三1";
        boolean b = hasDigit(name);
        System.out.println(b);
    }*/
}

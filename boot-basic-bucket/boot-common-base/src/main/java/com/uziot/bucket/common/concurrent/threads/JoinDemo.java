package com.uziot.bucket.common.concurrent.threads;

/**
 * @author shidt
 * @version V1.0
 * @className JoinDemo
 * @date 2021-05-31 14:20:04
 * @description
 */

public class JoinDemo extends Thread {
    public static void main(String[] args) throws InterruptedException {
        JoinDemo demo = new JoinDemo();
        Thread t = new Thread(demo);

        t.start();
        for (int i = 0; i < 1000; i++) {
            if (50 == i) {
                // 当i在50的时候，让新线程执行完成之后，再执行主线程
                t.join(); //main阻塞...
            }
            System.out.println("main...." + i);
        }
    }


    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("join...." + i);
        }
    }
}

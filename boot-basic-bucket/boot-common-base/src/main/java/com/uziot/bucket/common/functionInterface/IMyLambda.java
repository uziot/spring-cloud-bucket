package com.uziot.bucket.common.functionInterface;

/**
 * @author DELL
 */
@FunctionalInterface
public interface IMyLambda {
    String buildMess();
}
package com.uziot.bucket.common.dcl;

/**
 * @author shidt
 * @version V1.0
 * @className DoubleCheckLocking
 * @date 2019-09-13 00:45:32
 * @description 此处示范为DCL单例模式的使用
 */

public class DoubleCheckLocking {

    /**
     * 单例操作对象成员变量线程可见
     */
    private static volatile DoubleCheckLocking instance;

    /**
     * 1、DCL要求构造器私有化
     */
    private DoubleCheckLocking() {
    }

    /**
     * 2、DCL提供获取对象方法
     *
     * @return 当前对象
     */
    public static DoubleCheckLocking getInstance() {
        // 3、再避免多线程已存在对象的等待
        if (null != instance) {
            return instance;
        }
        // 4、线程安全的构造对象
        synchronized (DoubleCheckLocking.class) {
            // 5、由于构造对象可能发生指令重排，因此需要声明当前instance为线程可见。
            if (null == instance) {
                instance = new DoubleCheckLocking();
            }
        }
        return instance;
    }

    /**
     * 直接new对象
     *
     * @return instance
     */
    public DoubleCheckLocking simpleNew() {
        return new DoubleCheckLocking();
    }

}

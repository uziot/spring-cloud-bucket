package com.uziot.bucket.common.queue;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author shidt
 * @version V1.0
 * @className QueueDemo
 * @date 2021-05-18 23:04:40
 * @description
 */

public class QueueDemo {
    public static void main(String[] args) {
        // 普通队列(一端进另一端出)
        Queue<String> queue = new LinkedList<>();
        Deque<String> deque = new LinkedList<>();

        // Queue 中 add() 和 offer()都是用来向队列添加一个元素。
        // 在容量已满的情况下，add() 方法会抛出IllegalStateException异常，offer() 方法只会返回 false 。
        // 头部添加
        queue.add("aaa");
        deque.addFirst("aaa");

        // 尾部添加
        queue.offer("bbb");
        deque.addLast("bbb");

        // Queue 中 remove() 和 poll()都是用来从队列头部删除一个元素。
        // 在队列元素为空的情况下，remove() 方法会抛出NoSuchElementException异常，poll() 方法只会返回 null 。
        // 移除头部
        queue.remove();
        deque.removeFirst();

        // 头部出栈
        String poll1 = queue.poll();
        String poll2 = deque.pollFirst();

        // Queue 中 element() 和 peek()都是用来返回队列的头元素，不删除。
        // 在队列元素为空的情况下，element() 方法会抛出NoSuchElementException异常，
        // 获取首个
        String element = queue.element();
        String first = deque.getFirst();

        // peek() 方法只会返回 null。
        String peek1 = queue.peek();
        String peek2 = deque.peekFirst();

        // 双端队列(两端都可进出)
        Deque<String> deque2 = new LinkedList<>();
        // 堆栈
        Deque<String> deque3 = new LinkedList<>();

        // 堆栈
        Stack<Object> stack = new Stack<>();

    }
}

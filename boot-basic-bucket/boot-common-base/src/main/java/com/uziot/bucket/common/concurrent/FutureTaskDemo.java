package com.uziot.bucket.common.concurrent;

import com.uziot.bucket.common.pool.ThreadPoolFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * 设置方法执行的超时时间，超过指定的时间里没有执行完毕，则抛出异常。
 * ******注意：最后得把FutureTask给cancel掉，如果FutureTask代码块里，包含有try catch代码块，
 * 这时没有把FutureTask给cancel掉话，会一直执行直到任务执行完毕。会导致设置的任务超时不生效的情况。
 * 问题： 线程池最好不用newSingleThreadExecutor，高并发访问下有可能会导致数据不对？？
 *
 * @author shidt
 */
public class FutureTaskDemo {

    public void futureTaskDemo() {
        //2、创建一个执行任务
        FutureTask<String> future = new FutureTask<>(() -> {
            //业务逻辑
            for (int i = 0; i < 10; i++) {
                try {
                    int j = 1 / 0;
                } catch (Exception e) {
                    System.out.println(e);
                }
                Thread.sleep(1000);
            }
            System.out.println("***********************");
            return "success";
        });

        try {
            //3、执行任务
            ThreadPoolFactory.threadPoolTaskExecutor().execute(future);
            //4、任务5秒钟后没有执行完毕则抛出异常
            String result = future.get(5, TimeUnit.SECONDS);
            System.out.println("result:" + result);
        } catch (TimeoutException e) {
            System.out.println("执行任务超时了!");
//			return;
        } catch (Exception e) {
            System.out.println("fail:" + e);
        } finally {
            System.out.println("cancel");
            //6、cancel任务
            future.cancel(true);
//			executorService.shutdown();
        }
        System.out.println("FutureTask end.................");
    }


    //测试
    public static void main(String[] args) {
        FutureTaskDemo demo = new FutureTaskDemo();

        Runnable runnable1 = () -> {
            for (int i = 0; i < 2; i++) {
                System.out.println("Runnable1 start");
                demo.futureTaskDemo();
                System.out.println("Runnable1 end");
            }
        };
        Runnable runnable2 = () -> {
            for (int i = 0; i < 2; i++) {
                System.out.println("Runnable2 start");
                demo.futureTaskDemo();
                System.out.println("Runnable2 end");
            }
        };
        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        thread1.start();
        thread2.start();

    }

}
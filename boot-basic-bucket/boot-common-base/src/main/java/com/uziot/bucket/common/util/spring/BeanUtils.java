package com.uziot.bucket.common.util.spring;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.1
 * @className ObjectUtil
 * @date 2018-01-03 11:27:38
 * @description 主要实现了几个转换的功能，此类需要依赖FastJSON包以及依赖,需要依赖apache的FileUpload
 * 主要对Spring框架的BeanUtils做了新的拓展
 */
@Slf4j
public class BeanUtils extends org.springframework.beans.BeanUtils {

    /**
     * 将List<Map<String,String>对象转换成泛型对象，实现了HashMap到对象的转换，
     * 返回List<泛型对象> ，将map中的k,v分别和对象指定类型中的属性值对应
     * 转换成对象的属性
     *
     * @param mapList List<Map<String,String>对象
     * @param clazz   泛型类名
     * @param <T>     泛型
     * @return List<泛型对象>
     */
    public static <T> List<T> listMap2Object(List<Map<String, String>> mapList, Class<T> clazz) {
        ArrayList<T> resultList = new ArrayList<>();
        for (Map<String, String> m : mapList) {
            T t = map2Object(m, clazz);
            resultList.add(t);
        }
        return resultList;
    }

    public static <T> List<T> listMap2ObjectInt(List<Map<String, Integer>> mapList, Class<T> clazz) {
        ArrayList<T> resultList = new ArrayList<>();
        for (Map<String, Integer> m : mapList) {
            T t = map2ObjectInt(m, clazz);
            resultList.add(t);
        }
        return resultList;
    }

    /**
     * 将Map<String, String> 转换成指定类型的class对象
     * 主要用户少量的赋值转换，简化代码属性拷贝
     *
     * @param map   map对象
     * @param clazz class泛型
     * @param <T>   泛型
     * @return 对象
     */
    public static <T> T map2Object(Map<String, String> map, Class<T> clazz) {
        String s = JSON.toJSONString(map);
        return JSON.parseObject(s, clazz);
    }

    public static <T> T map2ObjectInt(Map<String, Integer> map, Class<T> clazz) {
        String s = JSON.toJSONString(map);
        return JSON.parseObject(s, clazz);
    }

    /**
     * List对象属性拷贝，带有泛型或者自定义属性的不支持拷贝,
     * 但可以支持泛型取出拷贝从新赋值
     *
     * @param list  list
     * @param clazz 返回类型
     * @param <T>   泛型
     * @return 列表
     */
    public static <T> List<T> listCopyProperties(List<?> list, Class<T> clazz) {
        ArrayList<T> ts = new ArrayList<>();
        for (Object o : list) {
            T t = instantiateClass(clazz);
            copyProperties(o, t);
            ts.add(t);
        }
        return ts;
    }

    /**
     * 对象属性复制
     *
     * @param object o
     * @param clazz  clazz
     * @param <T>    T
     * @return T
     */
    public static <T> T copyProperties(Object object, Class<T> clazz) {
        T t = instantiateClass(clazz);
        copyProperties(object, t);
        return t;
    }
}

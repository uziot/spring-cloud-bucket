package com.uziot.bucket.common.optional;

import java.util.Optional;

/**
 * @author shidt
 * @version V1.0
 * @className OptionalOfNullable
 * @date 2021-02-03 14:37:31
 * @description ofNullable 方法是和 of 方式一样，都是用于创建 Optional 对象，
 * 只是传入的参数 null 时，会返回一个空的 Optional 对象，而不会抛出 NullPointerException 异常。
 */

public class OptionalOfNullable {
    public static void main(String[] args) {
        // 传入正常值，正常返回一个 Optional 对象
        Optional<String> optional1 = Optional.ofNullable("mydlq");
        System.out.println(optional1);

        // 传入 null 参数，正常返回 Optional 对象
        Optional optional2 = Optional.ofNullable(null);
        System.out.println(optional2);


    }
}

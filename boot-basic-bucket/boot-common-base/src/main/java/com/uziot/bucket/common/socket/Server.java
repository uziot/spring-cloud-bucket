package com.uziot.bucket.common.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Lenovo
 * <p>
 * 简易的Socket服务端程序
 */
public class Server {
    public static void main(String[] args) {
        try {
            System.out.println("服务器已启动端口号为：8888，等待连联中...");
            ServerSocket serverSocket = new ServerSocket(8888);
            Socket socket;
            while (!Thread.currentThread().isInterrupted()) {
                socket = serverSocket.accept();
                ResponseThread rt = new ResponseThread(socket);
                rt.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
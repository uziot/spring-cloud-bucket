package com.uziot.bucket.common.concurrent;

import java.time.Duration;
import java.time.Instant;
import java.util.stream.LongStream;

/**
 * @author shidt
 * @version V1.0
 * @className sas
 * @date 2020-12-30 22:43:30
 * @description 并行流案例
 */

public class LongStreamDemo {
    public static void main(String[] args) {
        test03();
    }

    public static void test03() {
        Instant start = Instant.now();
        //并行流
        long reduce1 = LongStream.rangeClosed(0, 110)
                .parallel()
                .reduce(0, Long::sum);
        System.out.println("reduce1:" + reduce1);
        //顺序流
        long reduce2 = LongStream.rangeClosed(0, 110)
                .sequential()
                .reduce(0, Long::sum);
        System.out.println("reduce2:" + reduce2);
        Instant end = Instant.now();
        System.out.println("耗费时间" + Duration.between(start, end).toMillis());
    }

}

package com.uziot.bucket.common.functionInterface;

/**
 * @author shidt
 * @version V1.0
 * @className UserService
 * @date 2021-01-29 09:03:44
 * @description
 */
@FunctionalInterface
public interface UserExecutor<T, R> {
    R execute(T t);
}

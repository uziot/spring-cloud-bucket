package com.uziot.bucket.common.util;

import com.uziot.bucket.common.util.spring.RequestHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: <br>
 * 请求工具类
 *
 * @author shidt
 * @date 2020-03-24 15:24
 */
public class RequestUtil {

    private static final RequestEnvironment ENVIRONMENT = new RequestEnvironment();

    public static RequestEnvironment getRequestEnvironment() {
        ENVIRONMENT.setIp(getIp());
        ENVIRONMENT.setUrl(getRequestUrl());
        ENVIRONMENT.setUserAgent(getUserAgent());
        ENVIRONMENT.setMethod(getMethod());
        ENVIRONMENT.setParameters(getParameters());
        ENVIRONMENT.setParametersMap(getParametersMap());
        return ENVIRONMENT;
    }


    public static String getParameters() {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return null;
        }
        Enumeration<String> paraNames = request.getParameterNames();
        if (paraNames == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        while (paraNames.hasMoreElements()) {
            String paraName = paraNames.nextElement();
            sb.append("&").append(paraName).append("=").append(request.getParameter(paraName));
        }
        return sb.toString();
    }

    public static Map<String, String[]> getParametersMap() {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return new HashMap<>(16);
        }
        return request.getParameterMap();
    }

    public static String getHeader(String headerName) {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return null;
        }
        return request.getHeader(headerName);
    }

    public static String getReferer() {
        return getHeader("Referer");
    }

    public static String getUserAgent() {
        return getHeader("User-Agent");
    }

    public static String getIp() {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return null;
        }
        return IPUtil.getIpAddr(request);
    }

    public static String getRequestUrl() {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return null;
        }
        return request.getRequestURL().toString();
    }

    public static String getMethod() {
        HttpServletRequest request = RequestHolder.getRequest();
        if (null == request) {
            return null;
        }
        return request.getMethod();
    }

    public static boolean isAjax(HttpServletRequest request) {
        if (null == request) {
            request = RequestHolder.getRequest();
        }
        if (null == request) {
            return false;
        }
        return "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))
                || request.getParameter("ajax") != null;

    }

    public static class RequestEnvironment {
        private String ip;
        private String url;
        private String userAgent;
        private String method;
        private String parameters;
        private Map<String, String[]> parametersMap;

        private void setIp(String ip) {
            this.ip = ip;
        }

        private void setUrl(String url) {
            this.url = url;
        }

        private void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        private void setMethod(String method) {
            this.method = method;
        }

        private void setParameters(String parameters) {
            this.parameters = parameters;
        }

        private void setParametersMap(Map<String, String[]> parametersMap) {
            this.parametersMap = parametersMap;
        }

        public String getIp() {
            return ip;
        }

        public String getUrl() {
            return url;
        }

        public String getUserAgent() {
            return userAgent;
        }

        public String getMethod() {
            return method;
        }

        public String getParameters() {
            return parameters;
        }

        public Map<String, String[]> getParametersMap() {
            return parametersMap;
        }

        @Override
        public String toString() {
            return "RequestEnvironment{" +
                    "ip='" + ip + '\'' +
                    ", url='" + url + '\'' +
                    ", userAgent='" + userAgent + '\'' +
                    ", method='" + method + '\'' +
                    ", parameters='" + parameters + '\'' +
                    ", parametersMap=" + parametersMap +
                    '}';
        }
    }

}

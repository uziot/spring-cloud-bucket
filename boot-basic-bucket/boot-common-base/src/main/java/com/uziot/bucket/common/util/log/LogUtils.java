package com.uziot.bucket.common.util.log;

import lombok.extern.slf4j.Slf4j;


/**
 * @author shidt
 * @version V1.0
 * @className FastJson2JsonRedisSerializer
 * @date 2020-12-09 21:50:05
 * @description 处理并记录日志文件
 */
@Slf4j
public class LogUtils {
    public static String getBlock(Object msg) {
        if (msg == null) {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}

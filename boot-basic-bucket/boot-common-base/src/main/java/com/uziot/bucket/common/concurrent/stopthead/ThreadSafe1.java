package com.uziot.bucket.common.concurrent.stopthead;

/**
 * @author shidt
 * @version V1.0
 * @className ThreadSafe
 * @date 2021-05-12 23:31:58
 * @description </p>
 * 使用退出标志退出线程
 * 一般 run()方法执行完，线程就会正常结束，然而，常常有些线程是伺服线程。它们需要长时间的
 * 运行，只有在外部某些条件满足的情况下，才能关闭这些线程。使用一个变量来控制循环，例如：
 * 最直接的方法就是设一个 boolean 类型的标志，并通过设置这个标志为 true 或 false 来控制 while
 * 循环是否退出，代码示例：
 * 定义了一个退出标志 exit，当 exit 为 true 时，while 循环退出，exit 的默认值为 false.在定义 exit
 * 时，使用了一个 Java 关键字 volatile，这个关键字的目的是使 exit 同步，也就是说在同一时刻只
 * 能由一个线程来修改 exit 的值。
 */

public class ThreadSafe1 extends Thread {
    public static volatile boolean STATUS = true;

    @Override
    public void run() {
        while (STATUS) {
            //do something
            System.out.println("正在运行中");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new ThreadSafe1().start();
    }


}
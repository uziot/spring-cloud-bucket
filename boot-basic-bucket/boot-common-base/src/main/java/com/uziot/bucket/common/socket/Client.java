package com.uziot.bucket.common.socket;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author Lenovo
 * 简易的Socket客户端程序
 */
public class Client {
    public static void main(String[] args) {
        try {
            //创建与服务器通讯实例
            Socket socket = new Socket("127.0.0.1", 8888);
            OutputStream outPutStream = socket.getOutputStream();
            //写入请求数据
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(outPutStream, StandardCharsets.UTF_8));
            bufferedWriter.write("这是测试客户端发起的报文......");
            bufferedWriter.flush();
            socket.shutdownOutput();

            //获取服务器响应信息
            InputStream inputStream = socket.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String info = bufferedReader.readLine();
            while (info != null) {
                System.out.println(info);
                info = bufferedReader.readLine();
            }
            socket.shutdownInput();
            bufferedReader.close();
            bufferedWriter.close();
            inputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
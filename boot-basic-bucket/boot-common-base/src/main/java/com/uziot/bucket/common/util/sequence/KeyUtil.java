package com.uziot.bucket.common.util.sequence;

import lombok.extern.slf4j.Slf4j;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Random;
import java.util.UUID;


/**
 * 功能描述: <br>
 * 1、雪花算法分布式ID生成工具类，经测试生成30W条数据无重复，仅消费150MS
 * 2、随机数主键生成，效果不错
 * 3、UUID主键生成，效果不错
 *
 * @author shidt
 * @date 2020-01-03 22:42
 */
@Slf4j
public class KeyUtil {
    /**
     * 获取一个指定长度的随机数
     */
    private static final String SYS_NUMBER = "0123456789";
    /**
     * 随机数获取
     */
    private static final Random RANDOM = new SecureRandom();
    /**
     * 时间部分所占长度
     */
    private static final int TIME_LEN = 41;
    /**
     * 数据中心id所占长度
     */
    private static final int DATA_LEN = 5;
    /**
     * 机器id所占长度
     */
    private static final int WORK_LEN = 5;
    /**
     * 毫秒内序列所占长度
     */
    private static final int SEQ_LEN = 12;

    /**
     * 定义起始时间 2015-01-01 00:00:00
     */
    private static final long START_TIME = 1420041600000L;
    /**
     * 上次生成ID的时间截
     */
    private static long LAST_TIME_STAMP = -1L;
    /**
     * 时间部分向左移动的位数 22
     */
    private static final int TIME_LEFT_BIT = 64 - 1 - TIME_LEN;

    /**
     * 自动获取数据中心id（可以手动定义 0-31之间的数）
     */
    private static final long DATA_ID = getDataId();
    /**
     * 自动机器id（可以手动定义 0-31之间的数）
     */
    private static final long WORK_ID = getWorkId();
    /**
     * 数据中心id最大值 31
     */
    private static final int DATA_MAX_NUM = ~(-1 << DATA_LEN);
    /**
     * 机器id最大值 31
     */
    private static final int WORK_MAX_NUM = ~(-1 << WORK_LEN);
    /**
     * 随机获取数据中心id的参数 32
     */
    private static final int DATA_RANDOM = DATA_MAX_NUM + 1;
    /**
     * 随机获取机器id的参数 32
     */
    private static final int WORK_RANDOM = WORK_MAX_NUM + 1;
    /**
     * 数据中心id左移位数 17
     */
    private static final int DATA_LEFT_BIT = TIME_LEFT_BIT - DATA_LEN;
    /**
     * 机器id左移位数 12
     */
    private static final int WORK_LEFT_BIT = DATA_LEFT_BIT - WORK_LEN;

    /**
     * 上一次的毫秒内序列值
     */
    private static long LAST_SEQ = 0L;
    /**
     * 毫秒内序列的最大值 4095
     */
    private static final long SEQ_MAX_NUM = ~(-1 << SEQ_LEN);


    /**
     * 获取雪花算法ID加前缀 String类型
     *
     * @param prefix 前缀
     * @return id
     */
    public static String genIdStr(String prefix) {
        long genId = genId();
        String strId = Long.toString(genId);
        return prefix + strId;
    }

    /**
     * 获取雪花算法ID String类型
     *
     * @return id
     */
    public static String genIdStr() {
        long genId = genId();
        return Long.toString(genId);
    }

    /**
     * 必须制定长度大于19的位数
     *
     * @param length 长度
     * @return key
     */
    public static String genIdStrLength(int length) {
        long genId = genId();
        String strId = Long.toString(genId);
        if (length <= 18) {
            return strId;
        }
        if (length == 19) {
            return "1" + strId;
        }
        String leftZero = addLeftZero(strId, length - 1);
        return "1" + leftZero;
    }

    /**
     * 调用雪花算法生成的ID
     *
     * @return id
     */
    public synchronized static long genId() {
        long now = System.currentTimeMillis();
        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (now < LAST_TIME_STAMP) {
            //throw new RuntimeException(String.format("系统时间错误！ %d 毫秒内拒绝生成雪花ID！", START_TIME - now));
            log.error(String.format("系统时间错误！ %d 毫秒内生成雪花ID异常，可能会发生重复！", START_TIME - now));
        }
        if (now == LAST_TIME_STAMP) {
            LAST_SEQ = (LAST_SEQ + 1) & SEQ_MAX_NUM;
            if (LAST_SEQ == 0) {
                now = nextMillis(LAST_TIME_STAMP);
            }
        } else {
            LAST_SEQ = 0;
        }
        //上次生成ID的时间截
        LAST_TIME_STAMP = now;
        return ((now - START_TIME) << TIME_LEFT_BIT) | (DATA_ID << DATA_LEFT_BIT) | (WORK_ID << WORK_LEFT_BIT) | LAST_SEQ;
    }

    /**
     * 获取下一不同毫秒的时间戳，不能与最后的时间戳一样
     */
    private static long nextMillis(long lastMillis) {
        long now = System.currentTimeMillis();
        while (now <= lastMillis) {
            now = System.currentTimeMillis();
        }
        return now;
    }

    /**
     * 获取字符串s的字节数组，然后将数组的元素相加，对（max+1）取余
     */
    private static int getHostId(String s, int max) {
        byte[] bytes = s.getBytes();
        int sums = 0;
        for (int b : bytes) {
            sums += b;
        }
        return sums % (max + 1);
    }

    /**
     * 根据 host address 取余，发生异常就获取 0到31之间的随机数
     */
    private static int getWorkId() {
        try {
            return getHostId(Inet4Address.getLocalHost().getHostAddress(), WORK_MAX_NUM);
        } catch (UnknownHostException e) {
            return new Random().nextInt(WORK_RANDOM);
        }
    }

    /**
     * 根据 host name 取余，发生异常就获取 0到31之间的随机数
     */
    private static int getDataId() {
        try {
            return getHostId(Inet4Address.getLocalHost().getHostName(), DATA_MAX_NUM);
        } catch (UnknownHostException e) {
            return new Random().nextInt(DATA_RANDOM);
        }
    }


    /**
     * 随机数生成，长度自定义，最小一位随机数
     *
     * @param length 长度
     * @return 随机数
     */
    public static synchronized String random(int length) {
        if (length == 0) {
            length = 1;
        }
        char[] s = new char[length];
        for (int i = 0; i < s.length; i++) {
            s[i] = SYS_NUMBER.charAt(RANDOM.nextInt(SYS_NUMBER.length()));
        }
        return new String(s);
    }

    /**
     * 通过时间戳 + 随机数的方式获得纯数字的字符串
     *
     * @param randomLength 随机数的长度
     * @return 数字串
     */
    public static String getRandomKey(int randomLength) {
        String random = random(randomLength);
        long start = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return start + random;
    }

    /**
     * 获取UUIDKey字符串 去除横线
     *
     * @return uuid
     */
    public static String getUuidKey() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 左填0
     * 2018年4月28日
     *
     * @return .
     */
    private static String addLeftZero(String s, int length) {
        int old = s.length();
        if (length > old) {
            char[] c = new char[length];
            char[] x = s.toCharArray();
            if (x.length > length) {
                throw new IllegalArgumentException(
                        "Numeric value is larger than intended length: " + s + " LEN " + length);
            }
            int lim = c.length - x.length;
            for (int i = 0; i < lim; i++) {
                c[i] = '0';
            }
            System.arraycopy(x, 0, c, lim, x.length);
            return new String(c);
        }
        return s.substring(0, length);
    }

}

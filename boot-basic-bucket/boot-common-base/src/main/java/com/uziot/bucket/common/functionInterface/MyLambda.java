package com.uziot.bucket.common.functionInterface;

/**
 * @author DELL
 */
public class MyLambda {
    public static void showLog(int level, IMyLambda mess) {
        if (level == 1) {
            System.out.println(mess.buildMess());
        }
    }
 
    public static void main(String[] args) {
        String mess1 = "Hello";
        String mess2 = "java";
 
        //此处，第二个参数是函数式接口类型，可以写Lambda表达式进行表示
        showLog(1, () -> {
            return mess1 + mess2;
        });
    }
}
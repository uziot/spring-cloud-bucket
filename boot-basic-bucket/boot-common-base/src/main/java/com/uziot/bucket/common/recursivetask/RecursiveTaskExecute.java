package com.uziot.bucket.common.recursivetask;

import cn.hutool.core.util.CreditCodeUtil;
import com.uziot.bucket.common.util.ToStringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

/**
 * @author shidt
 * @version V1.0
 * @className RecursiveTaskExecute
 * @date 2021-05-08 22:30:40
 * @description
 */

@Slf4j
public class RecursiveTaskExecute {
    public static void main(String[] args) {

        List<String> urlList = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            urlList.add(CreditCodeUtil.randomCreditCode());
        }

        ForkJoinPool forkjoinPool = new ForkJoinPool();
        //生成一个计算任务
        RecursiveTaskDemo task = new RecursiveTaskDemo(0, urlList.size(), urlList);
        //执行一个任务 （这个是有等待返回的调用方式）
        Future<List<String>> result = forkjoinPool.submit(task);
        List<String> executeResult;
        try {
            executeResult = result.get();
            log.info("----------------执行完成结果[" + executeResult.size() + "]如下--------------");
            log.info(ToStringUtils.prettyJson(executeResult));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            forkjoinPool.shutdown();
        }
    }
}

package com.uziot.bucket.common.util.map;

import cn.hutool.core.util.CreditCodeUtil;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * @author shidt
 * @version V1.0
 * @className MapTest
 * @date 2021-05-14 09:45:52
 * @description
 */

@Slf4j
public class MapTest {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        for (int i = 0; i < 1000;i++){
            map.put(CreditCodeUtil.randomCreditCode(),CreditCodeUtil.randomCreditCode());
        }
        map.getOrDefault("","");
        map.forEach((k,v)->{
            log.info("key:{}",k);
            log.info("value:{}",v);
        });

        ThreadFactoryBuilder threadFactoryBuilder = new ThreadFactoryBuilder();
        threadFactoryBuilder.setNameFormat("Test-1").build().newThread(()->{

            log.info("线程内部Hello...........");


        }).start();
        
    }
}

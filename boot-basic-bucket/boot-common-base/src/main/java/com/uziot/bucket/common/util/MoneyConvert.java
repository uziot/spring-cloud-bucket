package com.uziot.bucket.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 思路：
 * 1.定义三张表，存储中文数字和货币单位。
 * 2.键盘录入使用IO流的BufferedReader进行接收键盘录入。
 * 3.把录入的阿拉伯数字弄成字符数组，如果有小数位就先分割成整数位和小数位。
 * 4.先处理整数位再处理小数位，把每个数字所在位置的角标对着中文数字的表进行转换，
 * 转换完后再从对应的位置插入货币单位。
 * 5.把格式进行修正匹配后打印输出。
 *
 * @author Lenovo
 */
public class MoneyConvert {
    /**
     * 日志
     */
    private static final Log log = LogFactory.getLog(MoneyConvert.class);

    /**
     * 创建中文大写数字的表和货币单位的表
     */
    static String[] cnNumTab = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    static String[] integerUnitTab = {"", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟"};
    static String[] decimalUnitTab = {"角", "分", "厘"};

    public static String convertMoney(String inputStr) {
        String rtnStr = null;
        try {
            //把键盘接收的金额数据的小数位和整数位分隔开
            String[] strArr = inputStr.split("\\.");

            //把分隔好的数位金额数据分别打散成字符数组
            char[] chNumArrInteger = strArr[0].toCharArray();

            //把阿拉伯数字转换为中文的大写数字
            String[] convertIntegerNum = convertUpper(chNumArrInteger);

            //定义合拼数字和单位用的容器
            StringBuilder container = new StringBuilder("圆整");

            //嵌入货币单位到转换好的中文大写数字里去
            for (int x = convertIntegerNum.length - 1, y = 0; x >= 0; x--, y++) {
                String num = convertIntegerNum[x];
                if ("零".equals(num)) {
                    //判断数位是否是仟万亿的位置，是则插入单位
                    if (y == 4 || y == 8) {
                        container.insert(0, num + integerUnitTab[y]);
                    } else {
                        container.insert(0, num);
                    }
                } else {
                    container.insert(0, num + integerUnitTab[y]);
                }
            }

            //如果有小数位，则对小数位的数也进行同样的转换操作
            if (strArr.length == 2) {
                int len = container.length();
                container.replace(len - 2, len, "圆");

                char[] chNumArrDecimal = strArr[1].toCharArray();
                String[] convertDecimalNum = convertUpper(chNumArrDecimal);

                //嵌入货币单位到转换好的中文大写数字里去
                for (int x = 0; x < convertDecimalNum.length; x++) {
                    container.append(convertDecimalNum[x]).append(decimalUnitTab[x]);
                }
            }

            //格式修正
            rtnStr = container.toString();
            rtnStr = rtnStr.replaceAll("佰零+万", "佰万");
            rtnStr = rtnStr.replaceAll("佰零+亿", "佰亿");
            rtnStr = rtnStr.replaceAll("仟零+万", "仟万");
            rtnStr = rtnStr.replaceAll("仟零+亿", "仟亿");
            rtnStr = rtnStr.replaceAll("零+", "零");
            rtnStr = rtnStr.replaceAll("零万", "万");
            rtnStr = rtnStr.replaceAll("零万零", "零");
            rtnStr = rtnStr.replaceAll("拾零", "拾");
            rtnStr = rtnStr.replaceAll("零圆", "圆");

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("[MoneyConvert][convertMoney]", e);
            }
        }
        return rtnStr;
    }


    /**
     * 把阿拉伯数字转换为中文的大写数字
     *
     * @param arr arr
     * @return s
     */
    public static String[] convertUpper(char[] arr) {
        String[] convertNum = new String[arr.length];
        for (int x = 0; x < arr.length; x++) {
            int num = Integer.parseInt(arr[x] + "");
            convertNum[x] = cnNumTab[num];
        }

        return convertNum;
    }


    public static void main(String[] args) {
        System.out.println(convertMoney("1000366.56"));
    }

}
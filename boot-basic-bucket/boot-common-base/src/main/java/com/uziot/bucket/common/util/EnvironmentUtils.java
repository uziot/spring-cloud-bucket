package com.uziot.bucket.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className EnvironmentUtils
 * @date 2021-08-07 16:45:04
 * @description 环境常量获取工具类
 */

@Slf4j
@Component
@SuppressWarnings("All")
public class EnvironmentUtils {
    private final Environment environment;
    private static Environment env;

    /**
     * 保存全局属性值
     */
    private static final Map<String, Object> MAP = new HashMap<>();

    @Autowired
    private EnvironmentUtils(Environment env) {
        this.environment = env;
    }

    @PostConstruct
    public void init() {
        env = environment;
    }

    /**
     * 获取配置
     */
    public static String getConfig(String key) {
        Object o = MAP.get(key);
        String value = null;
        if (o instanceof String) {
            value = (String) o;
        }
        if (StringUtils.isEmpty(value)) {
            try {
                value = env.getProperty(key);
                MAP.put(key, StringUtils.isNotEmpty(value) && !"null".equals(value) ? value : "");
            } catch (Exception e) {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return StringUtils.isNotEmpty(value) && !"null".equals(value) ? value : "";
    }

    /**
     * 获取配置
     *
     * @param key   配置key
     * @param clazz 返回值类型
     * @param <T>   返回值泛型
     * @return 配置值
     */
    @Nullable
    public static <T> T getConfig(String key, Class<T> clazz) {
        T value = (T) MAP.get(key);
        if (value == null) {
            try {
                value = env.getProperty(key, clazz);
                MAP.put(key, value);
            } catch (Exception e) {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return value;
    }

    /**
     * 获取配置
     *
     * @param key          配置key
     * @param clazz        返回值类型
     * @param defaultValue 默认值
     * @param <T>          返回值泛型
     * @return 配置值
     */
    public static <T> T getConfig(String key, Class<T> clazz, T defaultValue) {
        Object o = MAP.get(key);
        T value = (T) MAP.get(key);
        if (value == null) {
            try {
                value = env.getProperty(key, clazz, defaultValue);
                MAP.put(key, value);
            } catch (Exception e) {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return value;
    }
}
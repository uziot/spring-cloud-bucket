package com.uziot.bucket.common.util;

import java.nio.file.Path;
import java.nio.file.Paths;
public class PathExample {
    public static void main(String[] args) {
        // 创建一个绝对路径
        Path absolutePath = Paths.get("C:\\Users\\phoenix\\file.txt");     // 这里传入 "example\\file.txt" 创建的相对路径
        System.out.println("Absolute path: " + absolutePath);
        // 获取父路径
        System.out.println("Parent path: " + absolutePath.getParent());
        // 获取文件名
        System.out.println("File name: " + absolutePath.getFileName());
        // 获取根路径
        System.out.println("Root path: " + absolutePath.getRoot());
        // 合并路径
        Path resolvePath = Paths.get("C:\\Users\\phoenix").resolve("file.txt");
        System.out.println("Merged path:" + resolvePath);
    }
}
package com.uziot.bucket.common.optional;

/**
 * @author shidt
 * @version V1.0
 * @className package_info
 * @date 2021-02-03 14:33:25
 * @description Optional 是一个容器对象，可以存储对象、字符串等值，当然也可以存储 null 值。
 * Optional 提供很多有用的方法，能帮助我们将 Java 中的对象等一些值存入其中，
 * 这样我们就不用显式进行空值检测，使我们能够用少量的代码完成复杂的流程。
 * 比如它提供了：
 * <p>
 * of() 方法，可以将值存入 Optional 容器中，如果存入的值是 null 则抛异常。
 * ofNullable() 方法，可以将值存入 Optional 容器中，即使值是 null 也不会抛异常。
 * get() 方法，可以获取容器中的值，如果值为 null 则抛出异常。
 * getElse() 方法，可以获取容器中的值，如果值为 null 则返回设置的默认值。
 * isPresent() 方法，该方法可以判断存入的值是否为空。
 * …等等一些其它常用方法，下面会进行介绍。
 * 可以说，使用 Optional 可以帮助我们解决业务中，减少值动不动就抛出空指针异常问题，
 * 也减少 null 值的判断，提高代码可读性等，这里我们介绍下，如果使用这个 Optional 类。
 */

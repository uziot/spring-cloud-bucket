package com.uziot.bucket.common.functionInterface;


import com.uziot.bucket.common.util.ToStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author shidt
 * @version V1.0
 * @className ListStreamGroup
 * @date 2023-03-26 22:26:26
 * @description 使用lambda表达式对list进行条件分组
 */

public class ListStreamGroup {
    public static void main(String[] args) {
        ArrayList<User> userList = new ArrayList<>();

        userList.add(new User("zhangsan", "男"));
        userList.add(new User("lisi", "男"));
        userList.add(new User("wangwu", "男"));
        userList.add(new User("xixi", "女"));
        userList.add(new User("caici", "女"));

        Stream<User> userStream = userList.stream();

        Map<String, List<User>> sexMap = userStream.collect(
                Collectors.groupingBy(User::getSex)
        );

        System.out.println(ToStringUtils.prettyJson(sexMap));

    }


    static class User {
        private String userName;
        private String sex;

        public User(String userName, String sex) {
            this.userName = userName;
            this.sex = sex;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        @Override
        public String toString() {
            return "User{" +
                    "userName='" + userName + '\'' +
                    ", sex='" + sex + '\'' +
                    '}';
        }
    }
}

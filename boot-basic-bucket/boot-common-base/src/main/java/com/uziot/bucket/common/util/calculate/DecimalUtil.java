package com.uziot.bucket.common.util.calculate;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author shidt
 * @version V1.0
 * @className DecimalUtil
 * @date 2020-12-11 23:54:20
 * @description
 */

public class DecimalUtil {
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("###,##0.00");

    public String format(String num, String pattern) {
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(new BigDecimal(num));
    }

    public String format(BigDecimal num, String pattern) {
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(num);
    }

    public String format(Double num, String pattern) {
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        return decimalFormat.format(num);
    }

    public static void main(String[] args) {
        BigDecimal money = new BigDecimal("0");
        BigDecimal add;
        add = money.add(new BigDecimal("500"));
        add = add.add(new BigDecimal("500"));
        String format = new DecimalUtil().format(add, "###,##0.00");
        System.out.println(format);
    }
}

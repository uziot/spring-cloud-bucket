package com.uziot.bucket.common.concurrent.task.rallback;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author shidt
 * @version V1.0
 * @className CompletableFutureCallBackDemo
 * @date 2021-06-16 20:59:36
 * @description
 *
 * 简单的演示异步任务完成后回调
 *
 */

public class CompletableFutureCallBackDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //异步回调
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t completableFuture2");
            int i = 10 / 0;
            return 1024;
        });
        // 执行完成回调和异常回调
        completableFuture2.whenComplete((result, throwable) -> {
            System.out.println("-------result=" + result);
            System.out.println("-------throwable=" + throwable);
        }).exceptionally(throwable -> {
            System.out.println("-------exception:" + throwable.getMessage());
            return 444;
        }).get();
    }
}

package com.uziot.bucket.common.optional;

import java.util.Optional;

/**
 * @author shidt
 * @version V1.0
 * @className OptionalIfPresent
 * @date 2021-02-03 14:39:27
 * @description
 * 该方法其实就是用于判断创建 Optional 时传入参数的值是否为空，
 * 实现代码就简单一行，即 value != null 所以如果不为空则返回 true，否则返回 false。
 */

public class OptionalIsPresent {
    public static void main(String[] args) {
        // 传入正常值，正常返回一个 Optional 对象，并使用 isPresent 方法
        Optional optional1 = Optional.ofNullable("mydlq");
        System.out.println("传入正常值返回：" + optional1.isPresent());

        // 传入参数为 null 生成一个 Optional 对象，并使用 isPresent 方法
        Optional optional2 = Optional.ofNullable(null);
        System.out.println("传入 null 值返回：" + optional2.isPresent());
    }
}

package com.uziot.bucket.common.optional;

import java.util.Optional;

/**
 * @author shidt
 * @version V1.0
 * @className OptionalOf
 * @date 2021-02-03 14:35:18
 * @description of 方法通过工厂方法创建 Optional 实例，
 * 需要注意的是传入的参数不能为 null，否则抛出 NullPointerException。
 */

public class OptionalOf {
    public static void main(String[] args) {
        Optional<String> helloWorld = Optional.of("HelloWorld");
        System.out.println(helloWorld);
        Optional<Object> optionalO = Optional.of(null);
        System.out.println(optionalO);
    }
}

package com.uziot.bucket.common.functionInterface;

/**
 * @author shidt
 * @version V1.0
 * @className UserService
 * @date 2021-01-29 09:07:32
 * @description
 */

public interface UserService {
    /**
     * @param u   u
     * @param <T> >
     * @return r
     * ASD
     */
    <T> T executeByExecutor(UserExecutor<UserService, T> u);

    void print();
}

package com.uziot.bucket.mongodb.config;

import com.uziot.bucket.mongodb.config.props.MultipleMongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author shidt
 * @version V1.0
 * @className SecondaryMongoConfig
 * @date 2020-12-05 22:37:29
 * @description
 */

@Configuration
@EnableConfigurationProperties(MultipleMongoProperties.class)
@EnableMongoRepositories(
        basePackages = "com.uziot.bucket.mongodb.repository.secondary",
        mongoTemplateRef = "secondaryMongoTemplate")
public class SecondaryMongoConfig {

}

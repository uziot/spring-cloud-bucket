package com.uziot.bucket.mongodb.config.props;

import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author shidt
 * @version V1.0
 * @className MultipleMongoProperties
 * @date 2020-12-05 22:37:29
 * @description
 */
@ConfigurationProperties(prefix = "mongodb")
public class MultipleMongoProperties {

    /**
     * 对象命名需要和配置文件注意映射关系
     */
    private MongoProperties primary = new MongoProperties();
    private MongoProperties secondary = new MongoProperties();

    public MongoProperties getPrimary() {
        return primary;
    }

    public void setPrimary(MongoProperties primary) {
        this.primary = primary;
    }

    public MongoProperties getSecondary() {
        return secondary;
    }

    public void setSecondary(MongoProperties secondary) {
        this.secondary = secondary;
    }
}

package com.uziot.bucket.mongodb.repository.primary;

import com.uziot.bucket.mongodb.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author neo
 */
public interface PrimaryRepository extends MongoRepository<User, String> {
}

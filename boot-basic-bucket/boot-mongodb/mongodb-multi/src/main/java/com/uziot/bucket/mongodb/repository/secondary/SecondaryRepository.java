package com.uziot.bucket.mongodb.repository.secondary;

import com.uziot.bucket.mongodb.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author neo
 */
public interface SecondaryRepository extends MongoRepository<User, String> {
}

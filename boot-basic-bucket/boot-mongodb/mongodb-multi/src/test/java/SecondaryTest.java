import com.uziot.bucket.MultiMongodbApplication;
import com.uziot.bucket.mongodb.model.User;
import com.uziot.bucket.mongodb.repository.secondary.SecondaryRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

/**
 * @author shidt
 * @version V1.0
 * @className MongoTest
 * @date 2020-12-05 22:42:12
 * @description
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        MultiMongodbApplication.class
})
@SpringBootConfiguration
public class SecondaryTest {
    @Autowired
    private SecondaryRepository secondaryRepository;


    @Test
    public void secondarySaveUser() {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setUserName("王五");
        user.setPassWord("123456");
        secondaryRepository.insert(user);
        log.info("插入MongoDB数据库成功：{}", user);
    }
}

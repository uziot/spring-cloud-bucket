package com.uziot.bucket.mongo.simple.repository;


import com.uziot.bucket.mongo.simple.domain.User;

/**
 * @author shidt
 * @version V1.0
 * @className MongoSimApplicaiton
 * @date 2020-12-05 22:37:29
 * @description
 */
public interface UserRepository {

    void saveUser(User user);

    User findUserByUserName(String userName);

    long updateUser(User user);

    void deleteUserById(Long id);

}

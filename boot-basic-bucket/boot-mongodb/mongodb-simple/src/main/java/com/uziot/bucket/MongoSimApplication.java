package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className MongoSimApplicaiton
 * @date 2020-12-05 22:37:29
 * @description
 */
@SpringBootApplication
public class MongoSimApplication {
    public static void main(String[] args) {
        SpringApplication.run(MongoSimApplication.class, args);
    }
}

import com.uziot.bucket.MongoSimApplication;
import com.uziot.bucket.mongo.simple.domain.User;
import com.uziot.bucket.mongo.simple.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shidt
 * @version V1.0
 * @className MongoTest
 * @date 2020-12-05 22:42:12
 * @description
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        MongoSimApplication.class
})
@SpringBootConfiguration
public class MongoTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSaveUser() {
        User user = new User();
        user.setId(101L);
        user.setUserName("王五");
        user.setPassWord("123456");
        userRepository.saveUser(user);
        log.info("插入MongoDB数据库成功：{}", user);
    }

    @Test
    public void testQueryUser() {
        User user = userRepository.findUserByUserName("王五");
        log.info("查询MongoDB数据库成功：{}", user);
    }

    @Test
    public void testUpdateUser() {
        User user = userRepository.findUserByUserName("张三");
        user.setUserName("李四");
        long i = userRepository.updateUser(user);
        log.info("更新MongoDB数据库成功：{}，影像了：{}", user, i);
    }

    @Test
    public void testDeleteUser() {
        userRepository.deleteUserById(100L);
        log.info("删除MongoDB数据成功！");
    }
}

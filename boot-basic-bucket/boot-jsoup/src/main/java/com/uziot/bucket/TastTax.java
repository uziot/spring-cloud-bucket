package com.uziot.bucket;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URL;
import java.time.Instant;
import java.util.Scanner;

/**
 * @author shidt
 * @version V1.0
 * @className TastTax
 * @date 2021-03-27 21:34:10
 * @description
 */

@Slf4j
public class TastTax {
    public static void main(String[] args) throws Exception {
        while (true) {

            Scanner scanner = new Scanner(System.in);
            System.out.print("---------请输入企业信息查询条件---------\n请输入：");
            String txt = scanner.next();

            long start = Instant.now().toEpochMilli();
            String path = "http://xabj.banksrv.cn/index.php?s=/Weixin/index/skeys/" + txt;
            URL url = new URL(path);
            Document document = Jsoup.parse(url, 10000);
            Elements elements = document.getElementsByAttribute("data-clipboard-text");
            elements.forEach(ele -> {
                String tax = ele.attr("data-clipboard-text");
                String companyName = ele.parent().text();
                companyName = companyName.substring(0, companyName.lastIndexOf("（")).trim();
                log.info("税号:[{}],公司名称:[{}]", tax, companyName);
            });
            long end = Instant.now().toEpochMilli();
            log.info("合计耗时:[{}]MS", end - start);
        }
    }
}

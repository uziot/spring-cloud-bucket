package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className JsopApplication
 * @date 2020-12-19 17:30:24
 * @description
 */
@SpringBootApplication
public class JsopApplication {
    public static void main(String[] args) {
        SpringApplication.run(JsopApplication.class, args);
    }
}

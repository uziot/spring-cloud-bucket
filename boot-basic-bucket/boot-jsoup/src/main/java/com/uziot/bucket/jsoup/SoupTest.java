package com.uziot.bucket.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

/**
 * @author shidt
 * @version V1.0
 * @className SoupTest
 * @date 2020-12-19 17:38:07
 * @description
 */

public class SoupTest {
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://bing.ioliu.cn/?p=2");
        Document document = Jsoup.parse(url, 10000);
        Elements elements = document.getElementsByAttribute("data-progressive");
        elements.forEach(ele->{
            String text = ele.attr("data-progressive");
            System.out.println(text);
        });
    //    System.out.println(elements);
    }
}

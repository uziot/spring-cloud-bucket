package com.uziot;

import com.alibaba.fastjson.JSON;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author shidt
 * @version V1.0
 * @className HelloJson
 * @date 2021-02-03 12:29:31
 * @description
 */
@RestController
public class HelloJson {
    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public String stringMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("name", "zhangsan");
        hashMap.put("password", "123456");
        return JSON.toJSONString(hashMap);
    }
}

package com.uziot.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author shidt
 * @version V1.0
 * @className HelloApi
 * @date 2022-03-10 23:13:02
 * @description 用SpringMVC方式实现Rest接口
 */

@Controller
@RequestMapping(value = "/hello")
public class HelloApi {

    @GetMapping(value = "/test/{aaa}")
    public @ResponseBody
    Object hello(@PathVariable String aaa) {
        return "Hello :" + aaa;
    }
}

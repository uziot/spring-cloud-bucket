package com.uziot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author shidt
 * @version V1.0
 * @className Hello
 * @date 2021-02-03 11:58:00
 * @description
 */
@Controller
public class Hello {
    @GetMapping(value = "/user")
    public ModelAndView test1(ModelAndView modelAndView) {
        modelAndView.setViewName("user");
        modelAndView.addObject("username", "张三");
        return modelAndView;
    }
}

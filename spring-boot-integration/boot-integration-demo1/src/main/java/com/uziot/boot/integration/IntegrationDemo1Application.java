package com.uziot.boot.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className IntegrationDemo1Application
 * @date 2022-03-22 22:15:10
 * @description
 */

@SpringBootApplication
public class IntegrationDemo1Application {
    public static void main(String[] args) {
        SpringApplication.run(IntegrationDemo1Application.class, args);
    }
}

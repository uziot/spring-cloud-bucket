package com.uziot.test;

import com.uziot.boot.integration.IntegrationDemo1Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;

/**
 * @author shidt
 * @version V1.0
 * @className Test1
 * @date 2022-03-22 22:17:20
 * @description
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IntegrationDemo1Application.class})
public class Test1 {
    @Autowired
    private MessageChannel inputChannel;
    @Autowired
    private QueueChannel queueChannel;

    /**
     * 消息发送与接收
     */
    @Test
    public void testSend() {
        String payload = "DOG:foo";
        Message<String> msg = MessageBuilder.withPayload(payload).build();
        inputChannel.send(msg);
        Message<?> outMsg = queueChannel.receive();
        assert outMsg.getPayload().toString().equals("DOG:foo");
        log.info("getHeaders============>[{}]",outMsg.getHeaders());
        log.info("getPayload============>[{}]",outMsg.getPayload());
    }
}

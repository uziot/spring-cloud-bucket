package com.uziot.bucket.dubbo.service;

/**
 * @author shidt
 * @version V1.0
 * @className TestService
 * @date 2020-11-28 15:46:46
 * @description 一些模拟数据库事务的方法
 */
public interface TestService {
    /**
     * insert
     */
    void insert();

    /**
     * delete
     */
    void delete();

    /**
     * update
     */
    void update();

    /**
     * select
     */
    void select();
}

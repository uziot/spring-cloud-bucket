package com.uziot.bucket.dubbo.service;

import java.util.concurrent.CompletableFuture;

/**
 * @author shidt
 * @version V1.0
 * @className AsyncTestService
 * @date 2020-11-28 15:46:46
 * @description 模拟异步服务接口
 */
public interface AsyncTestService {

    CompletableFuture<String> sayHello(String name);

}

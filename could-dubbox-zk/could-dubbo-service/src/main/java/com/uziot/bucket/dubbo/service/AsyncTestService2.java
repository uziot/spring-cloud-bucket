package com.uziot.bucket.dubbo.service;

/**
 * @author shidt
 * @version V1.0
 * @className AsyncTestService2
 * @date 2020-11-28 15:46:46
 * @description 模拟异步服务接口
 */
public interface AsyncTestService2 {

    String sayHello(String name);

}

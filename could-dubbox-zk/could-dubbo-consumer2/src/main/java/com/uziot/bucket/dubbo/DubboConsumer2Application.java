package com.uziot.bucket.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shidt
 * @version V1.0
 * @className DubboConsumerApplication
 * @date 2021-01-09 22:06:18
 * @description
 */
@SpringBootApplication
public class DubboConsumer2Application {
    public static void main(String[] args) {
        SpringApplication.run(DubboConsumer2Application.class, args);
    }
}

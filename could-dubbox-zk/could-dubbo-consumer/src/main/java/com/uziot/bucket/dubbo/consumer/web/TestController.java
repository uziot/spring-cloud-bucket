package com.uziot.bucket.dubbo.consumer.web;

import com.uziot.bucket.dubbo.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author shidt
 * @version V1.0
 * @className TestController
 * @date 2020-11-28 15:46:46
 * @description 调用Dubbo
 */
@Slf4j
@RestController
public class TestController {

    @DubboReference(group = "dev", version = "1.0.0")
    private TestService testServiceImpl;

    @RequestMapping("/ins")
    public String ins() {
        testServiceImpl.insert();
        log.info("调用了consumer 的 insert 控制器");
        return "ins";
    }

    @RequestMapping("/del")
    public String del() {
        testServiceImpl.delete();
        log.info("调用了consumer 的 del 控制器");
        return "del";
    }

    @RequestMapping("/upd")
    public String upd() {
        testServiceImpl.update();
        log.info("调用了consumer 的 upd 控制器");
        return "upd";
    }

    @RequestMapping("/sel")
    public String sel() {
        testServiceImpl.select();
        log.info("调用了consumer 的 sel 控制器");
        return "sel";
    }
}

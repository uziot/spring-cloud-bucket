/*
 * MIT License
 *
 * Copyright (c) 2020 SvenAugustus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.uziot.bucket.dubbo.provider;

import com.uziot.bucket.dubbo.service.AsyncTestService2;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.AsyncContext;
import org.apache.dubbo.rpc.RpcContext;

/**
 * @author shidt
 * @version V1.0
 * @className AsyncTestServiceImpl2
 * @date 2020-11-28 15:46:46
 * @description 模拟异步服务接口
 * 注意：Provider端异步执行和Consumer端异步调用是相互独立的，你可以任意正交组合两端配置
 */
@Slf4j
@DubboService(
        group = "dev",
        version = "1.0.0",
        cluster = "failover",
        retries = 3,
        loadbalance = "leastactive",
        interfaceClass = AsyncTestService2.class)
public class AsyncTestServiceImpl2 implements AsyncTestService2 {

    @Override
    public String sayHello(String name) {
        return syncExec(name);
//        return asyncExec(name);
    }

    private String syncExec(String name) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String result = "提供商的同步响应（正常方式）： " + name;
        log.info(result);
        return result;
    }

    private String asyncExec(String name) {
        final AsyncContext asyncContext = RpcContext.startAsync();
        new Thread(() -> {
            // 如果要使用上下文，则必须要放在第一句执行
            asyncContext.signalContextSwitch();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 写回响应
            String resp = "提供者的异步响应（AsyncContext方式）：" + name;
            log.info(resp);
            asyncContext.write(resp);
        }).start();
        return null;
    }

}

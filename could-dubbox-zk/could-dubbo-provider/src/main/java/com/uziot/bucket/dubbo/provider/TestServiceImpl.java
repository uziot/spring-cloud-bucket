package com.uziot.bucket.dubbo.provider;

import com.uziot.bucket.dubbo.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author shidt
 * @version V1.0
 * @className TestServiceImpl
 * @date 2020-11-28 15:46:46
 * @description 模拟数据库事务实现
 */
@Slf4j
@DubboService(
        group = "dev",
        version = "1.0.0",
        cluster = "failover",
        retries = 3,
        loadbalance = "leastactive",
        interfaceClass = TestService.class)
public class TestServiceImpl implements TestService {

    @Override
    public void insert() {
        System.out.println("insert");
        log.info("调用了consumer 的 insert 服务实现类");
    }

    @Override
    public void delete() {
        System.out.println("delete");
        log.info("调用了consumer 的 delete 服务实现类");
    }

    @Override
    public void update() {
        System.out.println("update");
        log.info("调用了consumer 的 update 服务实现类");
    }

    @Override
    public void select() {
        System.out.println("select");
        log.info("调用了consumer 的 select 服务实现类");
    }
}

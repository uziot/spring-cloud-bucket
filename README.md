# SpringCloud-Bucket

## 项目介绍 
整理spring全家桶的各种各样使用案例，包含springMVC~SpringBoot~SpringCloud，
以及集成的各种各样第三方使用案例的整合集，准备开放该仓库，方便自己以及爱学习的各位，
直接拉取案例。毕竟~~~好记性~不如烂笔头~

从另一方面来讲，由于SpringBoot框架目前越来越火，
慢慢的市场上大多数企业都逐渐使用SpringBoot或者SpringCloud作为基础开发框架，
作为程序员来讲，一时半会也不可能记住形形色色的框架配置。当在使用的时候，去度娘找相关文章，
一搜索出来一大片，找起来也不是很方便，也不成篇章，因此想了很长时间，结合自己的所学所看，
决定整理这么一个大而全的项目，尽力的容纳常见的使用频率高的知识点集合。

也许对资深的程序老师来讲，看起来觉得冗乱，但也许对初学者以及有想法整理这么一片常用记录的你来讲，
也许有所作用。面对越来越多的知识点，也不可能全部来自自己手写，
项目中也有很多是在互联网的开源项目中抄写的，并做了验证和规整。
另外，我也在尽量的检查和测试案例中的BUG，但肯定有很多的问题，也欢迎的大家指出。
有很多代码来自互联网或者开源看项目，如果有您觉得不合适的，欢迎指出，笔者第一时间整改，
谢谢您的宽容和理解！也期待您的支持，欢迎点上红心! 

## 项目目录
### SpringBoot常用功能包 	--boot-basic-bucket
    -boot-actuator 			--集成actuator监控
    -boot-advice			--SpringBoot统一响应和统一异常响应包装
    -boot-aop				--SpringBoot切面AOP示例
    -boot-async				--简单的异步操作包
    -boot-banner			--自定义启动图像
    -boot-batch				--SpringBatch的基础使用
    -boot-captcha			--集成图形验证码
    -boot-common-base		--基础工具类包
    -boot-cros				--SpringBoot跨域处理
    -boot-data-jpa			--SpringBoot集成jpa作为持久框架
    -boot-db-lock			--简单的基于数据库的分布式锁
    -boot-dynamic-dataSource--多数据源动态数据源包
    -boot-easyexcel			--集成阿里巴巴的Excel读取和写出
    -boot-ehcache			--集成Ehcache作为缓存使用
    -boot-email				--邮件发送
    -boot-event				--时间绑定和触发
    -boot-feign				--集成feign作为Http伪客户端的使用
    -boot-file-upload		--文件上传以及多文件同时上传
    -boot-ftp				--ftp上传和下载
    -boot-guava-cache		--guava缓存等
    -boot-i18n				--国际化
    -boot-interceptor		--SpringBoot拦截器的使用
    -boot-ipaddr			--SpringBoot读取和解析IP地址
    -boot-jasypt			--SpringBoot配置文件配置加密如数据库地址密码加密
    -boot-jsoup				--java爬虫的简单使用
    -boot-jwt				--JsonWebToken的使用
    -boot-logback			--日志打印和分类打印输出
    -boot-mdc				--MDC日志框架的使用
    -boot-memcache			--SpringBoot集成memcache
    -boot-mongodb			--SpringBoot集成mongodb持久框架
    -boot-mybatis			--SpringBoot整合Mybatis使用功能Mysql或者Oracle
    -boot-param-validate	--SpringBoot方便的参数校验
    -boot-poi				--集成POI解析OFFICE文档
    -boot-rabbitmq			--SpringBoot集成rabbit作为消息队列的使用
    -boot-redis				--集成Redis缓存的使用
    -boot-rest-template		--使用RestTemplate发起请求和接收响应
    -boot-retry				--SpringBoot的重试机制
    -boot-runner			--SpringBoot启动运行配置
    -boot-scan-login		--开源的扫码登陆
    -boot-scheduler			--SpringBoot定时任务的使用
    -boot-security			--SpringBoot集成SpringSecurity做为安全框架的使用
    -boot-sequence			--Oracle序列的使用
    -boot-sharding-jdbc		--分库分表框架的使用
    -boot-shiro				--SpringBoot集成Shiro做为安全框架的使用
    -boot-socketio			--Socket通信的使用功能
    -boot-swagger			--SpringBoot集成Swagger作为接口文档
    -boot-test				--SpringBoot单元测试使用方法
    -boot-ureport			--SpringBoot集成Ureport自定义报表的使用
    -boot-webflux			--响应式编码的简单使用
    -boot-webservice		--集成webservice接口写法介绍
    -boot-websocket			--使用websocket前后台通讯
    -boot-work-activiti		--SpringBoot集成activiti工作流的简单使用
    -boot-work-flowable		--SpringBoot集成flowable工作流的简单使用
    -boot-xss               --springboot跨域配置的使用
    -boot-xstream           --springboot使用xStream转换对象和xml的使用
    -boot-smart-stop        --springboot优雅停机的几种方式使用
    -boot-servlet           --springboot使用原生Servlet开发接口
    -boot-jetty             --springboot集成Jetty作为容器使用
    -boot-dozermapper       --dozermapper使用XML对象映射
    -boot-autoconfig        --Springboot自动配置默认清单配置
    -boot-maven-package     --Springboot项目Maven打包分离lib和配置
### spring-boot-integration的使用案例
    -boot-integration-demo1 --使用简单的案例1

### Dubbox简单使用			--分布式微服务dubbox简单使用could-dubbox-zk
    -could-dubbo-consumer	--消费者1
    -could-dubbo-consumer2	--消费者2
    -could-dubbo-provider	--服务提供者
    -could-dubbo-service	--接口Interface包
### SpringCloud之Eureka注册中心微服务			----could-eureka	
	-spring-cloud-config-client					 	--配置中心客户端
	-spring-cloud-config-server						--配置中心服务器-从Git或者文件读取配置
	-spring-cloud-config-server-db					--配置中心服务器-从数据库读取配置
	-spring-cloud-config-server-db-client			--配置中心从数据库读取客户端测试
	-spring-cloud-eureka							--模块的Eureka注册中心
	-spring-cloud-feign								--Feign伪客户端的使用
	-spring-cloud-feign-fileupload-client			--Feign客户端上传文件客户端测试
	-spring-cloud-feign-fileupload-server			--Feign客户端上传文件服务端
	-spring-cloud-feign-hystrix						--hystrix熔断器的使用
	-spring-cloud-hystrix-collapser-consumer		--collapser并发请求合并的使用(客户端)
	-spring-cloud-hystrix-collapser-provider		--collapser并发请求合并的使用(服务端)
	-spring-cloud-hystrix-dashboard					--hystrix监控面板
	-spring-cloud-producer							--服务提供者1
	-spring-cloud-producer2							--服务提供者2
	-spring-cloud-ribbon							--ribbon负载均衡使用
	-spring-cloud-zipkin-client						--使用zipkin链路跟踪客户端入口
	-spring-cloud-zipkin-client2					--使用zipkin链路跟踪客户端调用
	-spring-cloud-zipkin-server						--使用zipkin链路跟踪服务端
	-spring-cloud-zuul								--zuul网关全配置和局过滤器介绍
	-spring-cloud-zuul-router						--zuul网关动态路由配置
### SpringCloud集成Oauth2.0认证授权				 ----could-eureka-security-oauth2	
	-distributed-security-discovery					--注册中心
	-distributed-security-gateway					--网关配置
	-distributed-security-resource					--资源服务
	-distributed-security-uaa						--认证授权中心
### 23种设计模式包							    ----design-patterns	
### SpringCloud常用文档						    ----spring-cloud-document
    -centos                                         --CENTOS服务器常用技巧
    -deepin                                         --深度服务器使用技巧
    -docker                                         --Docker的安装以及配置
    -docker构建微服务平台                            --Docker构建微服务平台、命令等
    -mysql                                          --Mysql常用文档
    -ngnix                                          --Ngnix常用文档
    -oracle                                         --Oracle常用文档
    -redis                                          --Redis常用文档
    -spring-could-book                              --Spring-could-book微服务相关安装和使用
    -zookeeper                                      --zookeeper常用文档
### SpringMVC简单的使用						    ----spring-mvc-basic
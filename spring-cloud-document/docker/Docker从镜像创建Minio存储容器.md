docker run  -p 9010:9000 --name minio \
 -d --restart=always \
 -e MINIO_ACCESS_KEY=minio \
 -e MINIO_SECRET_KEY=minio@321 \
 -v /data/docker/minio/data:/data \
 -v /data/docker/minio/config:/root/.minio \
  minio/minio server /data
  
#需要在根目录下存在  data config 文件夹
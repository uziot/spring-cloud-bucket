# Docker中安装MongoDB
## 1. 搜索、拉取和查看镜像
docker search mongo

docker pull mongo

docker images | grep mongo

我是用docker pull mongo:3.6 安装3.6版本

## 2. 使用 docker 安装 mongodb(创建容器并运行)
docker ps -a 查看所有容器

docker images 查看所有镜像

从镜像创建容器
docker run -p 27917:27017 -d $PWD/db:/data/db -d mongo:3.6 --name mongodb

启动容器
docker start/stop/stats/restart mongodb

进入容器mongodb
docker exec -it mongodb bash

使用数据库
use admin



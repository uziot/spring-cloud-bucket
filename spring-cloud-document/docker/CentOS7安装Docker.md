1.检查内核版本，返回的值大于3.10即可：# uname -r
3.更新yum(时间视网速而定),确保yum是最新的：$yum updata
4.安装 Docker。从 2017 年 3 月开始 docker 在原来的基础上分为两个分支版本: Docker CE 和 Docker EE。
Docker CE 即社区免费版，Docker EE 即企业版，强调安全，但需付费使用。
移除旧的版本（如果是刚安装的纯净系统可跳过此步骤）：# yum remove docker
docker-client
docker-client-latest
docker-common
docker-latest
docker-latest-logrotate
docker-logrotate
docker-selinux
docker-engine-selinux
docker-engine
5.安装一些必要的系统工具:
$yum install -y yum-utils device-mapper-persistent-data lvm2

6.添加软件源信息：
$ yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
7.更新 yum 缓存： $yum makecache fast
8.安装 Docker-ce：# yum -y install docker-ce
9.启动 Docker 后台服务: $ systemctl start docker
开机启动：systemctl enable docker

10. 测试运行 hello-world : $ docker run hello-world
11.删除 Docker CE：
$yum remove docker-ce
$ rm -rf /var/lib/docker

文章来源
https://blog.csdn.net/qq_38531678/article/details/84098471
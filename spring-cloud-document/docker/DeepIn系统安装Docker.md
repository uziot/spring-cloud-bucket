1.安装yml
1.1 首先切换到root用户身份
1.2 apt-get install build-essential
1.3 apt-get install yum
1.4 apt-get install  curl
2.安装docker
2.1 sudo apt-get update 
2.2 sudo apt-get install -y docker.io
2.3 启动Docker :  systemctl start docker
2.4 设置开机启动Docker : systemctl enable docker
2.5 查看Docker是否成功 ： docker version
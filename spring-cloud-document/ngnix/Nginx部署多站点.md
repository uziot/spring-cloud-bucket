nginx指定文件路径有两种方式root和alias。root的作用域http、server、location、if，alias的作用域是location。

root与alias主要区别在于nginx如何解释location后面的uri，这会使两者分别以不同的方式将请求映射到服务器文件上。

root的处理结果是：root路径＋location路径

alias的处理结果是：使用alias路径替换location路径，所以必须以/结尾
####不同域名
```
http {
    server {
        listen          80;
        server_name     www.domain1.com;
        access_log      logs/domain1.access.log main;
        location / {
            index index.html;
            root  /var/www/domain1.com/htdocs;
        }
    }
    server {
        listen          80;
        server_name     www.domain2.com;
        access_log      logs/domain2.access.log main;
        location / {
            index index.html;
            root  /var/www/domain2.com/htdocs;
        }
    }
}
```

####同域名
```
http {
    server {
        listen          80;
        server_name     www.domain.com;
        access_log      logs/domain.access.log main;
		
        location /domain1/ {
            index index.html;
            alias  /var/www/domain1.com/htdocs/;
        }
		
		location /domain2 {
            index index.html;
            alias  /var/www/domain2.com/htdocs/;
        }
    }
}
```
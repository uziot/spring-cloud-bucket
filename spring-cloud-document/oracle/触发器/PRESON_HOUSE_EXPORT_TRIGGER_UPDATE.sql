create or replace trigger persion_house_trigger_update
after
update on persion_house
for each row
    declare
      count_ph number;
      old_time date;
    begin
      SELECT COUNT(*) INTO count_ph FROM PERSON_HOUSE_EXPORT t WHERE t.ctf_id=:new.CTFID;
      if(count_ph > 0) then
         SELECT t.create_date INTO old_time FROM PERSON_HOUSE_EXPORT t  WHERE t.ctf_id=:new.CTFID;
         DELETE FROM  PERSON_HOUSE_EXPORT t WHERE t.CTF_ID=:new.CTFID;
         INSERT INTO PERSON_HOUSE_EXPORT(id,User_Name,CTF_ID,CREATE_DATE,UPDATE_DATE)
         VALUES(:new.rowid,:new.NAME,:new.CTFID,old_time,sysdate);
      else
         INSERT INTO PERSON_HOUSE_EXPORT(id,User_Name,CTF_ID,CREATE_DATE,UPDATE_DATE)
         VALUES(:new.rowid,:new.NAME,:new.CTFID,sysdate,sysdate);
      end if;
     end;
1、timedatectl查看时间各种状态：
Local time: 四 2014-12-25 10:52:10 CST
Universal time: 四 2014-12-25 02:52:10 UTC
RTC time: 四 2014-12-25 02:52:10
Timezone: Asia/Shanghai (CST, +0800)
NTP enabled: yes
NTP synchronized: yes
RTC in local TZ: no

2、timedatectl list-timezones: 列出所有时区
3、timedatectl set-local-rtc 1 将硬件时钟调整为与本地时钟一致, 0 为设置为 UTC 时间
4、timedatectl set-timezone Asia/Shanghai 设置系统时区为上海
————————————————
校准时间
yum -y install ntp
#通过阿里云时间服务器校准时间
ntpdate ntp1.aliyun.com



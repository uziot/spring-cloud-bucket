发现 telnet报错: telnet: connect to address IP : No route to host
于是检查目标主机的 firewalld ,发先没有放行对应端口

在 firewalld 防火墙中放行端口，可以使用 firewall-cmd 命令。

首先，通过以下命令检查防火墙状态是否为活动状态：

sudo firewall-cmd --state

如果防火墙处于活动状态，则需要使用以下命令以永久方式将端口添加到防火墙例外规则中。例如，要将 TCP 端口 80 和 443 添加到防火墙例外规则中：

sudo firewall-cmd --permanent --add-port=80/tcp

sudo firewall-cmd --permanent --add-port=443/tcp
请注意，上述命令使用了 --permanent 选项，这会将规则添加到永久配置文件中。

将更改加载到防火墙中：

sudo firewall-cmd --reload

确认端口是否已放行：

sudo firewall-cmd --list-ports

在输出结果中，可以看到已添加的端口列表：

80/tcp 443/tcp

如果您只想暂时放行一个端口，则可以使用 --zone 选项指定要更改的区域，并省略 --permanent 选项：

复制代码

sudo firewall-cmd --zone=public --add-port=80/tcp

这将在当前会话中放行端口，但不会将其保存为永久规则。


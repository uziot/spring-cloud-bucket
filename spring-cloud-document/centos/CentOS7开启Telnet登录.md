Centos7开启telnet登录
telnet登录服务器没有ssh方式登录安全，但由于升级openssh，需要使用telnet做备用登录方式，现记录过程。

$sudo yum install telnet telnet-server -y
$sudo yum install xinetd
$sudo systemctl start telnet.socket
$sudo systemctl start xinetd
$sudo systemctl status telnet.socket
$sudo status xinetd

在终端输入telnet ip ，而后输入用户名和密码即可登录服务器。
注意：需要在防火墙放行telnet的缺省端口23，如果是临时使用telnet，可暂时关闭firewalld.

##另外，记录下更改非默认登录端口的过程。
1. 修改/etc/services的48行和49行的端口为非23端口
2.同步修改/usr/lib/systemd/system/telnet.socket里的端口号
3.重启telnet服务
```
$sudo systemctl daemon-reload 
$sudo systemctl restart telnet.socket
```
4.firewalld基于服务放行。
5.修改firewalld关于telnet服务放行新的端口号，文件在/etc/firewalld/services/telnet.xml
6.firewalld基于端口放行。
7.登录测试。
附注：
#开启Root的telnet登录。
$sudo mv /etc/securetty /etc/securetty.bak

结束。
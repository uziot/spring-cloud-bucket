1、需要检查端口连通情况：ping IP以及telnet IP Port 
2、TOP查看当前虚拟机内存使用情况，调用监控查看内存时序图。
3、查看weblogic控制台的状态，服务状态，运行日志。
4、查看domain项目日志，查看日志中是否有严重的错误。
5、查询数据库连接池情况，是否和配置的链接池大小已经用完，检查是否慢查询。
6、探测接口情况，可使用CURL 命令请求服务接口。
7、JVM运行情况监测命令
命令	说明
jstat -gcutil PID
jstat -gc PID2000 20	查看堆内存以及GC信息
C初始化容量，U已使用
每间隔2000ms输出GC情况，共20次
-XX:+HeapDumpOnOutOfMemoryError
-XX:HeapDumpPath=./home	OOM时自动保存堆内存快照信息
快照保存地址
jmap -histo PID>/home/jmap-histo-pid.txt	输出堆内存活对象统计到文件
jmap -dump:live,format=b,file=/home/dumpaaaa.hprof PID	Dump堆信息到文件，后续可使用工具，如AMT(Memory Anlysis Tool)等工具查看
-F 强制
jmap -heap PID	查看Heap使用以及垃圾
Jstack PID	查询栈内情况/可导出文件后续
Jstack <OPTION> PID 	-F 当正常输出不响应时，强制输出线程堆栈
-l 除开堆栈以外，附加锁信息
-m附加Native方法调用（一般不使用）
top -H -p PID	查看进程中的线程数
Jps -l 	
netstat -ano|grep PID	
netstat -ant|grep -i PORT|wc -l
netstat -anp|grep -i PORT|wc -l
netstat -anp|grep -i PORT|grep TIME_WAIT
	查看端口连接数，不输入wc -l则查看连接明细,nap附带查看进程，-统计数量
grep LISTEN监听
grep TIME_WAIT  超时
grep ESTABLISHED 正在通信
grep CLOSE_WAIT 对方主动断开或网络断开
Ps -ef|grep httd|wc -l	统计http协议连接数
jps -v	查看java进程

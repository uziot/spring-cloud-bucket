深度deepin跟ubuntu使用几乎一样的，安装时默认是禁用root登录的，只能普通用户登录后，sudo su - root输入安装时的普通用户密码去切换到root权限操作，或用安装时的普通用户去sudo passwd root去设置root的密码即可。不过deepin是深度系统旗下的，有众多安装包比较好用，比如支持安装微信、QQ、百度云盘和360浏览器等等....

1、安装ssh服务

apt-get install openssh-server

2、修改/etc/ssh/sshd_config配置

vim  /etc/ssh/sshd_config
(PermitRootLogin yes是允许可root登录)

3、重启服务
/etc/init.d/ssh restart
(远程的虚拟机可连接上我的电脑了)
       由于深度的deepin跟CentOS不一样，默认是没有开启防火墙和iptables， 所以开放端口时需额外注意安全问题，尽量不采用默认的端口，如22、80和443等等... 可手动安装ufw和gufw(可视防火墙)去设置出口入口的规则...

apt-get install ufw或apt-get install gufw

4 有时候链接不上重启服务就可以了
/etc/init.d/ssh restart

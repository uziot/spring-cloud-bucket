package com.uziot.bucket.feign.service;

import lombok.Data;

/**
 * @author shidt
 * @version V1.0
 * @className ProductDTO
 * @date 2021-02-19 16:30:54
 * @description
 */
@Data
public class ProductDTO {
    private String name;
    private String type;
}

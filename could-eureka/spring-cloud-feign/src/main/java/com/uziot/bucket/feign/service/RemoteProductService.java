package com.uziot.bucket.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shidt
 * @version V1.0
 * @className RemoteProductService
 * @date 2021-02-19 16:37:04
 * @description
 * 注意是通过服务名访问服务的地址，而不是使用IP地址访问，
 * 因此是实现负载均衡的必要条件
 *
 */

@FeignClient(value = "producer-service")
public interface RemoteProductService {
    /**
     * 使用Ribbon封装的伪http客户端请求远程微服务，并实现负载均衡
     *
     * @param name name
     * @return list
     */
    @GetMapping(value = "/producer/hello?name={name}")
    String getProductByName(@PathVariable String name);
}

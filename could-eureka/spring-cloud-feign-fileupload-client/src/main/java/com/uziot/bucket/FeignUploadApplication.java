package com.uziot.bucket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shidt
 * @version V1.0
 * @className FeignUploadApplication
 * @date 2021-02-24 09:36:48
 * @description
 */

@Slf4j
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class FeignUploadApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignUploadApplication.class, args);
    }
}

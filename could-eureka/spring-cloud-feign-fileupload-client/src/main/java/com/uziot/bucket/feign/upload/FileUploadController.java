package com.uziot.bucket.feign.upload;

import com.uziot.bucket.feign.upload.client.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author shidt
 * @version V1.0
 * @className FileUploadController
 * @date 2021-02-24 10:08:12
 * @description
 */
@RestController
public class FileUploadController {

    @Autowired
    private UploadService uploadService;

    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String handleFileUpload(@RequestPart(value = "file") MultipartFile file) {
        return uploadService.handleFileUpload(file);
    }
}

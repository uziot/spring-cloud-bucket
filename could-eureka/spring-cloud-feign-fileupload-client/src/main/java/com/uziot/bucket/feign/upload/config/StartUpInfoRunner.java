package com.uziot.bucket.feign.upload.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import java.time.LocalDateTime;

/**
 * @author shidt
 * @version V1.0
 * @className RedisConfig
 * @date 2020-02-28 21:38:01
 * @description redis程序启动后通过ApplicationRunner处理一些事务配置类
 */
@Slf4j
@Component
public class StartUpInfoRunner implements ApplicationRunner {

    @Value("${server.port}")
    private int port;

    @Autowired
    private ConfigurableApplicationContext configurableApplicationContext;

    @Override
    public void run(ApplicationArguments applicationArguments) {
        if (configurableApplicationContext.isActive()) {
            log.info(" __    ___   _      ___   _     ____ _____  ____ ");
            log.info("/ /`  / / \\ | |\\/| | |_) | |   | |_   | |  | |_  ");
            log.info("\\_\\_, \\_\\_/ |_|  | |_|   |_|__ |_|__  |_|  |_|__ ");
            log.info("                                                      ");
            log.info("平台部署完成，项目访问地址：http://localhost:" + port + "/swagger-ui/index.html#/");
            log.info("SpringbootApplication Successful startup，Time：" + LocalDateTime.now());
        }
    }
}

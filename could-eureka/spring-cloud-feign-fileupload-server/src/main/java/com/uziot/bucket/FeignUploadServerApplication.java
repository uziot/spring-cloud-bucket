package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shidt
 * @version V1.0
 * @className FeignUploadServerApplication
 * @date 2021-02-24 09:37:43
 * @description
 */

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class FeignUploadServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignUploadServerApplication.class, args);
    }
}

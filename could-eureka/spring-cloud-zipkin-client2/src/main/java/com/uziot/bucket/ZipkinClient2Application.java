package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author shidt
 * @version V1.0
 * @className ZipkinClientApplication
 * @date 2021-03-03 15:22:00
 * @description
 */

@EnableDiscoveryClient
@SpringBootApplication
public class ZipkinClient2Application {
    public static void main(String[] args) {
        SpringApplication.run(ZipkinClient2Application.class, args);
    }
}

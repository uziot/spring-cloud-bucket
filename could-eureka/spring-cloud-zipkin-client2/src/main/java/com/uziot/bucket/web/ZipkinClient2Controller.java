package com.uziot.bucket.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Lenovo
 */
@RestController
public class ZipkinClient2Controller {
    private final Logger logger = LoggerFactory.getLogger(ZipkinClient2Controller.class);

    @RequestMapping("/hello")
    public String index(@RequestParam String name, HttpServletRequest request) {
        logger.info("request one  name is " + name);
        return "hello " + name + "，this is producer2 messge";
    }
}
 ### Zipkin Server
 
 1. 下载zipkin.jar
 
 本地jar手动去下载太大不提供：https://zipkin.io/pages/quickstart.html
 
 ```bash
 curl -sSL https://zipkin.io/quickstart.sh | bash -s
 ```
 
2. 启动zipkin-server

>指明数据库、rabbitMQ启动。如不指明，默认记录在内存里，重启丢记录

```bash
java -jar zipkin.jar --server.port=9411 --zipkin.storage.type=mysql --zipkin.storage.mysql.db=fw_zipkin --zipkin.storage.mysql.username=root --zipkin.storage.mysql.password=123456 --zipkin.storage.mysql.host=localhost --zipkin.storage.mysql.port=3306 --zipkin.collector.rabbitmq.addresses=localhost:5672 --zipkin.collector.rabbitmq.username=fwcloud  --zipkin.collector.rabbitmq.password=fwcloud
```

[**MySQL脚本地址下载**](https://github.com/openzipkin/zipkin/blob/master/zipkin-storage/mysql-v1/src/main/resources/mysql.sql)
 
执行顺序：
**需要先启动zipkin-版本jar包管理控制台
mysql启动命令，内存启动直接java -jar 即可
java -jar C:\zipkin-server-2.23.2-exec.jar --server.port=9411 --zipkin.storage.type=mysql --zipkin.storage.mysql.db=zipkin --zipkin.storage.mysql.username=root --zipkin.storage.mysql.password=root --zipkin.storage.mysql.host=localhost --zipkin.storage.mysql.port=3306
也可保存到es或者消息队列中


1.启动eureka注册中心

2.启动zipkin-client

3.启动zipkin-client2

4.启动zuul网关

访问控制台：http://localhost:9411/zipkin/

注册中心地址：http://localhost:53000/

第一个微服务节点：http://localhost:53200/zipkin1/hello-zipkin

第二个微服务节点：http://localhost:53201/zipkin2/hello?name=zipkin2

网关访问第一个微服务节点：http://localhost:53010/zipkin1/hello-zipkin


从网关访问第一个微服务节点，第一个微服务节点调用第二个微服务节点，实现多链路跟踪

然后可以查询链路信息

提供的接口文档：https://zipkin.io/zipkin-api/#/




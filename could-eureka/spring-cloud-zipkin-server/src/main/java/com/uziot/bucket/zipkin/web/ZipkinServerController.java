package com.uziot.bucket.zipkin.web;

import com.alibaba.fastjson.JSONArray;
import com.uziot.bucket.zipkin.remote.ZipkinOpenApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className ZipkinServerController
 * @date 2021-03-04 09:19:34
 * @description 目前仅提供两条简单的查询，其他查询方式类似，在接口文档查看即可
 * @see ZipkinOpenApiService
 * 接口文档地址：https://zipkin.io/zipkin-api/
 * 实际访问本地：http://127.0.0.1:9411/zipkin
 */

@RestController
public class ZipkinServerController {

    @Autowired
    private ZipkinOpenApiService zipkinOpenApiService;

    /**
     * 查询已注册的服务列表
     *
     * @return 列表
     */
    @GetMapping(value = "/services")
    List<String> services() {
        return zipkinOpenApiService.services();
    }

    /**
     * 根据跟踪ID查询链路信息
     *
     * @param traceId 跟踪ID
     * @return 链路信息
     */
    @GetMapping(value = "/trace/{traceId}")
    JSONArray trace(@PathVariable String traceId) {
        return zipkinOpenApiService.trace(traceId);
    }
}

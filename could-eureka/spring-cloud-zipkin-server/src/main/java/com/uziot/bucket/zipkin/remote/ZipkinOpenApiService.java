package com.uziot.bucket.zipkin.remote;

import com.alibaba.fastjson.JSONArray;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className ZipkinServicesService
 * @date 2021-03-04 09:12:23
 * @description
 */

@FeignClient(name = "zipkin-center", url = "http://localhost:9411/api/v2")
public interface ZipkinOpenApiService {

    /**
     * 查询已注册的服务列表
     *
     * @return 列表
     */
    @GetMapping(value = "/services")
    List<String> services();

    /**
     * 根据traceId查询跟踪信息
     *
     * @param traceId traceId
     * @return 跟踪对象
     */
    @GetMapping(value = "/trace/{traceId}")
    JSONArray trace(@PathVariable String traceId);


}

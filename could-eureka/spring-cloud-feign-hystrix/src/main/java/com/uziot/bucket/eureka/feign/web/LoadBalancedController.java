package com.uziot.bucket.eureka.feign.web;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.eureka.feign.service.ProductDTO;
import com.uziot.bucket.eureka.feign.service.ProductService;
import com.uziot.bucket.eureka.feign.service.RemoteProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className LoadBalancedController
 * @date 2021-02-19 16:07:48
 * @description 使用Feign客户端访问远程微服务
 * Feign客户端的好处是：可以访问远程微服务就像访问本地服务一样方便
 * 远程服务封装和代码风格实现完美统一，统一封装微服务访问端点
 * 实现负载均衡
 */

@Slf4j
@RestController
public class LoadBalancedController {

    @Autowired
    private ProductService productService;

    @Autowired
    private RemoteProductService remoteProductService;

    @GetMapping(value = "/feign/hello")
    public String getObject() {
        String feignRemote = remoteProductService.getProductByName("FeignRemote");
        log.info("微服务响应结果为：{}", feignRemote);
        return feignRemote;
    }

    @GetMapping(value = "/feign/local")
    public String getProducerLocal() {
        ProductDTO product = productService.getProductByName("local");
        return JSON.toJSONString(product);
    }
}

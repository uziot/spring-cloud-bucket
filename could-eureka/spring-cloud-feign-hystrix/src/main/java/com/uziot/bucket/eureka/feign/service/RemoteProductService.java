package com.uziot.bucket.eureka.feign.service;

import com.uziot.bucket.eureka.feign.config.FeignConfiguration;
import com.uziot.bucket.eureka.feign.service.impl.RemoteProductServiceFeignFailBackImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shidt
 * @version V1.0
 * @className RemoteProductService
 * @date 2021-02-19 16:37:04
 * @description
 */
@FeignClient(value = "producer-service",
        // 异常时调用此类中的实现方法
        fallback = RemoteProductServiceFeignFailBackImpl.class,
        // 请求配置设定
        configuration = FeignConfiguration.class)
public interface RemoteProductService {
    /**
     * 使用Ribbon封装的伪http客户端请求远程微服务，并实现负载均衡
     *
     * @param name name
     * @return list
     */
    @GetMapping(value = "/producer/hello?name={name}")
    String getProductByName(@PathVariable String name);
}

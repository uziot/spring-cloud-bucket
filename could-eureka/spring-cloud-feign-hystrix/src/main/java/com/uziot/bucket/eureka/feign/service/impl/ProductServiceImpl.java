package com.uziot.bucket.eureka.feign.service.impl;


import com.uziot.bucket.eureka.feign.service.ProductDTO;
import com.uziot.bucket.eureka.feign.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className ProductServiceImpl
 * @date 2021-02-19 16:32:18
 * @description
 */
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public ProductDTO getProductByName(String name) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName(name);
        productDTO.setType("ProductServiceImpl.class");
        log.info("使用本地服务查询返回商品信息结果：{}", productDTO);
        return productDTO;
    }
}

package com.uziot.bucket.eureka.feign.service;

/**
 * @author shidt
 * @version V1.0
 * @className ProductService
 * @date 2021-02-19 16:30:29
 * @description
 */

public interface ProductService {
    ProductDTO getProductByName(String name);
}

package com.uziot.bucket.eureka.feign.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;

/**
 * @author Lenovo
 * Feign调用的时候添加请求头from
 * 给feign执行伪http请求时拦截请求，并在请求头中添加网关携带鉴权内容
 * 可在服务提供端进行请求头拦截，鉴别非网关调用的请求可即时拦截了处理
 */
@Configuration
public class FeignConfiguration implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("from", "gateway");
    }
}
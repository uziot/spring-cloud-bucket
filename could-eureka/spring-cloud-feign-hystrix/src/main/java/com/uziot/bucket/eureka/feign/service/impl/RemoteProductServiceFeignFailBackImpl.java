package com.uziot.bucket.eureka.feign.service.impl;

import com.uziot.bucket.eureka.feign.service.RemoteProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shidt
 * @version V1.0
 * @className RemoteProductServiceImpl
 * @date 2021-02-20 16:43:57
 * @description
 */
@Slf4j
@Service
public class RemoteProductServiceFeignFailBackImpl implements RemoteProductService {
    @Override
    public String getProductByName(String name) {
        log.info("我是远程服务发生异常熔断之后，调用的服务..........");
        return "This is FeignFailBack....：" + name;
    }
}

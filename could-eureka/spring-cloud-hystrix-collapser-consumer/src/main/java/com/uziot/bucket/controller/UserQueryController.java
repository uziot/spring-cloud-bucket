package com.uziot.bucket.controller;

import com.uziot.bucket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className UserQueryController
 * @date 2021-02-24 11:09:03
 * @description
 */
@RestController
public class UserQueryController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/users/{userId}")
    public Object userQuery(@PathVariable Long userId) {
        return userService.findById(userId);
    }
}

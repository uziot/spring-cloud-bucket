package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author shidt
 * @version V1.0
 * @className ConfigSerDbApplication
 * @date 2021-02-24 14:52:11
 * @description
 */

@EnableEurekaClient
@EnableConfigServer
@SpringBootApplication
public class ConfigSerDbApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigSerDbApplication.class, args);
    }
}

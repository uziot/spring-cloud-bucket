/*
 Navicat Premium Data Transfer

 Source Server         : mySQL
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : spring-cloud-bucket

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 24/02/2021 15:02:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for properties
-- ----------------------------
DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties`  (
  `id` int(11) NOT NULL,
  `key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `application` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `profile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of properties
-- ----------------------------
INSERT INTO `properties` VALUES (1, 'com.uziot.msg', 'test-stage-master', 'config-client', 'stage', 'master');
INSERT INTO `properties` VALUES (2, 'com.uziot.msg', 'test-online-master', 'config-client', 'online', 'master');
INSERT INTO `properties` VALUES (3, 'com.uziot.msg', 'test-online-develop', 'config-client', 'online', 'develop');
INSERT INTO `properties` VALUES (4, 'com.uziot.msg', 'hello-online-master', 'hello-service', 'online', 'master');
INSERT INTO `properties` VALUES (5, 'com.uziot.msg', 'hello-online-develop', 'hello-service', 'online', 'develop');

SET FOREIGN_KEY_CHECKS = 1;

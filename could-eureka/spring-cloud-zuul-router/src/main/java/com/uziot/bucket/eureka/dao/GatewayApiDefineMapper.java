package com.uziot.bucket.eureka.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author shidt
 * @version V1.0
 * @className BasicRoute
 * @date 2020-12-16 23:48:56
 * @description Zuul网关配置，数据库MAPPER
 */
@Mapper
public interface GatewayApiDefineMapper {
    int deleteByPrimaryKey(String id);

    int insert(GatewayApiDefine record);

    int insertSelective(GatewayApiDefine record);

    GatewayApiDefine selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(GatewayApiDefine record);

    int updateByPrimaryKey(GatewayApiDefine record);

    List<GatewayApiDefine> loadAll();
}
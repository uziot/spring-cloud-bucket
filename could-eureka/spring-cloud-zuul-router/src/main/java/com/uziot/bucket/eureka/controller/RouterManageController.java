package com.uziot.bucket.eureka.controller;

import com.uziot.bucket.eureka.dao.GatewayApiDefine;
import com.uziot.bucket.eureka.dao.GatewayApiDefineMapper;
import com.uziot.bucket.eureka.service.RefreshRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className EventApplication
 * @date 2020-12-15 11:02:07
 * @description 实现接口刷新Zuul网关实现读取数据库配置动态路由
 */
@RestController
public class RouterManageController {

    @Autowired
    private RefreshRouteService refreshRouteService;
    @Autowired
    private ZuulHandlerMapping zuulHandlerMapping;
    @Autowired
    private GatewayApiDefineMapper gatewayApiDefineMapper;

    @RequestMapping(value = "/refreshRoute")
    public String refreshRoute() {
        refreshRouteService.refreshRoute();
        return "刷新路由成功";
    }

    @GetMapping(value = "/watchNowRoute")
    public Map<String, Object> watchNowRoute() {
        return zuulHandlerMapping.getHandlerMap();
    }

    @GetMapping(value = "/router")
    public List<GatewayApiDefine> gatewayApiDefine() {
        return gatewayApiDefineMapper.loadAll();
    }


}

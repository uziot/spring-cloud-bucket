package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author shidt
 * @version V1.0
 * @className BasicRoute
 * @date 2020-12-16 23:48:56
 * @description Zuul网关配置，动态加载路由
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulRouterServer {
    public static void main(String[] args) {
        SpringApplication.run(ZuulRouterServer.class, args);
    }
}
package com.uziot.bucket.eureka.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
 * @author shidt
 * @version V1.0
 * @className BasicRoute
 * @date 2020-12-16 23:48:56
 * @description ZuulAuthFilter网关过滤器
 */
public class ZuulAuthFilter extends ZuulFilter {

    @Override
    public boolean shouldFilter() {
        // 是否执行过滤操作
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();

        //把身份信息和权限信息放在json中，加入http的header中,转发给微服务
        ctx.addZuulRequestHeader("json-token", "请求头携带的信息");

        return null;
    }
}

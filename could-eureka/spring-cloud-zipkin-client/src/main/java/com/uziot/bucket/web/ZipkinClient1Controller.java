package com.uziot.bucket.web;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Lenovo
 */
@Slf4j
@RestController
public class ZipkinClient1Controller {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/hello-zipkin")
    public String getObject() {
        String url = "http://zipkin-service2/zipkin2/hello?name=HiRibbon";
        String str = restTemplate.getForObject(url, String.class);
        log.info("微服务响应结果为：{}", str);
        return str;
    }
}
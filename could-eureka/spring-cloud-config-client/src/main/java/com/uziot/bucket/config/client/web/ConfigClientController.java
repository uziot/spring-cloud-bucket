package com.uziot.bucket.config.client.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className ConfigClientController
 * @date 2020-12-02 21:27:28
 * @description
 */
@Slf4j
@RestController
public class ConfigClientController {

    @Value("${user.info}")
    private String userInfo;

    @GetMapping(value = "/userInfo")
    public Object configTest() {
        log.info("用户配置信息：[{}]", userInfo);
        return userInfo;
    }
}

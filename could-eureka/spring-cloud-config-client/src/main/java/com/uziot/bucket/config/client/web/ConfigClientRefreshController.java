package com.uziot.bucket.config.client.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className ConfigClientController
 * @date 2020-12-02 21:27:28
 * @description 携带 RefreshScope注解实例中的配置中心对象会在容器刷新的时候，执行刷新
 * 不携带当前注解，不会被更新
 */
@Slf4j
@RefreshScope
@RestController
public class ConfigClientRefreshController {

    @Value("${user.info}")
    private String userInfo;

    @GetMapping(value = "/userInfo2")
    public Object configTest() {
        log.info("用户配置信息：[{}]", userInfo);
        return userInfo;
    }
}

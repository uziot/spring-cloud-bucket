package com.uziot.bucket.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shidt
 * @version V1.0
 * @className UserController
 * @date 2021-02-24 10:59:18
 * @description 提供两个接口分别是单条查询用户信息和批量查询用户信息
 * 当Hystrix客户端调用接口时候，测试合并请求
 */

@Slf4j
@RestController
public class UserController {

    private static final Map<Long, String> USERS = new HashMap<>();

    static {
        USERS.put(1L, "zhangsan");
        USERS.put(2L, "lisi");
        USERS.put(3L, "wangwu");
        USERS.put(4L, "wangba");
        USERS.put(5L, "liuqi");
    }

    /**
     * 单条查询用户信息
     *
     * @param id 用户ID
     * @return 用户名
     */
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String findById(@PathVariable Long id) {
        log.info("findById : " + id);
        return USERS.get(id);
    }

    /**
     * 批量查询用户信息
     *
     * @param ids ids
     * @return 用户名列表
     */
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<String> findByIds(@RequestParam String ids) {
        log.info("findByIds : " + ids);
        List<String> result = new ArrayList<>();
        for (String id : ids.split(",")) {
            if (USERS.get(Long.valueOf(id)) != null) {
                result.add(USERS.get(Long.valueOf(id)));
            }
        }
        log.info("findByIds : " + result);
        return result;
    }

}

package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author shidt
 * @version V1.0
 * @className CollapserProviderApplication
 * @date 2021-02-24 10:58:18
 * @description
 */

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CollapserProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(CollapserProviderApplication.class, args);
    }
}

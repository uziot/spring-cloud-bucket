package com.uziot.bucket.eureka.ribbon.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author shidt
 * @version V1.0
 * @className LoadBalancedController
 * @date 2021-02-19 16:07:48
 * @description 使用Ribbon负载均衡
 */

@Slf4j
@RestController
public class LoadBalancedController {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 通过连接注册中心，使用服务名访问服务，通过负载均衡实现默认负载均衡访问，
     * 当多个节点启动时候，由于Ribbon默认为轮询请求，我们可以观察到轮询请求不同的微服务
     * 返回了不同的结果。
     *
     * @return 响应
     */
    @GetMapping(value = "/ribbon/hello")
    public String getObject() {
        String url = "http://producer-service/producer/hello?name=HiRibbon";
        String str = restTemplate.getForObject(url, String.class);
        log.info("微服务响应结果为：{}", str);
        return str;
    }
}

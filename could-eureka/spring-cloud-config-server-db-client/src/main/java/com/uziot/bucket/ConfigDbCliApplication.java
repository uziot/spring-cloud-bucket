package com.uziot.bucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author shidt
 * @version V1.0
 * @className ConfigDbCliApplication
 * @date 2021-02-24 15:11:02
 * @description
 */

@EnableEurekaClient
@SpringBootApplication
public class ConfigDbCliApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigDbCliApplication.class, args);
    }

    // 1. 访问客户端获取数据
    // curl http://localhost:53031/config-client/test
    // test-stage-master

    // 2. 数据库中修改配置，刷新配置
    // curl -X POST http://localhost:53031/config-client/actuator/refresh
    // ["com.uziot.msg"]


}

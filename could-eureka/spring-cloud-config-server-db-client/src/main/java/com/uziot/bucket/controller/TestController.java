package com.uziot.bucket.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shidt
 * @version V1.0
 * @className TestController
 * @date 2021-02-24 15:11:02
 * @description
 */

@RefreshScope
@RestController
public class TestController {

    @Value("${com.uziot.msg}")
    private String message;

    @GetMapping("/test")
    public String test() {
        return message;
    }

}
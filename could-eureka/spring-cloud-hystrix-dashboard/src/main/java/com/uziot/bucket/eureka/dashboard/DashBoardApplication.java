package com.uziot.bucket.eureka.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author shidt
 * @version V1.0
 * @className DashBordApplication
 * @date 2020-12-02 23:45:24
 * @description 控制面板访问地址
 * http://localhost:53033/dashboard/hystrix
 */
@EnableHystrixDashboard
@SpringBootApplication
public class DashBoardApplication {
    public static void main(String[] args) {
        SpringApplication.run(DashBoardApplication.class, args);
    }
}

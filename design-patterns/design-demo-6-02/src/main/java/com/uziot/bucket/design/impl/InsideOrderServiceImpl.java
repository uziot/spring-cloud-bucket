package com.uziot.bucket.design.impl;

import com.uziot.bucket.design.OrderAdapterService;
import com.uziot.bucket.design.service.OrderService;

/**
 * @author DELL
 * 内部商品接口
 * 在这两个接口中都实现了各自的判断方式，尤其像是提供订单数量的接口，
 * 需要自己判断当前接到mq时订单数量是否<= 1，以此判断是否为首单。
 */
public class InsideOrderServiceImpl implements OrderAdapterService {

    private final OrderService orderService = new OrderService();

    @Override
    public boolean isFirst(String uId) {
        return orderService.queryUserOrderCount(uId) <= 1;
    }

}

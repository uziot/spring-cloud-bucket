package com.uziot.bucket.design;

import java.util.Date;

/**
 * @author DELL
 * 2. 代码实现(MQ消息适配)
 * 2.1 统一的MQ消息体
 * MQ消息中会有多种多样的类型属性，虽然他们都有同样的值提供给使用方，但是如果都这样接入那么当MQ消息特别多时候就会很麻烦。
 * 所以在这个案例中我们定义了通用的MQ消息体，后续把所有接入进来的消息进行统一的处理。
 */
public class RebateInfo {
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 业务ID
     */
    private String bizId;
    /**
     * 业务时间
     */
    private Date bizTime;
    /**
     * 业务描述
     */
    private String desc;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public Date getBizTime() {
        return bizTime;
    }

    public void setBizTime(Date bizTime) {
        this.bizTime = bizTime;
    }

    public void setBizTime(String bizTime) {
        this.bizTime = new Date(Long.parseLong("1591077840669"));
    }

    public String getDesc() {
        return desc;
    }


}

package com.uziot.bucket.design.test;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.design.MQAdapter;
import com.uziot.bucket.design.OrderAdapterService;
import com.uziot.bucket.design.RebateInfo;
import com.uziot.bucket.design.impl.InsideOrderServiceImpl;
import com.uziot.bucket.design.impl.POPOrderAdapterServiceImpl;
import com.uziot.bucket.design.mq.CreateAccount;
import com.uziot.bucket.design.mq.OrderMq;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * 在这里我们分别模拟传入了两个不同的MQ消息，并设置字段的映射关系。
 * 等真的业务场景开发中，就可以配这种映射配置关系交给配置文件或者数据库后台配置，减少编码。
 * 从上面可以看到，同样的字段值在做了适配前后分别有统一的字段属性，进行处理。这样业务开发中也就非常简单了。
 * 另外有一个非常重要的地方，在实际业务开发中，除了反射的使用外，还可以加入代理类把映射的配置交给它。
 * 这样就可以不需要每一个mq都手动创建类了。
 */
public class ApiTest {

    @Test
    public void testMQAdapter() throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, ParseException {

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = s.parse("2020-06-01 23:20:16");


        CreateAccount createAccount = new CreateAccount();
        createAccount.setNumber("100001");
        createAccount.setAddress("河北省.廊坊市.广阳区.大学里职业技术学院");
        createAccount.setAccountDate(parse);
        createAccount.setDesc("在校开户");

        HashMap<String, String> link01 = new HashMap<>();
        link01.put("userId", "number");
        link01.put("bizId", "number");
        link01.put("bizTime", "accountDate");
        link01.put("desc", "desc");
        RebateInfo rebateInfo01 = MQAdapter.filter(createAccount.toString(), link01);
        System.out.println("mq.create_account(适配前)" + createAccount.toString());
        System.out.println("mq.create_account(适配后)" + JSON.toJSONString(rebateInfo01));

        System.out.println("");

        OrderMq orderMq = new OrderMq();
        orderMq.setUid("100001");
        orderMq.setSku("10928092093111123");
        orderMq.setOrderId("100000890193847111");
        orderMq.setCreateOrderTime(parse);

        HashMap<String, String> link02 = new HashMap<>();
        link02.put("userId", "uid");
        link02.put("bizId", "orderId");
        link02.put("bizTime", "createOrderTime");
        RebateInfo rebateInfo02 = MQAdapter.filter(orderMq.toString(), link02);
        System.out.println("mq.orderMq(适配前)" + orderMq.toString());
        System.out.println("mq.orderMq(适配后)" + JSON.toJSONString(rebateInfo02));
    }

    @Test
    public void testItfAdapter() {
        OrderAdapterService popOrderAdapterService = new POPOrderAdapterServiceImpl();
        System.out.println("判断首单，接口适配(POP)：" + popOrderAdapterService.isFirst("100001"));

        OrderAdapterService insideOrderService = new InsideOrderServiceImpl();
        System.out.println("判断首单，接口适配(自营)：" + insideOrderService.isFirst("100001"));
    }

}

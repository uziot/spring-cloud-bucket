package com.uziot.bucket.design;
import com.uziot.bucket.design.ceiling.LevelOneCeiling;
import com.uziot.bucket.design.ceiling.LevelTwoCeiling;
import com.uziot.bucket.design.coat.DuluxCoat;
import com.uziot.bucket.design.coat.LiBangCoat;
import com.uziot.bucket.design.floor.ShengXiangFloor;
import com.uziot.bucket.design.tile.DongPengTile;
import com.uziot.bucket.design.tile.MarcoPoloTile;

/**
 * @author DELL
 */
public class Builder {

    public IMenu levelOne(Double area) {
        return new DecorationPackageMenu(area, "豪华欧式")
                // 吊顶，二级顶
                .appendCeiling(new LevelTwoCeiling())
                // 涂料，多乐士
                .appendCoat(new DuluxCoat())
                // 地板，圣象
                .appendFloor(new ShengXiangFloor());
    }

    public IMenu levelTwo(Double area) {
        return new DecorationPackageMenu(area, "轻奢田园")
                // 吊顶，二级顶
                .appendCeiling(new LevelTwoCeiling())
                // 涂料，立邦
                .appendCoat(new LiBangCoat())
                // 地砖，马可波罗
                .appendTile(new MarcoPoloTile());
    }

    public IMenu levelThree(Double area) {
        return new DecorationPackageMenu(area, "现代简约")
                // 吊顶，二级顶
                .appendCeiling(new LevelOneCeiling())
                // 涂料，立邦
                .appendCoat(new LiBangCoat())
                // 地砖，东鹏
                .appendTile(new DongPengTile());
    }

}

package com.uziot.bucket.test;

import com.uziot.bucket.design.CacheService;
import com.uziot.bucket.design.factory.JdkProxy;
import com.uziot.bucket.design.factory.impl.EGMCacheAdapter;
import com.uziot.bucket.design.factory.impl.IIRCacheAdapter;
import com.uziot.bucket.design.impl.CacheServiceImpl;
import org.junit.Test;

/**
 * 功能描述: <br>
 * <>
 * 在测试的代码中通过传入不同的集群类型，
 * 就可以调用不同的集群下的方法。
 * JDKProxy.getProxy(CacheServiceImpl.class, new EGMCacheAdapter());
 * 如果后续有扩展的需求，也可以按照这样的类型方式进行补充，
 * 同时对于改造上来说并没有改动原来的方法，降低了修改成本。
 *
 * @author shidt
 * @date 2021/1/26 10:43
 */
public class ApiTest {

    @Test
    public void testCacheService() {

        CacheService proxyEGM = JdkProxy.newProxy(CacheServiceImpl.class, new EGMCacheAdapter());
        proxyEGM.set("user_name_01", "小傅哥");
        String val01 = proxyEGM.get("user_name_01");
        System.out.println("测试结果：" + val01);

        CacheService proxyIIR = JdkProxy.newProxy(CacheServiceImpl.class, new IIRCacheAdapter());
        proxyIIR.set("user_name_01", "小傅哥");
        String val02 = proxyIIR.get("user_name_01");
        System.out.println("测试结果：" + val02);

    }

}

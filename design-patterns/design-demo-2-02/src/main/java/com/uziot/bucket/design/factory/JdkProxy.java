package com.uziot.bucket.design.factory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 功能描述: <br>
 * <>
 * 定义抽象工程代理类和实现
 * 这里主要的作用就是完成代理类，同时对于使用哪个集群有外部通过入参进行传递。
 *
 * @author shidt
 * @date 2021/1/26 10:37
 */
@SuppressWarnings("unchecked")
public class JdkProxy {

    public static <T> T newProxy(Class<T> interfaceClass, ICacheAdapter cacheAdapter) {
        // 构造一个反射实现处理类
        InvocationHandler handler = new JdkInvocationHandler(cacheAdapter);
        // 获取类加载器
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        // 获取接口类中的接口方法
        Class<?>[] classes = interfaceClass.getInterfaces();
        // 为实现方法创建代理对象
        return (T) Proxy.newProxyInstance(classLoader, new Class[]{classes[0]}, handler);
    }

}

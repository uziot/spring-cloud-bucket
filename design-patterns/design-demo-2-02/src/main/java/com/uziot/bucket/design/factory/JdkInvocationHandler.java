package com.uziot.bucket.design.factory;

import com.uziot.bucket.design.util.ClassLoaderUtils;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 功能描述: <br>
 * <>
 * 在代理类的实现中其实也非常简单，通过穿透进来的集群服务进行方法操作。
 * 另外在invoke中通过使用获取方法名称反射方式，调用对应的方法功能，也就简化了整体的使用。
 * 到这我们就已经将整体的功能实现完成了，关于抽象工厂这部分也可以使用非代理的方式进行实现。
 *
 * @author shidt
 * @date 2021/1/26 10:37
 */
public class JdkInvocationHandler implements InvocationHandler {

    private final ICacheAdapter cacheAdapter;

    public JdkInvocationHandler(ICacheAdapter cacheAdapter) {
        this.cacheAdapter = cacheAdapter;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return ICacheAdapter.class.getMethod(method.getName(),
                ClassLoaderUtils.getClazzByArgs(args)).invoke(cacheAdapter, args);
    }

}

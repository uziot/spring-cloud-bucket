package com.uziot.bucket.design.factory;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>
 * 实现集群使用服务接口类
 *
 * @author shidt
 * @date 2021/1/26 10:36
 */
public interface ICacheAdapter {

    String get(String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}

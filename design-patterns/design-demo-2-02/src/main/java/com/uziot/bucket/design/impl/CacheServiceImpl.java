package com.uziot.bucket.design.impl;

import com.uziot.bucket.design.CacheService;
import com.uziot.bucket.design.RedisUtils;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 缓存操作实现类
 *
 * @author shidt
 * @date 2021/1/26 10:51
 */
public class CacheServiceImpl implements CacheService {

    private final RedisUtils redisUtils = new RedisUtils();

    @Override
    public String get(String key) {
        return redisUtils.get(key);
    }

    @Override
    public void set(String key, String value) {
        redisUtils.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        redisUtils.set(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        redisUtils.del(key);
    }

    @Override
    public void expire(long timeout) {
        System.out.println("测试expire");
    }

}

package com.uziot.bucket.design;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>
 * 工程中涉及的部分核心功能代码，如下；
 * ICacheAdapter，定义了适配接口，
 * 分别包装两个集群中差异化的接口名称。EGMCacheAdapter、IIRCacheAdapter
 * JDKProxy、JDKInvocationHandler，是代理类的定义和实现，这
 * 部分也就是抽象工厂的另外一种实现方式。通过这样的方式可以很好的把原有操作Redis的方法进行代理操作，
 * 通过控制不同的入参对象，控制缓存的使用。
 *
 * @author shidt
 * @date 2021/1/26 10:55
 */
public interface CacheService {

    String get(final String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

    void expire(long timeout);

}

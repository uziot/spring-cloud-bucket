package com.uziot.bucket.design.factory.impl;


import com.uziot.bucket.design.factory.ICacheAdapter;
import com.uziot.bucket.design.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>
 * 实现集群使用服务
 *
 * @author shidt
 * @date 2021/1/26 10:36
 */
public class IIRCacheAdapter implements ICacheAdapter {

    private final IIR iir = new IIR();

    @Override
    public String get(String key) {
        return iir.get(key);
    }

    @Override
    public void set(String key, String value) {
        iir.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        iir.setExpire(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        iir.del(key);
    }

}

package com.uziot.bucket.design.factory.impl;

import com.uziot.bucket.design.factory.ICacheAdapter;
import com.uziot.bucket.design.matter.EGM;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>
 * 实现集群使用服务
 *
 * @author shidt
 * @date 2021/1/26 10:36
 */
public class EGMCacheAdapter implements ICacheAdapter {

    private final EGM egm = new EGM();

    @Override
    public String get(String key) {
        return egm.gain(key);
    }

    @Override
    public void set(String key, String value) {
        egm.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        egm.setEx(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        egm.delete(key);
    }
}

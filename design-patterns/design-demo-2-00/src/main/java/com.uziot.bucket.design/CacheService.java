package com.uziot.bucket.design;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 以下是案例模拟中原有的单集群Redis使用方式，后续会通过对这里的代码进行改造。
 *
 * @author shidt
 * @date 2021/1/26 10:23
 */
public interface CacheService {

    String get(final String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}

package com.uziot.bucket.design;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 模拟单机服务 RedisUtils
 * 模拟Redis功能，也就是假定目前所有的系统都在使用的服务
 * 类和方法名次都固定写死到各个业务系统中，改动略微麻烦
 *
 * @author shidt
 * @date 2021/1/26 10:20
 */
public class RedisUtils {

    private final Logger logger = LoggerFactory.getLogger(RedisUtils.class);

    private final Map<String, String> dataMap = new ConcurrentHashMap<String, String>();

    public String get(String key) {
        logger.info("Redis获取数据 key：{}", key);
        return dataMap.get(key);
    }

    public void set(String key, String value) {
        logger.info("Redis写入数据 key：{} val：{}", key, value);
        dataMap.put(key, value);
    }

    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        logger.info("Redis写入数据 key：{} val：{} timeout：{} timeUnit：{}", key, value, timeout, timeUnit.toString());
        dataMap.put(key, value);
    }

    public void del(String key) {
        logger.info("Redis删除数据 key：{}", key);
        dataMap.remove(key);
    }

}

package com.uziot.bucket.design.matter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * 模拟集群 EGM
 * 模拟一个集群服务，但是方法名与各业务系统中使用的方法名不同。
 * 有点像你mac，我用win。做一样的事，但有不同的操作。
 *
 * @author shidt
 * @date 2021/1/26 10:20
 */
public class EGM {

    private final Logger logger = LoggerFactory.getLogger(EGM.class);

    private final Map<String, String> dataMap = new ConcurrentHashMap<String, String>();

    public String gain(String key) {
        logger.info("EGM获取数据 key：{}", key);
        return dataMap.get(key);
    }

    public void set(String key, String value) {
        logger.info("EGM写入数据 key：{} val：{}", key, value);
        dataMap.put(key, value);
    }

    public void setEx(String key, String value, long timeout, TimeUnit timeUnit) {
        logger.info("EGM写入数据 key：{} val：{} timeout：{} timeUnit：{}", key, value, timeout, timeUnit.toString());
        dataMap.put(key, value);
    }

    public void delete(String key) {
        logger.info("EGM删除数据 key：{}", key);
        dataMap.remove(key);
    }
}

package com.uziot.bucket.design;

import java.util.Map;

/**
 * 单选题
 * 以上两个类就是我们场景中需要的物料内容，相对来说比较简单。
 * 如果你在测试的时候想扩充学习，可以继续添加一些其他物料(题目类型)。
 *
 * @author DELL
 */
public class ChoiceQuestion {
    /**
     * 题目
     */
    private String name;
    /**
     * 选项；A、B、C、D
     */
    private Map<String, String> option;
    /**
     * 答案；B
     */
    private String key;

    public ChoiceQuestion() {
    }

    public ChoiceQuestion(String name, Map<String, String> option, String key) {
        this.name = name;
        this.option = option;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getOption() {
        return option;
    }

    public void setOption(Map<String, String> option) {
        this.option = option;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

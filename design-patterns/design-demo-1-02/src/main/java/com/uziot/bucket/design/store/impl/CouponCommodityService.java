package com.uziot.bucket.design.store.impl;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.design.coupon.CouponResult;
import com.uziot.bucket.design.coupon.CouponService;
import com.uziot.bucket.design.store.ICommodity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
/**
 * @author shidt
 * @version V1.0
 * @className CouponCommodityService
 * @date 2020-12-07 12:30:57
 * @description 优惠券商品服务
 * 每一种奖品的实现都包括在自己的类中，新增、修改或者删除都不会影响其他奖品功能的测试，降低回归测试的可能。
 * 后续在新增的奖品只需要按照此结构进行填充即可，非常易于维护和扩展。
 * 在统一了入参以及出参后，调用方不在需要关心奖品发放的内部逻辑，按照统一的方式即可处理。
 */
public class CouponCommodityService implements ICommodity {

    private final Logger logger = LoggerFactory.getLogger(CouponCommodityService.class);
    /**
     * 模拟注入
     */
    private final CouponService couponService = new CouponService();

    @Override
    public void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception {
        CouponResult couponResult = couponService.sendCoupon(uId, commodityId, bizId);
        logger.info("请求参数[优惠券] => uId：{} commodityId：{} bizId：{} extMap：{}", uId, commodityId, bizId, JSON.toJSON(extMap));
        logger.info("测试结果[优惠券]：{}", JSON.toJSON(couponResult));
        if (!"0000".equals(couponResult.getCode())) {
            throw new RuntimeException(couponResult.getInfo());
        }
    }

}

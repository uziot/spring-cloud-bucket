package com.uziot.bucket.design;

import com.uziot.bucket.design.store.ICommodity;
import com.uziot.bucket.design.store.impl.CardCommodityService;
import com.uziot.bucket.design.store.impl.CouponCommodityService;
import com.uziot.bucket.design.store.impl.GoodsCommodityService;

/**
 * @author shidt
 * @version V1.0
 * @className StoreFactory
 * @date 2020-12-07 12:30:57
 * @description 首先，从上面的工程结构中你是否一些感觉，比如；
 * 它看上去清晰了、这样分层可以更好扩展了、似乎可以想象到每一个类做了什么。
 * 如果还不能理解为什么这样修改，也没有关系。
 * 因为你是在通过这样的文章，来学习设计模式的魅力。
 * 并且再获取源码后，进行实际操作几次也就慢慢掌握了工厂模式的技巧。
 */
public class StoreFactory {

    public ICommodity getCommodityService(Integer commodityType) {
        if (null == commodityType) {
            return null;
        }
        if (1 == commodityType) {
            return new CouponCommodityService();
        }
        if (2 == commodityType) {
            return new GoodsCommodityService();
        }
        if (3 == commodityType) {
            return new CardCommodityService();
        }
        throw new RuntimeException("不存在的商品服务类型");
    }

}

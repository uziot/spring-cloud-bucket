package com.uziot.bucket.design;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.design.mq.CreateAccount;

import java.util.Date;

/**
 * @author DELL
 * 目前需要接收三个MQ消息，所有就有了三个对应的类，
 * 和我们平时的代码几乎一样。如果你的MQ量不多，
 * 这样的写法也没什么问题，但是随着数量的增加，就需要考虑用一些设计模式来解决。
 */
public class CreateAccountMqService {

    public void onMessage(String message) {

        CreateAccount mq = JSON.parseObject(message, CreateAccount.class);

        String number = mq.getNumber();
        Date accountDate = mq.getAccountDate();

        // ... 处理自己的业务
    }

}

package com.uziot.bucket.design;

import com.alibaba.fastjson.JSON;
import com.uziot.bucket.design.mq.OrderMq;

import java.util.Date;

/**
 * @author DELL
 * 目前需要接收三个MQ消息，所有就有了三个对应的类，和我们平时的代码几乎一样。
 * 如果你的MQ量不多，这样的写法也没什么问题，但是随着数量的增加，就需要考虑用一些设计模式来解决。
 */
public class OrderMqService {

    public void onMessage(String message) {
        OrderMq mq = JSON.parseObject(message, OrderMq.class);

        String uid = mq.getUid();
        String orderId = mq.getOrderId();
        Date createOrderTime = mq.getCreateOrderTime();

        // ... 处理自己的业务
    }

}

package com.uziot.bucket.design;

/**
 * @author shidt
 * @version V1.0
 * @className AwardRes
 * @date 2020-12-07 12:30:57
 * @description 奖品发放响应
 */
public class AwardRes {
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String info;

    public AwardRes(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}

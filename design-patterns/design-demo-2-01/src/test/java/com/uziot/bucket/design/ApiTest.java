package com.uziot.bucket.design;

import com.uziot.bucket.design.impl.CacheServiceImpl;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testCacheService() {

        CacheService cacheService = new CacheServiceImpl();

        cacheService.set("user_name_01", "小傅哥", 1);
        String val01 = cacheService.get("user_name_01", 1);
        System.out.println("测试结果：" + val01);

    }

}

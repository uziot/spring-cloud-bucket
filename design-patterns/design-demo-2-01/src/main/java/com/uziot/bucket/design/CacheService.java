package com.uziot.bucket.design;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>这里的实现过程非常简单，主要根据类型判断是哪个Redis集群。
 * 虽然实现是简单了，但是对使用者来说就麻烦了，并且也很难应对后期的拓展和不停的维护。
 *
 * @author shidt
 * @date 2021/1/26 10:29
 */
public interface CacheService {

    String get(final String key, int redisType);

    void set(String key, String value, int redisType);

    void set(String key, String value, long timeout, TimeUnit timeUnit, int redisType);

    void del(String key, int redisType);

}

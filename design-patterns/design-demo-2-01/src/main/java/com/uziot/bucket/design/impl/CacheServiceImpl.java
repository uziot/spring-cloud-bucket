package com.uziot.bucket.design.impl;

import com.uziot.bucket.design.CacheService;
import com.uziot.bucket.design.RedisUtils;
import com.uziot.bucket.design.matter.EGM;
import com.uziot.bucket.design.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * 功能描述: <br>
 * <>这里的实现过程非常简单，主要根据类型判断是哪个Redis集群。
 * 虽然实现是简单了，但是对使用者来说就麻烦了，并且也很难应对后期的拓展和不停的维护。
 *
 * @author shidt
 * @date 2021/1/26 10:23
 */
public class CacheServiceImpl implements CacheService {

    private final RedisUtils redisUtils = new RedisUtils();

    private final EGM egm = new EGM();

    private final IIR iir = new IIR();

    @Override
    public String get(String key, int redisType) {

        if (1 == redisType) {
            return egm.gain(key);
        }

        if (2 == redisType) {
            return iir.get(key);
        }

        return redisUtils.get(key);
    }

    @Override
    public void set(String key, String value, int redisType) {

        if (1 == redisType) {
            egm.set(key, value);
            return;
        }

        if (2 == redisType) {
            iir.set(key, value);
            return;
        }

        redisUtils.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit, int redisType) {

        if (1 == redisType) {
            egm.setEx(key, value, timeout, timeUnit);
            return;
        }

        if (2 == redisType) {
            iir.setExpire(key, value, timeout, timeUnit);
            return;
        }

        redisUtils.set(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key, int redisType) {

        if (1 == redisType) {
            egm.delete(key);
            return;
        }

        if (2 == redisType) {
            iir.del(key);
            return;
        }

        redisUtils.del(key);
    }


}

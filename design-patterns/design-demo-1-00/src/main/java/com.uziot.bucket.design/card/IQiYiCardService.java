package com.uziot.bucket.design.card;

/**
 * @author shidt
 * @version V1.0
 * @className IQiYiCardService
 * @date 2020-12-07 12:30:57
 * @description
 * 模拟爱奇艺会员卡服务
 */
public class IQiYiCardService {

    public void grantToken(String bindMobileNumber, String cardId) {
        System.out.println("模拟发放爱奇艺会员卡一张：" + bindMobileNumber + "，" + cardId);
    }

}

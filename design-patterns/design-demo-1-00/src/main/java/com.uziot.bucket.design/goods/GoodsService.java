package com.uziot.bucket.design.goods;

import com.alibaba.fastjson.JSON;

/**
 * @author shidt
 * @version V1.0
 * @className GoodsService
 * @date 2020-12-07 12:30:57
 * @description 模拟实物商品服务
 */
public class GoodsService {

    public Boolean deliverGoods(DeliverReq req) {
        System.out.println("模拟发货实物商品一个：" + JSON.toJSONString(req));
        return true;
    }

}

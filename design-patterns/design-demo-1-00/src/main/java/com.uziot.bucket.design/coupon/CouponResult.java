package com.uziot.bucket.design.coupon;

/**
 * @author shidt
 * @version V1.0
 * @className CouponResult
 * @date 2020-12-07 12:30:57
 * @description
 */
public class CouponResult {
    /**
     * 编码
     */
    private String code;
    /**
     * 描述
     */
    private String info;

    public CouponResult(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}

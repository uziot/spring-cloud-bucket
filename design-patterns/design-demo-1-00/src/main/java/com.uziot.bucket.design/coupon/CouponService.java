package com.uziot.bucket.design.coupon;

/**
 * @author shidt
 * @version V1.0
 * @className CouponService
 * @date 2020-12-07 12:30:57
 * @description 模拟优惠券服务
 */
public class CouponService {

    public CouponResult sendCoupon(String uId, String couponNumber, String uuid) {
        System.out.println("模拟发放优惠券一张：" + uId + "," + couponNumber + "," + uuid);
        return new CouponResult("0000", "发放成功");
    }

}

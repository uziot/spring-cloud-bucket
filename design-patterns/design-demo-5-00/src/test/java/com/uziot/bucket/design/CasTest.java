package com.uziot.bucket.design;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author shidt
 * @version V1.0
 * @className CasTest
 * @date 2021-01-29 15:06:06
 * @description
 */

public class CasTest {
    public static void main(String[] args) {

        String initialReference = "initial value referenced";
        AtomicReference<String> atomicStringReference = new AtomicReference<>(initialReference);
        String newReference = "new value referenced";

        boolean exchanged = atomicStringReference.compareAndSet(initialReference, newReference);
        System.out.println("exchanged: " + exchanged);

        exchanged = atomicStringReference.compareAndSet(initialReference, newReference);
        System.out.println("exchanged: " + exchanged);

    }
}

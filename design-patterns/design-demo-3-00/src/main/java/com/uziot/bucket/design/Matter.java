package com.uziot.bucket.design;

import java.math.BigDecimal;

/**
 * 装修物料
 * 物料接口提供了基本的信息，以保证所有的装修材料都可以按照统一标准进行获取。
 * @author DELL
 */
public interface Matter {

    /**
     * 场景；地板、地砖、涂料、吊顶
     */
    String scene();

    /**
     * 品牌
     */
    String brand();

    /**
     * 型号
     */
    String model();

    /**
     * 平米报价
     */
    BigDecimal price();

    /**
     * 描述
     */
    String desc();

}

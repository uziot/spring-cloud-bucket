package com.uziot.bucket.design.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author DELL
 * 1.5 查询用户第三方下单首单接口
 */
public class POPOrderService {

    private final Logger logger = LoggerFactory.getLogger(POPOrderService.class);

    /**
     * 出参boolean，判断是否首单
     *
     * @param uId uId
     * @return boolean
     */
    public boolean isFirstOrder(String uId) {
        logger.info("POP商家，查询用户的订单是否为首单：{}", uId);
        return true;
    }

}

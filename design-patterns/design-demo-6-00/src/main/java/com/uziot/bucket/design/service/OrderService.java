package com.uziot.bucket.design.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author DELL
 * 1.4 查询用户内部下单数量接口
 */
public class OrderService {

    private final Logger logger = LoggerFactory.getLogger(POPOrderService.class);

    /**
     * 出参long，查询订单数量
     * @param userId userId
     * @return long
     */
    public long queryUserOrderCount(String userId) {
        logger.info("自营商家，查询用户的订单是否为首单：{}", userId);
        return 10L;
    }

}

package com.uziot.bucket.design.mq;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单妥投消息
 *
 * @author DELL
 * 这里模拟了三个不同类型的MQ消息，而在消息体中都有一些必要的字段，
 * 比如；用户ID、时间、业务ID，但是每个MQ的字段属性并不一样。就像用户ID在不同的MQ里也有不同的字段：uId、userId等。
 * 同时还提供了两个不同类型的接口，一个用于查询内部订单订单下单数量，一个用于查询第三方是否首单。
 * 后面会把这些不同类型的MQ和接口做适配兼容。
 */
@Data
public class POPOrderDelivered {
    /**
     * 用户ID
     */
    private String uId;
    /**
     * 订单号
     */
    private String orderId;
    /**
     * 下单时间
     */
    private Date orderTime;
    /**
     * 商品
     */
    private Date sku;
    /**
     * 商品名称
     */
    private Date skuName;
    /**
     * 金额
     */
    private BigDecimal decimal;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}

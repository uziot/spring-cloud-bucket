package com.uziot.bucket.design.pay.mode;

/**
 * @author DELL
 * IPayMode是一个接口，往下是它的两个支付模型；刷脸支付、指纹支付。
 * 那么，支付类型 × 支付模型 = 就可以得到相应的组合。
 * 每种支付方式的不同，刷脸和指纹校验逻辑也有差异，可以使用适配器模式进行处理，
 * 这里不是本文重点不做介绍，可以看适配器模式章节。
 * <p>
 * 2.3 定义支付模式接口
 */
public interface IPayMode {

    boolean security(String uId);

}

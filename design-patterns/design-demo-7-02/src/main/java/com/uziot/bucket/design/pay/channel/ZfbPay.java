package com.uziot.bucket.design.pay.channel;

import com.uziot.bucket.design.pay.mode.IPayMode;

import java.math.BigDecimal;

/**
 * @author DELL
 *
 * 支付宝支付
 *
 * 这里分别模拟了调用第三方的两个支付渠道；微信、支付宝，
 * 当然作为支付综合平台可能不只是接了这两个渠道，还会有其很跟多渠道。
 * 另外可以看到在支付的时候分别都调用了风控的接口进行验证，
 * 也就是不同模式的支付(刷脸、指纹)，都需要过指定的风控，才能保证支付安全。
 */
public class ZfbPay extends Pay {

    public ZfbPay(IPayMode payMode) {
        super(payMode);
    }

    @Override
    public String transfer(String uId, String tradeId, BigDecimal amount) {
        logger.info("模拟支付宝渠道支付划账开始。uId：{} tradeId：{} amount：{}", uId, tradeId, amount);
        boolean security = payMode.security(uId);
        logger.info("模拟支付宝渠道支付风控校验。uId：{} tradeId：{} security：{}", uId, tradeId, security);
        if (!security) {
            logger.info("模拟支付宝渠道支付划账拦截。uId：{} tradeId：{} amount：{}", uId, tradeId, amount);
            return "0001";
        }
        logger.info("模拟支付宝渠道支付划账成功。uId：{} tradeId：{} amount：{}", uId, tradeId, amount);
        return "0000";
    }

}

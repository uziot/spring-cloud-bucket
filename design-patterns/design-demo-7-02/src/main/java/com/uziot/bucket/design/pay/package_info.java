package com.uziot.bucket.design.pay;

/**
 * @author shidt
 * @version V1.0
 * @className package_info
 * @date 2021-02-02 15:35:41
 * @description 从上面的ifelse方式实现来看，这是两种不同类型的相互组合。
 * 那么就可以把支付方式和支付模式进行分离通过抽象类依赖实现类的方式进行桥接，
 * 通过这样的拆分后支付与模式其实是可以单独使用的，当需要组合时候只需要把模式传递给支付即可。
 * <p>
 * 桥接模式的关键是选择的桥接点拆分，是否可以找到这样类似的相互组合，如果没有就不必要非得使用桥接模式。
 */
